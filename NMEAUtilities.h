/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/
/*                                                                            */
/* NMEAUtilities.h                                                            */
/* 06.01.20                                                                   */
/*                                                                            */
/* Ref : NMEA 0183 Specification v2.3                                         */
/*       ublox NEO-6 - Data Sheet                                             */
/*                                                                            */
/* - GPS data conversion routines                                             */
/*                                                                            */
/******************************************************************************/

#ifndef _NMEA_UTILITIES_H_
#define _NMEA_UTILITIES_H_

/******************************************************************************/

#include "NMEAGps.h"

/******************************************************************************/

#define NMEA_GPS_ASCII_DIGIT_ZERO                                            ('0') // use to reduce ASCII "digits" to decimal
                                                                             
#define NMEA_GPS_ASCII_DIGIT_MINIMUM                                         ('0') // use to range-limit test legal ASCII "digits"
#define NMEA_GPS_ASCII_DIGIT_MAXIMUM                                         ('9')
            
#define NMEA_GPS_ASCII_DIGIT_PLUS                                            ('+')
#define NMEA_GPS_ASCII_DIGIT_MINUS                                           ('-')

#define NMEA_GPS_DIGIT_x_100                                 ((NMEA_GPS_UINT8)100) // use to multiply resolved digits by positional power-of-ten
#define NMEA_GPS_DIGIT_x_10                                   ((NMEA_GPS_UINT8)10)
#define NMEA_GPS_DIGIT_x_1                                     ((NMEA_GPS_UINT8)1) // for completeness!
#define NMEA_GPS_DIGIT_x_0_1                                 ((NMEA_GPS_FLOAT)0.1)
#define NMEA_GPS_DIGIT_x_0_01                               ((NMEA_GPS_FLOAT)0.01)
#define NMEA_GPS_DIGIT_x_0_001                             ((NMEA_GPS_FLOAT)0.001)
#define NMEA_GPS_DIGIT_x_0_0001                           ((NMEA_GPS_FLOAT)0.0001)
#define NMEA_GPS_DIGIT_x_0_00001                         ((NMEA_GPS_FLOAT)0.00001)
#define NMEA_GPS_DIGIT_x_0_000001                       ((NMEA_GPS_FLOAT)0.000001)

#define NMEA_GPS_DEGREE_POWERS                                                 (3)
#define NMEA_GPS_INTEGER_MINUTES_POWERS                                        (2)
#define NMEA_GPS_FRACTIONAL_MINUTES_POWERS                                     (5)
                                                                              
#define NMEA_GPS_DEGREE_TABLE_START                                            (0)
#define NMEA_GPS_MINUTES_TABLE_START             (NMEA_GPS_DEGREE_TABLE_START + 1) // reuse the degree-powers for integer minutes

#define NMEA_GPS_NYBBLE_SHIFT                                                  (4)
#define NMEA_GPS_NYBBLE_UPPER_MASK                                           (0xF0)
#define NMEA_GPS_NYBBLE_LOWER_MASK                                           (0x0F)
                                                                      
#define NMEA_ASCII_TO_HEX_OFFSET                                             (0x0A) // used when converting hex digit characters to numbers
                                                                      
#define NMEA_HEX_TO_ASCII_DECIMAL_LOW                                           (0) // used when converting numbers to hex digit characters
#define NMEA_HEX_TO_ASCII_DECIMAL_HIGH                                          (9)
#define NMEA_HEX_TO_ASCII_UPPERCASE_HEX_LOW                                  (0x0A)
#define NMEA_HEX_TO_ASCII_UPPERCASE_HEX_HIGH                                 (0x0F)
#define NMEA_HEX_TO_ASCII_LOWERCASE_HEX_LOW     NMEA_HEX_TO_ASCII_UPPERCASE_HEX_LOW
#define NMEA_HEX_TO_ASCII_LOWERCASE_HEX_HIGH    NMEA_HEX_TO_ASCII_UPPERCASE_HEX_HIGH

// Single-precision floating-point integer and fraction tables
#define NMEA_FLOAT_DIGIT_x_1000000                        ((NMEA_GPS_FLOAT)1000000) // use to multiply resolved digits by positional power-of-ten
#define NMEA_FLOAT_DIGIT_x_100000                          ((NMEA_GPS_FLOAT)100000) 
#define NMEA_FLOAT_DIGIT_x_10000                            ((NMEA_GPS_FLOAT)10000) 
#define NMEA_FLOAT_DIGIT_x_1000                              ((NMEA_GPS_FLOAT)1000) 
#define NMEA_FLOAT_DIGIT_x_100                                ((NMEA_GPS_FLOAT)100) // use to multiply resolved digits by positional power-of-ten
#define NMEA_FLOAT_DIGIT_x_10                                  ((NMEA_GPS_FLOAT)10)
#define NMEA_FLOAT_DIGIT_x_1                                    ((NMEA_GPS_FLOAT)1) // for completeness!

#define NMEA_FLOAT_MINIMUM_INTEGER_PLACE                                        (7) // powers-of-ten are reverse-indexed!
#define NMEA_FLOAT_MAXIMUM_INTEGER_PLACE                                        (1)
                                                                               
#define NMEA_FLOAT_DECIMAL_POINT_LENGTH                                         (1)
                                                                               
#define NMEA_FLOAT_MINIMUM_DECIMAL_PLACE                                        (1) // from 0.01     --> 0.1x
#define NMEA_FLOAT_MAXIMUM_DECIMAL_PLACE                                        (6) // from 0.000001 --> 0.00001
                                                                               
#define NMEA_FLOAT_INTEGER_TABLE_SIZE                                           NMEA_FLOAT_MINIMUM_INTEGER_PLACE
#define NMEA_FLOAT_FRACTIONAL_TABLE_SIZE                                        NMEA_FLOAT_MAXIMUM_DECIMAL_PLACE
                                                                               
#define NMEA_FLOAT_MAXIMUM_ASCII_LENGTH                                         (NMEA_FLOAT_MINIMUM_INTEGER_PLACE + NMEA_FLOAT_DECIMAL_POINT_LENGTH + NMEA_FLOAT_MAXIMUM_DECIMAL_PLACE)
                                                                               
#define NMEA_GPS_BITFIELD_DEFAULT_BITS                                          (32) // used to define one or more bit-fields of 32-bits length ("uint32_t")
                                                                               
// Definitions for decimal-equivalent to "uint32_t" bit-weights look-up table
#define NMEA_DECIMAL_EQUIVALENT_DIGITS                                          (10) // maximum number of decimal digits in a "uint32_t"
#define NMEA_DECIMAL_EQUIVALENT_BIT_WEIGHTS                                     (32) // bit-positions in a "uint32_t"
#define NMEA_DECIMAL_EQUIVALENT_BASE                                            (10) // decimal base-10 and "power-of-ten" limit 
                                                                                     // i.e. [ 0 ... 9 ] -> [ 0 ... 9 ] carry = 0 : [ 10 ... ] -> [ 0 ... 9 ]  carry = 1
#define NMEA_DECIMAL_EQUIVALENT_MAXIMUM_FIELD_WIDTH                             (NMEA_DECIMAL_EQUIVALENT_DIGITS)

#define NMEA_MAXIMUM_INTEGER                                                    ((NMEA_GPS_INT32)2147483647)
#define NMEA_MINIMUM_INTEGER                                                    ((NMEA_GPS_INT32)-NMEA_MAXIMUM_INTEGER)

// Definition of the field widths for floating-point numbers
#define NMEA_MINIMUM_INTEGER_DECIMAL_PLACES                                     (1)
#define NMEA_MAXIMUM_INTEGER_DECIMAL_PLACES                                     (9)

#define NMEA_MINIMUM_FRACTIONAL_DECIMAL_PLACES                                  (1)
#define NMEA_MAXIMUM_FRACTIONAL_DECIMAL_PLACES                                  (6)

#define NMEA_FRACTIONAL_MAXIMUM_VALUE                                           ((NMEA_GPS_FLOAT)(99999999.9999))
#define NMEA_FRACTIONAL_MINIMUM_VALUE                                           (-NMEA_FRACTIONAL_MAXIMUM_VALUE)

#define NMEA_BINARY_DECISION_THRESHOLD_FLOAT                                    ((NMEA_GPS_FLOAT)0.5)

/******************************************************************************/

typedef enum nmeaNumericalSignFlag_tTag
{
  NMEA_SENTENCE_NUMERICAL_SIGN_UNSIGNED = 0,
  NMEA_SENTENCE_NUMERICAL_SIGN_SIGNED,
  NMEA_SENTENCE_NUMERICAL_SIGN_FLAGS
} nmeaNumericalSignFlag_t;

// Used to select which ASCII hex digit case to handle
typedef enum nmeaGenerateAsciiDigitCase_tTag
{
  NMEA_GENERATE_ASCII_CASE_LOWER = 0,
  NMEA_GENERATE_ASCII_CASE_UPPER,
  NMEA_GENERATE_ASCII_CASES
} nmeaGenerateAsciiDigitCase_t;

typedef enum nmeaAsciiHexDigits_tTag
{
  NMEA_ASCII_0 = NMEA_GPS_ASCII_DIGIT_ZERO,
  NMEA_ASCII_1,
  NMEA_ASCII_2,
  NMEA_ASCII_3,
  NMEA_ASCII_4,
  NMEA_ASCII_5,
  NMEA_ASCII_6,
  NMEA_ASCII_7,
  NMEA_ASCII_8,
  NMEA_ASCII_9,
  NMEA_ASCII_UPPERCASE_A = 'A',
  NMEA_ASCII_UPPERCASE_B = 'B',
  NMEA_ASCII_UPPERCASE_C = 'C',
  NMEA_ASCII_UPPERCASE_D = 'D',
  NMEA_ASCII_UPPERCASE_E = 'E',
  NMEA_ASCII_UPPERCASE_F = 'F',
  NMEA_ASCII_LOWERCASE_a = 'a',
  NMEA_ASCII_LOWERCASE_b = 'b',
  NMEA_ASCII_LOWERCASE_c = 'c',
  NMEA_ASCII_LOWERCASE_d = 'd',
  NMEA_ASCII_LOWERCASE_e = 'e',
  NMEA_ASCII_LOWERCASE_f = 'f',
} nmeaAsciiHexDigits_t;

// The variable parser has internal structure
typedef enum nmeaVariableParserState_tTag
{
  NMEA_VARIABLE_STATE_INITIALISATION = 0,
  NMEA_VARIABLE_STATE_SIGN,
  NMEA_VARIABLE_STATE_INTEGER,
  NMEA_VARIABLE_STATE_DECIMAL_POINT,
  NMEA_VARIABLE_STATE_FRACTIONAL,
  NMEA_VARIABLE_STATE_EXIT,
  NMEA_VARIABLE_STATE_FAILED,
  NMEA_VARIABLE_STATES
}  nmeaVariableParserState_t;

// The variable parser looks for integer or floating-point numbers
typedef enum nmeaSentenceVariableType_tTag
{
  NMEA_FIELD_DECIMAL_INTEGER = 0,
  NMEA_FIELD_DECIMAL_FLOATING,
  NMEA_FIELD_DECIMAL_TYPES
} nmeaSentenceVariableType_t;

// After parsing report the state (or absence) of the variable's sign
typedef enum nmeaVariableSignState_tTag
{
  NMEA_VARIABLE_SIGN_STATE_NONE = 0,
  NMEA_VARIABLE_SIGN_STATE_PLUS,
  NMEA_VARIABLE_SIGN_STATE_MINUS,
  NMEA_VARIABLE_SIGN_STATES
} nmeaVariableSignState_t;

// The variable parser controlling and reporting structure
typedef struct nmeaVariableParser_tTag
{
  nmeaSentenceVariableType_t variableType;                // --> [ NMEA_FIELD_DECIMAL_INTEGER | NMEA_FIELD_DECIMAL_FLOATING ]
  nmeaNumericalSignFlag_t    signFlag;                    // --> signals the variable to be parsed is signed [ NMEA_SENTENCE_SIGN_PLUS | NMEA_SENTENCE_SIGN_MINUS ]
  nmeaVariableSignState_t    signState;                   // <-- reports the state of the sign detected (or not)
  NMEA_GPS_UINT8             integerPartIndex;            // <-- reports the parsed length of a variable's integer part
  NMEA_GPS_UINT8             fractionalPartIndex;         // <-- reports the parsed length of a variable's fractional part
  NMEA_GPS_UINT8             integerPartMinimumLength;    // --> minimum number of characters required for a variable's integer part
  NMEA_GPS_UINT8             integerPartMaximumLength;    // --> maximum number of characters required for a variable's integer part
  NMEA_GPS_UINT8             fractionalPartMinimumLength; // --> minimum number of characters required for a variable's fractional part
  NMEA_GPS_UINT8             fractionalPartMaximumLength; // --> maximum number of characters required for a variable's fractional part
  nmeaNumericalVariant_t     variableResult;              // <-- computed value of the complete variable
} nmeaVariableParser_t;

// Signal numbers to be signed or unsigned
typedef enum nmeaEncoderSignFlag_tTag
{
  NMEA_ENCODER_SIGN_FLAG_UNSIGNED = 0,
  NMEA_ENCODER_SIGN_FLAG_SIGNED,
  NMEA_ENCODER_SIGN_FLAGS
} nmeaEncoderSignFlag_t;

/******************************************************************************/

extern const NMEA_GPS_UINT8 degreePowers[NMEA_GPS_DEGREE_POWERS];
extern const NMEA_GPS_FLOAT minutesIntegerPowers[NMEA_GPS_INTEGER_MINUTES_POWERS];
extern const NMEA_GPS_FLOAT minutesFractionalPowers[NMEA_GPS_FRACTIONAL_MINUTES_POWERS];
extern const NMEA_GPS_FLOAT nmeaFloatIntegers[NMEA_FLOAT_INTEGER_TABLE_SIZE];
extern const NMEA_GPS_FLOAT nmeaFloatFractionals[NMEA_FLOAT_FRACTIONAL_TABLE_SIZE];
extern const NMEA_GPS_UCHAR nmeaBitWeightToDecimal[NMEA_DECIMAL_EQUIVALENT_BIT_WEIGHTS][NMEA_DECIMAL_EQUIVALENT_DIGITS];

/******************************************************************************/

#ifdef _NMEA_ENABLE_STRING_TO_FLOAT_
extern bool nmeaConvertStringToFloat(const NMEA_GPS_CHAR           *numericalCharacters,
                                     const NMEA_GPS_UINT8           numericalCharactersLength,
                                     const NMEA_GPS_FLOAT           numericalMinimumLimit,
                                     const NMEA_GPS_FLOAT           numericalMaximumLimit,
                                     const nmeaNumericalSignFlag_t  numericalSignFlag,
                                           NMEA_GPS_FLOAT          *numericalResult);
#endif

extern NMEA_GPS_BOOLEAN initialiseSentence(      NMEA_GPS_UCHAR  *sentence,
                                           const NMEA_GPS_UINT16  sentenceLength,
                                           const NMEA_GPS_UCHAR   initialisationCharacter);

extern bool nmeaConvertDegrees(const NMEA_GPS_CHAR  *degreesString,
                               const NMEA_GPS_UINT8  degreesStringLength,
                               const NMEA_GPS_UINT8  degreesTableIndexStart,
                               const NMEA_GPS_UINT8  degreesLength,
                               const NMEA_GPS_UINT8  minutesLength,
                               const NMEA_GPS_FLOAT  degreesMinimumLimit,
                               const NMEA_GPS_FLOAT  degreesMaximumLimit,
                                     NMEA_GPS_UINT8 *degrees,
                                     NMEA_GPS_FLOAT *minutes);

extern _NMEA_INLINE_FUNCTION_ NMEA_GPS_BOOLEAN nmeaConvertFloatingPoint(const NMEA_GPS_UCHAR *asciiFloatingPoint,
                                                                        const NMEA_GPS_UINT8  asciiFloatingPointLength,
                                                                        const NMEA_GPS_UINT8  asciiFractionalLength,
                                                                              NMEA_GPS_FLOAT *floatingPoint);

extern NMEA_GPS_BOOLEAN nmeaValidateUtcTime(const NMEA_GPS_UINT8 utcHours,
                                            const NMEA_GPS_UINT8 utcMinutes,
                                            const NMEA_GPS_UINT8 utcSeconds,
                                            const NMEA_GPS_UINT8 utcCentiseconds);

extern NMEA_GPS_BOOLEAN nmeaValidateDate(const NMEA_GPS_UINT8 dateDays,
                                         const NMEA_GPS_UINT8 dateMonths,
                                         const NMEA_GPS_UINT8 dateYears);

extern NMEA_GPS_BOOLEAN nmeaConvertAsciiHexToHex(NMEA_GPS_UINT8  hexCharacterIn,
                                                 NMEA_GPS_UINT8 *hexNumberOut);

extern NMEA_GPS_BOOLEAN nmeaConvertHexToAsciiHex(const NMEA_GPS_UINT8                hexNumberIn,
                                                       NMEA_GPS_UINT8               *hexCharacterOut,
                                                 const nmeaGenerateAsciiDigitCase_t  hexCase);

/******************************************************************************/

#endif

/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/#pragma once
