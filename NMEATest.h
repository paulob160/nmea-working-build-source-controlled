/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/
/*                                                                            */
/* NMEATest.h                                                                 */
/* 10.03.20                                                                   */
/*                                                                            */
/* - test harnesses                                                           */
/*                                                                            */
/******************************************************************************/

#ifndef _NMEA_TEST_H_
#define _NMEA_TEST_H_

/******************************************************************************/

#include "NMEAGps.h"
#include "NMEADecoder.h"

/******************************************************************************/

#define NMEA_TEST_EXIT_CODE                         ('x')
#define NMEA_TEST_WAIT_CODE                         ('w')

#define NMEA_TEST_SENTENCE_SLEEP_TIME_MILLISECONDS    (2)
#define NMEA_TEST_VARIABLE_SLEEP_TIME_MILLISECONDS    (5)

/******************************************************************************/

extern NMEA_GPS_BOOLEAN nmeaTestInitialiseSentenceState(NMEASentenceCaptureState_t *sentenceState);
extern NMEA_GPS_BOOLEAN nmeaTestRandomVariableGenerator(void);
extern NMEA_GPS_BOOLEAN nmeaTestSpeedOverGroundField(void);

/******************************************************************************/

#endif

/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/
