/******************************************************************************/

#include <stdint.h>
#include <ctype.h>
#include "NMEADecoder.h"
#include "NMEAUtilities.h"

/******************************************************************************/
/* nmeaDateParser() :                                                         */
/*  --> charactersToParse : point in the sentence to expect == "date"         */
/*  --> captureState      : the sentence capture state                        */
/*  --> domainHandling    : aggregation of the latest fix updates             */
/*  <-- sentenceState     : result of parsing success or failure              */
/*                                                                            */
/* - parse and decode a sentence's date fields :                              */
/*     ddmmyy                                                                 */
/*                                                                            */
/* - sub-field values are built "on-the-fly"                                  */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaDateParser(const NMEA_GPS_UCHAR            *charactersToParse,
                                          NMEASentenceCaptureState_t *captureState,
                                          void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t sentenceState   = captureState->nmeaSentenceInnerState;
  NMEA_GPS_UCHAR       nextCharacter   = 0;
  NMEA_GPS_UINT8       dateDays        = 0,
                       dateMonths      = 0,
                       dateYears       = 0;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    // On entering a field initialisation is detected by "matchIndex == 0"
    if (captureState->nmeaSentenceGenericState == NMEA_SENTENCE_STATE_INITIALISATION)
      {
      captureState->nmeaSentenceGenericState = NMEA_FIELD_DATE_DAYS_STATE;
      }

    while (captureState->nmeaSentenceCharactersUsed < captureState->nmeaSentenceNewCharacters)
      {
      switch (captureState->nmeaSentenceGenericState)
        {
        // The date sub-field sizes are all the same length and type - 
        // so we cheat a bit to reduce the code size
        case NMEA_FIELD_DATE_DAYS_STATE        : // The first integer legal characters are digits [ 0 .. 9 ]. The leading ',' 
                                                 // (comma) in a sentence is handled by the "inner" parser
        case NMEA_FIELD_DATE_HOURS_STATE       :
        case NMEA_FIELD_DATE_YEARS_STATE       :
    
        nextCharacter = *(charactersToParse + captureState->nmeaSentenceCharactersUsed);
    
        if (!isdigit(nextCharacter))
          {
          captureState->nmeaSentenceGenericState = NMEA_FIELD_DATE_FAILED_STATE;
          }
        else
          {
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
          captureState->nmeaSentence[captureState->nmeaSentenceCharacterIndex] = nextCharacter;
#endif   
          // Update the checksum
          nmeaComputeChecksum(&nextCharacter,
                               1,
                              &captureState->nmeaSentenceComputedChecksum);

          captureState->nmeaSentenceScratch[captureState->nmeaSentenceMatchIndex] = nextCharacter;

          captureState->nmeaSentenceMatchIndex     = captureState->nmeaSentenceMatchIndex     + 1;
          captureState->nmeaSentenceCharactersUsed = captureState->nmeaSentenceCharactersUsed + 1;
          captureState->nmeaSentenceCharacterIndex = captureState->nmeaSentenceCharacterIndex + 1;
    
          if ((captureState->nmeaSentenceMatchIndex % NMEA_FIELD_DATE_DAYS_LENGTH) == 0) // or months, years-length...
            { // Sub-field values are built "on-the-fly"
            captureState->nmeaSentenceMatchIndex = 0;

            switch (captureState->nmeaSentenceGenericState)
              {
              // Legal time ranges are validated at the end of parsing ("nmeaValidateUtcTime")...
              case NMEA_FIELD_DATE_DAYS_STATE   :      dateDays       = ((captureState->nmeaSentenceScratch[0] - NMEA_GPS_ASCII_DIGIT_ZERO) * NMEA_GPS_DIGIT_x_10) + (nextCharacter - NMEA_GPS_ASCII_DIGIT_ZERO);
                                                       captureState->nmeaSentenceGenericState = NMEA_FIELD_DATE_HOURS_STATE;
                                                       break;
              case NMEA_FIELD_DATE_HOURS_STATE  :      dateMonths     = ((captureState->nmeaSentenceScratch[0] - NMEA_GPS_ASCII_DIGIT_ZERO) * NMEA_GPS_DIGIT_x_10) + (nextCharacter - NMEA_GPS_ASCII_DIGIT_ZERO);
                                                       captureState->nmeaSentenceGenericState = NMEA_FIELD_DATE_YEARS_STATE;
                                                       break;
              case NMEA_FIELD_DATE_YEARS_STATE  :      dateYears      = ((captureState->nmeaSentenceScratch[0] - NMEA_GPS_ASCII_DIGIT_ZERO) * NMEA_GPS_DIGIT_x_10) + (nextCharacter - NMEA_GPS_ASCII_DIGIT_ZERO);
                                                       captureState->nmeaSentenceGenericState = NMEA_FIELD_DATE_EXIT_STATE;
                                                       break;
              } /* end of INNER "switch (captureState->nmeaSentenceGenericState ..."*/
            } /* end of "if (captureState->nmeaSentenceMatchIndex == ..."          */
          }
                                                 break;

        default                                : captureState->nmeaSentenceGenericState = NMEA_FIELD_DATE_FAILED_STATE;
                                                 break;
        } /* end of OUTER "switch (captureState->nmeaSentenceGenericState ..."*/

      // On termination propagate the final state to the caller
      if (captureState->nmeaSentenceGenericState == NMEA_FIELD_DATE_EXIT_STATE)
        {
        if (nmeaValidateDate(dateDays,
                             dateMonths,
                             dateYears) == true)
          {
          // Update the fix group
          gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                 NMEA_GPS_GROUP_GEO_POSITION,
                                 NMEA_GPS_FIELD_GEO_POSITION_DATE_DAYS,
                                 (void *)&dateDays);

          gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                  NMEA_GPS_GROUP_GEO_POSITION,
                                  NMEA_GPS_FIELD_GEO_POSITION_DATE_HOURS,
                                  (void *)&dateMonths);

          gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                  NMEA_GPS_GROUP_GEO_POSITION,
                                  NMEA_GPS_FIELD_GEO_POSITION_DATE_YEARS,
                                  (void *)&dateYears);

          sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;
          }
        else
          {
          sentenceState = NMEA_SENTENCE_STATE_FAILED;
          break;
          }
        break;
        }
      else
        {
        if (captureState->nmeaSentenceGenericState == NMEA_FIELD_DATE_FAILED_STATE)
          {
          sentenceState = NMEA_SENTENCE_STATE_FAILED;
          break;
          }
        }
     } /* end of "while (captureState->nmeaSentenceCharactersUsed ..."        */
  } /* end of "if (characterParse != NULL ..."                                */

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaDateParser                                                    */

/******************************************************************************/

