/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/
/*                                                                            */
/* NMEAGps.c                                                                  */
/* 06.01.20                                                                   */
/*                                                                            */
/* Ref : NMEA 0183 Specification v2.3                                         */
/*       ublox NEO-6 - Data Sheet                                             */
/*                                                                            */
/* - GPS data handling                                                        */
/*                                                                            */
/******************************************************************************/

#include <stdint.h>
#include"NMEADecoder.h"

/******************************************************************************/

NMEAGpsUpdate_t gpsLatestFix;

/******************************************************************************/
/* gpsValidateFixGroup() :                                                    */
/*  <--> nmeaGpsFix : an instance of the aggregated GPS fix groups and fields */
/*   --> nmeaGroup  : the group (or all groups) to initialise                 */
/*   <-- fixStatus  : status of this operation                                */
/*                                                                            */
/* - validate the instance of the aggregated GPS fix by setting one or all of */
/*   the group "valid" fields : [ VALID | INVALID ]                           */
/*                                                                            */
/******************************************************************************/

_NMEA_INLINE_FUNCTION_ nmeaGpsStatus_t gpsValidateFixGroup(      NMEAGpsUpdate_t      *nmeaGpsFix,
                                                           const NMEAGpsGroups_t       nmeaGroup,
                                                           const NMEAGpsFieldStatus_t  nmeaFieldStatus)
{
/******************************************************************************/

  nmeaGpsStatus_t fixStatus = NMEA_GPS_STATUS_INVALID;

/******************************************************************************/

  if ((nmeaGpsFix != NULL) && ((nmeaFieldStatus == NMEA_GPS_FIELD_STATUS_INVALID) || (nmeaFieldStatus == NMEA_GPS_FIELD_STATUS_VALID)))
    {
    switch(nmeaGroup)
    {
    case NMEA_GPS_ALL_GROUPS         : nmeaGpsFix->geoPositionValid = nmeaFieldStatus;
                                       fixStatus                    = NMEA_GPS_STATUS_VALID;
                                       break;
    
    case NMEA_GPS_GROUP_GEO_POSITION : nmeaGpsFix->geoPositionValid = nmeaFieldStatus;
                                       fixStatus                    = NMEA_GPS_STATUS_VALID;
                                       break;
    
    default                          : 
                                       break;
    }
  }

/******************************************************************************/

  return(fixStatus);

/******************************************************************************/
} /* end of gpsValidateFixGroup                                               */

/******************************************************************************/
/* gpsUpdateFixGroupField() :                                                 */
/******************************************************************************/

_NMEA_INLINE_FUNCTION_ nmeaGpsStatus_t gpsUpdateFixGroupField(      NMEAGpsUpdate_t *nmeaGpsFix,
                                                              const NMEAGpsGroups_t  nmeaGroup,
                                                              const NMEAGpsField_t   nmeaField,
                                                              const NMEA_GPS_UINT8   nmeaFieldOffset,
                                                                    void            *nmeaFieldupdate)
{
/******************************************************************************/

  nmeaGpsStatus_t fixStatus = NMEA_GPS_STATUS_INVALID;

/******************************************************************************/

  if ((nmeaGpsFix != NULL) && (nmeaFieldupdate != NULL))
    {
    switch(nmeaGroup)
      {
      case NMEA_GPS_GROUP_GEO_POSITION : if (gpsUpdateGeoPosition(nmeaGpsFix,
                                                                  nmeaField,
                                                                  nmeaFieldupdate) == true)
                                           {
                                           fixStatus = NMEA_GPS_STATUS_VALID;
                                           }
                                         break;

      case NMEA_GPS_GROUP_GEO_STATUS   : if (gpsUpdateGeoStatus(nmeaGpsFix,
                                                                nmeaField,
                                                                nmeaFieldOffset,
                                                                nmeaFieldupdate) == true)
                                           {
                                           fixStatus = NMEA_GPS_STATUS_VALID;
                                           }
                                         break;

      default :
                                         break;
      }
    }

/******************************************************************************/

  return(fixStatus);

/******************************************************************************/
} /* end of gpsUpdateFixGroupField                                            */

/******************************************************************************/
/* gpsUpdateGeoStatus() :                                                     */
/******************************************************************************/

_NMEA_INLINE_FUNCTION_ bool gpsUpdateGeoStatus(      NMEAGpsUpdate_t *nmeaGpsFix,
                                               const NMEAGpsField_t   nmeaGeoStatusField,
                                               const NMEA_GPS_UINT8   nmeaGeoStatusFieldOffset,
                                               void                  *nmeaGeoStatusFieldUpdate)
{
/******************************************************************************/

  bool           fieldUpdate = true;
  NMEA_GPS_UINT8 satelliteID = 0;

/******************************************************************************/

  switch(nmeaGeoStatusField)
    {
    case NMEA_GPS_FIELD_GEO_STATUS_S_MODE                      : nmeaGpsFix->geoStatus.sMode                      = *((NMEA_GPS_CHAR *)nmeaGeoStatusFieldUpdate);
                                                                 break;                                           
    case NMEA_GPS_FIELD_GEO_STATUS_FIX_MODE                    : nmeaGpsFix->geoStatus.fixMode                    = *((NMEA_GPS_CHAR *)nmeaGeoStatusFieldUpdate);
                                                                 break;                                           
    case NMEA_GPS_FIELD_GEO_STATUS_FIX_QUALITY                 : nmeaGpsFix->geoStatus.fixQuality                 = *((NMEA_GPS_CHAR *)nmeaGeoStatusFieldUpdate);
                                                                 break;                                         
    case NMEA_GPS_FIELD_GEO_STATUS_SATELLITES_IN_VIEW          : nmeaGpsFix->geoStatus.totalSatellitesInSolution = *((NMEA_GPS_CHAR *)nmeaGeoStatusFieldUpdate);
                                                                 break;

    /******************************************************************************/
    /* GSA Message : list of active satellites                                    */
    /******************************************************************************/

    case NMEA_GPS_FIELD_GEO_STATUS_ACTIVE_SATELLITE_ID_NULL    : for (satelliteID = 0; satelliteID < NMEA_ACTIVE_SATELLITES_MAXIMUM; satelliteID++)
                                                                   {
                                                                   nmeaGpsFix->geoStatus.activeSatelliteId[satelliteID] = *((NMEA_GPS_UINT8 *)nmeaGeoStatusFieldUpdate);
                                                                   }
                                                                 break;

    case NMEA_GPS_FIELD_GEO_STATUS_ACTIVE_SATELLITE_ID_1       : nmeaGpsFix->geoStatus.activeSatelliteId[nmeaGeoStatusFieldOffset] = *((NMEA_GPS_UINT8 *)nmeaGeoStatusFieldUpdate);
                                                                 break;
                                                     
    /******************************************************************************/
    /* GSV Message : satellite id and statistics                                  */
    /******************************************************************************/

    case NMEA_GPS_FIELD_GEO_STATUS_SATELLITES_IN_VIEW_NULL    : for (satelliteID = 0; satelliteID < NMEA_GSV_MAXIMUM_NUMBER_OF_SATELLITES; satelliteID++)
                                                                  {
                                                                  nmeaGpsFix->geoStatus.satellitesInView[satelliteID].satelliteId        =                   *((NMEA_GPS_UINT8 *)nmeaGeoStatusFieldUpdate);
                                                                  nmeaGpsFix->geoStatus.satellitesInView[satelliteID].satelliteElevation =                   *((NMEA_GPS_INT8  *)nmeaGeoStatusFieldUpdate);
                                                                  nmeaGpsFix->geoStatus.satellitesInView[satelliteID].satelliteAzimuth   = (NMEA_GPS_UINT16)(*((NMEA_GPS_UINT8 *)nmeaGeoStatusFieldUpdate));
                                                                  nmeaGpsFix->geoStatus.satellitesInView[satelliteID].satellite_C_N0     =                   *((NMEA_GPS_UINT8 *)nmeaGeoStatusFieldUpdate);
                                                                  }
                                                                break;

    case NMEA_GPS_FIELD_GEO_STATUS_STATISTICS_ID              : nmeaGpsFix->geoStatus.satellitesInView[nmeaGeoStatusFieldOffset].satelliteId        = *((NMEA_GPS_UINT8  *)nmeaGeoStatusFieldUpdate);
                                                                break;
    case NMEA_GPS_FIELD_GEO_STATUS_STATISTICS_ELEVATION       : nmeaGpsFix->geoStatus.satellitesInView[nmeaGeoStatusFieldOffset].satelliteElevation = *((NMEA_GPS_INT8   *)nmeaGeoStatusFieldUpdate);
                                                                break;
    case NMEA_GPS_FIELD_GEO_STATUS_STATISTICS_AZIMUTH         : nmeaGpsFix->geoStatus.satellitesInView[nmeaGeoStatusFieldOffset].satelliteAzimuth   = *((NMEA_GPS_UINT16 *)nmeaGeoStatusFieldUpdate);
                                                                break;
    case NMEA_GPS_FIELD_GEO_STATUS_STATISTICS_C_N0            : nmeaGpsFix->geoStatus.satellitesInView[nmeaGeoStatusFieldOffset].satellite_C_N0     = *((NMEA_GPS_UINT8  *)nmeaGeoStatusFieldUpdate);
                                                                break;

    /******************************************************************************/

    case NMEA_GPS_FIELD_GEO_STATUS_PDOP                        : nmeaGpsFix->geoStatus.pDop       = *((NMEA_GPS_FLOAT *)nmeaGeoStatusFieldUpdate);
                                                                 break;                           
    case NMEA_GPS_FIELD_GEO_STATUS_HDOP                        : nmeaGpsFix->geoStatus.hDop       = *((NMEA_GPS_FLOAT *)nmeaGeoStatusFieldUpdate);
                                                                 break;                           
    case NMEA_GPS_FIELD_GEO_STATUS_VDOP                        : nmeaGpsFix->geoStatus.vDop       = *((NMEA_GPS_FLOAT *)nmeaGeoStatusFieldUpdate);
                                                                 break;                           
    case NMEA_GPS_FIELD_GEO_STATUS_DGPS_AGE                    : nmeaGpsFix->geoStatus.dGpsAge    = *((NMEA_GPS_UINT8 *)nmeaGeoStatusFieldUpdate);
                                                                 break;
    case NMEA_GPS_FIELD_GEO_STATUS_DGPS_ID                     : nmeaGpsFix->geoStatus.dGpsBaseId = *((NMEA_GPS_UINT16 *)nmeaGeoStatusFieldUpdate);
                                                                 break;
    default                                                    : nmeaGpsFix->geoPositionValid     = NMEA_GPS_STATUS_INVALID;
                                                                 fieldUpdate                      = false;
                                                                 break;
    }

/******************************************************************************/

  return(fieldUpdate);

/******************************************************************************/
} /* end of gpsUpdateGeoStatus                                                */

/******************************************************************************/
/* gpsUpdateGeoPosition() :                                                   */
/******************************************************************************/

_NMEA_INLINE_FUNCTION_ bool gpsUpdateGeoPosition(      NMEAGpsUpdate_t *nmeaGpsFix,
                                                 const NMEAGpsField_t   nmeaGeoPositionField,
                                                       void            *nmeaGeoPositionFieldupdate)
{
/******************************************************************************/

  bool fieldUpdate = true;

/******************************************************************************/

  switch(nmeaGeoPositionField)
    {
    case  NMEA_GPS_FIELD_GEO_POSITION_LATITIUDE_DEGREES   : nmeaGpsFix->geoPosition.latitudeDegrees           = *((NMEA_GPS_UINT8 *)nmeaGeoPositionFieldupdate);
                                                            break;                                            
    case  NMEA_GPS_FIELD_GEO_POSITION_LATITIUDE_MINUTES   : nmeaGpsFix->geoPosition.latitudeMinutes           = *((NMEA_GPS_FLOAT *)nmeaGeoPositionFieldupdate);
                                                            break;
    case  NMEA_GPS_FIELD_GEO_POSITION_LATITIUDE_NORTHINGS : nmeaGpsFix->geoPosition.latitudeNorthings         = *((NMEA_GPS_CHAR *)nmeaGeoPositionFieldupdate);
                                                            break;
    case  NMEA_GPS_FIELD_GEO_POSITION_LONGITUDE_DEGREES   : nmeaGpsFix->geoPosition.longitudeDegrees          = *((NMEA_GPS_UINT8 *)nmeaGeoPositionFieldupdate);
                                                            break;                                            
    case  NMEA_GPS_FIELD_GEO_POSITION_LONGITUDE_MINUTES   : nmeaGpsFix->geoPosition.longitudeMinutes          = *((NMEA_GPS_FLOAT *)nmeaGeoPositionFieldupdate);
                                                            break;
    case  NMEA_GPS_FIELD_GEO_POSITION_LONGITUDE_EASTINGS  : nmeaGpsFix->geoPosition.longitudeEastings         = *((NMEA_GPS_CHAR *)nmeaGeoPositionFieldupdate);
                                                            break;
    case  NMEA_GPS_FIELD_GEO_POSITION_UTC_HOURS           : nmeaGpsFix->geoPosition.utcHours                  = *((NMEA_GPS_UINT8 *)nmeaGeoPositionFieldupdate);
                                                            break;                                            
    case  NMEA_GPS_FIELD_GEO_POSITION_UTC_MINUTES         : nmeaGpsFix->geoPosition.utcMinutes                = *((NMEA_GPS_UINT8 *)nmeaGeoPositionFieldupdate);
                                                            break;                                            
    case  NMEA_GPS_FIELD_GEO_POSITION_UTC_SECONDS         : nmeaGpsFix->geoPosition.utcSeconds                = *((NMEA_GPS_UINT8 *)nmeaGeoPositionFieldupdate);
                                                            break;                                            
    case  NMEA_GPS_FIELD_GEO_POSITION_UTC_CENTISECONDS    : nmeaGpsFix->geoPosition.utcCentiseconds           = *((NMEA_GPS_UINT8 *)nmeaGeoPositionFieldupdate);
                                                            break;                                            
    case  NMEA_GPS_FIELD_GEO_POSITION_SPEED_OVER_GROUND   : nmeaGpsFix->geoPosition.speedOverGround           = *((NMEA_GPS_FLOAT *)nmeaGeoPositionFieldupdate);
                                                            break;                                            
    case  NMEA_GPS_FIELD_GEO_POSITION_COURSE_OVER_GROUND  : nmeaGpsFix->geoPosition.courseOverGround          = *((NMEA_GPS_FLOAT *)nmeaGeoPositionFieldupdate);
                                                            break;
    case NMEA_GPS_FIELD_GEO_POSITION_ALTITUDE             : nmeaGpsFix->geoPosition.altitude                  = *((NMEA_GPS_FLOAT *)nmeaGeoPositionFieldupdate);
                                                            break;
    case NMEA_GPS_FIELD_GEO_POSITION_GEOID_SEPERATION     : nmeaGpsFix->geoPosition.geoidSeperation           = *((NMEA_GPS_FLOAT *)nmeaGeoPositionFieldupdate);
                                                            break;
    case NMEA_GPS_FIELD_GEO_POSITION_MAGNETIC_VARIATION   : nmeaGpsFix->geoPosition.magneticVariationEastings = *((NMEA_GPS_CHAR *)nmeaGeoPositionFieldupdate);
                                                            break;
    case NMEA_GPS_FIELD_GEO_POSITION_DATE_DAYS            : nmeaGpsFix->geoPosition.dateDays                  = *((NMEA_GPS_UINT8 *)nmeaGeoPositionFieldupdate);
                                                            break;                                            
    case NMEA_GPS_FIELD_GEO_POSITION_DATE_MONTHS          : nmeaGpsFix->geoPosition.dateMonths                = *((NMEA_GPS_UINT8 *)nmeaGeoPositionFieldupdate);
                                                            break;                                            
    case NMEA_GPS_FIELD_GEO_POSITION_DATE_YEARS           : nmeaGpsFix->geoPosition.dateYears                 = *((NMEA_GPS_UINT8 *)nmeaGeoPositionFieldupdate);
                                                            break;                                            
    case  NMEA_GPS_FIELD_GEO_POSITION_FIX_STATUS          : nmeaGpsFix->geoPosition.fixStatus                 = *((NMEA_GPS_CHAR *)nmeaGeoPositionFieldupdate);
                                                            break;                                            
    case  NMEA_GPS_FIELD_GEO_POSITION_FAA_MODE            : nmeaGpsFix->geoPosition.faaMode                   = *((NMEA_GPS_CHAR *)nmeaGeoPositionFieldupdate);
                                                            break;

    default                                               : nmeaGpsFix->geoPositionValid                      = NMEA_GPS_STATUS_INVALID;
                                                            fieldUpdate                                       = false;
                                                            break;
    }

/******************************************************************************/

  return(fieldUpdate);

/******************************************************************************/
} /* end of gpsUpdateGeoPosition                                              */

/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/