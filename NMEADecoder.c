/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/
/*                                                                            */
/* NMEADecoderMain.c                                                          */
/* 23.12.19                                                                   */
/*                                                                            */
/* Ref : NMEA 0183 Specification v2.3                                         */
/*       ublox NEO-6 - Data Sheet                                             */
/*                                                                            */
/******************************************************************************/

#ifdef _NMEA_INCLUDE_CHECKSUM_DEBUG_
#include <stdio.h>
#endif
#include <stdint.h>
#include <ctype.h>
#include "NMEADecoder.h"
#include "NMEAUtilities.h"

/******************************************************************************/

const NMEA_GPS_UCHAR nmeaSentenceTalkerMaximumLength = NMEA_SENTENCE_TALKER_MAXIMUM_LENGTH;
const NMEA_GPS_UCHAR nmeaSentenceTypesMaximumLength  = NMEA_SENTENCE_TYPE_MAXIMUM_LENGTH;

/******************************************************************************/

// Definition of the recognised talker sentence types (a subset of NMEA 0183 
// sentence talker types). The ordering MUST match the enumeration 
// "NMEASentenceTalkers_t" !!!
const NMEATalkers_t nmeaTalkers[NMEA_TALKERS] =
{
  {
    NMEA_BD,
    NMEA_SENTENCE_TALKER_BD_STRING
  },
  {
    NMEA_CC,
    NMEA_SENTENCE_TALKER_CC_STRING
  }, 
  {
    NMEA_GA,
    NMEA_SENTENCE_TALKER_GA_STRING
  },
  {
    NMEA_GL,
    NMEA_SENTENCE_TALKER_GL_STRING
  },
  {
    NMEA_GN,
    NMEA_SENTENCE_TALKER_GN_STRING
  },
  {                
    NMEA_GP,       
    NMEA_SENTENCE_TALKER_GP_STRING
  },               
  {                
    NMEA_UP,       
    NMEA_SENTENCE_TALKER_UP_STRING
  },
};

// Definition of the default sentence types emitted by ublox NEO-6 : 
// The ordering MUST match the enumeration "NMEASentenceTypes_t" !!!
const NMEATypes_t nmeaTypes[NMEA_SENTENCES] = 
{
  {
  NMEA_GSV,
  NMEA_SENTENCE_TYPE_GSV_STRING,
  NMEA_GSV_SENTENCE_LENGTH,
  NMEA_FIELD_ATTRIBUTE_VARIABLE_LENGTH,
  &nmeaSentenceParserGSV[0],
  NMEA_GSV_SENTENCE_TUPLE_LENGTH,
  &nmeaSentenceTupleParserGSV[0],
  NULL
  },
  {
  NMEA_RMC,
  NMEA_SENTENCE_TYPE_RMC_STRING,
  NMEA_RMC_SENTENCE_LENGTH,
  NMEA_FIELD_ATTRIBUTE_FIXED_LENGTH,
  &nmeaSentenceParserRMC[0],
  NMEA_NIL_SENTENCE_TUPLE_LENGTH,
  NULL,
  NULL
  },
  {
  NMEA_GSA,
  NMEA_SENTENCE_TYPE_GSA_STRING,
  NMEA_GSA_SENTENCE_LENGTH,
  NMEA_FIELD_ATTRIBUTE_FIXED_LENGTH,
  &nmeaSentenceParserGSA[0],
  NMEA_NIL_SENTENCE_TUPLE_LENGTH,
  NULL,
  NULL
  },
  {
  NMEA_GGA,
  NMEA_SENTENCE_TYPE_GGA_STRING,
  NMEA_GGA_SENTENCE_LENGTH,
  NMEA_FIELD_ATTRIBUTE_FIXED_LENGTH,
  &nmeaSentenceParserGGA[0],
  NMEA_NIL_SENTENCE_TUPLE_LENGTH,
  NULL,
  NULL
  },
  {
  NMEA_GLL,
  NMEA_SENTENCE_TYPE_GLL_STRING,
  NMEA_GLL_SENTENCE_LENGTH,
  NMEA_FIELD_ATTRIBUTE_FIXED_LENGTH,
  &nmeaSentenceParserGLL[0],
  NMEA_NIL_SENTENCE_TUPLE_LENGTH,
  NULL,
  NULL
  },
#if _NMEA_SENTENCE_VTG_
  {
  NMEA_VTG,
  NMEA_SENTENCE_TYPE_VTG_STRING,
  NMEA_VTG_SENTENCE_LENGTH,
  NMEA_FIELD_ATTRIBUTE_FIXED_LENGTH,
  NULL,
  NMEA_NIL_SENTENCE_TUPLE_LENGTH,
  NULL,
  NULL
  },
#endif
#if _NMEA_SENTENCE_TXT_
  {
  NMEA_TXT,
  NMEA_SENTENCE_TYPE_TXT_STRING,
  NMEA_TXT_SENTENCE_LENGTH,
  NMEA_FIELD_ATTRIBUTE_FIXED_LENGTH,
  NULL,
  NMEA_NIL_SENTENCE_TUPLE_LENGTH,
  NULL,
  NULL
  }
#endif
};

/******************************************************************************/
/* Definition of the sentence parser function chains - the functions MUST     */
/* appear in the order they are required in the received sentence             */
/******************************************************************************/

NMEASentenceStates_t (*nmeaSentenceParserGLL[NMEA_GLL_SENTENCE_LENGTH])(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                              NMEASentenceCaptureState_t *captureState,
                                                                              void                       *domainHandling) = 
{
  nmeaLatitudeParser,
  nmeaNorthingsParser,
  nmeaLongitudeParser,
  nmeaEastingsParser,
  nmeaUTCParser,
  nmeaDataStatusParser,
  nmeaFAAModeParser
};

NMEASentenceStates_t (*nmeaSentenceParserGSV[NMEA_GSV_SENTENCE_LENGTH])(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                              NMEASentenceCaptureState_t *captureState,
                                                                              void                       *domainHandling) = 
{
  nmeaGSVGroupSizeParser,
  nmeaGSVSentenceNumberParser,
  nmeaGSVSatellitesInViewParser,
  nmeaGSVSatelliteFieldParser,
  nmeaGSVSatelliteFieldParser,
  nmeaGSVSatelliteFieldParser,
  nmeaGSVSatelliteFieldParser,
  nmeaGSVSatelliteFieldParser,
  nmeaGSVSatelliteFieldParser,
  nmeaGSVSatelliteFieldParser,
  nmeaGSVSatelliteFieldParser,
  nmeaGSVSatelliteFieldParser,
  nmeaGSVSatelliteFieldParser,
  nmeaGSVSatelliteFieldParser,
  nmeaGSVSatelliteFieldParser,
  nmeaGSVSatelliteFieldParser,
  nmeaGSVSatelliteFieldParser,
  nmeaGSVSatelliteFieldParser,
  nmeaGSVSatelliteFieldParser
};

// This block can be repeated a number of times in a "GSV" sentence
NMEASentenceStates_t (*nmeaSentenceTupleParserGSV[NMEA_GSV_SENTENCE_TUPLE_LENGTH])(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                                         NMEASentenceCaptureState_t *captureState,
                                                                                         void                       *domainHandling) = 
{
  nmeaGSVSatelliteFieldParser,
  nmeaGSVSatelliteFieldParser,
  nmeaGSVSatelliteFieldParser,
  nmeaGSVSatelliteFieldParser
};

NMEASentenceStates_t (*nmeaSentenceParserRMC[NMEA_RMC_SENTENCE_LENGTH])(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                              NMEASentenceCaptureState_t *captureState,
                                                                              void                       *domainHandling) = 
{
  nmeaUTCParser,
  nmeaDataStatusParser,
  nmeaLatitudeParser,
  nmeaNorthingsParser,
  nmeaLongitudeParser,
  nmeaEastingsParser,
  nmeaSpeedOverGroundParser,
  nmeaCourseOverGroundParser,
  nmeaDateParser,
  nmeaMagneticVariationParser,
  nmeaEastingsParser,
  nmeaFAAModeParser
};

NMEASentenceStates_t (*nmeaSentenceParserGSA[NMEA_GSA_SENTENCE_LENGTH])(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                              NMEASentenceCaptureState_t *captureState,
                                                                              void                       *domainHandling) = 
{
  nmeaOperatingModeParser,
  nmeaFixModeParser,
  nmeaSatelliteIDParser,
  nmeaSatelliteIDParser,
  nmeaSatelliteIDParser,
  nmeaSatelliteIDParser,
  nmeaSatelliteIDParser,
  nmeaSatelliteIDParser,
  nmeaSatelliteIDParser,
  nmeaSatelliteIDParser,
  nmeaSatelliteIDParser,
  nmeaSatelliteIDParser,
  nmeaSatelliteIDParser,
  nmeaSatelliteIDParser,
  nmeaPositionpDopParser,
  nmeaPositionhDopParser,
  nmeaPositionvDopParser
};

NMEASentenceStates_t (*nmeaSentenceParserGGA[NMEA_GGA_SENTENCE_LENGTH])(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                              NMEASentenceCaptureState_t *captureState,
                                                                              void                       *domainHandling) = 
{
  nmeaUTCParser,
  nmeaLatitudeParser,
  nmeaNorthingsParser,
  nmeaLongitudeParser,
  nmeaEastingsParser,
  nmeaFixQualityParser,
  nmeaGGASatellitesInViewParser,
  nmeaPositionhDopParser,
  nmeaAltitudeParser,
  nmeaMetresParser,
  nmeaGeoidSeperationParser,
  nmeaMetresParser,
  nmeaDgpsCorrectionAgeParser,
  nmeaDgpsBaseStationIdParser
};
/******************************************************************************/
/* THe common single-character parser uses these small arrays to load the     */
/* options for various fields :                                               */
/******************************************************************************/

const NMEA_GPS_UCHAR nmeaNorthings[NMEA_FIELD_NORTHINGS] = 
{
  ((NMEA_GPS_UCHAR)NMEA_FIELD_NORTHINGS_NORTH),
  ((NMEA_GPS_UCHAR)NMEA_FIELD_NORTHINGS_SOUTH)
};

const NMEA_GPS_UCHAR nmeaEastings[NMEA_FIELD_EASTINGS] = 
{
  ((NMEA_GPS_UCHAR)NMEA_FIELD_EASTINGS_EAST),
  ((NMEA_GPS_UCHAR)NMEA_FIELD_EASTINGS_WEST)
};

const NMEA_GPS_UCHAR nmeaDataValid[NMEA_FIELD_DATA_VALIDS] = 
{
  ((NMEA_GPS_UCHAR)NMEA_FIELD_DATA_VALID_VALID),
  ((NMEA_GPS_UCHAR)NMEA_FIELD_DATA_VALID_INVALID)
};

const NMEA_GPS_UCHAR nmeaFAAMode[NMEA_FIELD_FAA_MODES] = 
{
  ((NMEA_GPS_UCHAR)NMEA_FIELD_FAA_MODE_AUTONOMOUS),
  ((NMEA_GPS_UCHAR)NMEA_FIELD_FAA_MODE_DGPS)
};

const NMEA_GPS_UCHAR nmeaSMode[NMEA_FIELD_S_MODES] = 
{
  ((NMEA_GPS_UCHAR)NMEA_FIELD_S_MODE_MANUAL),
  ((NMEA_GPS_UCHAR)NMEA_FIELD_S_MODE_AUTO)
};

const NMEA_GPS_UCHAR nmeaFixMode[NMEA_FIELD_FIX_STATUSES] = 
{
  ((NMEA_GPS_UCHAR)NMEA_FIELD_STATUS_NONE),
  ((NMEA_GPS_UCHAR)NMEA_FIELD_STATUS_2D),
  ((NMEA_GPS_UCHAR)NMEA_FIELD_STATUS_3D)
};

const NMEA_GPS_UCHAR nmeaFixQuality[NMEA_FIELD_FIX_QUALITIES] =
  {
  ((NMEA_GPS_UCHAR)NMEA_FIELD_FIX_QUALITY_INVALID),
  ((NMEA_GPS_UCHAR)NMEA_FIELD_FIX_QUALITY_GPS),
  ((NMEA_GPS_UCHAR)NMEA_FIELD_FIX_QUALITY_DGPS),
  ((NMEA_GPS_UCHAR)NMEA_FIELD_FIX_QUALITY_DEAD_RECKONING)
  };

const NMEA_GPS_UCHAR nmeaMetres[NMEA_FIELD_LENGTHS] =
  {
  ((NMEA_GPS_UCHAR)NMEA_FIELD_METRES)
  };

/******************************************************************************/
/* nmeaMatchTalker() :                                                        */
/*  --> matchCharacters : character(s) to match                               */
/*  --> matchLength     : number of characters to match                       */
/* <--> matchSuccess    : [ true | false ]                                    */
/*                                                                            */
/* - match one or more characters of the potential talkers                    */
/*                                                                            */
/******************************************************************************/

bool nmeaMatchTalker(const NMEA_GPS_UCHAR *matchCharacters,
                     const NMEA_GPS_UINT8  matchLength)
{
/******************************************************************************/

  bool    matchSuccess = true;
  uint8_t talkersIndex = 0,
          matchIndex   = 0;

/******************************************************************************/

  if ((matchCharacters != NULL) && (matchLength > 0) && (matchLength <= NMEA_SENTENCE_TALKER_LENGTH))
    {
    while (talkersIndex < NMEA_TALKERS)
      {
      matchSuccess = true;

      for (matchIndex = 0; matchIndex < matchLength; matchIndex++)
        {
        if (*(matchCharacters + matchIndex) != nmeaTalkers[talkersIndex].NMEATalkerPrefix[matchIndex])
          {
          matchSuccess = false;
          break;
          }
        }

      if (matchSuccess == false)
        {
        talkersIndex = talkersIndex + 1;

        continue;
        }
      else
        {
        break;
        }
      }
    }
  else
    {
    matchSuccess = false;
    }

/******************************************************************************/

  return(matchSuccess);

/******************************************************************************/
} /* end of nmeaMatchTalker                                                   */

/******************************************************************************/
/* nmeaMatchSentence() :                                                      */
/*  --> matchCharacters   : character(s) to match                             */
/*  --> matchLength       : number of characters to match                     */
/*  <-- matchSentenceType : sentence type for onward parsing                  */
/*                                                                            */
/*  <-- matchSuccess      : [ true | false ]                                  */
/*                                                                            */
/* - match one or more characters of the potential sentence types. Just a     */
/*   linear search for now                                                    */
/*                                                                            */
/******************************************************************************/

bool nmeaMatchSentence(const NMEA_GPS_UCHAR      *matchCharacters,
                       const NMEA_GPS_UINT8       matchLength,
                             NMEASentenceTypes_t *matchSentenceType)
{
/******************************************************************************/

  bool           matchSuccess   = true;
  NMEA_GPS_UINT8 sentencesIndex = 0,
                 matchIndex     = 0;

/******************************************************************************/

  if ((matchCharacters != NULL) && (matchSentenceType != NULL) && (matchLength > 0) && (matchLength <= NMEA_SENTENCE_TYPE_LENGTH))
  {
    while (sentencesIndex < NMEA_SENTENCES)
    {
      matchSuccess = true;

      for (matchIndex = 0; matchIndex < matchLength; matchIndex++)
      {
        if (*(matchCharacters + matchIndex) != nmeaTypes[sentencesIndex].NMEASentenceType[matchIndex])
        {
          matchSuccess = false;
          break;
        }
      }

      if (matchSuccess == false)
        {
        sentencesIndex = sentencesIndex + 1;

        continue;
        }
      else
        {
        *matchSentenceType = nmeaTypes[sentencesIndex].NMEASentenceCode;
         break;
        }
    }
  }
  else
  {
    matchSuccess = false;
  }

/******************************************************************************/

  return(matchSuccess);

/******************************************************************************/
} /* end of nmeaMatchSentence                                                 */

/******************************************************************************/
/* nmeaComputeChecksum() :                                                    */
/*  --> matchedCharacters      : ASCII codes to include in the checksum       */
/*                               calculation                                  */
/*  --> matchedCharacterLength : number of characters to include              */
/*  <-- runningChecksum        : calculation running result                   */
/*                                                                            */
/* - NMEA 0183 checksum calculation is a simple XOR of all the ASCII codes in */
/*   a sentence between '$' and '*'. XOR'ing is the same operating forwards   */
/*   or backwards - here we err on the side of caution and compute in time-   */
/*   order i.e. f(t) = f(t - 1) XOR c(t) : t >= 0, t(0) = 0, t(1) = 1 ...     */
/*                                                                            */
/******************************************************************************/

bool nmeaComputeChecksum(const NMEA_GPS_UCHAR *matchedCharacters,
                         const NMEA_GPS_UINT8  matchedCharacterLength,
                               NMEA_GPS_UINT8 *runningChecksum)
{
/******************************************************************************/

  NMEA_GPS_UINT8 matchIndex      = 0;
  bool           checksumSuccess = false;

/******************************************************************************/

  if ((matchedCharacters != NULL) && (matchedCharacterLength > 0) && (runningChecksum != NULL))
    {
    for (matchIndex = 0; matchIndex < matchedCharacterLength; matchIndex++)
      {
      *runningChecksum = *runningChecksum ^ *(matchedCharacters + matchIndex);

#ifdef _NMEA_INCLUDE_CHECKSUM_DEBUG_
      loadChecksumTestBuffer(*runningChecksum);
#endif
      }

    checksumSuccess = true;
    }

/******************************************************************************/

  return(checksumSuccess);

/******************************************************************************/
} /* end of nmeaComputeChecksum                                               */

/******************************************************************************/
/* Checksum testing functions :                                               */
/******************************************************************************/

#ifdef _NMEA_INCLUDE_CHECKSUM_DEBUG_

#define NMEA_SENTENCE_CHECKSUM_BUFFER_OFFSET (1) // the first buffer location is always 
                                                 // populated with the checksum start value

static  NMEA_GPS_UINT8 checksumBufferIndex = 0;
static  NMEA_GPS_UINT8 checksumBuffer[NMEA_SENTENCE_MAXIMUM_LENGTH];

/******************************************************************************/
/* initialiseChecksumTestBuffer(() :                                          */
/*   --> initialChecksum : the starting value for the checksum calculation    */
/*                                                                            */
/* - for testing the checksum calculation                                     */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_VOID initialiseChecksumTestBuffer(NMEA_GPS_UINT8 initialChecksum)
{
/******************************************************************************/

  checksumBufferIndex = 0;

  for (checksumBufferIndex = 0; checksumBufferIndex < NMEA_SENTENCE_MAXIMUM_LENGTH; checksumBufferIndex++)
    {
    checksumBuffer[checksumBufferIndex] = 0;
    }

  checksumBufferIndex = NMEA_SENTENCE_CHECKSUM_BUFFER_OFFSET; // the checksum always starts at 0

/******************************************************************************/
} /* end of initialiseChecksumTestBuffer                                      */

/******************************************************************************/
/* loadChecksumTestBuffer() :                                                 */
/*   --> latestChecksum : latest value of the running checksum                */
/*                                                                            */
/* - for testing the checksum calculation                                     */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_VOID loadChecksumTestBuffer(NMEA_GPS_UINT8 latestChecksum)
{
/******************************************************************************/

  checksumBuffer[checksumBufferIndex] = latestChecksum;

  checksumBufferIndex = checksumBufferIndex + 1;

/******************************************************************************/
} /* end of loadChecksumTestBuffer                                            */

/******************************************************************************/
/* printChecksumTestBuffer() :                                                */
/* - for testing the checksum calculation                                     */
/******************************************************************************/

#define NMEA_CHECKSUM_TEST_COLUMNS (10)

NMEA_GPS_VOID printChecksumTestBuffer(NMEA_GPS_VOID)
{
/******************************************************************************/

  NMEA_GPS_UINT8 checksumIndex = 0;

/******************************************************************************/

  printf("\n");

  for (checksumIndex = 0; checksumIndex < NMEA_SENTENCE_MAXIMUM_LENGTH; checksumIndex++)
    {
    if (((checksumIndex % NMEA_CHECKSUM_TEST_COLUMNS) == 0) && (checksumIndex != 0))
      {
      printf("\n");
      }

    printf(" [%02d]=%02X", checksumIndex, checksumBuffer[checksumIndex]);
    }

  printf("\n");

/******************************************************************************/
} /* end of printChecksumTestBuffer                                           */

/******************************************************************************/

#endif

/******************************************************************************/
/* In-sentence parser :                                                       */
/******************************************************************************/
/* nmeaParseSentence() :                                                      */
/*  <--> sentenceState : aggregation of current sentence complete parsing     */
/*                       state                                                */
/*                                                                            */
/* - parse an NMEA 0183 sentence-type. The sentence is formed of 'n' fields,  */
/*   'n - 1' ',' (comma) characters and a terminating '*' (asterisk)          */
/*   character                                                                */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaParseSentence(const NMEA_GPS_UCHAR             *newSentenceCharacters,
                                             NMEASentenceCaptureState_t *sentenceState)
{
/******************************************************************************/

  NMEA_GPS_UCHAR sentenceFieldSeperator = NMEA_SENTENCE_FIELD_SEPERATOR; // for the first 'n - 1' field seperators

/******************************************************************************/

  // If parsing a new sentence setup the (fixed-sentence-length) field/comma index 
  // and look for any empty fields indicated by adjacent','s (comma) e.g. ',,' 
  // NOTE : the first ',' (comma) in any sentence is parsed in the OUTER parser!
  if (sentenceState->nmeaSentenceInnerState == NMEA_SENTENCE_STATE_INITIALISATION)
    {
    sentenceState->nmeaSentenceFields     = nmeaTypes[sentenceState->nmeaSentenceType].nmeaSentenceFieldLength;
    sentenceState->nmeaSentenceInnerState = NMEA_SENTENCE_STATE_PARSE_SEPERATOR;
    }

  // The inner parsing state machines must maintain the linear "character used" 
  // count and exit synchronously with the outer parsing state machine
  while (sentenceState->nmeaSentenceCharactersUsed < sentenceState->nmeaSentenceNewCharacters)
    {
    // Test for the field seperator character required
    if (sentenceState->nmeaSentenceFields == 1)
      {
      sentenceFieldSeperator = NMEA_SENTENCE_FIELD_CHECKSUM_PREFIX;
      }

    switch (sentenceState->nmeaSentenceInnerState)
      {
      case NMEA_SENTENCE_STATE_PARSE_SEPERATOR : // Advance if a comma is found else the new character (potentially) starts a field. This handles 
                                               // the case of empty sentence fields
                                               if ((*(newSentenceCharacters + sentenceState->nmeaSentenceCharactersUsed)) != sentenceFieldSeperator)
                                                 {
                                                 sentenceState->nmeaSentenceMatchIndex   = 0;
                                                 sentenceState->nmeaSentenceGenericState = (NMEASentenceGenericStates_t)NMEA_SENTENCE_STATE_INITIALISATION;
                                                 sentenceState->nmeaSentenceInnerState   = NMEA_SENTENCE_STATE_PARSE_FIELD;
                                                 }
                                               else
                                                 {
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
                                                 sentenceState->nmeaSentence[sentenceState->nmeaSentenceCharacterIndex] = sentenceFieldSeperator;
#endif
                                                 sentenceState->nmeaSentenceFields         = sentenceState->nmeaSentenceFields         - 1;
                                                 sentenceState->nmeaSentenceCharactersUsed = sentenceState->nmeaSentenceCharactersUsed + 1;
                                                 sentenceState->nmeaSentenceCharacterIndex = sentenceState->nmeaSentenceCharacterIndex + 1;

                                                 // Increment the "repetitive states" counter - this will be reset/incremented 
                                                 // by the scanning sentence parsers if they need to. This makes sure missing fields 
                                                 // are included in the state monotonic indexing
                                                 sentenceState->nmeaSentenceStateCounter    = sentenceState->nmeaSentenceStateCounter + 1;
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
                                                 sentenceState->nmeaSentence[sentenceState->nmeaSentenceCharacterIndex] = NMEA_SENTENCE_END_OF_STRING;
#endif                                           
                                                 // Compute the running checksum...but exclude the '*' (asterisk) character
                                                 if (sentenceFieldSeperator == NMEA_SENTENCE_FIELD_SEPERATOR)
                                                   {
                                                   nmeaComputeChecksum(&sentenceFieldSeperator, // (newSentenceCharacters + sentenceState->nmeaSentenceCharactersUsed),
                                                                        NMEA_SENTENCE_FIELD_SEPERATOR_LENGTH,
                                                                       &sentenceState->nmeaSentenceComputedChecksum);
                                                   }
                                                 
                                                 // If all the fields have been parsed then exit this sentence
                                                 if (sentenceState->nmeaSentenceFields == 0)
                                                   { // Is the sentence length OK ?
                                                   if (sentenceState->nmeaSentenceCharacterIndex <= NMEA_SENTENCE_MAXIMUM_LENGTH)
                                                     {
                                                     sentenceState->nmeaSentenceInnerState = NMEA_SENTENCE_STATE_COMPLETE;
                                                     }
                                                   else
                                                     {
                                                     sentenceState->nmeaSentenceInnerState = NMEA_SENTENCE_STATE_FAILED;
                                                     }
                                                   }
                                                 }
                                             
                                               break;

      case NMEA_SENTENCE_STATE_PARSE_FIELD     : // All fields types have special-to-type parsers and the sentences are built from these
                                                 sentenceState->nmeaSentenceInnerState = 
                                                   (nmeaTypes[sentenceState->nmeaSentenceType].nmeaSentenceFieidList[nmeaTypes[sentenceState->nmeaSentenceType].nmeaSentenceFieldLength - sentenceState->nmeaSentenceFields])(newSentenceCharacters,
                                                                                                                                                                                                                              sentenceState,
                                                                                                                                                                                                                              (void *)&gpsLatestFix);
                                               
                                                 if (sentenceState->nmeaSentenceInnerState == NMEA_SENTENCE_STATE_EXIT_FIELD)
                                                   {
                                                   sentenceState->nmeaSentenceInnerState = NMEA_SENTENCE_STATE_PARSE_SEPERATOR;
                                                   }
                                               
                                                 break;
                                               
      default                                  : sentenceState->nmeaSentenceInnerState   = NMEA_SENTENCE_STATE_FAILED;
                                                 break;

      }

    if ((sentenceState->nmeaSentenceInnerState == NMEA_SENTENCE_STATE_COMPLETE) || (sentenceState->nmeaSentenceInnerState == NMEA_SENTENCE_STATE_FAILED))
      {
      break;
      }
  }

/******************************************************************************/

  return(sentenceState->nmeaSentenceInnerState);

/******************************************************************************/
} /* end of nmeaParseSentence                                                 */

/******************************************************************************/
/* Sentence field parsers :                                                   */
/******************************************************************************/
/* nmeaLatitudeLongitudeParser() :                                            */
/*  --> charactersToParse : point in the sentence to expect "latitude" or     */
/*                          "longitude"                                       */
/*  --> captureState      : the sentence capture state                        */
/*  --> domainHandling    : aggregation of the latest fix updates             */
/*  <-- sentenceState     : result of parsing success or failure              */
/*                                                                            */
/* - parse and decode a sentence's "latitude" field in the form :             */
/*     1 { dd } 2 + mm.f - f                                                  */
/*   where 'dd' == integer degrees                                            */
/*         'mm' == integer minutes                                            */
/*         'ff' == fractional minutes                                         */
/* - OR                                                                       */
/* - parse and decode a sentence's "longitude" field in the form :            */
/*     1 { ddd } 3 + mm.f - f                                                 */
/*   where 'ddd' == integer degrees                                           */
/*         'mm'  == integer minutes                                           */
/*         'ff'  == fractional minutes                                        */
/*                                                                            */
/* - latitude and longitude parsing are almost identical so this is common    */
/*   to both with the differences passed in                                   */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaLatitudeLongitudeParser(const nmeaSentenceLatLongSelector_t  latLongSelector,
                                                 const NMEA_GPS_UCHAR                *charactersToParse,
                                                       NMEASentenceCaptureState_t    *captureState,
                                                       void                          *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t        sentenceState = captureState->nmeaSentenceInnerState;

  NMEA_GPS_UINT8              latitudeDegrees      = 0,
                              latLongDegreesLength = 0;
  NMEA_GPS_FLOAT              latitudeMinutes      = 0.0;
                              
  nmeaLatLongCommon_t         latLongCommonFields;

/******************************************************************************/

  // Select the values for the differences between "latitude" and "longitude" - 
  // NOTE : these are NOT static (all the parsing functions are re-entrant) and 
  // MUST be set up on each function entry!
  if (latLongSelector == NMEA_FIELD_LAT_LONG_SELECT_LATITUDE)
    {
    latLongCommonFields.latLongMinimumLimit         = NMEA_FIELD_LATITUDE_MINIMUM_LIMIT;
    latLongCommonFields.latLongMaximumLimit         = NMEA_FIELD_LATITUDE_MAXIMUM_LIMIT;
    latLongCommonFields.latLongGpsDegreesField      = NMEA_GPS_FIELD_GEO_POSITION_LATITIUDE_DEGREES;
    latLongCommonFields.latLongGpsMinutesField      = NMEA_GPS_FIELD_GEO_POSITION_LATITIUDE_MINUTES;
    }
  else
    {
    latLongCommonFields.latLongMinimumLimit         = NMEA_FIELD_LONGITUDE_MINIMUM_LIMIT;
    latLongCommonFields.latLongMaximumLimit         = NMEA_FIELD_LONGITUDE_MAXIMUM_LIMIT;
    latLongCommonFields.latLongGpsDegreesField      = NMEA_GPS_FIELD_GEO_POSITION_LONGITUDE_DEGREES;
    latLongCommonFields.latLongGpsMinutesField      = NMEA_GPS_FIELD_GEO_POSITION_LONGITUDE_MINUTES;
    }

  // On first entry initialise the field parsing state
  if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_SENTENCE_STATE_INITIALISATION)
    {
    // Parse the whole field AS IF it was floating-point so as to fill the scratch buffer
    captureState->nmeaSentenceVariableState.variableType                = NMEA_FIELD_DECIMAL_FLOATING;
    captureState->nmeaSentenceVariableState.variableResult.nmea_float_t = 0.0;

    captureState->nmeaSentenceVariableState.signFlag                    = NMEA_ENCODER_SIGN_FLAG_UNSIGNED;

    // Set the respective integer- and fractional-part field lengths
    if (latLongSelector == NMEA_FIELD_LAT_LONG_SELECT_LATITUDE)
      {
      captureState->nmeaSentenceVariableState.integerPartMinimumLength    = NMEA_FIELD_LATITUDE_DEGREES_MINUTES_MINIMUM_LENGTH;
      captureState->nmeaSentenceVariableState.integerPartMaximumLength    = NMEA_FIELD_LATITUDE_DEGREES_MINUTES_MAXIMUM_LENGTH;

      captureState->nmeaSentenceVariableState.fractionalPartMinimumLength = NMEA_FIELD_LATITUDE_FRACTION_MINIMUM_LENGTH;
      captureState->nmeaSentenceVariableState.fractionalPartMaximumLength = NMEA_FIELD_LATITUDE_FRACTION_MAXIMUM_LENGTH;
      }
    else
      {
      captureState->nmeaSentenceVariableState.integerPartMinimumLength    = NMEA_FIELD_LONGITUDE_DEGREES_MINUTES_MINIMUM_LENGTH;
      captureState->nmeaSentenceVariableState.integerPartMaximumLength    = NMEA_FIELD_LONGITUDE_DEGREES_MINUTES_MAXIMUM_LENGTH;

      captureState->nmeaSentenceVariableState.fractionalPartMinimumLength = NMEA_FIELD_LONGITUDE_FRACTION_MINIMUM_LENGTH;
      captureState->nmeaSentenceVariableState.fractionalPartMaximumLength = NMEA_FIELD_LONGITUDE_FRACTION_MAXIMUM_LENGTH;
      }

    captureState->nmeaSentenceMatchIndex   = 0;
    captureState->nmeaSentenceGenericState = NMEA_VARIABLE_STATE_INITIALISATION; // variable capture is driven by a dedicated parser

    captureState->nmeaSentenceScratch[captureState->nmeaSentenceMatchIndex] = NMEA_SENTENCE_END_OF_STRING;
    }

  // Hand-off to the variable parser state machine
  nmeaVariableNumericFieldParser(charactersToParse,
                                 captureState);

  // On termination propagate the final state to the caller
  if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_EXIT)
    {
    // Determine the number of characters used to represent degrees
    latLongDegreesLength = captureState->nmeaSentenceVariableState.integerPartIndex - NMEA_FIELD_LATITUDE_MINUTES_LENGTH;

    // First try to validate the field contents
    if (nmeaConvertDegrees((const NMEA_GPS_CHAR *)&captureState->nmeaSentenceScratch[0],
                            captureState->nmeaSentenceMatchIndex,
                            (NMEA_GPS_DEGREE_POWERS - latLongDegreesLength),
                            latLongDegreesLength,
                            NMEA_FIELD_LATITUDE_MINUTES_LENGTH,
                            latLongCommonFields.latLongMinimumLimit,
                            latLongCommonFields.latLongMaximumLimit,
                           &latitudeDegrees,
                           &latitudeMinutes) == true)
      {
      // On success update the fix latitude and make the sentence "dirty"
      gpsValidateFixGroup((NMEAGpsUpdate_t *)domainHandling,
                           NMEA_GPS_GROUP_GEO_POSITION,
                           NMEA_GPS_FIELD_STATUS_INVALID);
      
      gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                              NMEA_GPS_GROUP_GEO_POSITION,
                              latLongCommonFields.latLongGpsDegreesField,
                               0,
                              (void *)&latitudeDegrees);
      
      gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                              NMEA_GPS_GROUP_GEO_POSITION,
                              latLongCommonFields.latLongGpsMinutesField,
                              0,
                              (void *)&latitudeMinutes);

      sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;
      }
    else
      {
      captureState->nmeaSentenceGenericState = NMEA_VARIABLE_STATE_FAILED;
      sentenceState                          = NMEA_SENTENCE_STATE_FAILED;
      }
    }
  else
    {
    if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_FAILED)
      {
      sentenceState = NMEA_SENTENCE_STATE_FAILED;
      }
    }

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaLatitudeLongitudeParser                                       */

/******************************************************************************/
/* nmeaLatitudeParser() :                                                     */
/*  --> charactersToParse : point in the sentence to expect == "latitude"     */
/*  --> captureState      : the sentence capture state                        */
/*  --> domainHandling    : aggregation of the latest fix updates             */
/*  <-- sentenceState     : result of parsing success or failure              */
/*                                                                            */
/* - this is the "latitude" entry point for the joint latitude/longitude      */
/*   parser                                                                   */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaLatitudeParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                              NMEASentenceCaptureState_t *captureState,
                                              void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t sentenceState   = NMEA_SENTENCE_STATE_FAILED;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    sentenceState = nmeaLatitudeLongitudeParser(NMEA_FIELD_LAT_LONG_SELECT_LATITUDE,
                                                charactersToParse,
                                                captureState,
                                                domainHandling);
    }

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaLatitudeLongitudeParser                                       */

/******************************************************************************/
/* nmeaLongitudeParser() :                                                    */
/*  --> charactersToParse : point in the sentence to expect == "longitude"    */
/*  --> captureState      : the sentence capture state                        */
/*  --> domainHandling    : aggregation of the latest fix updates             */
/*  <-- sentenceState     : result of parsing success or failure              */
/*                                                                            */
/* - parse and decode a sentence's "longitude" field :                        */
/*     yyyyy.y - y                                                            */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaLongitudeParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                               NMEASentenceCaptureState_t *captureState,
                                               void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t sentenceState = NMEA_SENTENCE_STATE_FAILED;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    sentenceState = nmeaLatitudeLongitudeParser(NMEA_FIELD_LAT_LONG_SELECT_LONGITUDE,
                                                charactersToParse,
                                                captureState,
                                                domainHandling);
    }

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaLongitudeParser                                               */

/******************************************************************************/
/* nmeaFlagParser() :                                                         */
/*  --> charactersToParse   : next character in the sentence                  */
/*  --> captureState        : the sentence capture state                      */
/*  --> domainHandling      : aggregation of the latest fix updates           */
/*  --> flagOptions         : list of the legal flags                         */
/*  --> numberOfFlagOptions : number of flags in the list                     */
/*  --> nmeaGpsGroup        : GPS fix group                                   */
/*  --> nmeaGpsField        : field in the GPS group                          */
/*  <-- sentenceState       : result of parsing success or failure            */
/*                                                                            */
/* - parse one of the sentence flag fields (one or more single characters     */
/*   may have one or more options e.g. "Northings" : [ 'N' | 'S' ]            */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaFlagParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                          NMEASentenceCaptureState_t *captureState,
                                          void                       *domainHandling,
                                    const NMEA_GPS_UCHAR             *flagOptions,
                                    const NMEA_GPS_UINT8              numberOfFlagOptions,
                                    const NMEAGpsGroups_t             nmeaGpsGroup,
                                    const NMEAGpsField_t              nmeaGpsField)
{
/******************************************************************************/

  NMEASentenceStates_t sentenceState = NMEA_SENTENCE_STATE_FAILED;
  NMEA_GPS_UCHAR       nextCharacter = 0;
  NMEA_GPS_UINT8       flagIndex     = 0;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL) && (flagOptions != NULL))
    {
    nextCharacter = *(charactersToParse + captureState->nmeaSentenceCharactersUsed);

    for (flagIndex = 0; flagIndex < numberOfFlagOptions; flagIndex++)
      {
      if (nextCharacter == (*(flagOptions + flagIndex)))
        {
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
        captureState->nmeaSentence[captureState->nmeaSentenceCharacterIndex] = nextCharacter;
#endif

        captureState->nmeaSentenceCharactersUsed = captureState->nmeaSentenceCharactersUsed + 1;
        captureState->nmeaSentenceCharacterIndex = captureState->nmeaSentenceCharacterIndex + 1;
        
        // Update the checksum
        nmeaComputeChecksum(&nextCharacter,
                             1,
                            &captureState->nmeaSentenceComputedChecksum);
        
        // Update the fix group
        gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                nmeaGpsGroup,
                                nmeaGpsField,
                                0,
                                (void *)&nextCharacter);
        
        sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;

        break;
        }
      }
    }


/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaFlagParser                                                    */

/******************************************************************************/
/* nmeaNorthingsParser() :                                                    */
/*  --> charactersToParse : point in the sentence to expect == "latitudeNorthings"    */
/*  --> captureState      : the sentence capture state                        */
/*  --> domainHandling    : aggregation of the latest fix updates             */
/*  <-- sentenceState     : result of parsing success or failure              */
/*                                                                            */
/* - parse and decode a sentence's "Northings" field :                        */
/*     [ "N" | "S" ]                                                          */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaNorthingsParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                               NMEASentenceCaptureState_t *captureState,
                                               void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t sentenceState = NMEA_SENTENCE_STATE_FAILED;

/******************************************************************************/

  sentenceState = nmeaFlagParser(charactersToParse,
                                 captureState,
                                 domainHandling,
                                 (const NMEA_GPS_UCHAR *)&nmeaNorthings[0],
                                 NMEA_FIELD_NORTHINGS,
                                 NMEA_GPS_GROUP_GEO_POSITION,
                                 NMEA_GPS_FIELD_GEO_POSITION_LATITIUDE_NORTHINGS);

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaNorthingsParser                                               */

/******************************************************************************/
/* nmeaEastingsParser() :                                                     */
/*  --> charactersToParse : point in the sentence to expect == "longitudeEastings"     */
/*  --> captureState      : the sentence capture state                        */
/*  --> domainHandling    : aggregation of the latest fix updates             */
/*  <-- sentenceState     : result of parsing success or failure              */
/*                                                                            */
/* - parse and decode a sentence's "Eastings" field :                         */
/*     [ "E" | "W" ]                                                          */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaEastingsParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                              NMEASentenceCaptureState_t *captureState,
                                              void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t sentenceState = NMEA_SENTENCE_STATE_FAILED;

/******************************************************************************/

  sentenceState = nmeaFlagParser(charactersToParse,
                                 captureState,
                                 domainHandling,
                                 (const NMEA_GPS_UCHAR *)&nmeaEastings[0],
                                 NMEA_FIELD_EASTINGS,
                                 NMEA_GPS_GROUP_GEO_POSITION,
                                 NMEA_GPS_FIELD_GEO_POSITION_LONGITUDE_EASTINGS);

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaEastingsParser                                                */

/******************************************************************************/
/* nmeaUTCParser() :                                                          */
/*  --> charactersToParse : point in the sentence to expect == "UTC"          */
/*  --> captureState      : the sentence capture state                        */
/*  --> domainHandling    : aggregation of the latest fix updates             */
/*  <-- sentenceState     : result of parsing success or failure              */
/*                                                                            */
/* - parse and decode a sentence's "UTC" field :                              */
/*     hhmmss.ss                                                              */
/*                                                                            */
/* - sub-field values are built "on-the-fly"                                  */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaUTCParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                         NMEASentenceCaptureState_t *captureState,
                                         void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t sentenceState   = captureState->nmeaSentenceInnerState;
  NMEA_GPS_UCHAR       nextCharacter   = 0;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    // On entering a field initialisation is detected by "matchIndex == 0"
    if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_SENTENCE_STATE_INITIALISATION)
      {
      captureState->nmeaSentenceGenericState = NMEA_FIELD_UTC_HOURS_STATE;
      }

    while (captureState->nmeaSentenceCharactersUsed < captureState->nmeaSentenceNewCharacters)
      {
      switch (captureState->nmeaSentenceGenericState)
        {
        // The UTC sub-field sizes are all the same length and type - 
        // so we cheat a bit to reduce the code size
        case NMEA_FIELD_UTC_HOURS_STATE        : // The first integer legal characters are digits [ 0 .. 9 ]. The leading ',' 
                                                 // (comma) in a sentence is handled by the "inner" parser
        case NMEA_FIELD_UTC_MINUTES_STATE      :
        case NMEA_FIELD_UTC_SECONDS_STATE      :
        case NMEA_FIELD_UTC_CENTISECONDS_STATE :
    
        nextCharacter = *(charactersToParse + captureState->nmeaSentenceCharactersUsed);
    
        if (!isdigit(nextCharacter))
          {
          captureState->nmeaSentenceGenericState = NMEA_FIELD_UTC_FAILED_STATE;
          }
        else
          {
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
          captureState->nmeaSentence[captureState->nmeaSentenceCharacterIndex] = nextCharacter;
#endif   
          // Update the checksum
          nmeaComputeChecksum(&nextCharacter,
                               1,
                              &captureState->nmeaSentenceComputedChecksum);

          captureState->nmeaSentenceScratch[captureState->nmeaSentenceMatchIndex] = nextCharacter;

          captureState->nmeaSentenceMatchIndex     = captureState->nmeaSentenceMatchIndex     + 1;
          captureState->nmeaSentenceCharactersUsed = captureState->nmeaSentenceCharactersUsed + 1;
          captureState->nmeaSentenceCharacterIndex = captureState->nmeaSentenceCharacterIndex + 1;
    
          if ((captureState->nmeaSentenceMatchIndex % NMEA_FIELD_UTC_HOURS_LENGTH) == 0) // or minutes, seconds, centiseconds-length...
            { // Sub-field values are built "on-the-fly"
            captureState->nmeaSentenceMatchIndex = 0;

            switch ((NMEASentenceUTCFieldStates_t)captureState->nmeaSentenceGenericState)
              {
              // Legal time ranges are validated at the end of parsing ("nmeaValidateUtcTime")...
              case NMEA_FIELD_UTC_HOURS_STATE   :      captureState->nmeaSentenceNumberScratch[UTC_HOURS_CACHE_INDEX].nmea_uint8_t        = ((captureState->nmeaSentenceScratch[0] - NMEA_GPS_ASCII_DIGIT_ZERO) * NMEA_GPS_DIGIT_x_10) + (nextCharacter - NMEA_GPS_ASCII_DIGIT_ZERO);
                                                       captureState->nmeaSentenceGenericState = NMEA_FIELD_UTC_MINUTES_STATE;
                                                       break;
              case NMEA_FIELD_UTC_MINUTES_STATE :      captureState->nmeaSentenceNumberScratch[UTC_MINUTES_CACHE_INDEX].nmea_uint8_t      = ((captureState->nmeaSentenceScratch[0] - NMEA_GPS_ASCII_DIGIT_ZERO) * NMEA_GPS_DIGIT_x_10) + (nextCharacter - NMEA_GPS_ASCII_DIGIT_ZERO);
                                                       captureState->nmeaSentenceGenericState = NMEA_FIELD_UTC_SECONDS_STATE;
                                                       break;
              case NMEA_FIELD_UTC_SECONDS_STATE :      captureState->nmeaSentenceNumberScratch[UTC_SECONDS_CACHE_INDEX].nmea_uint8_t      = ((captureState->nmeaSentenceScratch[0] - NMEA_GPS_ASCII_DIGIT_ZERO) * NMEA_GPS_DIGIT_x_10) + (nextCharacter - NMEA_GPS_ASCII_DIGIT_ZERO);
                                                       captureState->nmeaSentenceGenericState = NMEA_FIELD_UTC_SEPERATOR_STATE;
                                                       break;
              case NMEA_FIELD_UTC_CENTISECONDS_STATE : captureState->nmeaSentenceNumberScratch[UTC_CENTISECONDS_CACHE_INDEX].nmea_uint8_t = ((captureState->nmeaSentenceScratch[0] - NMEA_GPS_ASCII_DIGIT_ZERO) * NMEA_GPS_DIGIT_x_10) + (nextCharacter - NMEA_GPS_ASCII_DIGIT_ZERO);
                                                       captureState->nmeaSentenceGenericState = NMEA_FIELD_UTC_EXIT_STATE;
                                                       break;
              case NMEA_FIELD_UTC_SEPERATOR_STATE    :
              case NMEA_FIELD_UTC_EXIT_STATE         :
              case NMEA_FIELD_UTC_EMPTY_STATE        :
              case NMEA_FIELD_UTC_FAILED_STATE       :
              case NMEA_FIELD_UTC_STATES             : captureState->nmeaSentenceGenericState = NMEA_FIELD_UTC_FAILED_STATE;
                                                       break;
              } /* end of INNER "switch (captureState->nmeaSentenceGenericState ..."*/
            } /* end of "if (captureState->nmeaSentenceMatchIndex == ..."          */
          }
                                                 break;

        /* *** THIS STATE DOESN'T EXIST *** */
        case NMEA_FIELD_UTC_SEPERATOR_STATE    : // This state is very short at one-characters' length
                                                 nextCharacter = *(charactersToParse + captureState->nmeaSentenceCharactersUsed);

                                                 if (nextCharacter == NMEA_FIELD_UTC_SEPERATOR)
                                                   {
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
                                                   captureState->nmeaSentence[captureState->nmeaSentenceCharacterIndex] = NMEA_FIELD_UTC_SEPERATOR;
#endif                                         
                                                   // Update the checksum
                                                   nmeaComputeChecksum(&nextCharacter,
                                                                        1,
                                                                       &captureState->nmeaSentenceComputedChecksum);

                                                   captureState->nmeaSentenceCharactersUsed = captureState->nmeaSentenceCharactersUsed + 1;
                                                   captureState->nmeaSentenceCharacterIndex = captureState->nmeaSentenceCharacterIndex + 1;
                                                   captureState->nmeaSentenceGenericState   = NMEA_FIELD_UTC_CENTISECONDS_STATE;
                                                   }
                                                 else
                                                   {
                                                   captureState->nmeaSentenceGenericState   = NMEA_FIELD_UTC_FAILED_STATE;
                                                   }

                                                 break;

        default                                : captureState->nmeaSentenceGenericState = NMEA_FIELD_UTC_FAILED_STATE;
                                                 break;
        } /* end of OUTER "switch (captureState->nmeaSentenceGenericState ..."*/

      // On termination propagate the final state to the caller
      if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_FIELD_UTC_EXIT_STATE)
        {
        if (nmeaValidateUtcTime(captureState->nmeaSentenceNumberScratch[UTC_HOURS_CACHE_INDEX].nmea_uint8_t,
                                captureState->nmeaSentenceNumberScratch[UTC_MINUTES_CACHE_INDEX].nmea_uint8_t,
                                captureState->nmeaSentenceNumberScratch[UTC_SECONDS_CACHE_INDEX].nmea_uint8_t,
                                captureState->nmeaSentenceNumberScratch[UTC_CENTISECONDS_CACHE_INDEX].nmea_uint8_t) == true)
          {
          // Update the fix group
          gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                 NMEA_GPS_GROUP_GEO_POSITION,
                                 NMEA_GPS_FIELD_GEO_POSITION_UTC_HOURS,
                                 0,
                                 (void *)&captureState->nmeaSentenceNumberScratch[UTC_HOURS_CACHE_INDEX].nmea_uint8_t);

          gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                  NMEA_GPS_GROUP_GEO_POSITION,
                                  NMEA_GPS_FIELD_GEO_POSITION_UTC_MINUTES,
                                  0,
                                  (void *)&captureState->nmeaSentenceNumberScratch[UTC_MINUTES_CACHE_INDEX].nmea_uint8_t);

          gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                  NMEA_GPS_GROUP_GEO_POSITION,
                                  NMEA_GPS_FIELD_GEO_POSITION_UTC_SECONDS,
                                  0,
                                  (void *)&captureState->nmeaSentenceNumberScratch[UTC_SECONDS_CACHE_INDEX].nmea_uint8_t);

          gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                  NMEA_GPS_GROUP_GEO_POSITION,
                                  NMEA_GPS_FIELD_GEO_POSITION_UTC_CENTISECONDS,
                                  0,
                                  (void *)&captureState->nmeaSentenceNumberScratch[UTC_CENTISECONDS_CACHE_INDEX].nmea_uint8_t);

          sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;
          }
        else
          {
          sentenceState = NMEA_SENTENCE_STATE_FAILED;
          break;
          }
        break;
        }
      else
        {
        if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_FIELD_UTC_FAILED_STATE)
          {
          sentenceState = NMEA_SENTENCE_STATE_FAILED;
          break;
          }
        }
     } /* end of "while (captureState->nmeaSentenceCharactersUsed ..."        */
  } /* end of "if (characterParse != NULL ..."                                */

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaUTCParser                                                     */

/******************************************************************************/
/* nmeaDateParser() :                                                         */
/*  --> charactersToParse : point in the sentence to expect == "date"         */
/*  --> captureState      : the sentence capture state                        */
/*  --> domainHandling    : aggregation of the latest fix updates             */
/*  <-- sentenceState     : result of parsing success or failure              */
/*                                                                            */
/* - parse and decode a sentence's date fields :                              */
/*     ddmmyy                                                                 */
/*                                                                            */
/* - sub-field values are built "on-the-fly"                                  */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaDateParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                          NMEASentenceCaptureState_t *captureState,
                                          void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t sentenceState = captureState->nmeaSentenceInnerState;
  NMEA_GPS_UCHAR       nextCharacter = 0;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    // On entering a field initialisation is detected by "matchIndex == 0"
    if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_SENTENCE_STATE_INITIALISATION)
      {
      captureState->nmeaSentenceGenericState = (NMEASentenceGenericStates_t)NMEA_FIELD_DATE_DAYS_STATE;
      }

    while (captureState->nmeaSentenceCharactersUsed < captureState->nmeaSentenceNewCharacters)
      {
      switch (captureState->nmeaSentenceGenericState)
        {
        // The date sub-field sizes are all the same length and type - 
        // so we cheat a bit to reduce the code size
        case NMEA_FIELD_DATE_DAYS_STATE        : // The first integer legal characters are digits [ 0 .. 9 ]. The leading ',' 
                                                 // (comma) in a sentence is handled by the "inner" parser
        case NMEA_FIELD_DATE_MONTHS_STATE      :
        case NMEA_FIELD_DATE_YEARS_STATE       :

        nextCharacter = *(charactersToParse + captureState->nmeaSentenceCharactersUsed);

        if (!isdigit(nextCharacter))
          {
          captureState->nmeaSentenceGenericState = NMEA_FIELD_DATE_FAILED_STATE;
          }
        else
          {
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
          captureState->nmeaSentence[captureState->nmeaSentenceCharacterIndex] = nextCharacter;
#endif   
          // Update the checksum
          nmeaComputeChecksum(&nextCharacter,
                               1,
                              &captureState->nmeaSentenceComputedChecksum);

          captureState->nmeaSentenceScratch[captureState->nmeaSentenceMatchIndex] = nextCharacter;

          captureState->nmeaSentenceMatchIndex     = captureState->nmeaSentenceMatchIndex     + 1;
          captureState->nmeaSentenceCharactersUsed = captureState->nmeaSentenceCharactersUsed + 1;
          captureState->nmeaSentenceCharacterIndex = captureState->nmeaSentenceCharacterIndex + 1;

          if ((captureState->nmeaSentenceMatchIndex % NMEA_FIELD_DATE_DAYS_LENGTH) == 0) // or months, years-length...
            { // Sub-field values are built "on-the-fly"
            captureState->nmeaSentenceMatchIndex = 0;

            switch ((NMEASentenceDateFieldStates_t)captureState->nmeaSentenceGenericState)
              {
              // Legal time ranges are validated at the end of parsing ("nmeaValidateUtcTime")...
              case NMEA_FIELD_DATE_DAYS_STATE   :              captureState->nmeaSentenceNumberScratch[UTC_DAYS_CACHE_INDEX].nmea_uint8_t   = ((captureState->nmeaSentenceScratch[0] - NMEA_GPS_ASCII_DIGIT_ZERO) * NMEA_GPS_DIGIT_x_10) + (nextCharacter - NMEA_GPS_ASCII_DIGIT_ZERO);
                                                               captureState->nmeaSentenceGenericState = NMEA_FIELD_DATE_MONTHS_STATE;
                                                               break;
              case NMEA_FIELD_DATE_MONTHS_STATE :              captureState->nmeaSentenceNumberScratch[UTC_MONTHS_CACHE_INDEX].nmea_uint8_t = ((captureState->nmeaSentenceScratch[0] - NMEA_GPS_ASCII_DIGIT_ZERO) * NMEA_GPS_DIGIT_x_10) + (nextCharacter - NMEA_GPS_ASCII_DIGIT_ZERO);
                                                               captureState->nmeaSentenceGenericState = NMEA_FIELD_DATE_YEARS_STATE;
                                                               break;
              case NMEA_FIELD_DATE_YEARS_STATE  :              captureState->nmeaSentenceNumberScratch[UTC_YEARS_CACHE_INDEX].nmea_uint8_t  = ((captureState->nmeaSentenceScratch[0] - NMEA_GPS_ASCII_DIGIT_ZERO) * NMEA_GPS_DIGIT_x_10) + (nextCharacter - NMEA_GPS_ASCII_DIGIT_ZERO);
                                                               captureState->nmeaSentenceGenericState = NMEA_FIELD_DATE_EXIT_STATE;
                                                               break;
              case NMEA_FIELD_DATE_EXIT_STATE   :
              case NMEA_FIELD_DATE_FAILED_STATE :
              case NMEA_FIELD_DATE_STATES       :
              default                           :              captureState->nmeaSentenceGenericState = NMEA_FIELD_DATE_FAILED_STATE;
                                                               break;

            } /* end of INNER "switch (captureState->nmeaSentenceGenericState ..."*/
          } /* end of "if (captureState->nmeaSentenceMatchIndex == ..."          */
        }
                                                 break;

        default                                : captureState->nmeaSentenceGenericState = NMEA_FIELD_DATE_FAILED_STATE;
                                                 break;
      } /* end of OUTER "switch (captureState->nmeaSentenceGenericState ..."*/

      // On termination propagate the final state to the caller
      if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_FIELD_DATE_EXIT_STATE)
        {
        if (nmeaValidateDate(captureState->nmeaSentenceNumberScratch[UTC_DAYS_CACHE_INDEX].nmea_uint8_t,
                             captureState->nmeaSentenceNumberScratch[UTC_MONTHS_CACHE_INDEX].nmea_uint8_t,
                             captureState->nmeaSentenceNumberScratch[UTC_YEARS_CACHE_INDEX].nmea_uint8_t) == true)
          {
          // Update the fix group
          gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                 NMEA_GPS_GROUP_GEO_POSITION,
                                 NMEA_GPS_FIELD_GEO_POSITION_DATE_DAYS,
                                 0,
                                 (void *)&captureState->nmeaSentenceNumberScratch[UTC_DAYS_CACHE_INDEX].nmea_uint8_t);

          gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                  NMEA_GPS_GROUP_GEO_POSITION,
                                  NMEA_GPS_FIELD_GEO_POSITION_DATE_MONTHS,
                                  0,
                                  (void *)&captureState->nmeaSentenceNumberScratch[UTC_MONTHS_CACHE_INDEX].nmea_uint8_t);

          gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                 NMEA_GPS_GROUP_GEO_POSITION,
                                 NMEA_GPS_FIELD_GEO_POSITION_DATE_YEARS,
                                 0,
                                 (void *)&captureState->nmeaSentenceNumberScratch[UTC_YEARS_CACHE_INDEX].nmea_uint8_t);

          sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;
          }
        else
          {
          sentenceState = NMEA_SENTENCE_STATE_FAILED;
          break;
          }
        break;
        }
      else
        {
        if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_FIELD_DATE_FAILED_STATE)
          {
          sentenceState = NMEA_SENTENCE_STATE_FAILED;
          break;
          }
        }
     } /* end of "while (captureState->nmeaSentenceCharactersUsed ..."        */
  } /* end of "if (characterParse != NULL ..."                                */

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaDateParser                                                    */

/******************************************************************************/
/* nmeaDataStatusParser() :                                                   */
/*  --> charactersToParse : point in the sentence to expect == "data valid"   */
/*  --> captureState      : the sentence capture state                        */
/*  --> domainHandling    : aggregation of the latest fix updates             */
/*  <-- sentenceState     : result of parsing success or failure              */
/*                                                                            */
/* - parse and decode a sentence's "status" field :                           */
/*     [ 'A' == data valid | 'V' == data invalid ]                            */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaDataStatusParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                NMEASentenceCaptureState_t *captureState,
                                                void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t sentenceState = NMEA_SENTENCE_STATE_FAILED;

/******************************************************************************/

  sentenceState = nmeaFlagParser(charactersToParse,
                                 captureState,
                                 domainHandling,
                                 (const NMEA_GPS_UCHAR *)&nmeaDataValid[0],
                                 NMEA_FIELD_DATA_VALIDS,
                                 NMEA_GPS_GROUP_GEO_POSITION,
                                 NMEA_GPS_FIELD_GEO_POSITION_FIX_STATUS);

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaDataStatusParser                                              */

/******************************************************************************/
/* nmeaFAAModeParser() :                                                      */
/*  --> charactersToParse : point in the sentence to expect == "FAA mode"     */
/*  --> captureState      : the sentence capture state                        */
/*  --> domainHandling    : aggregation of the latest fix updates             */
/*  <-- sentenceState     : result of parsing success or failure              */
/*                                                                            */
/* - parse and decode a sentence's "FAA mode" field :                         */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaFAAModeParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                             NMEASentenceCaptureState_t *captureState,
                                             void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t sentenceState = NMEA_SENTENCE_STATE_FAILED;

/******************************************************************************/

  sentenceState = nmeaFlagParser(charactersToParse,
                                 captureState,
                                 domainHandling,
                                 (const NMEA_GPS_UCHAR *)&nmeaFAAMode[0],
                                 NMEA_FIELD_FAA_MODES,
                                 NMEA_GPS_GROUP_GEO_POSITION,
                                 NMEA_GPS_FIELD_GEO_POSITION_FAA_MODE);

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaFAAModeParser                                                 */

/******************************************************************************/
/* nmeaOperatingModeParser() :                                                */
/*  --> charactersToParse : point in the sentence to expect == "operating     */
/*                          mode"                                             */
/*  --> captureState      : the sentence capture state                        */
/*  --> domainHandling    : aggregation of the latest fix updates             */
/*                                                                            */
/* - parse and decode a sentence's operating mode :                           */
/*        [ MANUAL | AUTOMATIC ]                                              */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaOperatingModeParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                   NMEASentenceCaptureState_t *captureState,
                                                   void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t sentenceState = NMEA_SENTENCE_STATE_FAILED;

/******************************************************************************/

  sentenceState = nmeaFlagParser(charactersToParse,
                                 captureState,
                                 domainHandling,
                                 (const NMEA_GPS_UCHAR *)&nmeaSMode[0],
                                 NMEA_FIELD_S_MODES,
                                 NMEA_GPS_GROUP_GEO_STATUS,
                                 NMEA_GPS_FIELD_GEO_STATUS_S_MODE);
                                 
/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaOperatingModeParser                                           */

/******************************************************************************/
/* nmeaFixModeParser() :                                                      */
/*  --> charactersToParse : point in the sentence to expect == "fix mode"     */
/*  --> captureState      : the sentence capture state                        */
/*  --> domainHandling    : aggregation of the latest fix updates             */
/*                                                                            */
/* - parse and decode a sentence's fix mode :                                 */
/*        [ MANUAL | AUTOMATIC ]                                              */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaFixModeParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                             NMEASentenceCaptureState_t *captureState,
                                             void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t sentenceState = NMEA_SENTENCE_STATE_FAILED;

/******************************************************************************/

  sentenceState = nmeaFlagParser(charactersToParse,
                                 captureState,
                                 domainHandling,
                                 (const NMEA_GPS_UCHAR *)&nmeaFixMode[0],
                                 NMEA_FIELD_FIX_STATUSES,
                                 NMEA_GPS_GROUP_GEO_STATUS,
                                 NMEA_GPS_FIELD_GEO_STATUS_FIX_MODE);
                                 
/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaFixModeParser                                                 */

/******************************************************************************/
/* nmeaFixQualityParser() :                                                   */
/*  --> charactersToParse : point in the sentence to expect == "fix quality"  */
/*  --> captureState      : the sentence capture state                        */
/*  --> domainHandling    : aggregation of the latest fix updates             */
/*                                                                            */
/* - parse and decode a sentence's fix quality :                              */
/*        '0' == invalid                                                      */
/*        '1' == GPS [ 2D | 3D ]                                              */
/*        '2' == DGPS                                                         */
/*        '6' == dead reckoning                                               */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaFixQualityParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                NMEASentenceCaptureState_t *captureState,
                                                void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t sentenceState = NMEA_SENTENCE_STATE_FAILED;

/******************************************************************************/

  sentenceState = nmeaFlagParser(charactersToParse,
                                 captureState,
                                 domainHandling,
                                 (const NMEA_GPS_UCHAR *)&nmeaFixQuality[0],
                                 NMEA_FIELD_FIX_QUALITIES,
                                 NMEA_GPS_GROUP_GEO_STATUS,
                                 NMEA_GPS_FIELD_GEO_STATUS_FIX_QUALITY);
                                 
/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaFixQualityParser                                              */

/******************************************************************************/
/* nmeaGGASatellitesInViewParser() :                                          */
/*                                                                            */
/* - parse the number of satellites in view in the "GGA" sentence             */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaGGASatellitesInViewParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                         NMEASentenceCaptureState_t *captureState,
                                                         void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t        sentenceState = captureState->nmeaSentenceInnerState;
  NMEASentenceGenericStates_t variableState = NMEA_VARIABLE_STATE_INITIALISATION;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_SENTENCE_STATE_INITIALISATION)
      {
      // Populate the variable parser controlling structure (this is currently a 
      // single variable - may need to revisit this if any concurrency is later 
      // needed!)
      captureState->nmeaSentenceVariableState.variableType                = NMEA_FIELD_DECIMAL_INTEGER;

      captureState->nmeaSentenceVariableState.integerPartMinimumLength    = NMEA_GGA_NUMBER_OF_SATELLITES_MINIMUM_FIELD_WIDTH;
      captureState->nmeaSentenceVariableState.integerPartMaximumLength    = NMEA_GGA_NUMBER_OF_SATELLITES_MAXIMUM_FIELD_WIDTH;

      captureState->nmeaSentenceVariableState.fractionalPartMinimumLength = 0;
      captureState->nmeaSentenceVariableState.fractionalPartMaximumLength = 0;

      captureState->nmeaSentenceVariableState.signFlag                    = NMEA_ENCODER_SIGN_FLAG_UNSIGNED;

      captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t = 0;

      // Start the variable parser state machine
      captureState->nmeaSentenceGenericState                              = NMEA_VARIABLE_STATE_INITIALISATION;
      } /* end of "if (captureState->nmeaSentenceGenericState ==  ..."        */

    // Hand-off to the variable parser state machine
    variableState = nmeaVariableNumericFieldParser(charactersToParse,
                                                   captureState);

    if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_EXIT)
      {
      // Check the variable range
      if ((captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t >= NMEA_GGA_NUMBER_OF_SATELLITES_MINIMUM) && 
          (captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t <= NMEA_GGA_NUMBER_OF_SATELLITES_MAXIMUM))
        {
        // Update the fix group
        gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                NMEA_GPS_GROUP_GEO_STATUS,
                                NMEA_GPS_FIELD_GEO_STATUS_SATELLITES_IN_VIEW,
                                0,
                                (void *)&captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t);

        sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;
        }
      else
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    else
      {
      if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_FAILED)
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    } /* end of "if (characterParse != NULL ..."                              */

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaGGASatellitesInViewParser                                     */

/******************************************************************************/
/* nmeaDgpsCorrectionAgeParser() :                                            */
/*                                                                            */
/* - parse the time in seconds since the last DGPS correction in the "GGA"    */
/*   sentence                                                                 */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaDgpsCorrectionAgeParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                       NMEASentenceCaptureState_t *captureState,
                                                       void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t        sentenceState = captureState->nmeaSentenceInnerState;
  NMEASentenceGenericStates_t variableState = NMEA_VARIABLE_STATE_INITIALISATION;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_SENTENCE_STATE_INITIALISATION)
      {
      // Populate the variable parser controlling structure (this is currently a 
      // single variable - may need to revisit this if any concurrency is later 
      // needed!)
      captureState->nmeaSentenceVariableState.variableType                = NMEA_FIELD_DECIMAL_INTEGER;
                                                                          
      captureState->nmeaSentenceVariableState.integerPartMinimumLength    = NMEA_GGA_DGPS_AGE_INTEGER_PART_MINIMUM_LENGTH;
      captureState->nmeaSentenceVariableState.integerPartMaximumLength    = NMEA_GGA_DGPS_AGE_INTEGER_PART_MAXIMUM_LENGTH;
                                                                          
      captureState->nmeaSentenceVariableState.fractionalPartMinimumLength = 0;
      captureState->nmeaSentenceVariableState.fractionalPartMaximumLength = 0;
                                                                          
      captureState->nmeaSentenceVariableState.signFlag                    = NMEA_ENCODER_SIGN_FLAG_UNSIGNED;

      captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t = 0;

      // Start the variable parser state machine
      captureState->nmeaSentenceGenericState                              = NMEA_VARIABLE_STATE_INITIALISATION;
      } /* end of "if (captureState->nmeaSentenceGenericState ==  ..."        */

    // Hand-off to the variable parser state machine
    variableState = nmeaVariableNumericFieldParser(charactersToParse,
                                                   captureState);

    if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_EXIT)
      {
      // Check the variable range
      if ((captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t >= NMEA_GGA_DGPS_AGE_MINIMUM) && 
          (captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t <= NMEA_GGA_DGPS_AGE_MAXIMUM))
        {
        // Update the fix group
        gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                NMEA_GPS_GROUP_GEO_STATUS,
                                NMEA_GPS_FIELD_GEO_STATUS_DGPS_AGE,
                                0,
                                (void *)&captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t);

        sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;
        }
      else
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    else
      {
      if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_FAILED)
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    } /* end of "if (characterParse != NULL ..."                              */

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaDgpsCorrectionAgeParser                                       */

/******************************************************************************/
/* nmeaDgpsBaseStationIdParser() :                                            */
/*                                                                            */
/* - parse the DGPS corrections' reference base station id in the "GGA"       */
/*   sentence                                                                 */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaDgpsBaseStationIdParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                       NMEASentenceCaptureState_t *captureState,
                                                       void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t        sentenceState = captureState->nmeaSentenceInnerState;
  NMEASentenceGenericStates_t variableState = NMEA_VARIABLE_STATE_INITIALISATION;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_SENTENCE_STATE_INITIALISATION)
      {
      // Populate the variable parser controlling structure (this is currently a 
      // single variable - may need to revisit this if any concurrency is later 
      // needed!)
      captureState->nmeaSentenceVariableState.variableType                = NMEA_FIELD_DECIMAL_INTEGER;
                                                                          
      captureState->nmeaSentenceVariableState.integerPartMinimumLength    = NMEA_GGA_DGPS_BASE_INTEGER_PART_MINIMUM_LENGTH;
      captureState->nmeaSentenceVariableState.integerPartMaximumLength    = NMEA_GGA_DGPS_BASE_INTEGER_PART_MAXIMUM_LENGTH;
                                                                          
      captureState->nmeaSentenceVariableState.fractionalPartMinimumLength = 0;
      captureState->nmeaSentenceVariableState.fractionalPartMaximumLength = 0;
                                                                          
      captureState->nmeaSentenceVariableState.signFlag                    = NMEA_ENCODER_SIGN_FLAG_UNSIGNED;

      captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t = 0;

      // Start the variable parser state machine
      captureState->nmeaSentenceGenericState                              = NMEA_VARIABLE_STATE_INITIALISATION;
      } /* end of "if (captureState->nmeaSentenceGenericState ==  ..."        */

    // Hand-off to the variable parser state machine
    variableState = nmeaVariableNumericFieldParser(charactersToParse,
                                                   captureState);

    if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_EXIT)
      {
      // Check the variable range
      if ((captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t >= NMEA_GGA_DGPS_BASE_MINIMUM) && 
          (captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t <= NMEA_GGA_DGPS_BASE_MAXIMUM))
        {
        // Update the fix group
        gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                NMEA_GPS_GROUP_GEO_STATUS,
                                NMEA_GPS_FIELD_GEO_STATUS_DGPS_ID,
                                0,
                                (void *)&captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t);

        sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;
        }
      else
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    else
      {
      if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_FAILED)
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    } /* end of "if (characterParse != NULL ..."                              */

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaDgpsBaseStationIdParser                                       */

/******************************************************************************/
/* nmeaAltitudeParser() :                                                     */
/*                                                                            */
/* - parse the altitude in metres in the "GGA" sentence                       */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaAltitudeParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                              NMEASentenceCaptureState_t *captureState,
                                              void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t        sentenceState = captureState->nmeaSentenceInnerState;
  NMEASentenceGenericStates_t variableState = NMEA_VARIABLE_STATE_INITIALISATION;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_SENTENCE_STATE_INITIALISATION)
      {
      // Populate the variable parser controlling structure (this is currently a 
      // single variable - may need to revisit this if any concurrency is later 
      // needed!)
      captureState->nmeaSentenceVariableState.variableType                = NMEA_FIELD_DECIMAL_FLOATING;

      captureState->nmeaSentenceVariableState.integerPartMinimumLength    = NMEA_GGA_ALTITUDE_INTEGER_PART_MINIMUM_LENGTH;
      captureState->nmeaSentenceVariableState.integerPartMaximumLength    = NMEA_GGA_ALTITUDE_INTEGER_PART_MAXIMUM_LENGTH;

      captureState->nmeaSentenceVariableState.fractionalPartMinimumLength = NMEA_GGA_ALTITUDE_FRACTIONAL_PART_MINIMUM_LENGTH;
      captureState->nmeaSentenceVariableState.fractionalPartMaximumLength = NMEA_GGA_ALTITUDE_FRACTIONAL_PART_MAXIMUM_LENGTH;
           
      captureState->nmeaSentenceVariableState.signFlag                    = NMEA_ENCODER_SIGN_FLAG_SIGNED;
        
      captureState->nmeaSentenceVariableState.variableResult.nmea_float_t = 0.0;

      // Start the variable parser state machine
      captureState->nmeaSentenceGenericState                              = NMEA_VARIABLE_STATE_INITIALISATION;
      } /* end of "if (captureState->nmeaSentenceGenericState ==  ..."        */

    // Hand-off to the variable parser state machine
    variableState = nmeaVariableNumericFieldParser(charactersToParse,
                                                   captureState);
    
    if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_EXIT)
      {
      // Check the variable range
      if ((captureState->nmeaSentenceVariableState.variableResult.nmea_float_t >= NMEA_GGA_ALTITUDE_MINIMUM) && 
          (captureState->nmeaSentenceVariableState.variableResult.nmea_float_t <= NMEA_GGA_ALTITUDE_MAXIMUM))
        {
        // Update the fix group
        gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                NMEA_GPS_GROUP_GEO_POSITION,
                                NMEA_GPS_FIELD_GEO_POSITION_ALTITUDE,
                                0,
                               (void *)&captureState->nmeaSentenceVariableState.variableResult.nmea_float_t);

        sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;
        }
      else
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    else
      {
      if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_FAILED)
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    } /* end of "if (characterParse != NULL ..."                              */

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaAltitudeParser                                                */

/******************************************************************************/
/* nmeaGeoidSeperationParser() :                                              */
/*                                                                            */
/* - parse the geoid seperation in metres in the "GGA" sentence               */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaGeoidSeperationParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                     NMEASentenceCaptureState_t *captureState,
                                                     void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t        sentenceState = captureState->nmeaSentenceInnerState;
  NMEASentenceGenericStates_t variableState = NMEA_VARIABLE_STATE_INITIALISATION;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_SENTENCE_STATE_INITIALISATION)
      {
      // Populate the variable parser controlling structure (this is currently a 
      // single variable - may need to revisit this if any concurrency is later 
      // needed!)
      captureState->nmeaSentenceVariableState.variableType                = NMEA_FIELD_DECIMAL_FLOATING;

      captureState->nmeaSentenceVariableState.integerPartMinimumLength    = NMEA_GGA_GEOID_INTEGER_PART_MINIMUM_LENGTH;
      captureState->nmeaSentenceVariableState.integerPartMaximumLength    = NMEA_GGA_GEOID_INTEGER_PART_MAXIMUM_LENGTH;

      captureState->nmeaSentenceVariableState.fractionalPartMinimumLength = NMEA_GGA_GEOID_FRACTIONAL_PART_MINIMUM_LENGTH;
      captureState->nmeaSentenceVariableState.fractionalPartMaximumLength = NMEA_GGA_GEOID_FRACTIONAL_PART_MAXIMUM_LENGTH;
           
      captureState->nmeaSentenceVariableState.signFlag                    = NMEA_ENCODER_SIGN_FLAG_SIGNED;
        
      captureState->nmeaSentenceVariableState.variableResult.nmea_float_t = 0.0;

      // Start the variable parser state machine
      captureState->nmeaSentenceGenericState                              = NMEA_VARIABLE_STATE_INITIALISATION;
      } /* end of "if (captureState->nmeaSentenceGenericState ==  ..."        */

    // Hand-off to the variable parser state machine
    variableState = nmeaVariableNumericFieldParser(charactersToParse,
                                                   captureState);
    
    if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_EXIT)
      {
      // Check the variable range
      if ((captureState->nmeaSentenceVariableState.variableResult.nmea_float_t >= NMEA_GGA_GEOID_MINIMUM) && 
          (captureState->nmeaSentenceVariableState.variableResult.nmea_float_t <= NMEA_GGA_GEOID_MAXIMUM))
        {
        // Update the fix group
        gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                NMEA_GPS_GROUP_GEO_POSITION,
                                NMEA_GPS_FIELD_GEO_POSITION_GEOID_SEPERATION,
                                0,
                               (void *)&captureState->nmeaSentenceVariableState.variableResult.nmea_float_t);

        sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;
        }
      else
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    else
      {
      if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_FAILED)
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    } /* end of "if (characterParse != NULL ..."                              */

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaGeoidSeperationParser                                         */

/******************************************************************************/
/* nmeaMetresParser() :                                                       */
/*                                                                            */
/* - parse the single fixed character for metres 'M'                          */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaMetresParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                            NMEASentenceCaptureState_t *captureState,
                                            void                       *domainHandling)
  {
/******************************************************************************/

  NMEASentenceStates_t sentenceState = NMEA_SENTENCE_STATE_FAILED;
  NMEA_GPS_UINT8       nextCharacter = 0;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_SENTENCE_STATE_INITIALISATION)
      {
      nextCharacter = *(charactersToParse + captureState->nmeaSentenceCharactersUsed);


#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
        captureState->nmeaSentence[captureState->nmeaSentenceCharacterIndex] = nextCharacter;
#endif   

      if (nextCharacter == NMEA_FIELD_METRES)
        {
        captureState->nmeaSentenceCharactersUsed = captureState->nmeaSentenceCharactersUsed + 1;
        captureState->nmeaSentenceCharacterIndex = captureState->nmeaSentenceCharacterIndex + 1;

        // Update the checksum
        nmeaComputeChecksum(&nextCharacter,
                             1,
                            &captureState->nmeaSentenceComputedChecksum);

        sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;
        }
      else
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    }

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
  } /* end of nmeaMetresParser                                                */

/******************************************************************************/
/* nmeaSatelliteIDParser() :                                                  */
/*  --> charactersToParse : point in the sentence to expect == "FAA mode"     */
/*  --> captureState      : the sentence capture state                        */
/*  --> domainHandling    : aggregation of the latest fix updates             */
/*                                                                            */
/* - parse and decode a sentence's "satellite ID" fields. These are just two  */
/*   digits long (unsigned with leading zeroes). In sentence "GSA" there are  */
/*   twelve satellite ID slots, 0 { <slot> } 12 may contain an active         */
/*   satellite ID. Accordingly this function keeps a count of the slots and   */
/*   can be used for each slot, no external counting mechanism is needed      */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaSatelliteIDParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                 NMEASentenceCaptureState_t *captureState,
                                                 void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t sentenceState = captureState->nmeaSentenceInnerState;

  NMEA_GPS_UINT8       satelliteId   = 0;
  NMEA_GPS_UCHAR       nextCharacter = 0;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_SENTENCE_STATE_INITIALISATION)
      {
      // Identify the start of a sequence of satellite IDs; this function will be called 
      // successively in one sentence e.g. "GSA"
      if (captureState->nmeaSentenceCommonFieldState == NMEA_SENTENCE_COMMON_FIELD_STATE_INITIALISATION)
        {
        captureState->nmeaSentenceNumberScratch[SATELLITE_ID_IDENTIFIER_CACHE_INDEX].nmea_uint8_t = 0;

        // Clear the set of satellite ID slots
        gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                NMEA_GPS_GROUP_GEO_STATUS,
                                NMEA_GPS_FIELD_GEO_STATUS_ACTIVE_SATELLITE_ID_NULL,
                                0,
                                (void *)&satelliteId);

        captureState->nmeaSentenceCommonFieldState = NMEA_SENTENCE_COMMON_FIELD_STATE_INDEXING;
        }

      captureState->nmeaSentenceGenericState = NMEA_FIELD_SATELLITE_ID_STATE;
      }

    // STATE : NMEA_FIELD_SATELLITE_ID_STATE:NMEA_SENTENCE_COMMON_FIELD_STATE_INDEXING
    while (captureState->nmeaSentenceCharactersUsed < captureState->nmeaSentenceNewCharacters)
      {
      nextCharacter = *(charactersToParse + captureState->nmeaSentenceCharactersUsed);
        
      if (!isdigit(nextCharacter))
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        break;
        }
      else
        {
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
        captureState->nmeaSentence[captureState->nmeaSentenceCharacterIndex] = nextCharacter;
#endif   
        // Update the checksum
        nmeaComputeChecksum(&nextCharacter,
                             1,
                            &captureState->nmeaSentenceComputedChecksum);

        // Only one character needs to be remembered but it is easier and quicker to just 
        // save both characters
        captureState->nmeaSentenceScratch[captureState->nmeaSentenceMatchIndex] = nextCharacter;

        captureState->nmeaSentenceMatchIndex     = captureState->nmeaSentenceMatchIndex     + 1;
        captureState->nmeaSentenceCharactersUsed = captureState->nmeaSentenceCharactersUsed + 1;
        captureState->nmeaSentenceCharacterIndex = captureState->nmeaSentenceCharacterIndex + 1;

        // Each satellite ID is a two-digit decimal number
        if ((captureState->nmeaSentenceMatchIndex % NMEA_FIELD_SATELLITE_ID_LENGTH) == 0)
          {
          satelliteId = ((captureState->nmeaSentenceScratch[0] - NMEA_GPS_ASCII_DIGIT_ZERO) * NMEA_GPS_DIGIT_x_10) + (nextCharacter - NMEA_GPS_ASCII_DIGIT_ZERO);

          // Validation is just a check against the maximum possible ID of active satellites
          if (satelliteId <= NMEA_ACITVE_SATELLITES_MAXIMUM_ID)
            {
            // Update the active satellite id status group
            gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                    NMEA_GPS_GROUP_GEO_STATUS,
                                    NMEA_GPS_FIELD_GEO_STATUS_ACTIVE_SATELLITE_ID_1,
                                    captureState->nmeaSentenceNumberScratch[SATELLITE_ID_IDENTIFIER_CACHE_INDEX].nmea_uint8_t,
                                    (void *)&satelliteId);

            // Increment the satellite ID sentence slot index and reset if the complete set of 
            // slots has been parsed : if there are empty slots this termination will never be called...
            if ((captureState->nmeaSentenceNumberScratch[SATELLITE_ID_IDENTIFIER_CACHE_INDEX].nmea_uint8_t + 1) == NMEA_ACTIVE_SATELLITES_MAXIMUM)
              {
              captureState->nmeaSentenceCommonFieldState = NMEA_SENTENCE_COMMON_FIELD_STATE_TERMINATION;
              }
            else
              {
              captureState->nmeaSentenceNumberScratch[SATELLITE_ID_IDENTIFIER_CACHE_INDEX].nmea_uint8_t =
                captureState->nmeaSentenceNumberScratch[SATELLITE_ID_IDENTIFIER_CACHE_INDEX].nmea_uint8_t + 1;
              }

            sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;
            }
          else
            {
            sentenceState = NMEA_SENTENCE_STATE_FAILED;
            }

          break;
          }
        }
      }
    }

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaSatelliteIDParser                                             */

/******************************************************************************/
/* nmeaSpeedOverGroundParser() :                                              */
/*                                                                            */
/* - parse the speed over the ground in knots in the "RMC" group              */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaSpeedOverGroundParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                     NMEASentenceCaptureState_t *captureState,
                                                     void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t        sentenceState = captureState->nmeaSentenceInnerState;
  NMEASentenceGenericStates_t variableState = NMEA_VARIABLE_STATE_INITIALISATION;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_SENTENCE_STATE_INITIALISATION)
      {
      // Populate the variable parser controlling structure (this is currently a 
      // single variable - may need to revisit this if any concurrency is later 
      // needed!)
      captureState->nmeaSentenceVariableState.variableType                = NMEA_FIELD_DECIMAL_FLOATING;

      captureState->nmeaSentenceVariableState.integerPartMinimumLength    = NMEA_SOG_INTEGER_PART_MINIMUM_LENGTH;
      captureState->nmeaSentenceVariableState.integerPartMaximumLength    = NMEA_SOG_INTEGER_PART_MAXIMUM_LENGTH;

      captureState->nmeaSentenceVariableState.fractionalPartMinimumLength = NMEA_SOG_FRACTIONAL_PART_MINIMUM_LENGTH;
      captureState->nmeaSentenceVariableState.fractionalPartMaximumLength = NMEA_SOG_FRACTIONAL_PART_MAXIMUM_LENGTH;
           
      captureState->nmeaSentenceVariableState.signFlag                    = NMEA_ENCODER_SIGN_FLAG_UNSIGNED;
        
      captureState->nmeaSentenceVariableState.variableResult.nmea_float_t = 0.0;

      // Start the variable parser state machine
      captureState->nmeaSentenceGenericState                              = NMEA_VARIABLE_STATE_INITIALISATION;
      } /* end of "if (captureState->nmeaSentenceGenericState ==  ..."        */

    // Hand-off to the variable parser state machine
    variableState = nmeaVariableNumericFieldParser(charactersToParse,
                                                   captureState);
    
    if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_EXIT)
      {
      // Check the variable range
      if ((captureState->nmeaSentenceVariableState.variableResult.nmea_float_t >= NMEA_SPEED_OVER_GROUND_MINIMUM) && 
          (captureState->nmeaSentenceVariableState.variableResult.nmea_float_t <= NMEA_SPEED_OVER_GROUND_MAXIMUM))
        {
        // Update the fix group
        gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                NMEA_GPS_GROUP_GEO_POSITION,
                                NMEA_GPS_FIELD_GEO_POSITION_SPEED_OVER_GROUND,
                                0,
                               (void *)&captureState->nmeaSentenceVariableState.variableResult.nmea_float_t);

        sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;
        }
      else
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    else
      {
      if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_FAILED)
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    } /* end of "if (characterParse != NULL ..."                              */

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaSpeedOverGroundParser                                         */

/******************************************************************************/
/* nmeaCourseOverGroundParser() :                                             */
/*                                                                            */
/* - parse the true course made good (0 .. 359.9 degrees) in the "RMC" group  */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaCourseOverGroundParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                      NMEASentenceCaptureState_t *captureState,
                                                      void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t        sentenceState = captureState->nmeaSentenceInnerState;
  NMEASentenceGenericStates_t variableState = NMEA_VARIABLE_STATE_INITIALISATION;

  /******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_SENTENCE_STATE_INITIALISATION)
      {
      // Populate the variable parser controlling structure (this is currently a 
      // single variable - may need to revisit this if any concurrency is later 
      // needed!)
      captureState->nmeaSentenceVariableState.variableType                = NMEA_FIELD_DECIMAL_FLOATING;

      captureState->nmeaSentenceVariableState.integerPartMinimumLength    = NMEA_COG_INTEGER_PART_MINIMUM_LENGTH;
      captureState->nmeaSentenceVariableState.integerPartMaximumLength    = NMEA_COG_INTEGER_PART_MAXIMUM_LENGTH;

      captureState->nmeaSentenceVariableState.fractionalPartMinimumLength = NMEA_COG_FRACTIONAL_PART_MINIMUM_LENGTH;
      captureState->nmeaSentenceVariableState.fractionalPartMaximumLength = NMEA_COG_FRACTIONAL_PART_MAXIMUM_LENGTH;

      captureState->nmeaSentenceVariableState.signFlag                    = NMEA_ENCODER_SIGN_FLAG_UNSIGNED;

      captureState->nmeaSentenceVariableState.variableResult.nmea_float_t = 0.0;

      // Start the variable parser state machine
      captureState->nmeaSentenceGenericState                              = NMEA_VARIABLE_STATE_INITIALISATION;
      } /* end of "if (captureState->nmeaSentenceGenericState ==  ..."        */

      // Hand-off to the variable parser state machine
    variableState = nmeaVariableNumericFieldParser(charactersToParse,
                                                   captureState);

    if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_EXIT)
      {
      // Check the variable range
      if ((captureState->nmeaSentenceVariableState.variableResult.nmea_float_t >= NMEA_COURSE_OVER_GROUND_MINIMUM) && 
          (captureState->nmeaSentenceVariableState.variableResult.nmea_float_t <= NMEA_COURSE_OVER_GROUND_MAXIMUM))
        {
        // Update the fix group
        gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                NMEA_GPS_GROUP_GEO_POSITION,
                                NMEA_GPS_FIELD_GEO_POSITION_COURSE_OVER_GROUND,
                                0,
                                (void *)&captureState->nmeaSentenceVariableState.variableResult.nmea_float_t);

        sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;
        }
      else
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    else
      {
      if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_FAILED)
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    } /* end of "if (characterParse != NULL ..."                              */

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaCourseOverGroundParser                                        */

/******************************************************************************/
/* nmeaMagneticVariationParser() :                                            */
/*                                                                            */
/* - parse the magnetic variation (0 .. 179.9 degrees) in the "RMC" group     */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaMagneticVariationParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                       NMEASentenceCaptureState_t *captureState,
                                                       void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t        sentenceState = captureState->nmeaSentenceInnerState;
  NMEASentenceGenericStates_t variableState = NMEA_VARIABLE_STATE_INITIALISATION;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_SENTENCE_STATE_INITIALISATION)
      {
      // Populate the variable parser controlling structure (this is currently a 
      // single variable - may need to revisit this if any concurrency is later 
      // needed!)
      captureState->nmeaSentenceVariableState.variableType                = NMEA_FIELD_DECIMAL_FLOATING;

      captureState->nmeaSentenceVariableState.integerPartMinimumLength    = NMEA_MAV_INTEGER_PART_MINIMUM_LENGTH;
      captureState->nmeaSentenceVariableState.integerPartMaximumLength    = NMEA_MAV_INTEGER_PART_MAXIMUM_LENGTH;

      captureState->nmeaSentenceVariableState.fractionalPartMinimumLength = NMEA_MAV_FRACTIONAL_PART_MINIMUM_LENGTH;
      captureState->nmeaSentenceVariableState.fractionalPartMaximumLength = NMEA_MAV_FRACTIONAL_PART_MAXIMUM_LENGTH;

      captureState->nmeaSentenceVariableState.signFlag                    = NMEA_ENCODER_SIGN_FLAG_UNSIGNED;

      captureState->nmeaSentenceVariableState.variableResult.nmea_float_t = 0.0;

      // Start the variable parser state machine
      captureState->nmeaSentenceGenericState                              = NMEA_VARIABLE_STATE_INITIALISATION;
      } /* end of "if (captureState->nmeaSentenceGenericState ==  ..."        */

      // Hand-off to the variable parser state machine
    variableState = nmeaVariableNumericFieldParser(charactersToParse,
                                                   captureState);

    if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_EXIT)
      {
      // Check the variable range
      if ((captureState->nmeaSentenceVariableState.variableResult.nmea_float_t >= NMEA_MAGNETIC_VARIATION_MINIMUM) && 
          (captureState->nmeaSentenceVariableState.variableResult.nmea_float_t <= NMEA_MAGNETIC_VARIATION_MAXIMUM))
        {
        // Update the fix group
        gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                NMEA_GPS_GROUP_GEO_POSITION,
                                NMEA_GPS_FIELD_GEO_POSITION_MAGNETIC_VARIATION,
                                0,
                                (void *)&captureState->nmeaSentenceVariableState.variableResult.nmea_float_t);

        sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;
        }
      else
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    else
      {
      if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_FAILED)
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    } /* end of "if (characterParse != NULL ..."                              */

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaMagneticVariationParser                                       */

/******************************************************************************/
/* nmeaPositionpDopParser() :                                                 */
/*                                                                            */
/* - parse the position dilution of precision in the "GSA" group              */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaPositionpDopParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                  NMEASentenceCaptureState_t *captureState,
                                                  void                       *domainHandling)
  {
  /******************************************************************************/

  NMEASentenceStates_t        sentenceState = captureState->nmeaSentenceInnerState;
  NMEASentenceGenericStates_t variableState = NMEA_VARIABLE_STATE_INITIALISATION;

  /******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_SENTENCE_STATE_INITIALISATION)
      {
      // Populate the variable parser controlling structure (this is currently a 
      // single variable - may need to revisit this if any concurrency is later 
      // needed!)
      captureState->nmeaSentenceVariableState.variableType = NMEA_FIELD_DECIMAL_FLOATING;

      captureState->nmeaSentenceVariableState.integerPartMinimumLength = NMEA_DOP_INTEGER_PART_MINIMUM_LENGTH;
      captureState->nmeaSentenceVariableState.integerPartMaximumLength = NMEA_DOP_INTEGER_PART_MAXIMUM_LENGTH;

      captureState->nmeaSentenceVariableState.fractionalPartMinimumLength = NMEA_DOP_FRACTIONAL_PART_MINIMUM_LENGTH;
      captureState->nmeaSentenceVariableState.fractionalPartMaximumLength = NMEA_DOP_FRACTIONAL_PART_MAXIMUM_LENGTH;

      captureState->nmeaSentenceVariableState.signFlag = NMEA_ENCODER_SIGN_FLAG_UNSIGNED;

      captureState->nmeaSentenceVariableState.variableResult.nmea_float_t = 0.0;

      // Start the variable parser state machine
      captureState->nmeaSentenceGenericState = NMEA_VARIABLE_STATE_INITIALISATION;
      } /* end of "if (captureState->nmeaSentenceGenericState ==  ..."        */

      // Hand-off to the variable parser state machine
    variableState = nmeaVariableNumericFieldParser(charactersToParse,
                                                   captureState);

    if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_EXIT)
      {
      // Check the variable range
      if ((captureState->nmeaSentenceVariableState.variableResult.nmea_float_t >= NMEA_DOP_MINIMUM) &&
          (captureState->nmeaSentenceVariableState.variableResult.nmea_float_t <= NMEA_DOP_MAXIMUM))
        {
        // Update the fix group
        gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                NMEA_GPS_GROUP_GEO_STATUS,
                                NMEA_GPS_FIELD_GEO_STATUS_PDOP,
                                0,
                                (void *)&captureState->nmeaSentenceVariableState.variableResult.nmea_float_t);

        sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;
        }
      else
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    else
      {
      if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_FAILED)
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    } /* end of "if (characterParse != NULL ..."                              */

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
  } /* end of nmeaPositionpDopParser                                          */

/******************************************************************************/
/* nmeaPositionhdopParser() :                                                 */
/*                                                                            */
/* - parse the horizontal dilution of precision in the "GSA" group            */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaPositionhDopParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                  NMEASentenceCaptureState_t *captureState,
                                                  void                       *domainHandling)
  {
  /******************************************************************************/

  NMEASentenceStates_t        sentenceState = captureState->nmeaSentenceInnerState;
  NMEASentenceGenericStates_t variableState = NMEA_VARIABLE_STATE_INITIALISATION;

  /******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_SENTENCE_STATE_INITIALISATION)
      {
      // Populate the variable parser controlling structure (this is currently a 
      // single variable - may need to revisit this if any concurrency is later 
      // needed!)
      captureState->nmeaSentenceVariableState.variableType = NMEA_FIELD_DECIMAL_FLOATING;

      captureState->nmeaSentenceVariableState.integerPartMinimumLength = NMEA_DOP_INTEGER_PART_MINIMUM_LENGTH;
      captureState->nmeaSentenceVariableState.integerPartMaximumLength = NMEA_DOP_INTEGER_PART_MAXIMUM_LENGTH;

      captureState->nmeaSentenceVariableState.fractionalPartMinimumLength = NMEA_DOP_FRACTIONAL_PART_MINIMUM_LENGTH;
      captureState->nmeaSentenceVariableState.fractionalPartMaximumLength = NMEA_DOP_FRACTIONAL_PART_MAXIMUM_LENGTH;

      captureState->nmeaSentenceVariableState.signFlag = NMEA_ENCODER_SIGN_FLAG_UNSIGNED;

      captureState->nmeaSentenceVariableState.variableResult.nmea_float_t = 0.0;

      // Start the variable parser state machine
      captureState->nmeaSentenceGenericState = NMEA_VARIABLE_STATE_INITIALISATION;
      } /* end of "if (captureState->nmeaSentenceGenericState ==  ..."        */

      // Hand-off to the variable parser state machine
    variableState = nmeaVariableNumericFieldParser(charactersToParse,
                                                   captureState);

    if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_EXIT)
      {
      // Check the variable range
      if ((captureState->nmeaSentenceVariableState.variableResult.nmea_float_t >= NMEA_DOP_MINIMUM) &&
        (captureState->nmeaSentenceVariableState.variableResult.nmea_float_t <= NMEA_DOP_MAXIMUM))
        {
        // Update the fix group
        gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                NMEA_GPS_GROUP_GEO_STATUS,
                                NMEA_GPS_FIELD_GEO_STATUS_HDOP,
                                0,
                                (void *)&captureState->nmeaSentenceVariableState.variableResult.nmea_float_t);

        sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;
        }
      else
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    else
      {
      if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_FAILED)
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    } /* end of "if (characterParse != NULL ..."                              */

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
  } /* end of nmeaPositionhDopParser                                          */

/******************************************************************************/
/* nmeaPositionvDopParser() :                                                 */
/*                                                                            */
/* - parse the vertical dilution of precision in the "GSA" group              */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaPositionvDopParser(const NMEA_GPS_UCHAR *charactersToParse,
                                                  NMEASentenceCaptureState_t *captureState,
                                                  void *domainHandling)
  {
  /******************************************************************************/

  NMEASentenceStates_t        sentenceState = captureState->nmeaSentenceInnerState;
  NMEASentenceGenericStates_t variableState = NMEA_VARIABLE_STATE_INITIALISATION;

  /******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_SENTENCE_STATE_INITIALISATION)
      {
      // Populate the variable parser controlling structure (this is currently a 
      // single variable - may need to revisit this if any concurrency is later 
      // needed!)
      captureState->nmeaSentenceVariableState.variableType = NMEA_FIELD_DECIMAL_FLOATING;

      captureState->nmeaSentenceVariableState.integerPartMinimumLength = NMEA_DOP_INTEGER_PART_MINIMUM_LENGTH;
      captureState->nmeaSentenceVariableState.integerPartMaximumLength = NMEA_DOP_INTEGER_PART_MAXIMUM_LENGTH;

      captureState->nmeaSentenceVariableState.fractionalPartMinimumLength = NMEA_DOP_FRACTIONAL_PART_MINIMUM_LENGTH;
      captureState->nmeaSentenceVariableState.fractionalPartMaximumLength = NMEA_DOP_FRACTIONAL_PART_MAXIMUM_LENGTH;

      captureState->nmeaSentenceVariableState.signFlag = NMEA_ENCODER_SIGN_FLAG_UNSIGNED;

      captureState->nmeaSentenceVariableState.variableResult.nmea_float_t = 0.0;

      // Start the variable parser state machine
      captureState->nmeaSentenceGenericState = NMEA_VARIABLE_STATE_INITIALISATION;
      } /* end of "if (captureState->nmeaSentenceGenericState ==  ..."        */

      // Hand-off to the variable parser state machine
    variableState = nmeaVariableNumericFieldParser(charactersToParse,
                                                   captureState);

    if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_EXIT)
      {
      // Check the variable range
      if ((captureState->nmeaSentenceVariableState.variableResult.nmea_float_t >= NMEA_DOP_MINIMUM) &&
          (captureState->nmeaSentenceVariableState.variableResult.nmea_float_t <= NMEA_DOP_MAXIMUM))
        {
        // Update the fix group
        gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                NMEA_GPS_GROUP_GEO_STATUS,
                                NMEA_GPS_FIELD_GEO_STATUS_VDOP,
                                0,
                                (void *)&captureState->nmeaSentenceVariableState.variableResult.nmea_float_t);

        sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;
        }
      else
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    else
      {
      if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_FAILED)
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    } /* end of "if (characterParse != NULL ..."                              */

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
  } /* end of nmeaPositionvDopParser                                          */

/******************************************************************************/
/* Message "GSV" adds extra complication as the number of messages can be     */
/* greater than one. Also the number of "satellites-in-view" fields in non-   */
/* final messages is always four but in the final message can be one to four. */
/* This implies the final message has (i) to recognise itself (ii) change the */
/* sentence parser-managed variable "sentenceState->nmeaSentenceFields" set   */
/* usually by the function "nmeaParseSentence()" to only parse the remaining  */
/* "satelite-in-view" fields"                                                 */
/******************************************************************************/
/* nmeaGSVGroupSizeParser() :                                                 */
/*                                                                            */
/* - parse and decode the total number of sentences in a 'GSV' group. This is */
/*  a single character [ 0 ... 9 ]                                            */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaGSVGroupSizeParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                  NMEASentenceCaptureState_t *captureState,
                                                  void                       *domainHandling)
  {
/******************************************************************************/

  NMEASentenceStates_t sentenceState = NMEA_SENTENCE_STATE_FAILED;
  NMEA_GPS_UINT8       nextCharacter = 0;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_SENTENCE_STATE_INITIALISATION)
      {
      nextCharacter = *(charactersToParse + captureState->nmeaSentenceCharactersUsed);

      if (!isdigit(nextCharacter) || (nextCharacter == NMEA_GPS_ASCII_DIGIT_ZERO))
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      else
        {
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
        captureState->nmeaSentence[captureState->nmeaSentenceCharacterIndex] = nextCharacter;
#endif   

        /******************************************************************************/
        // For the first sentence in the group this sets the number of sentences to   */
        /* expect. For subsequent sentences this MUST be equal to the cached value.   */
        /* Also the current message index is set to '1'                               */
        /******************************************************************************/

        if (captureState->multipleSentenceFlag == false)
          { // Processing the first message
          captureState->nmeaSentenceNumberScratch[NMEA_GSV_NUMBER_OF_SENTENCES_CACHE_INDEX].nmea_uint8_t = (nextCharacter - NMEA_GPS_ASCII_DIGIT_ZERO);
          captureState->nmeaSentenceNumberScratch[NMEA_GSV_CURRENT_NUMBER_CACHE_INDEX].nmea_uint8_t      = NMEA_GSV_MINIMUM_NUMBER_OF_MESSAGES;
          
          sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;
          }
        else
          { // Processing subsequent messages
          if ((nextCharacter - NMEA_GPS_ASCII_DIGIT_ZERO) == captureState->nmeaSentenceNumberScratch[NMEA_GSV_NUMBER_OF_SENTENCES_CACHE_INDEX].nmea_uint8_t)
            {
            sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;
            }
          else
            {
            sentenceState = NMEA_SENTENCE_STATE_FAILED;
            }
          }

        if (sentenceState == NMEA_SENTENCE_STATE_EXIT_FIELD)
          {
          captureState->nmeaSentenceCharactersUsed = captureState->nmeaSentenceCharactersUsed + 1;
          captureState->nmeaSentenceCharacterIndex = captureState->nmeaSentenceCharacterIndex + 1;

          // Update the checksum
          nmeaComputeChecksum(&nextCharacter,
                               1,
                              &captureState->nmeaSentenceComputedChecksum);
          }
        }
      }
    }

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
  } /* end of nmeaGSVGroupSizeParser                                          */

/******************************************************************************/
/* nmeaGSVSentenceNumberParser() :                                            */
/*                                                                            */
/* - parse the number of this sentence in the current 'GSV' group. This is    */
/*  a single character  [ 0 ... 9 ]. For the first of multiple sentence this  */
/*  is stored and the same field in subsequent sentences must match           */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaGSVSentenceNumberParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                       NMEASentenceCaptureState_t *captureState,
                                                       void                       *domainHandling)
  {
/******************************************************************************/

  NMEASentenceStates_t sentenceState = NMEA_SENTENCE_STATE_FAILED;
  NMEA_GPS_UINT8       nextCharacter = 0;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_SENTENCE_STATE_INITIALISATION)
      {
      nextCharacter = *(charactersToParse + captureState->nmeaSentenceCharactersUsed);

      if (!isdigit(nextCharacter) || (nextCharacter == NMEA_GPS_ASCII_DIGIT_ZERO))
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      else
        {
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
        captureState->nmeaSentence[captureState->nmeaSentenceCharacterIndex] = nextCharacter;
#endif
         
        // Check the sequence hasn't overrun
        if ((nextCharacter - NMEA_GPS_ASCII_DIGIT_ZERO) <= captureState->nmeaSentenceNumberScratch[NMEA_GSV_NUMBER_OF_SENTENCES_CACHE_INDEX].nmea_uint8_t)
          {
          // Check the sequence is monotonic
          if ((nextCharacter - NMEA_GPS_ASCII_DIGIT_ZERO) == captureState->nmeaSentenceNumberScratch[NMEA_GSV_CURRENT_NUMBER_CACHE_INDEX].nmea_uint8_t)
            {
            captureState->nmeaSentenceCharactersUsed = captureState->nmeaSentenceCharactersUsed + 1;
            captureState->nmeaSentenceCharacterIndex = captureState->nmeaSentenceCharacterIndex + 1;

            // Update the checksum
            nmeaComputeChecksum(&nextCharacter,
                                 1,
                                &captureState->nmeaSentenceComputedChecksum);

            sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;
            }
          else
            {
            sentenceState = NMEA_SENTENCE_STATE_FAILED;
            }
          }
        else
          {
          sentenceState = NMEA_SENTENCE_STATE_FAILED;
          }
        }
      }
    }

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
  } /* end of nmeaGSVSentenceNumberParser                                     */

/******************************************************************************/
/* nmeaGSVSatellitesInViewParser() :                                            */
/*                                                                            */
/* - parse the total number of satellites in the current 'GSV' group :        */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaGSVSatellitesInViewParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                         NMEASentenceCaptureState_t *captureState,
                                                         void                       *domainHandling)
  {
/******************************************************************************/
  
  NMEASentenceStates_t        sentenceState = captureState->nmeaSentenceInnerState;
  NMEASentenceGenericStates_t variableState = NMEA_VARIABLE_STATE_INITIALISATION;
  NMEA_GPS_UINT8              zeroCharacter = 0;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_SENTENCE_STATE_INITIALISATION)
      {
      // Populate the variable parser controlling structure (this is currently a 
      // single variable - may need to revisit this if any concurrency is later 
      // needed!)
      captureState->nmeaSentenceVariableState.variableType                = NMEA_FIELD_DECIMAL_INTEGER;

      captureState->nmeaSentenceVariableState.integerPartMinimumLength    = NMEA_GSV_SATELLITES_IN_VIEW_MINIMUM_LENGTH;
      captureState->nmeaSentenceVariableState.integerPartMaximumLength    = NMEA_GSV_SATELLITES_IN_VIEW_MAXIMUM_LENGTH;

      captureState->nmeaSentenceVariableState.fractionalPartMinimumLength = 0;
      captureState->nmeaSentenceVariableState.fractionalPartMaximumLength = 0;

      captureState->nmeaSentenceVariableState.signFlag                    = NMEA_ENCODER_SIGN_FLAG_UNSIGNED;

      captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t = 0;

      // Start the variable parser state machine
      captureState->nmeaSentenceGenericState = NMEA_VARIABLE_STATE_INITIALISATION;

      } /* end of "if (captureState->nmeaSentenceGenericState ==  ..."        */

      // Hand-off to the variable parser state machine
    variableState = nmeaVariableNumericFieldParser(charactersToParse,
                                                   captureState);

    if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_EXIT)
      {
      // Check the variable range
      if ((captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t >= NMEA_GSV_SATELLITES_IN_VIEW_MINIMUM) &&
          (captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t <= NMEA_GSV_SATELLITES_IN_VIEW_MAXIMUM))
        {
        // Start the "repetitive states" counter. In all cases the following fields 
        // are up to four blocks of four variables (id, elevation, azimuth and C / N0)
        // per sentence. The counter is adjusted by -1 to account for ^-this-^ field 
        // and then (continues to) count(s) up monotonically
        captureState->nmeaSentenceStateCounter = captureState->nmeaSentenceNumberScratch[NMEA_GSV_CURRENT_NUMBER_CACHE_INDEX].nmea_uint8_t - 1; // messages are indexed from 1 but
                                                                                                                                                // the counter indexes from 0
        captureState->nmeaSentenceStateCounter = captureState->nmeaSentenceStateCounter * ((NMEA_GPS_INT16)(NMEA_GSV_MAXIMUM_SATELLITES_PER_MESSAGE * NMEA_GSV_FIELDS_PER_SATELLITE));
          
        captureState->nmeaSentenceStateCounter = captureState->nmeaSentenceStateCounter + NMEA_GSV_SATELLITE_BLOCK_COUNTER_START;

        // If this is the first sentence save the number of satellites in view and signal 
        // the sentence sequence has started
        if (captureState->multipleSentenceFlag == false)
          {

          // Zero the set of satellite statistics slots
          gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                  NMEA_GPS_GROUP_GEO_STATUS,
                                  NMEA_GPS_FIELD_GEO_STATUS_SATELLITES_IN_VIEW_NULL,
                                  0,
                                  (void *)&zeroCharacter);

          // Zero the satellite statistics block counter
          /* captureState->nmeaSentenceNumberScratch[NMEA_GSV_SATELLITES_STATISTICS_CACHE_INDEX].nmea_uint8_t = 0; */

          // Check if the first sentence is also the last sentence
          if (captureState->nmeaSentenceNumberScratch[NMEA_GSV_CURRENT_NUMBER_CACHE_INDEX].nmea_uint8_t == 
                                      captureState->nmeaSentenceNumberScratch[NMEA_GSV_NUMBER_OF_SENTENCES_CACHE_INDEX].nmea_uint8_t)
            {
            /******************************************************************************/
            /* The final sentence may contain 1 { <satellite fields> } 4 where <satellite */
            /* fields> is four (<satellite id> + <elevation> + <azimuth> + < C / N0 >)    */
            /* content parsers.                                                           */
            /* The sentence parsers are built from a fixed number of content parsers. To  */
            /* cope with a varying number of content parsers it is necessary to :         */
            /*   (i) calculate the outstanding number of fields in this final message     */
            /*  (ii) modify the sentence field counter "captureState->nmeaSentenceFields" */
            /*       which is used at an outer state machine level to index through the   */
            /*       content parsers. This is decremented after the comma/asterisk        */
            /*       delimiter of each field has been parsed. So the final message number */
            /*       of fields is :                                                       */
            /*         1 { <satellites in view content parser> } 1 +                      */
            /*         1 {        <satellite fields>           } 4                        */
            /******************************************************************************/

            NMEA_GPS_UINT8 finalSentenceFields = 0;

            // Calculate the remaining number of satellites in view in the final message...
            finalSentenceFields                    = captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t % NMEA_GSV_MAXIMUM_SATELLITES_PER_MESSAGE;

            // ( x * 4 ) % 4 == 0
            if (finalSentenceFields == 0)
              {
              finalSentenceFields                  = NMEA_GSV_MAXIMUM_SATELLITES_PER_MESSAGE;
              }

            finalSentenceFields                    = finalSentenceFields * NMEA_GSV_FIELDS_PER_SATELLITE;

            // Modify the field counter to account for this field
            captureState->nmeaSentenceFields       = finalSentenceFields + 1;
            }
          else
            {
            // Increment the message count
            captureState->nmeaSentenceNumberScratch[NMEA_GSV_CURRENT_NUMBER_CACHE_INDEX].nmea_uint8_t = 
                      captureState->nmeaSentenceNumberScratch[NMEA_GSV_CURRENT_NUMBER_CACHE_INDEX].nmea_uint8_t + 1;

            // Cache the number of satellites in view and set the "multiple sentences" state
            captureState->nmeaSentenceNumberScratch[NMEA_GSV_SATELLITES_IN_VIEW_CACHE_INDEX].nmea_int32_t = captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t;
            captureState->multipleSentenceFlag                                                            = true;
            }
        
          sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;
          }
        else
          {
          // In subsequent sentences this field must match the first sentence field
          if (captureState->nmeaSentenceNumberScratch[NMEA_GSV_SATELLITES_IN_VIEW_CACHE_INDEX].nmea_int32_t == captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t)
            {
            // Check if this is also the last sentence
            if (captureState->nmeaSentenceNumberScratch[NMEA_GSV_CURRENT_NUMBER_CACHE_INDEX].nmea_uint8_t == 
                                      captureState->nmeaSentenceNumberScratch[NMEA_GSV_NUMBER_OF_SENTENCES_CACHE_INDEX].nmea_uint8_t)
              { 
              NMEA_GPS_UINT8 finalSentenceFields = 0;
            
              /******************************************************************************/
              /* Calculate the remaining number of satellites in view in the final message. */
              /* This cannot be zero so if the final number of satellites in view is        */
              /* divisible by four and the result is one : four satellites remain. If the   */
              /* result is zero then either one, two or three satellites remain (modulus    */
              /* four)                                                                      */
              /******************************************************************************/

              finalSentenceFields                = captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t % NMEA_GSV_MAXIMUM_SATELLITES_PER_MESSAGE;
            
              // ( x * 4 ) % 4 == 0
              if (finalSentenceFields == 0)
                {
                finalSentenceFields              = NMEA_GSV_MAXIMUM_SATELLITES_PER_MESSAGE;
                }
            
              finalSentenceFields                = finalSentenceFields * NMEA_GSV_FIELDS_PER_SATELLITE;
            
              // Modify the field counter
              captureState->nmeaSentenceFields   = finalSentenceFields + 1;

              // End the "multiple sentence" state
              captureState->multipleSentenceFlag = false;
              }
            else
              {
              // Increment the message counter to account for this field
              captureState->nmeaSentenceNumberScratch[NMEA_GSV_CURRENT_NUMBER_CACHE_INDEX].nmea_uint8_t = 
                        captureState->nmeaSentenceNumberScratch[NMEA_GSV_CURRENT_NUMBER_CACHE_INDEX].nmea_uint8_t + 1;
              }
            
            sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;
            }
          else
            {
            sentenceState = NMEA_SENTENCE_STATE_FAILED;
            }
          }
        }
      else
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    else
      {
      if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_FAILED)
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    } /* end of "if (characterParse != NULL ..."                              */

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
  } /* end of nmeaGSVSatellitesInViewParser                                   */

/******************************************************************************/
/* nmeaGSVSatelliteStatisticsParser() :                                       */
/*  - this parser takes advantage of the commonality in the "GSV" satellite   */
/*    statistics blocks of 1 { <satellite id> +                               */
/*                             <elevation>    +                               */
/*                             <azimuth>      +                               */
/*                             < C / N0 >       } n                           */
/*    n = [ 1 .. 16 ]                                                         */
/*                                                                            */
/*    to provide a common "driver" for those block elements                   */
/******************************************************************************/

NMEASentenceStates_t nmeaGSVSatelliteStatisticsParser(const NMEA_GPS_INT16                 satelliteStatisticsSelector,
                                                      // const nmeaGSVSatelliteStatistics_t   satelliteStatisticsSelector,
                                                      const NMEA_GPS_UCHAR                *charactersToParse,
                                                            NMEASentenceCaptureState_t    *captureState,
                                                            void                          *domainHandling)
  {
/******************************************************************************/

  NMEASentenceStates_t         sentenceState   = captureState->nmeaSentenceInnerState;
  NMEASentenceGenericStates_t  variableState   = NMEA_VARIABLE_STATE_INITIALISATION;
                               
  NMEA_GPS_INT32               variableMinimum = 0,
                               variableMaximum = 0;
  NMEAGpsField_t               variableType    = NMEA_GPS_FIELD_GEO_STATUS_STATISTICS_ID;
                               
  NMEA_GPS_UINT8               satelliteBlock  = 0;
  nmeaGSVSatelliteStatistics_t satelliteState  = NMEA_GSV_STATISTIC_SATELLITE_ID;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    satelliteBlock = (NMEA_GPS_UINT8)(satelliteStatisticsSelector / NMEA_GSV_STATISTIC_SATELLITE_FIELDS);
    satelliteState = (nmeaGSVSatelliteStatistics_t)(satelliteStatisticsSelector % NMEA_GSV_STATISTIC_SATELLITE_FIELDS);

    if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_SENTENCE_STATE_INITIALISATION)
      {
      // Populate the variable parser controlling structure (this is currently a 
      // single variable - may need to revisit this if any concurrency is later 
      // needed!)
      captureState->nmeaSentenceVariableState.variableType = NMEA_FIELD_DECIMAL_INTEGER;

      switch(satelliteState /* satelliteStatisticsSelector */)
        {
        case NMEA_GSV_STATISTIC_SATELLITE_ID        : captureState->nmeaSentenceVariableState.integerPartMinimumLength = NMEA_GSV_SATELLITE_ID_MINIMUM_LENGTH;
                                                      captureState->nmeaSentenceVariableState.integerPartMaximumLength = NMEA_GSV_SATELLITE_ID_MAXIMUM_LENGTH;
                                                      captureState->nmeaSentenceVariableState.signFlag                 = NMEA_ENCODER_SIGN_FLAG_UNSIGNED;
                                                      break;
        case NMEA_GSV_STATISTIC_SATELLITE_ELEVATION : captureState->nmeaSentenceVariableState.integerPartMinimumLength = NMEA_GSV_SATELLITE_ELEVATION_MINIMUM_LENGTH;
                                                      captureState->nmeaSentenceVariableState.integerPartMaximumLength = NMEA_GSV_SATELLITE_ELEVATION_MAXIMUM_LENGTH;
                                                      captureState->nmeaSentenceVariableState.signFlag                 = NMEA_ENCODER_SIGN_FLAG_SIGNED;
                                                      break;
        case NMEA_GSV_STATISTIC_SATELLITE_AZIMUTH   : captureState->nmeaSentenceVariableState.integerPartMinimumLength = NMEA_GSV_SATELLITE_AZIMUTH_MINIMUM_LENGTH;
                                                      captureState->nmeaSentenceVariableState.integerPartMaximumLength = NMEA_GSV_SATELLITE_AZIMUTH_MAXIMUM_LENGTH;
                                                      captureState->nmeaSentenceVariableState.signFlag                 = NMEA_ENCODER_SIGN_FLAG_UNSIGNED;
                                                      break;
        default                                     :
        case NMEA_GSV_STATISTIC_SATELLITE_C_N0      : captureState->nmeaSentenceVariableState.integerPartMinimumLength = NMEA_GSV_SATELLITE_C_N0_MINIMUM_LENGTH;
                                                      captureState->nmeaSentenceVariableState.integerPartMaximumLength = NMEA_GSV_SATELLITE_C_N0_MAXIMUM_LENGTH;
                                                      captureState->nmeaSentenceVariableState.signFlag                 = NMEA_ENCODER_SIGN_FLAG_UNSIGNED;
                                                      break;
        }

      captureState->nmeaSentenceVariableState.fractionalPartMinimumLength = 0;
      captureState->nmeaSentenceVariableState.fractionalPartMaximumLength = 0;

      captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t = 0;

      // Start the variable parser state machine
      captureState->nmeaSentenceGenericState = NMEA_VARIABLE_STATE_INITIALISATION;
      } /* end of "if (captureState->nmeaSentenceGenericState ==  ..."        */

    // Hand-off to the variable parser state machine
    variableState = nmeaVariableNumericFieldParser(charactersToParse,
                                                   captureState);

    if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_EXIT)
      {
      switch(satelliteState /* satelliteStatisticsSelector */)
        {
        case NMEA_GSV_STATISTIC_SATELLITE_ID        : variableMinimum = NMEA_GSV_SATELLITE_ID_MINIMUM;
                                                      variableMaximum = NMEA_GSV_SATELLITE_ID_MAXIMUM;
                                                      variableType    = NMEA_GPS_FIELD_GEO_STATUS_STATISTICS_ID;
                                                      break;
        case NMEA_GSV_STATISTIC_SATELLITE_ELEVATION : variableMinimum = NMEA_GSV_ELEVATION_MINIMUM;
                                                      variableMaximum = NMEA_GSV_ELEVATION_MAXIMUM;
                                                      variableType    = NMEA_GPS_FIELD_GEO_STATUS_STATISTICS_ELEVATION;
                                                      break;
        case NMEA_GSV_STATISTIC_SATELLITE_AZIMUTH   : variableMinimum = NMEA_GSV_AZIMUTH_MINIMUM;
                                                      variableMaximum = NMEA_GSV_AZIMUTH_MAXIMUM;
                                                      variableType    = NMEA_GPS_FIELD_GEO_STATUS_STATISTICS_AZIMUTH;
                                                      break;
        case NMEA_GSV_STATISTIC_SATELLITE_C_N0      : variableMinimum = NMEA_GSV_C_N0_MINIMUM;
                                                      variableMaximum = NMEA_GSV_C_N0_MAXIMUM;
                                                      variableType    = NMEA_GPS_FIELD_GEO_STATUS_STATISTICS_C_N0;
                                                      break;
        default                                     :
                                                      break;
        }

      // Check the variable range
      if ((captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t >= variableMinimum) &&
          (captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t <= variableMaximum))
        {
        /******************************************************************************/
        // The number of satellite statistics blocks found is incremented at the end  */
        /* of the block of :                                                          */
        /*    <satellite id> + <elevation> + <azimuth> + < C / N0 >                   */
        /******************************************************************************/

        // Write the block element to it's respective cache
        gpsUpdateFixGroupField((NMEAGpsUpdate_t *)domainHandling,
                                NMEA_GPS_GROUP_GEO_STATUS,
                                variableType,
                                satelliteBlock, // captureState->nmeaSentenceNumberScratch[NMEA_GSV_SATELLITES_STATISTICS_CACHE_INDEX].nmea_uint8_t,
                                (void *)&(captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t));

        // C / N0 is the final element of the statistics block so the block counter is incremented
        /*if (satelliteStatisticsSelector == NMEA_GSV_STATISTIC_SATELLITE_C_N0)
          {
          captureState->nmeaSentenceNumberScratch[NMEA_GSV_SATELLITES_STATISTICS_CACHE_INDEX].nmea_uint8_t = 
                 captureState->nmeaSentenceNumberScratch[NMEA_GSV_SATELLITES_STATISTICS_CACHE_INDEX].nmea_uint8_t + 1;
          } */

        sentenceState = NMEA_SENTENCE_STATE_EXIT_FIELD;
        }
      else
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    else
      {
      if (variableState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_FAILED)
        {
        sentenceState = NMEA_SENTENCE_STATE_FAILED;
        }
      }
    }

/******************************************************************************/

  return(sentenceState);


/******************************************************************************/
  } /* end of nmeaGSVSatelliteStatisticsParser                                */

#if (0)
/******************************************************************************/
/* nmeaGSVSatelliteIdParser() :                                               */
/******************************************************************************/

NMEASentenceStates_t nmeaGSVSatelliteIdParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                    NMEASentenceCaptureState_t *captureState,
                                                    void                       *domainHandling)
  {
/******************************************************************************/

  NMEASentenceStates_t sentenceState = captureState->nmeaSentenceInnerState;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    sentenceState = nmeaGSVSatelliteStatisticsParser(NMEA_GSV_STATISTIC_SATELLITE_ID,
                                                     charactersToParse,
                                                     captureState,
                                                     domainHandling);
    }

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
  } /* end of nmeaGSVSatelliteIdParser                                        */


/******************************************************************************/
/* nmeaGSVElevationParser() :                                                 */
/*                                                                            */
/* - parse and decode a sentence's "elevation" field :                        */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaGSVElevationParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                  NMEASentenceCaptureState_t *captureState,
                                                  void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t sentenceState = NMEA_SENTENCE_STATE_FAILED;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    sentenceState = nmeaGSVSatelliteStatisticsParser(NMEA_GSV_STATISTIC_SATELLITE_ELEVATION,
                                                     charactersToParse,
                                                     captureState,
                                                     domainHandling);
    }

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaGSVElevationParser                                            */

/******************************************************************************/
/* nmeaGSVAzimuthParser() :                                                   */
/*                                                                            */
/* - parse and decode a sentence's "azimuth" field :                          */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaGSVAzimuthParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                NMEASentenceCaptureState_t *captureState,
                                                void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t sentenceState = NMEA_SENTENCE_STATE_FAILED;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    sentenceState = nmeaGSVSatelliteStatisticsParser(NMEA_GSV_STATISTIC_SATELLITE_AZIMUTH,
                                                     charactersToParse,
                                                     captureState,
                                                     domainHandling);
    }
/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaGSVAzimuthParser                                              */

/******************************************************************************/
/* nmeaGSVSNRdBParser() :                                                     */
/*                                                                            */
/* - parse and decode a sentence's "SNR in dB" field :                        */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaGSVSNRdBParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                              NMEASentenceCaptureState_t *captureState,
                                              void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t sentenceState = NMEA_SENTENCE_STATE_FAILED;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    sentenceState = nmeaGSVSatelliteStatisticsParser(NMEA_GSV_STATISTIC_SATELLITE_C_N0,
                                                    charactersToParse,
                                                    captureState,
                                                    domainHandling);
    }
/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaGSVSNRdBParser                                                */

#endif

/******************************************************************************/
/* nmeaGSVSateliteFieldParser() :                                             */
/*                                                                            */
/* - parse and decode one of the block variables :                            */
/*    - id                                                                    */
/*    - elevation                                                             */
/*    - azimuth                                                               */
/*    - C / N0                                                                */
/*   the field to parse is selected by an external state counter which is     */
/*   reset for each sentence                                                  */
/*                                                                            */
/******************************************************************************/

NMEASentenceStates_t nmeaGSVSatelliteFieldParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                       NMEASentenceCaptureState_t *captureState,
                                                       void                       *domainHandling)
{
/******************************************************************************/

  NMEASentenceStates_t sentenceState = NMEA_SENTENCE_STATE_FAILED;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL) && (domainHandling != NULL))
    {
    sentenceState = nmeaGSVSatelliteStatisticsParser(captureState->nmeaSentenceStateCounter,
                                                     charactersToParse,
                                                     captureState,
                                                     domainHandling);
    }

/******************************************************************************/

  return(sentenceState);

/******************************************************************************/
} /* end of nmeaGSVSNRdBParser                                                */

/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/
