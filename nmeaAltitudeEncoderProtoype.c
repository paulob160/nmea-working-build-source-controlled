/******************************************************************************/
/* nmeaEncoderAddAltitude() :                                                 */
/* <--> newSentence              : new sentence character array               */
/* <--> newSentenceIndex         : new sentence character array index         */
/*  --> randomState              : random state for this sentence             */
/*                                                                            */
/* - generate generic "altitude"                                              */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaEncoderAddAltitude(NMEA_GPS_UCHAR *newSentence,
                                        NMEA_GPS_UINT8 *newSentenceIndex,
                                        tinymt32_t     *randomState)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN       altitudeResult = false;
                         
  NMEA_GPS_UCHAR         generatedField[NMEA_ENCODE_MAXIMUM_GENERATED_FIELD_LENGTH];
                         
  NMEA_GPS_UINT8         variableLength = 0,
                         variableIndex  = 0;

  nmeaNumericalVariant_t variableValue;

/******************************************************************************/

  if ((newSentence != NULL) && (newSentenceIndex != NULL) && (randomState != NULL))
    {
    // Generate a random value for "[ <speed> | <course> ] over ground"
    if (nmeaGenerateRandomVariable(&generatedField[0],
                                   &variableValue,
                                   &variableLength,
                                    NMEA_FIELD_DECIMAL_FLOATING,
                                    NMEA_SENTENCE_NUMERICAL_SIGN_UNSIGNED,
                                    NMEA_ALTITUDE_INTEGER_PART_MAXIMUM_LENGTH,
                                    NMEA_ALTITUDE_INTEGER_MINIMUM_LIMIT,
                                    NMEA_ALTITUDE_INTEGER_MAXIMUM_LIMIT,
                                    NMEA_ALTITUDE_FRACTIONAL_PART_MAXIMUM_LENGTH,
                                    NMEA_ALTITUDE_FRACTIONAL_MINIMUM_LIMIT,
                                    NMEA_ALTITUDE_FRACTIONAL_MAXIMUM_LIMIT) == true)
       {
       if ((variableValue.nmea_float_t >= NMEA_ALTITUDE_MINIMUM) && (variableValue.nmea_float_t <= NMEA_ALTITUDE_MAXIMUM))
         {
         for (variableIndex = 0; variableIndex < variableLength; variableIndex++)
           {
           // Strip out any leading zeros
           if (variableIndex == 0)
             {
             if (generatedField[variableIndex] != NMEA_GPS_ASCII_DIGIT_ZERO)
               {
               *(newSentence + *newSentenceIndex) = generatedField[variableIndex];

               *newSentenceIndex                  = *newSentenceIndex + 1;
               }
             }
           else
             {
             *(newSentence + *newSentenceIndex) = generatedField[variableIndex];

             *newSentenceIndex                  = *newSentenceIndex + 1;
             }
           }

         *(newSentence + *newSentenceIndex) = NMEA_SENTENCE_END_OF_STRING;

          altitudeResult = true;
         }
       }
    }

  // Force the random-number-generator to generate a new number next time round
  Sleep(NMEA_ENCODE_RANDOMISATION_SLEEP_DELAY);

/******************************************************************************/

  return(altitudeResult);

/******************************************************************************/
} /* 

