/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/
/*                                                                            */
/* NMEAEncoder.h                                                              */
/* 13.01.20                                                                   */
/*                                                                            */
/* - provides test facilities for the decoder                                 */
/*                                                                            */
/* Ref : NMEA 0183 Specification v2.3                                         */
/*       ublox NEO-6 - Data Sheet                                             */
/*                                                                            */
/******************************************************************************/

#ifndef _NMEA_ENCODER_H_
#define _NMEA_ENCODER_H_

/******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "NMEADecoder.h"
#include "NMEAUtilities.h"
#include "NMEAGps.h"
#include "tinymt32.h"

/******************************************************************************/

#define NMEA_ENCODER_SIGN_DECISION_LIMIT           ((NMEA_GPS_FLOAT)0.5)
#define NMEA_ENCODER_SIGN_POSITIVE                 ((NMEA_GPS_FLOAT)1.0)
#define NMEA_ENCODER_SIGN_NEGATIVE                 ((NMEA_GPS_FLOAT)-1.0)

// Highest power of 10 in latitude and longitude degrees fields
#define NMEA_ENCODE_LATITUDE_DEGREES_INDEX         (2) // i.e. 10 ^ 2
#define NMEA_ENCODE_LATITUDE_DEGREES_x10           ((NMEA_GPS_FLOAT)10.0)
                                                   
#define NMEA_ENCODE_LONGITUDE_DEGREES_INDEX        (3) // i.e. 10 ^ 3
#define NMEA_ENCODE_LONGITUDE_DEGREES_x100         ((NMEA_GPS_FLOAT)100.0)
                                                   
#define MT_INITIAL_SEED                            ((NMEA_GPS_UINT32)0x53771037) // use this to seed the random-number 
                                                                                // generator embedded in the encoder

#define NMEA_ENCODE_MAXIMUM_GENERATED_FIELD_LENGTH (24) // room for even long doubles!

#define NMEA_ENCODE_RANDOMISATION_SLEEP_DELAY       (1) // force a time delay between random-number generation 
                                                        // calls to ensure the seed has changed

#define NMEA_GSV_EMPTY_FIELD_THRESHOLD              ((NMEA_GPS_FLOAT)0.9) // high threshold for empty fields

/******************************************************************************/

extern NMEA_GPS_UINT8 nmeaEncoderSentenceIndex;
extern NMEA_GPS_UCHAR nmeaEncoderSentence[NMEA_SENTENCE_MAXIMUM_LENGTH_STRING];

/******************************************************************************/

// Select the seeding method for sentence generation
typedef enum nmeaEncoderSentenceSeedScheme_tTag
{
  NMEA_ENCODER_SEED_SCHEME_INITIAL = 0, // use the fixed seed "MT_INITIAL_SEED"
  NMEA_ENCODER_SEED_SCHEME_TIME,
  NMEA_ENCODER_SEED_SCHEMES
} nmeaEncoderSentenceSeedScheme_t;

// Flag the number-generators to work within the given maximum/minimums or not
typedef enum nmeaEncoderRespectNumericalLimits_tTag
{
  NMEA_ENCODER_NUMERICAL_LIMITS_IGNORE = 0,
  NMEA_ENCODER_NUMERICAL_LIMITS_RESPECT,
  NMEA_ENCODER_NUMERICAL_LIMITS
} nmeaEncoderRespectNumericalLimits_t;

// Select the limits for random number generation
typedef enum nmeaEncoderRandomLimitTypes_tTag
{
  NMEA_RANDOM_RANGE_LIMIT_X_Y_INCLUSIVE = 0,       // X <= <limit> <= Y
  NMEA_RANDOM_RANGE_LIMIT_X_Y_EXCLUSIVE,           // X  < <limit> <  Y
  NMEA_RANDOM_RANGE_LIMIT_X_INCLUSIVE_Y_EXCLUSIVE, // X <= <limit> <  Y
  NMEA_RANDOM_RANGE_LIMIT_X_EXCLUSIVE_Y_INCLUSIVE, // X  < <limit> <= Y
  NMEA_RANDOM_RANGE_LIMITS
} nmeaEncoderRandomLimitTypes_t;

// Select compass points pairs [ [ 'E' | 'W' ] | [ 'N' | 'S' ] ]
typedef enum nmeaEncoderSelectCompassPoints_tTag
{
  NMEA_ENCODER_COMPASS_POINTS_NS = 0,
  NMEA_ENCODER_COMPASS_POINTS_EW,
  NMEA_ENCODER_COMPASS_POINTS
} nmeaEncoderSelectCompassPoints_t;

// Select "course over ground" or "speed over ground"
typedef enum nmeaEncoderXOverGround_tTag
{
  NMEA_ENCODER_OVER_GROUND_COURSE = 0,
  NMEA_ENCODER_OVER_GROUND_SPEED,
  NMEA_ENCODER_OVER_GROUND_MAGNETIC_VARIATION,
  NMEA_ENCODER_OVER_GROUND_TYPES
} nmeaEncoderXOverGround_t;

// Pad random number ASCII-representations with leading-zeroes or not
typedef enum nmeaEncoderZeroPad_tTag
{
  NMEA_ENCODER_ZEROES_PAD,
  NMEA_ENCODER_ZEROES_IGNORE,
  NMEA_ENCODER_ZEROES
} nmeaEncoderZeroPad_t;

typedef enum nmeaNumberSign_tTag
  {
  NMEA_NUMBER_SIGN_NONE = 0,
  NMEA_NUMBER_SIGN_PLUS,
  NMEA_NUMBER_SIGN_MINUS,
  NMEA_NUMBER_SIGN_PLUS_MINUS,
  NMEA_NUMBER_SIGNS
  } nmeaNumberSign_t;

/******************************************************************************/

extern NMEA_GPS_BOOLEAN nmeaEncoderGenerateSentenceGLL(      NMEA_GPS_UCHAR                  *newSentence,
                                                             NMEA_GPS_UINT8                  *newSentenceIndex,
                                                             tinymt32_t                      *randomState,
                                                       const nmeaEncoderSentenceSeedScheme_t  randomSeedScheme);
extern NMEA_GPS_BOOLEAN nmeaEncoderGenerateSentenceRMC(      NMEA_GPS_UCHAR                  *newSentence,
                                                             NMEA_GPS_UINT8                  *newSentenceIndex,
                                                             tinymt32_t                      *randomState,
                                                       const nmeaEncoderSentenceSeedScheme_t  randomSeedScheme);
extern NMEA_GPS_BOOLEAN nmeaEncoderGenerateSentenceGSA(      NMEA_GPS_UCHAR                  *newSentence,
                                                             NMEA_GPS_UINT8                  *newSentenceIndex,
                                                             tinymt32_t                      *randomState,
                                                       const nmeaEncoderSentenceSeedScheme_t  randomSeedScheme);
extern NMEA_GPS_BOOLEAN nmeaEncoderGenerateSentenceGGA(      NMEA_GPS_UCHAR                  *newSentence,
                                                             NMEA_GPS_UINT8                  *newSentenceIndex,
                                                             tinymt32_t                      *randomState,
                                                       const nmeaEncoderSentenceSeedScheme_t  randomSeedScheme);
extern NMEA_GPS_BOOLEAN nmeaEncoderGenerateSentenceGSV(      NMEA_GPS_UCHAR                  ***newGSVSentences,
                                                             NMEA_GPS_UINT8                   **newGSVSentenceIndices,
                                                             NMEA_GPS_UINT8                    *numberOfGSVSentences,
                                                             tinymt32_t                        *randomState,
                                                       const nmeaEncoderSentenceSeedScheme_t    randomSeedScheme);
extern NMEA_GPS_BOOLEAN nmeaEncoderStartSentence(NMEA_GPS_UCHAR *newSentence,
                                                 NMEA_GPS_UINT8 *newSentenceIndex,
                                                 NMEA_GPS_UINT8  newSentenceMaximumLength,
                                                 tinymt32_t      *randomState,
                                                 NMEA_GPS_UINT32  randomSeed);
extern NMEA_GPS_BOOLEAN nmeaEncoderAddGNSSAndSentencePrefix(      NMEA_GPS_UCHAR *newSentence,
                                                                  NMEA_GPS_UINT8 *newSentenceIndex,
                                                                  tinymt32_t     *randomState,
                                                            const NMEA_GPS_UCHAR *sentenceType);
extern NMEA_GPS_BOOLEAN nmeaEncoderGenerateRandomSender(tinymt32_t     *randomState,
                                                        NMEA_GPS_UCHAR *sender);
extern NMEA_GPS_BOOLEAN nmeaEncoderGenerateRandomSingleField(      NMEA_GPS_UCHAR *newSentence,
                                                             NMEA_GPS_UINT8 *newSentenceIndex,
                                                             tinymt32_t     *randomState,
                                                             const NMEA_GPS_UCHAR *singleFieldOptions,
                                                             const NMEA_GPS_UINT8  numberOfOptions);
extern NMEA_GPS_BOOLEAN nmeaEncoderAddCharacter(NMEA_GPS_UCHAR *newSentence,
                                                NMEA_GPS_UINT8 *newSentenceIndex,
                                                NMEA_GPS_UCHAR  newCharacter);
extern NMEA_GPS_BOOLEAN nmeaEncoderAddCompassPoints(      NMEA_GPS_UCHAR                   *newSentence,
                                                          NMEA_GPS_UINT8                   *newSentenceIndex,
                                                          tinymt32_t                       *randomState,
                                                    const nmeaEncoderSelectCompassPoints_t  compassPoints);
extern NMEA_GPS_BOOLEAN nmeaEncoderAddLatLongField(      NMEA_GPS_UCHAR                *newSentence,
                                                         NMEA_GPS_UINT8                *newSentenceIndex,
                                                         nmeaSentenceLatLongSelector_t  latLongSelector,
                                                         tinymt32_t                    *randomState,
                                                   const nmeaEncoderZeroPad_t           zeroPad);
extern NMEA_GPS_BOOLEAN nmeaGenerateSignedIntegerRandomFields(      NMEA_GPS_UCHAR                *generatedField,
                                                                    NMEA_GPS_INT32                *generatedValue,
                                                              const NMEA_GPS_INT32                *minimumLimit,
                                                              const NMEA_GPS_INT32                *maximumLimit,
                                                              const nmeaEncoderSignFlag_t          signFlag,
                                                                    NMEA_GPS_UINT8                 fieldWidth,
                                                                    tinymt32_t                    *randomState);
extern NMEA_GPS_BOOLEAN nmeaGenerateRandomFields(      NMEA_GPS_FLOAT                      *generatedField,
                                                 const NMEA_GPS_FLOAT                      *minimumLimit,
                                                 const NMEA_GPS_FLOAT                      *maximumLimit,
                                                 const NMEA_GPS_FLOAT                      *scaler,
                                                 const nmeaEncoderRespectNumericalLimits_t  limitRespect,
                                                       tinymt32_t                          *randomState);
extern NMEA_GPS_BOOLEAN nmeaGenerateRandomVariable(      NMEA_GPS_UCHAR             *variableString,
                                                         nmeaNumericalVariant_t     *variableValue,
                                                         NMEA_GPS_UINT8             *variableLength,
                                                   const nmeaSentenceVariableType_t  variableType,
                                                   const nmeaNumericalSignFlag_t     signFlag,
                                                   const NMEA_GPS_UINT8              integerFieldLength,
                                                   const NMEA_GPS_INT32              integerFieldMinimumLimit,
                                                   const NMEA_GPS_INT32              integerFieldMaximumLimit,
                                                   const NMEA_GPS_UINT8              fractionalFieldLength,
                                                   const NMEA_GPS_INT32              fractionalFieldMinimumLimit,
                                                   const NMEA_GPS_INT32              fractionalFieldMaximumLimit);
extern NMEA_GPS_BOOLEAN nmeaEncoderGenerateUTCTime(NMEA_GPS_UCHAR *newSentence,
                                                   NMEA_GPS_UINT8 *newSentenceIndex,
                                                   tinymt32_t     *randomState);
extern NMEA_GPS_BOOLEAN nmeaEncoderAddDilutionOfPrecision(NMEA_GPS_UCHAR *newSentence,
                                                          NMEA_GPS_UINT8 *newSentenceIndex,
                                                          tinymt32_t     *randomState);
extern NMEA_GPS_BOOLEAN nmeaEncoderAddXOverGround(      NMEA_GPS_UCHAR           *newSentence,
                                                        NMEA_GPS_UINT8           *newSentenceIndex,
                                                  const nmeaEncoderXOverGround_t  overGroundType,
                                                  const nmeaEncoderZeroPad_t      zeroPad);
extern NMEA_GPS_BOOLEAN nmeaEncoderAddDate(NMEA_GPS_UCHAR *newSentence,
                                           NMEA_GPS_UINT8 *newSentenceIndex,
                                           tinymt32_t     *randomState);
extern NMEA_GPS_BOOLEAN nmeaEncoderAddActiveSatellites(      NMEA_GPS_UCHAR *newSentence,
                                                             NMEA_GPS_UINT8 *newSentenceIndex,
                                                             tinymt32_t     *randomState,
                                                       const NMEA_GPS_UINT8  activeSatellitesMinimum,
                                                       const NMEA_GPS_UINT8  activeSatellitesMaximum,
                                                       const NMEA_GPS_UINT8  activeSatellitesMinimumId,
                                                       const NMEA_GPS_UINT8  activeSatellitesMaximumId,
                                                       const NMEA_GPS_UINT8  idFieldWidth,
                                                       const NMEA_GPS_UCHAR  fieldSeperator);
extern NMEA_GPS_BOOLEAN nmeaEncoderAddChecksum(NMEA_GPS_UCHAR *newSentence,
                                               NMEA_GPS_UINT8 *newSentenceIndex,
                                               NMEA_GPS_UINT8  runningChecksum);
extern NMEA_GPS_BOOLEAN nmeaEncoderAddTerminator(NMEA_GPS_UCHAR *newSentence,
                                                 NMEA_GPS_UINT8 *newSentenceIndex);
extern NMEA_GPS_BOOLEAN nmeaGenerateScrambledSequentialNumbers(nmeaNumericalVariant_t *numberBuffer,
                                                               NMEA_GPS_UINT32         numberRange,
                                                               NMEA_GPS_UINT32         randomSeed);
extern NMEA_GPS_BOOLEAN nmeaLoadIntegerAsDecimalAscii(NMEA_GPS_UCHAR  *newSentence,
                                                      NMEA_GPS_UINT8  *newSentenceIndex,
                                                      NMEA_GPS_INT32   newNumber,
                                                      NMEA_GPS_UINT8   fieldWidth,
                                                      nmeaNumberSign_t signState);

/******************************************************************************/

#endif

/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/