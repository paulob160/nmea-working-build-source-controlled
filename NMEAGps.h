/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/
/*                                                                            */
/* NMEAGps.h                                                                  */
/* 06.01.20                                                                   */
/*                                                                            */
/* Ref : NMEA 0183 Specification v2.3                                         */
/*       ublox NEO-6 - Data Sheet                                             */
/*                                                                            */
/******************************************************************************/


#ifndef _NMEA_GPS_H_
#define _NMEA_GPS_H_

/******************************************************************************/

#include <stdbool.h>
#include <stdint.h>
#include "NMEATypes.h"

/******************************************************************************/
/* General constants :                                                        */
/******************************************************************************/

// Number of active satellite ID slots in "GSA"
#define NMEA_ACTIVE_SATELLITES_MINIMUM             (0)
#define NMEA_ACTIVE_SATELLITES_MAXIMUM            (12)
                                                  
#define NMEA_ACITVE_SATELLITES_NULL_ID             (0)
#define NMEA_ACITVE_SATELLITES_MINIMUM_ID          (1)
#define NMEA_ACITVE_SATELLITES_MAXIMUM_ID         (64)

#define NMEA_GSV_MAXIMUM_SATELLITES_PER_MESSAGE    (4)
#define NMEA_GSV_FIELDS_PER_SATELLITE              (4)
                                                   
#define NMEA_GSV_MINIMUM_NUMBER_OF_MESSAGES        (1)
#define NMEA_GSV_MAXIMUM_NUMBER_OF_MESSAGES        (9) // this is a single digit field
#define NMEA_GSV_MINIMUM_NUMBER_OF_SATELLITES      NMEA_ACITVE_SATELLITES_MINIMUM_ID
#define NMEA_GSV_MAXIMUM_NUMBER_OF_SATELLITES      (NMEA_GSV_MAXIMUM_NUMBER_OF_MESSAGES * NMEA_GSV_MAXIMUM_SATELLITES_PER_MESSAGE)
                                                   
/******************************************************************************/
/* Aggregation of all the received GPS fields :                               */
/******************************************************************************/

// General status flags for function returning
typedef enum nmeaGpsStatus_tTag
{
  NMEA_GPS_STATUS_INVALID = 0,
  NMEA_GPS_STATUS_VALID,
  NMEA_GPS_STATUSES
} nmeaGpsStatus_t;

// Flags to signal one or all fields' required vaiidation status
typedef enum NMEAGpsFieldStatus_tTag
{
  NMEA_GPS_FIELD_STATUS_INVALID = 0,
  NMEA_GPS_FIELD_STATUS_VALID,
  NMEA_GPS_FIELD_STATUSES
} NMEAGpsFieldStatus_t;

// Group identifiers are binary powers so one or more group operations can be 
// carried out simultaneously
#define NMEA_GPS_GROUP_MASK        (NMEA_GPS_GROUP_GEO_POSITION) // used to shift through the available groups
#define NMEA_GPS_GROUP_SHIFT       (1)

#define NMEA_GPS_GROUP_LOWER_LIMIT (NMEA_GPS_GROUP_GEO_POSITION)
#define NMEA_GPS_GROUP_UPPER_LIMIT (NMEA_GPS_ALL_GROUPS)

typedef enum NMEAGpsGroups_tTag
{
  NMEA_GPS_GROUP_NO_GROUP     = 0,
  // *** ACTUAL GROUP FLAGS START HERE ***
  NMEA_GPS_GROUP_GEO_POSITION = 1,
  NMEA_GPS_GROUP_GEO_STATUS   = (NMEA_GPS_GROUP_GEO_POSITION << 1),
  NMEA_GPS_ALL_GROUPS         = (NMEA_GPS_GROUP_GEO_STATUS   << 1),
  // *** ACTUAL GROUP FLAGS END HERE ***
  NMEA_GPS_GROUPS_DEFINED     = 3, // used to limit the shift through the available groups
  NMEA_GPS_GROUPS             = 7  // sum of the enumerations defined
} NMEAGpsGroups_t;

typedef enum NMEAGpsField_tTag
{
  NMEA_GPS_FIELD_GEO_POSITION_LATITIUDE_DEGREES = 0,
  NMEA_GPS_FIELD_GEO_POSITION_LATITIUDE_MINUTES,
  NMEA_GPS_FIELD_GEO_POSITION_LATITIUDE_NORTHINGS,
  NMEA_GPS_FIELD_GEO_POSITION_LONGITUDE_DEGREES,
  NMEA_GPS_FIELD_GEO_POSITION_LONGITUDE_MINUTES,
  NMEA_GPS_FIELD_GEO_POSITION_LONGITUDE_EASTINGS,
  NMEA_GPS_FIELD_GEO_POSITION_UTC_HOURS,
  NMEA_GPS_FIELD_GEO_POSITION_UTC_MINUTES,
  NMEA_GPS_FIELD_GEO_POSITION_UTC_SECONDS,
  NMEA_GPS_FIELD_GEO_POSITION_UTC_CENTISECONDS,
  NMEA_GPS_FIELD_GEO_POSITION_SPEED_OVER_GROUND,
  NMEA_GPS_FIELD_GEO_POSITION_COURSE_OVER_GROUND,
  NMEA_GPS_FIELD_GEO_POSITION_ALTITUDE,
  NMEA_GPS_FIELD_GEO_POSITION_GEOID_SEPERATION,
  NMEA_GPS_FIELD_GEO_POSITION_MAGNETIC_VARIATION,
  NMEA_GPS_FIELD_GEO_POSITION_DATE_DAYS,
  NMEA_GPS_FIELD_GEO_POSITION_DATE_MONTHS,
  NMEA_GPS_FIELD_GEO_POSITION_DATE_YEARS,
  NMEA_GPS_FIELD_GEO_POSITION_FIX_STATUS,
  NMEA_GPS_FIELD_GEO_POSITION_FAA_MODE,
  NMEA_GPS_FIELD_GEO_POSITION_FIELDS,
  NMEA_GPS_FIELD_GEO_STATUS_S_MODE,
  NMEA_GPS_FIELD_GEO_STATUS_FIX_MODE,
  NMEA_GPS_FIELD_GEO_STATUS_FIX_QUALITY,
  NMEA_GPS_FIELD_GEO_STATUS_SATELLITES_IN_VIEW,
  NMEA_GPS_FIELD_GEO_STATUS_ACTIVE_SATELLITE_ID_NULL,
  NMEA_GPS_FIELD_GEO_STATUS_ACTIVE_SATELLITE_ID_1,
  NMEA_GPS_FIELD_GEO_STATUS_SATELLITES_IN_VIEW_NULL,
  NMEA_GPS_FIELD_GEO_STATUS_STATISTICS_ID,
  NMEA_GPS_FIELD_GEO_STATUS_STATISTICS_ELEVATION,
  NMEA_GPS_FIELD_GEO_STATUS_STATISTICS_AZIMUTH,
  NMEA_GPS_FIELD_GEO_STATUS_STATISTICS_C_N0,
  NMEA_GPS_FIELD_GEO_STATUS_PDOP,
  NMEA_GPS_FIELD_GEO_STATUS_HDOP,
  NMEA_GPS_FIELD_GEO_STATUS_VDOP,
  NMEA_GPS_FIELD_GEO_STATUS_DGPS_AGE,
  NMEA_GPS_FIELD_GEO_STATUS_DGPS_ID,
  NMEA_GPS_FIELD_GEO_STATUS_FIELDS = (NMEA_GPS_FIELD_GEO_STATUS_VDOP   - NMEA_GPS_FIELD_GEO_POSITION_FIELDS),
  NMEA_GPS_FIELDS                  = (NMEA_GPS_FIELD_GEO_STATUS_FIELDS + NMEA_GPS_FIELD_GEO_POSITION_FIELDS)
} NMEAGpsField_t;

typedef struct NMEAGpsGeoPosition_tTag
{
  NMEA_GPS_UINT8 latitudeDegrees;
  NMEA_GPS_FLOAT latitudeMinutes;
  NMEA_GPS_CHAR  latitudeNorthings;
  NMEA_GPS_UINT8 longitudeDegrees;
  NMEA_GPS_FLOAT longitudeMinutes;
  NMEA_GPS_CHAR  longitudeEastings;
  NMEA_GPS_UINT8 utcHours;
  NMEA_GPS_UINT8 utcMinutes;
  NMEA_GPS_UINT8 utcSeconds;
  NMEA_GPS_UINT8 utcCentiseconds;
  NMEA_GPS_FLOAT speedOverGround;
  NMEA_GPS_FLOAT courseOverGround;
  NMEA_GPS_FLOAT altitude;
  NMEA_GPS_FLOAT geoidSeperation;
  NMEA_GPS_FLOAT magneticVariation;
  NMEA_GPS_CHAR  magneticVariationEastings;
  NMEA_GPS_UINT8 dateDays;
  NMEA_GPS_UINT8 dateMonths;
  NMEA_GPS_UINT8 dateYears;
  NMEA_GPS_CHAR  fixStatus;
  NMEA_GPS_CHAR  faaMode;
} NMEAGpsGeoPosition_t;

// Active satellite statistics
typedef struct nmeaGpsActiveSatelliteStatus_tTag
  {
  NMEA_GPS_UINT8  satelliteId;
  NMEA_GPS_INT8   satelliteElevation;
  NMEA_GPS_UINT16 satelliteAzimuth;
  NMEA_GPS_UINT8  satellite_C_N0;
  } nmeaGpsActiveSatelliteStatus_t;

typedef struct nmeaGpsGeoStatus_tTag
{
  NMEA_GPS_UCHAR                 sMode;
  NMEA_GPS_UCHAR                 fixMode;
  NMEA_GPS_UINT8                 fixQuality;
  NMEA_GPS_UINT8                 totalSatellitesInSolution;
  NMEA_GPS_FLOAT                 pDop;
  NMEA_GPS_FLOAT                 hDop;
  NMEA_GPS_FLOAT                 vDop;
  NMEA_GPS_UINT8                 dGpsAge;
  NMEA_GPS_UINT16                dGpsBaseId;
  NMEA_GPS_UINT8                 activeSatelliteId[NMEA_ACTIVE_SATELLITES_MAXIMUM];
  nmeaGpsActiveSatelliteStatus_t satellitesInView[NMEA_GSV_MAXIMUM_NUMBER_OF_SATELLITES];
} nmeaGpsGeoStatus_t;

typedef struct NMEAGpsUpdate_tTag
{
  NMEAGpsFieldStatus_t geoPositionValid;
  NMEAGpsGeoPosition_t geoPosition;
  nmeaGpsGeoStatus_t   geoStatus;
} NMEAGpsUpdate_t;

/******************************************************************************/

extern NMEAGpsUpdate_t gpsLatestFix;

/******************************************************************************/

extern _NMEA_INLINE_INCLUDE_ nmeaGpsStatus_t  gpsValidateFixGroup(      NMEAGpsUpdate_t      *nmeaGpsFix,
                                                                  const NMEAGpsGroups_t       nmeaGroup,
                                                                  const NMEAGpsFieldStatus_t  nmeaFieldStatus);
                                              
extern _NMEA_INLINE_INCLUDE_ nmeaGpsStatus_t  gpsUpdateFixGroupField(      NMEAGpsUpdate_t      *nmeaGpsFix,
                                                                     const NMEAGpsGroups_t       nmeaGroup,
                                                                     const NMEAGpsField_t        nmeaField,
                                                                     const NMEA_GPS_UINT8        nmeaFieldOffset,
                                                                           void                 *nmeaFieldupdate);
                                              
extern _NMEA_INLINE_INCLUDE_ bool             gpsUpdateGeoStatus(      NMEAGpsUpdate_t *nmeaGpsFix,
                                                                 const NMEAGpsField_t   nmeaGeoStatusField,
                                                                 const NMEA_GPS_UINT8   nmeaGeoStatusFieldOffset,
                                                                 void                  *nmeaGeoStatusFieldUpdate);
                                              
extern _NMEA_INLINE_INCLUDE_ bool             gpsUpdateGeoPosition(      NMEAGpsUpdate_t *nmeaGpsFix,
                                                                   const NMEAGpsField_t   nmeaGeoPositionField,
                                                                         void            *nmeaGeoPositionFieldupdate);
extern                       NMEA_GPS_BOOLEAN nmeaEncoderRandomSeed(NMEA_GPS_UINT32 *randomSeed);

/******************************************************************************/

#endif

/******************************************************************************/
/* NMEAGps.h                                                                  */
/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/