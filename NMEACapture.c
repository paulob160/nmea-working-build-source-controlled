/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/
/*                                                                            */
/* NMEACapture.c                                                              */
/* 29.12.19                                                                   */
/*                                                                            */
/* Ref : NMEA 0183 Specification v2.3                                         */
/*       ublox NEO-6 - Data Sheet                                             */
/*                                                                            */
/******************************************************************************/

#ifdef _NMEA_PARSER_DEBUG_PRINT_
#include <stdio.h>
#endif
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>
#include "NMEADecoder.h"
#include "NMEAUtilities.h"

/******************************************************************************/

NMEASentenceCaptureState_t activeSentenceState;

/******************************************************************************/
/* nmeaCaptureSentence() :                                                    */
/*  --> newSentenceCharacters : 0 { ascii_characters } n                      */
/*  --> numberOfCharacters    : [ 0 .. n ]                                    */
/* <--> sentenceState         : NMEASentenceCaptureState_t                    */
/*                                                                            */
/*  <-- sentenceStatus        : sentence construction status                  */
/*                                                                            */
/* - capture a potential GNSS sentence delivered one or more characters at    */
/*   each call. Early validation decodes the talker and sentence-type or      */
/*   rejects the message. Late validation examines the validation flags (if   */
/*   any) and conpares the computed and received checksums.                   */
/*   Passing 0 characters causes the re-initialisation of the message capture */
/*                                                                            */
/******************************************************************************/

NMEASentenceCaptureStates_t nmeaCaptureSentence(const NMEA_GPS_UCHAR              *newSentenceCharacters,
                                                const NMEA_GPS_UINT8               numberOfCharacters,
                                                      NMEASentenceCaptureState_t  *sentenceState)
{
/******************************************************************************/

  NMEASentenceCaptureStates_t sentenceStatus = NMEA_SENTENCE_CAPTURE_STATE_SEARCHING;

  NMEA_GPS_UINT8              sentenceIndex  = 0;
  NMEA_GPS_UCHAR              nextCharacter  = 0; // a small optimisation

/******************************************************************************/

  if ((sentenceState != NULL) && (newSentenceCharacters != NULL))
    {
    if (numberOfCharacters == 0) // no characters is an implicit return to the search state
      {
      sentenceState->nmeaSentenceOuterState       = NMEA_SENTENCE_CAPTURE_STATE_SEARCHING;
      sentenceState->nmeaSentenceInnerState       = NMEA_SENTENCE_STATE_INITIALISATION;
      sentenceState->nmeaSentenceGenericState     = NMEA_SENTENCE_STATE_INITIALISATION;
      sentenceState->nmeaSentenceCommonFieldState = NMEA_SENTENCE_COMMON_FIELD_STATE_INITIALISATION;
      sentenceState->nmeaSentenceNewCharacters    = 0;
      sentenceState->nmeaSentenceCharactersUsed   = 0;
      sentenceState->nmeaSentenceMatchIndex       = 0;
      sentenceState->nmeaSentenceMatchLength      = 0;
      sentenceState->nmeaSentenceCharacterIndex   = 0;
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
      sentenceState->nmeaSentence[0]              = NMEA_SENTENCE_END_OF_STRING;
#endif
      sentenceState->nmeaSentenceScratch[0]       = NMEA_SENTENCE_END_OF_STRING;
      sentenceState->nmeaSentenceComputedChecksum = 0;
      sentenceState->nmeaSentenceEscapeFlag       = false;
      sentenceState->nmeaSentenceType             = NMEA_NIL;
      sentenceState->multipleSentenceFlag         = false;
      sentenceState->nmeaSentenceStateCounter     = NMEA_GSV_SATELLITE_BLOCK_COUNTER_START;
                     
      // Reset the sentence general numerical cache
      for (sentenceIndex = 0; sentenceIndex < NMEA_SENTENCE_NUMBER_SCRATCH_LENGTH; sentenceIndex++)
        {
        sentenceState->nmeaSentenceNumberScratch[sentenceIndex].nmea_ulong_t  = 0;
        }

      sentenceStatus                              = NMEA_SENTENCE_CAPTURE_STATE_SEARCHING;
      }
    else
      {
      // Get ready to parse the new set of characters passed in
      sentenceState->nmeaSentenceNewCharacters  = numberOfCharacters;
      sentenceState->nmeaSentenceCharactersUsed = 0;

      // Always consume all the characters passed
      while (sentenceState->nmeaSentenceCharactersUsed < sentenceState->nmeaSentenceNewCharacters)
        { // This switch is the outer parser i.e. '$' character(s), talker code, sentence code and received checksum
        sentenceStatus = sentenceState->nmeaSentenceOuterState;

        switch (sentenceState->nmeaSentenceOuterState)
          {
          case NMEA_SENTENCE_CAPTURE_STATE_SEARCHING    : // Looking for "$" - try to avoid escaped instances, which can only 
                                                          // occur mid-sentence
                                                          if (*(newSentenceCharacters + sentenceState->nmeaSentenceCharactersUsed) == NMEA_SENTENCE_ESCAPE_CHARACTER)
                                                            {
                                                            if (sentenceState->nmeaSentenceEscapeFlag == false)
                                                              {
                                                              sentenceState->nmeaSentenceEscapeFlag = true;
                                                              }
                                                            else
                                                              {
                                                              sentenceState->nmeaSentenceEscapeFlag = false;
                                                              }
                                                            }
                                                          else
                                                            {
                                                            if (*(newSentenceCharacters + sentenceState->nmeaSentenceCharactersUsed) == NMEA_SENTENCE_PREFIX)
                                                              {
                                                              if (sentenceState->nmeaSentenceEscapeFlag == true) // sentence prefix found but escaped
                                                                {
                                                                sentenceState->nmeaSentenceEscapeFlag = false;
                                                                }
                                                              else // sentence prefix found!
                                                                {
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
                                                                // Load the new character and re-terminate the string
                                                                sentenceState->nmeaSentence[sentenceState->nmeaSentenceCharacterIndex]     = NMEA_SENTENCE_PREFIX;
                                                                sentenceState->nmeaSentence[sentenceState->nmeaSentenceCharacterIndex + 1] = NMEA_SENTENCE_END_OF_STRING;
#endif                                                        
                                                                sentenceState->nmeaSentenceCharacterIndex = sentenceState->nmeaSentenceCharacterIndex + 1;        

                                                                // Change state to find the talker code
                                                                sentenceState->nmeaSentenceOuterState     = NMEA_SENTENCE_CAPTURE_STATE_MATCH_TALKER;
                                                                sentenceState->nmeaSentenceMatchIndex     = 0;
                                                                sentenceState->nmeaSentenceMatchLength    = NMEA_SENTENCE_TALKER_LENGTH;

                                                                sentenceStatus                            = NMEA_SENTENCE_CAPTURE_STATE_MATCH_TALKER;
                                                                }
                                                              }
                                                            else
                                                              {
                                                              sentenceState->nmeaSentenceEscapeFlag = false;
                                                              }
                                                            }
                                                        
                                                          sentenceState->nmeaSentenceCharactersUsed = sentenceState->nmeaSentenceCharactersUsed + 1;
                                                          break;
        
          case NMEA_SENTENCE_CAPTURE_STATE_MATCH_TALKER : sentenceStatus = NMEA_SENTENCE_CAPTURE_STATE_MATCH_TALKER;
            
                                                          // Keep looking for multiple sentence non-alpha characters, which cannot occur in the 
                                                          // talker code
                                                          if (!isalpha(*(newSentenceCharacters + sentenceState->nmeaSentenceCharactersUsed)))
                                                            {
                                                            // DO NOT INCREMENT THE "characters used" COUNTER!!! Any remaining characters will 
                                                            // cause a re-entry at the "searching" state

                                                            sentenceState->nmeaSentenceOuterState = NMEA_SENTENCE_CAPTURE_STATE_FAILED;
                                                            }
                                                          else
                                                            {
                                                            // Look for the talker code
                                                            if (sentenceState->nmeaSentenceMatchIndex < sentenceState->nmeaSentenceMatchLength)
                                                              { // Assemble the talker code
                                                              sentenceState->nmeaSentenceScratch[sentenceState->nmeaSentenceMatchIndex] = *(newSentenceCharacters + sentenceState->nmeaSentenceCharactersUsed);
                                                            
                                                              sentenceState->nmeaSentenceMatchIndex     = sentenceState->nmeaSentenceMatchIndex     + 1;
                                                              sentenceState->nmeaSentenceCharactersUsed = sentenceState->nmeaSentenceCharactersUsed + 1;
                                                            
                                                              if (sentenceState->nmeaSentenceMatchIndex == sentenceState->nmeaSentenceMatchLength)
                                                                { // Terminate the code and test for a valid talker
                                                                sentenceState->nmeaSentenceScratch[sentenceState->nmeaSentenceMatchIndex] = NMEA_SENTENCE_END_OF_STRING;
                                                            
                                                                // Test for the talker code. If no match is found go back to searching
                                                                if (nmeaMatchTalker(&sentenceState->nmeaSentenceScratch[0],
                                                                                     sentenceState->nmeaSentenceMatchLength) == false)
                                                                  {
                                                                  sentenceState->nmeaSentenceOuterState = NMEA_SENTENCE_CAPTURE_STATE_FAILED;                                                            
                                                                  sentenceStatus                        = NMEA_SENTENCE_CAPTURE_STATE_FAILED;
                                                                  }
                                                                else
                                                                  {
                                                                  // Load the new characters and re-terminate the string
                                                                  for (sentenceIndex = 0; sentenceIndex < sentenceState->nmeaSentenceMatchLength; sentenceIndex++)
                                                                    {
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
                                                                    sentenceState->nmeaSentence[sentenceState->nmeaSentenceCharacterIndex] = sentenceState->nmeaSentenceScratch[sentenceIndex];
#endif
                                                                    sentenceState->nmeaSentenceCharacterIndex = sentenceState->nmeaSentenceCharacterIndex + 1;
                                                                    }

#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
                                                                  sentenceState->nmeaSentence[sentenceState->nmeaSentenceCharacterIndex] = NMEA_SENTENCE_END_OF_STRING;
#endif
                                                                  // Compute the running checksum
                                                                  nmeaComputeChecksum(&sentenceState->nmeaSentenceScratch[0],
                                                                                       sentenceState->nmeaSentenceMatchLength,
                                                                                      &sentenceState->nmeaSentenceComputedChecksum);

                                                                  // Change state to find the talker sentence type
                                                                  sentenceState->nmeaSentenceOuterState     = NMEA_SENTENCE_CAPTURE_STATE_MATCH_SENTENCE;
                                                                  sentenceState->nmeaSentenceMatchIndex     = 0;
                                                                  sentenceState->nmeaSentenceMatchLength    = NMEA_SENTENCE_TYPE_LENGTH;

                                                                  sentenceStatus                            = NMEA_SENTENCE_CAPTURE_STATE_MATCH_SENTENCE;
                                                                  }
                                                                }
                                                              }
                                                            }

                                                          break;

          case NMEA_SENTENCE_CAPTURE_STATE_MATCH_SENTENCE : // Keep looking for multiple sentence non-alpha characters, which cannot occur in the 
                                                            // sentence code
                                                            if (!isalpha(*(newSentenceCharacters + sentenceState->nmeaSentenceCharactersUsed)))
                                                              {
                                                              // DO NOT INCREMENT THE "characters used" COUNTER!!! Any remaining characters will 
                                                              // cause a re-entry at the "searching" state
                                                            
                                                              sentenceState->nmeaSentenceOuterState = NMEA_SENTENCE_CAPTURE_STATE_FAILED;
                                                              sentenceStatus                        = NMEA_SENTENCE_CAPTURE_STATE_FAILED;
                                                              }
                                                            else
                                                              {
                                                              // Look for the sentence type
                                                              if (sentenceState->nmeaSentenceMatchIndex < sentenceState->nmeaSentenceMatchLength)
                                                              { // Assemble the sentence type
                                                              sentenceState->nmeaSentenceScratch[sentenceState->nmeaSentenceMatchIndex] = *(newSentenceCharacters + sentenceState->nmeaSentenceCharactersUsed);

                                                              sentenceState->nmeaSentenceMatchIndex     = sentenceState->nmeaSentenceMatchIndex     + 1;
                                                              sentenceState->nmeaSentenceCharactersUsed = sentenceState->nmeaSentenceCharactersUsed + 1;

                                                              if (sentenceState->nmeaSentenceMatchIndex == sentenceState->nmeaSentenceMatchLength)
                                                                { // Terminate the code and test for a valid sentence
                                                                sentenceState->nmeaSentenceScratch[sentenceState->nmeaSentenceMatchIndex] = NMEA_SENTENCE_END_OF_STRING;

                                                                // Test for the sentence type. If no match is found go back to searching
                                                                if (nmeaMatchSentence(&sentenceState->nmeaSentenceScratch[0],
                                                                                       sentenceState->nmeaSentenceMatchLength,
                                                                                      &sentenceState->nmeaSentenceType) == false)
                                                                  {
                                                                  sentenceState->nmeaSentenceOuterState = NMEA_SENTENCE_CAPTURE_STATE_FAILED;
                                                                  sentenceStatus                        = NMEA_SENTENCE_CAPTURE_STATE_FAILED;
                                                                  }
                                                                else
                                                                  {
                                                                  // Load the new characters and re-terminate the string
                                                                  for (sentenceIndex = 0; sentenceIndex < sentenceState->nmeaSentenceMatchLength; sentenceIndex++)
                                                                    {
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
                                                                    sentenceState->nmeaSentence[sentenceState->nmeaSentenceCharacterIndex] = sentenceState->nmeaSentenceScratch[sentenceIndex];
#endif
                                                                    sentenceState->nmeaSentenceCharacterIndex = sentenceState->nmeaSentenceCharacterIndex + 1;
                                                                    }

#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
                                                                  sentenceState->nmeaSentence[sentenceState->nmeaSentenceCharacterIndex] = NMEA_SENTENCE_END_OF_STRING;
#endif
                                                                  // Compute the running checksum
                                                                  nmeaComputeChecksum(&sentenceState->nmeaSentenceScratch[0],
                                                                                       sentenceState->nmeaSentenceMatchLength,
                                                                                      &sentenceState->nmeaSentenceComputedChecksum);

                                                                  // Change state to decode the sentence-leading ',' (comma)
                                                                  sentenceState->nmeaSentenceOuterState     = NMEA_SENTENCE_CAPTURE_STATE_PARSE_COMMA;
                                                                  sentenceState->nmeaSentenceMatchIndex     = 0;
                                                                  sentenceState->nmeaSentenceMatchLength    = 0;
                                                                  sentenceState->nmeaSentenceInnerState     = NMEA_SENTENCE_STATE_INITIALISATION;

                                                                  sentenceStatus                            = NMEA_SENTENCE_CAPTURE_STATE_PARSE_COMMA;
                                                                  }
                                                                }
                                                              }
                                                            }

                                                          break;

                                                          /******************************************************************************/
                                                          /* The ',' (comma) immediately following the sentence type fields is common   */
                                                          /* to all NMEA 0183 messages. Rather than complicate the sentence type field  */
                                                          /* detection code it is handled as a single state here                        */
                                                          /******************************************************************************/

          case NMEA_SENTENCE_CAPTURE_STATE_PARSE_COMMA    : if ((*(newSentenceCharacters + sentenceState->nmeaSentenceCharactersUsed)) != NMEA_SENTENCE_FIELD_SEPERATOR)
                                                              {
                                                              // DO NOT INCREMENT THE "characters used" COUNTER!!! Any remaining characters will 
                                                              // cause a re-entry at the "searching" state
                                                              
                                                              sentenceState->nmeaSentenceOuterState = NMEA_SENTENCE_CAPTURE_STATE_FAILED;
                                                              sentenceStatus                        = NMEA_SENTENCE_CAPTURE_STATE_FAILED;
                                                              }
                                                            else
                                                              {
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
                                                              // Load the new character and re-terminate the string
                                                              sentenceState->nmeaSentence[sentenceState->nmeaSentenceCharacterIndex]     = NMEA_SENTENCE_FIELD_SEPERATOR;
                                                              sentenceState->nmeaSentence[sentenceState->nmeaSentenceCharacterIndex + 1] = NMEA_SENTENCE_END_OF_STRING;
#endif                                                        
                                                              sentenceState->nmeaSentenceCharacterIndex = sentenceState->nmeaSentenceCharacterIndex + 1; 

                                                              // Compute the running checksum
                                                              nmeaComputeChecksum( (const NMEA_GPS_UCHAR *)NMEA_SENTENCE_FIELD_SEPERATOR_STRING,
                                                                                   1,
                                                                                  &sentenceState->nmeaSentenceComputedChecksum);

                                                              sentenceState->nmeaSentenceCharactersUsed = sentenceState->nmeaSentenceCharactersUsed + 1;

                                                              sentenceState->nmeaSentenceOuterState     = NMEA_SENTENCE_CAPTURE_STATE_PARSE_SENTENCE;
                                                              sentenceStatus                            = NMEA_SENTENCE_CAPTURE_STATE_PARSE_SENTENCE;
                                                              }

                                                            break;

          case NMEA_SENTENCE_CAPTURE_STATE_PARSE_SENTENCE : // Select the in-sentence parser to use at each iteration
                                                            {
                                                            /******************************************************************************/
                                                            // Non-persistent sentence state parsing result variable
                                                            NMEASentenceStates_t sentenceParsingState = NMEA_SENTENCE_STATE_INITIALISATION;

                                                            /******************************************************************************/

                                                            sentenceParsingState = nmeaParseSentence(newSentenceCharacters,
                                                                                                     sentenceState);

                                                            if (sentenceParsingState == NMEA_SENTENCE_STATE_FAILED)
                                                              {
                                                              sentenceState->nmeaSentenceOuterState = NMEA_SENTENCE_CAPTURE_STATE_FAILED;
                                                              sentenceStatus                        = NMEA_SENTENCE_CAPTURE_STATE_FAILED;
                                                              }
                                                            else
                                                              {
                                                              if (sentenceParsingState == NMEA_SENTENCE_STATE_COMPLETE)
                                                                {
                                                                sentenceState->nmeaSentenceOuterState   = NMEA_SENTENCE_CAPTURE_STATE_COMPARE_CHECKSUMS;
                                                                sentenceState->nmeaSentenceMatchIndex   = 0;
                                                                sentenceState->nmeaSentenceMatchLength  = NMEA_SENTENCE_CHECKSUM_LENGTH;
                                                                sentenceStatus                          = NMEA_SENTENCE_CAPTURE_STATE_COMPARE_CHECKSUMS;
                                                                }
                                                              }

                                                            }

                                                            break;

          case NMEA_SENTENCE_CAPTURE_STATE_COMPARE_CHECKSUMS : // NMEA 0183 checksum digits are hexadecimal
                                                               if (!isxdigit(*(newSentenceCharacters + sentenceState->nmeaSentenceCharactersUsed)))
                                                                 {
                                                                 // DO NOT INCREMENT THE "characters used" COUNTER!!! Any remaining characters will 
                                                                 // cause a re-entry at the "searching" state
                                                                
                                                                 sentenceState->nmeaSentenceOuterState = NMEA_SENTENCE_CAPTURE_STATE_FAILED;
                                                                 sentenceStatus                        = NMEA_SENTENCE_CAPTURE_STATE_FAILED;
                                                                 }
                                                               else
                                                                 {
                                                                 sentenceState->nmeaSentenceScratch[sentenceState->nmeaSentenceMatchIndex] = *(newSentenceCharacters + sentenceState->nmeaSentenceCharactersUsed);
                                                                 
                                                                 sentenceState->nmeaSentenceMatchIndex     = sentenceState->nmeaSentenceMatchIndex     + 1;
                                                                 sentenceState->nmeaSentenceCharactersUsed = sentenceState->nmeaSentenceCharactersUsed + 1;
                                                                 
                                                                 if (sentenceState->nmeaSentenceMatchIndex == sentenceState->nmeaSentenceMatchLength)
                                                                   {
                                                                   // Rebuild the checksum and compare to the received sentence checksum
                                                                   nmeaConvertAsciiHexToHex( sentenceState->nmeaSentenceScratch[NMEA_SENTENCE_CHECKSUM_INDEX_0],
                                                                                            &sentenceState->nmeaSentenceMatchIndex);

                                                                   sentenceState->nmeaSentenceMatchIndex = sentenceState->nmeaSentenceMatchIndex << NMEA_GPS_NYBBLE_SHIFT;

                                                                   nmeaConvertAsciiHexToHex( sentenceState->nmeaSentenceScratch[NMEA_SENTENCE_CHECKSUM_INDEX_1],
                                                                                            &sentenceState->nmeaSentenceScratch[NMEA_SENTENCE_CHECKSUM_INDEX_0]);

                                                                   sentenceState->nmeaSentenceMatchIndex = sentenceState->nmeaSentenceMatchIndex + sentenceState->nmeaSentenceScratch[NMEA_SENTENCE_CHECKSUM_INDEX_0];

                                                                   if (sentenceState->nmeaSentenceMatchIndex == sentenceState->nmeaSentenceComputedChecksum)
                                                                     {
#ifdef _NMEA_PARSER_DEBUG_PRINT_
                                                                     // Print the newly-constructed sentence
                                                                     printf("\n CHECKSUMS MATCH : New Sentence = %s \n", sentenceState->nmeaSentence);
#endif
                                                                     // Ready to (in-)validate one or more fix groups
                                                                     NMEAGpsGroups_t nmeaGroups     = NMEA_GPS_GROUP_NO_GROUP,
                                                                                     nmeaGroupIndex = NMEA_GPS_GROUP_NO_GROUP;

                                                                     // Each sentence type can (in-)validate one or more fix groups
                                                                     switch(sentenceState->nmeaSentenceType)
                                                                       {
                                                                       case NMEA_GLL : nmeaGroups = nmeaGroups | NMEA_GPS_GROUP_GEO_POSITION;
                                                                                       break;

                                                                       default       : break;
                                                                       }

                                                                     for (nmeaGroupIndex = NMEA_GPS_GROUP_LOWER_LIMIT; nmeaGroupIndex <= NMEA_GPS_GROUP_UPPER_LIMIT; nmeaGroupIndex = nmeaGroupIndex << NMEA_GPS_GROUP_SHIFT)
                                                                       {
                                                                       if (nmeaGroups & nmeaGroupIndex)
                                                                         {
                                                                         gpsValidateFixGroup((void *)&gpsLatestFix,
                                                                                             nmeaGroupIndex,
                                                                                             NMEA_GPS_STATUS_VALID);
                                                                         }
                                                                       }

                                                                     sentenceState->nmeaSentenceOuterState = NMEA_SENTENCE_CAPTURE_STATE_PASSED;
                                                                     sentenceStatus                        = NMEA_SENTENCE_CAPTURE_STATE_PASSED;
                                                                     }
                                                                   else
                                                                     {
                                                                     sentenceState->nmeaSentenceOuterState = NMEA_SENTENCE_CAPTURE_STATE_FAILED;
                                                                     sentenceStatus                        = NMEA_SENTENCE_CAPTURE_STATE_FAILED;
                                                                     }
                                                                   }
                                                                 }
                                                               break;

          case NMEA_SENTENCE_CAPTURE_STATE_PASSED            : // Sentence search was successful...
                                                               nextCharacter = *(newSentenceCharacters + sentenceState->nmeaSentenceCharactersUsed);

                                                               // Sentence terminators are always <CR><LF> - here a relaxed attitude is taken to these 
                                                               // with respect to both order and repetition (even no-show) - if either occur, hold state 
                                                               // otherwise restart the searching procedure
                                                               if ((nextCharacter == NMEA_SENTENCE_TERMINATOR_CR) || (nextCharacter == NMEA_SENTENCE_TERMINATOR_LF))
                                                                 {
                                                                 sentenceState->nmeaSentenceCharactersUsed = sentenceState->nmeaSentenceCharactersUsed + 1;
                                                                 }
                                                               else
                                                                 {
                                                                 sentenceState->nmeaSentenceOuterState       = NMEA_SENTENCE_CAPTURE_STATE_SEARCHING;
                                                                 sentenceState->nmeaSentenceInnerState       = NMEA_SENTENCE_STATE_INITIALISATION;
                                                                 sentenceState->nmeaSentenceCommonFieldState = NMEA_SENTENCE_COMMON_FIELD_STATE_INITIALISATION;
                                                                 sentenceState->nmeaSentenceMatchIndex       = 0;
                                                                 sentenceState->nmeaSentenceMatchLength      = 0;
                                                                 sentenceState->nmeaSentenceCharacterIndex   = 0;
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_                            
                                                                 sentenceState->nmeaSentence[0]              = NMEA_SENTENCE_END_OF_STRING;
#endif                                                          
                                                                 sentenceState->nmeaSentenceScratch[0]       = NMEA_SENTENCE_END_OF_STRING;
                                                                 sentenceState->nmeaSentenceComputedChecksum = 0;
                                                                 sentenceState->nmeaSentenceEscapeFlag       = false;
                                                                 sentenceState->nmeaSentenceType             = NMEA_NIL;

                                                                 // If not processing a multiple sentence group, reset the sentence general numerical cache 
                                                                 // and repetitive state tracking variables
                                                                 if (sentenceState->multipleSentenceFlag == false)
                                                                   {
                                                                   sentenceState->nmeaSentenceStateCounter = NMEA_GSV_SATELLITE_BLOCK_COUNTER_START;

                                                                   for (sentenceIndex = 0; sentenceIndex < NMEA_SENTENCE_NUMBER_SCRATCH_LENGTH; sentenceIndex++)
                                                                     {
                                                                     sentenceState->nmeaSentenceNumberScratch[sentenceIndex].nmea_ulong_t = NMEA_SENTENCE_NUMBER_SCRATCH_EMPTY;
                                                                     }
                                                                   }

                                                                 sentenceStatus                              = NMEA_SENTENCE_CAPTURE_STATE_SEARCHING;
                                                                 }
                                                               break;

          case NMEA_SENTENCE_CAPTURE_STATE_FAILED            :
          default                                            : // Unknown state : go back to searching
                                                               sentenceState->nmeaSentenceOuterState       = NMEA_SENTENCE_CAPTURE_STATE_SEARCHING;
                                                               sentenceState->nmeaSentenceInnerState       = NMEA_SENTENCE_STATE_INITIALISATION;
                                                               sentenceState->nmeaSentenceCommonFieldState = NMEA_SENTENCE_COMMON_FIELD_STATE_INITIALISATION;
                                                               sentenceState->nmeaSentenceMatchIndex       = 0;
                                                               sentenceState->nmeaSentenceMatchLength      = 0;
                                                               sentenceState->nmeaSentenceCharacterIndex   = 0;
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_                         
                                                               sentenceState->nmeaSentence[0]              = NMEA_SENTENCE_END_OF_STRING;
#endif                                                       
                                                               sentenceState->nmeaSentenceScratch[0]       = NMEA_SENTENCE_END_OF_STRING;
                                                               sentenceState->nmeaSentenceComputedChecksum = 0;
                                                               sentenceState->nmeaSentenceEscapeFlag       = false;
                                                               sentenceState->nmeaSentenceType             = NMEA_NIL;
                                                               sentenceState->multipleSentenceFlag         = false;
                                                               sentenceState->nmeaSentenceStateCounter     = NMEA_GSV_SATELLITE_BLOCK_COUNTER_START;

                                                               // Reset the sentence general numerical cache
                                                               for (sentenceIndex = 0; sentenceIndex < NMEA_SENTENCE_NUMBER_SCRATCH_LENGTH; sentenceIndex++)
                                                                 {
                                                                 sentenceState->nmeaSentenceNumberScratch[sentenceIndex].nmea_ulong_t = NMEA_SENTENCE_NUMBER_SCRATCH_EMPTY;
                                                                 }

                                                               sentenceStatus                              = NMEA_SENTENCE_CAPTURE_STATE_SEARCHING;
                                                               break;
          } // end of switch "sentenceState->nmeaSentenceState"
        } // end of while "sentenceState->nmeaSentenceCharactersUsed ..."
      } // end of if "numberOfCharacters == 0 ..."
    } // end of if "sentenceState != NULL ..."
  else
    {
    // Bad actual parameters!
    sentenceStatus = NMEA_SENTENCE_CAPTURE_STATE_FAILED;
    } // end of if "sentenceState != NULL ..." else ...

/******************************************************************************/

  return(sentenceStatus);

/******************************************************************************/
} /* nmeaCaptureSentence                                        */

/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/
