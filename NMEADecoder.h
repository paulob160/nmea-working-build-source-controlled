/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/
/*                                                                            */
/* NMEADecoder.h                                                              */
/* 23.12.19                                                                   */
/*                                                                            */
/* Ref : NMEA 0183 Specification v2.3                                         */
/*       ublox NEO-6 - Data Sheet                                             */
/*                                                                            */
/******************************************************************************/

#ifndef _NMEA_DECODER_H_
#define _NMEA_DECODER_H_

/******************************************************************************/

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "NMEAUtilities.h"
#include "NMEAGps.h"

/******************************************************************************/

// NMEA setence component definitions
#define NMEA_SENTENCE_PREFIX                        ('$')
#define NMEA_SENTENCE_PREFIX_STRING                 ("$")
#define NMEA_SENTENCE_PREFIX_LENGTH                   (1)

#define NMEA_SENTENCE_SIGN_PLUS                     ('+')
#define NMEA_SENTENCE_SIGN_MINUS                    ('-')
#define NMEA_SENTENCE_SIGN_LENGTH                     (1)

#define NMEA_SENTENCE_CHARACTER_LENGTH                (1) // could be two for wide characters

#define NMEA_SENTENCE_TALKER_LENGTH                   (2) // two ASCII characters
#define NMEA_SENTENCE_TALKER_MAXIMUM_LENGTH           (3) // two ASCII characters + end-of-string

#define NMEA_SENTENCE_TYPE_LENGTH                     (3) // three ASCII characters
#define NMEA_SENTENCE_TYPE_MAXIMUM_LENGTH             (4) // three ASCII characters + end-of-string

#define NMEA_SENTENCE_FIELD_SEPERATOR               (',')
#define NMEA_SENTENCE_FIELD_SEPERATOR_STRING        (",")
#define NMEA_SENTENCE_FIELD_SEPERATOR_LENGTH          (1) 

#define NMEA_SENTENCE_PREAMBLE_LENGTH                  (NMEA_SENTENCE_PREFIX_LENGTH + \
                                                        NMEA_SENTENCE_TALKER_LENGTH + \
                                                        NMEA_SENTENCE_TYPE_LENGTH   + \
                                                        NMEA_SENTENCE_FIELD_SEPERATOR_LENGTH)

#define NMEA_SENTENCE_FIELD_CHECKSUM_PREFIX         ('*')
#define NMEA_SENTENCE_FIELD_CHECKSUM_PREFIX_STRING  ("*")
#define NMEA_SENTENCE_FIELD_CHECKSUM_PREFIX_LENGTH    (1) // no "strlen()" here!

#define NMEA_SENTENCE_TERMINATOR_CR                ('\r')
#define NMEA_SENTENCE_TERMINATOR_CR_STRING         ("\r")

#define NMEA_SENTENCE_TERMINATOR_LF                ('\n')
#define NMEA_SENTENCE_TERMINATOR_LF_STRINGF        ("\n")

#define NMEA_SENTENCE_ESCAPE_CHARACTER              ('^')
#define NMEA_SENTENCE_ESCAPE_CHARACTER_STRING       ("^")

#define NMEA_SENTENCE_DECIMAL_POINT                 ('.')
#define NMEA_SENTENCE_DECIMAL_POINT_STRING          (".")
#define NMEA_SENTENCE_DECIMAL_POINT_STRING_LENGTH     (1)

#define NMEA_SENTENCE_END_OF_STRING                 (0x00)
#define NMEA_SENTENCE_END_OF_STRING_STRING        ("\r\n")

#define NMEA_SENTENCE_TALKER_BD_STRING              ("BD")
#define NMEA_SENTENCE_TALKER_CC_STRING              ("CC")
#define NMEA_SENTENCE_TALKER_GA_STRING              ("GA")
#define NMEA_SENTENCE_TALKER_GL_STRING              ("GL")
#define NMEA_SENTENCE_TALKER_GN_STRING              ("GN")
#define NMEA_SENTENCE_TALKER_GP_STRING              ("GP")
#define NMEA_SENTENCE_TALKER_UP_STRING              ("UP")

#define NMEA_SENTENCE_TALKER_STRING_LENGTH            (NMEA_SENTENCE_TALKER_LENGTH + 1)

#define NMEA_SENTENCE_TYPE_NIL_STRING               "NIL"
#define NMEA_SENTENCE_TYPE_GSV_STRING               "GSV"
#define NMEA_SENTENCE_TYPE_RMC_STRING               "RMC"
#define NMEA_SENTENCE_TYPE_GSA_STRING               "GSA"
#define NMEA_SENTENCE_TYPE_GGA_STRING               "GGA"
#define NMEA_SENTENCE_TYPE_GLL_STRING               "GLL"
#define NMEA_SENTENCE_TYPE_VTG_STRING               "VTG"
#define NMEA_SENTENCE_TYPE_TXT_STRING               "TXT"

#define NMEA_SENTENCE_TYPE_STRING_LENGTH             (NMEA_SENTENCE_TYPE_LENGTH + 1)

#define NMEA_SENTENCE_CHECKSUM_INDEX_0               (0)
#define NMEA_SENTENCE_CHECKSUM_INDEX_1               (NMEA_SENTENCE_CHECKSUM_INDEX_0 + 1)
#define NMEA_SENTENCE_CHECKSUM_LENGTH                (NMEA_SENTENCE_CHECKSUM_INDEX_1 + 1)

#define NMEA_SENTENCE_MAXIMUM_LENGTH                 (128)
#define NMEA_SENTENCE_MAXIMUM_LENGTH_STRING          (NMEA_SENTENCE_MAXIMUM_LENGTH + 1)

#define NMEA_SENTENCE_PRE_CHECKSUM_LENGTH            (NMEA_SENTENCE_MAXIMUM_LENGTH - NMEA_SENTENCE_FIELD_CHECKSUM_PREFIX_LENGTH - NMEA_SENTENCE_CHECKSUM_LENGTH)
#define NMEA_SENTENCE_SCRATCH_MAXIMUM_LENGTH         (16)
#define NMEA_SENTENCE_SCRATCH_MAXIMUM_LENGTH_STRING  (NMEA_SENTENCE_SCRATCH_MAXIMUM_LENGTH + 1)

#define NMEA_SENTENCE_NUMBER_SCRATCH_LENGTH           (4)
#define NMEA_SENTENCE_NUMBER_SCRATCH_EMPTY            ((NMEA_GPS_ULONG)-1)

#define NMEA_SENTENCE_FIELD_TYPE_OFFSET               (0)
#define NMEA_SENTENCE_FIELD_SUBTYPE_OFFSET            (16)

#define NMEA_SENTENCE_FIELD_MAXIMUM_COMPONENTS        (3)

#define NMEA_SENTENCE_FIELD_MAXIMUM_RESULT_STRING     (16) // possibly make shorter ?

/******************************************************************************/
/* GLL sentence field parser definitions :                                    */
/******************************************************************************/

// Selector for the commmon lat/long decoder field differences 
typedef enum nmeaSentenceLatLongSelector_tTag
{
  NMEA_FIELD_LAT_LONG_SELECT_LATITUDE = 0,
  NMEA_FIELD_LAT_LONG_SELECT_LONGITUDE,
  NMEA_FIELD_LAT_LONG_SELECTS
} nmeaSentenceLatLongSelector_t;

// Contents of the lat/long common decoder field differences
typedef struct nmeaLatLongCommon_tTag
{
  NMEA_GPS_UINT8 latLongDegreesLength;
  NMEA_GPS_FLOAT latLongMinimumLimit;
  NMEA_GPS_FLOAT latLongMaximumLimit;
  NMEAGpsField_t latLongGpsDegreesField;
  NMEAGpsField_t latLongGpsMinutesField;
} nmeaLatLongCommon_t;

// Latitude constants : many of these are reused for the longitude field
#define NMEA_FIELD_LATITUDE_DEGREES_MINIMUM_LENGTH         (1)
#define NMEA_FIELD_LATITUDE_DEGREES_MAXIMUM_LENGTH         (2)
#define NMEA_FIELD_LATITUDE_MINUTES_LENGTH                 (2)
#define NMEA_FIELD_LATITUDE_DEGREES_MINUTES_MINIMUM_LENGTH (NMEA_FIELD_LATITUDE_DEGREES_MINIMUM_LENGTH + NMEA_FIELD_LATITUDE_MINUTES_LENGTH)
#define NMEA_FIELD_LATITUDE_DEGREES_MINUTES_MAXIMUM_LENGTH (NMEA_FIELD_LATITUDE_DEGREES_MAXIMUM_LENGTH + NMEA_FIELD_LATITUDE_MINUTES_LENGTH)
#define NMEA_FIELD_LATITUDE_SEPERATOR_LENGTH               (1)
                                                           
#define NMEA_FIELD_LATITUDE_SEPERATOR                      NMEA_SENTENCE_DECIMAL_POINT
#define NMEA_FIELD_LATITUDE_SEPERATOR_STRING               NMEA_SENTENCE_DECIMAL_POINT_STRING
                                                           
#define NMEA_FIELD_LATITUDE_FRACTION_MINIMUM_LENGTH        (1) // reasonable ???
#define NMEA_FIELD_LATITUDE_FRACTION_MAXIMUM_LENGTH        (5) // reasonable ???
                                                           
#define NMEA_FIELD_LATITUDE_FRACTION_POWER_SHIFT           (7) // attempt to fill all the fractional digits
                                                           
#define NMEA_FIELD_LATITUDE_MINIMUM_LENGTH                 (NMEA_FIELD_LATITUDE_DEGREES_MINUTES_MINIMUM_LENGTH + \
                                                            NMEA_FIELD_LATITUDE_SEPERATOR_LENGTH       + \
                                                            NMEA_FIELD_LATITUDE_FRACTION_MINIMUM_LENGTH)
                                                           
#define NMEA_FIELD_LATITUDE_MAXIMUM_LENGTH                 (NMEA_FIELD_LATITUDE_DEGREES_MINUTES_MAXIMUM_LENGTH + \
                                                            NMEA_FIELD_LATITUDE_SEPERATOR_LENGTH       + \
                                                            NMEA_FIELD_LATITUDE_FRACTION_MAXIMUM_LENGTH)
                                                           
#define NMEA_MINUTES_PER_DEGREE                            ((NMEA_GPS_FLOAT)60.0)
                                                           
#define NMEA_FIELD_LATITUDE_MINIMUM_MINUTES                ((NMEA_GPS_FLOAT)0.0)
#define NMEA_FIELD_LATITUDE_MAXIMUM_MINUTES                ((NMEA_GPS_FLOAT)60.0)
                                                           
#define NMEA_FIELD_LATITUDE_FRACTIONAL_MINIMUM_MINUTES     (((NMEA_GPS_FLOAT)0.0)
#define NMEA_FIELD_LATITUDE_FRACTIONAL_MAXIMUM_MINUTES     (((NMEA_GPS_FLOAT)0.99999) // maximum NMEA fractional resolution is five digits (?)
                                                           
#define NMEA_FIELD_LATITUDE_MINIMUM_LIMIT                  ((NMEA_GPS_FLOAT)0.0)
#define NMEA_FIELD_LATITUDE_MAXIMUM_LIMIT                  ((NMEA_GPS_FLOAT)90.0)

// The latitude field has internal structure
typedef enum NMEASentenceLatitudeFieldStates_tTag
{
  NMEA_FIELD_LATITUDE_DEGREES_MINUTES_STATE = 0,
  NMEA_FIELD_LATITUDE_SEPERATOR_STATE,
  NMEA_FIELD_LATITUDE_FRACTION_MINUTES_STATE,
  NMEA_FIELD_LATITUDE_EXIT_STATE,
  NMEA_FIELD_LATITUDE_EMPTY_STATE,
  NMEA_FIELD_LATITUDE_FAILED_STATE,
  NMEA_FIELD_LATITUDE_STATES
} NMEASentenceLatitudeFieldStates_t;

// Longitude constants : only the differences from the latitude constants are needed
#define NMEA_FIELD_LONGITUDE_DEGREES_MINIMUM_LENGTH            (1)
#define NMEA_FIELD_LONGITUDE_DEGREES_MAXIMUM_LENGTH            (3)
#define NMEA_FIELD_LONGITUDE_DEGREES_MINUTES_MINIMUM_LENGTH    (NMEA_FIELD_LONGITUDE_DEGREES_MINIMUM_LENGTH + NMEA_FIELD_LATITUDE_MINUTES_LENGTH)
#define NMEA_FIELD_LONGITUDE_DEGREES_MINUTES_MAXIMUM_LENGTH    (NMEA_FIELD_LONGITUDE_DEGREES_MAXIMUM_LENGTH + NMEA_FIELD_LATITUDE_MINUTES_LENGTH)

#define NMEA_FIELD_LONGITUDE_FRACTION_MINIMUM_LENGTH           NMEA_FIELD_LATITUDE_FRACTION_MINIMUM_LENGTH
#define NMEA_FIELD_LONGITUDE_FRACTION_MAXIMUM_LENGTH           NMEA_FIELD_LATITUDE_FRACTION_MAXIMUM_LENGTH

#define NMEA_FIELD_LONGITUDE_MINIMUM_LENGTH                    (NMEA_FIELD_LONGITUDE_DEGREES_MINUTES_MINIMUM_LENGTH + \
                                                               NMEA_FIELD_LATITUDE_SEPERATOR_LENGTH       + \
                                                               NMEA_FIELD_LATITUDE_FRACTION_MINIMUM_LENGTH)
                                                              
#define NMEA_FIELD_LONGITUDE_MAXIMUM_LENGTH                    (NMEA_FIELD_LONGITUDE_DEGREES_MINUTES_MAXIMUM_LENGTH + \
                                                               NMEA_FIELD_LATITUDE_SEPERATOR_LENGTH       + \
                                                               NMEA_FIELD_LATITUDE_FRACTION_MAXIMUM_LENGTH)
                                                              
#define NMEA_FIELD_LONGITUDE_MINIMUM_LIMIT                     ((NMEA_GPS_FLOAT)0.0)
#define NMEA_FIELD_LONGITUDE_MAXIMUM_LIMIT                     ((NMEA_GPS_FLOAT)180.0)
                                                              
// Single character fields                                    
#define NMEA_FIELD_MAXIMUM_OPTIONS                             (NMEA_FIELD_FAA_MODES) // *** REVIEW ***  - as of 20.01.20

typedef enum NMEASentenceNorthingsField_tTag
{
  NMEA_FIELD_NORTHINGS_NORTH = 'N',
  NMEA_FIELD_NORTHINGS_SOUTH = 'S',
  NMEA_FIELD_NORTHINGS       =   2
} NMEASentenceNorthingsField_t;

typedef enum NMEASentenceEastingsField_tTag
{
  NMEA_FIELD_EASTINGS_EAST = 'E',
  NMEA_FIELD_EASTINGS_WEST = 'W',
  NMEA_FIELD_EASTINGS      =   2
} NMEASentenceEastingsField_t;

typedef enum NMEASentenceDataValidField_tTag
{
  NMEA_FIELD_DATA_VALID_VALID   = 'A',
  NMEA_FIELD_DATA_VALID_INVALID = 'V',
  NMEA_FIELD_DATA_VALIDS        =   2
} NMEASentenceDataValidField_t;

typedef enum nmeaSentenceFAAModeField_tTag
{
  NMEA_FIELD_FAA_MODE_AUTONOMOUS  = 'A',
  NMEA_FIELD_FAA_MODE_DGPS        = 'D',
  NMEA_FIELD_FAA_MODES            =   2
} nmeaSentenceFAAModeField_t;

typedef enum nmeaSentenceSModeField_tTag
{
  NMEA_FIELD_S_MODE_MANUAL = 'M',
  NMEA_FIELD_S_MODE_AUTO   = 'A',
  NMEA_FIELD_S_MODES       =    2
} nmeaSentenceSModeField_t;

typedef enum nmeaSentenceFixStatusField_tTag
{
  NMEA_FIELD_STATUS_NONE  = '1',
  NMEA_FIELD_STATUS_2D    = '2',
  NMEA_FIELD_STATUS_3D    = '3',
  NMEA_FIELD_FIX_STATUSES =    3
} nmeaSentenceFixStatusField_t;

/******************************************************************************/
/* UTC field and sub-fields                                                   */
/******************************************************************************/

#define NMEA_FIELD_UTC_SEPERATOR                       NMEA_SENTENCE_DECIMAL_POINT
#define NMEA_FIELD_UTC_SEPERATOR_STRING                NMEA_SENTENCE_DECIMAL_POINT_STRING


#define NMEA_FIELD_UTC_HOURS_LENGTH                     (2)
#define NMEA_FIELD_UTC_MINUTES_LENGTH                   (2)
#define NMEA_FIELD_UTC_SECONDS_LENGTH                   (2)

#define NMEA_FIELD_UTC_HHMMSS_LENGTH                    (NMEA_FIELD_UTC_HOURS_LENGTH   + \
                                                         NMEA_FIELD_UTC_MINUTES_LENGTH + \
                                                         NMEA_FIELD_UTC_SECONDS_LENGTH)

#define NMEA_FIELD_UTC_SEPERATOR_LENGTH                 (1)

#define NMEA_FIELD_UTC_CENTISECONDS_LENGTH              (2)

#define NMEA_FIELD_UTC_HOURS_MINIMUM                    (0)
#define NMEA_FIELD_UTC_HOURS_MAXIMUM                   (23)

#define NMEA_FIELD_UTC_MINUTES_MINIMUM                  (0)
#define NMEA_FIELD_UTC_MINUTES_MAXIMUM                 (59)

#define NMEA_FIELD_UTC_SECONDS_MINIMUM                  (0)
#define NMEA_FIELD_UTC_SECONDS_MAXIMUM                 (59)

#define NMEA_FIELD_UTC_CENTISECONDS_MINIMUM             (0)
#define NMEA_FIELD_UTC_CENTISECONDS_MAXIMUM            (99)

// Index into the state cache while the time is being assembled
typedef enum NMEASentenceUTCCacheIndicies_tTag
{
  UTC_HOURS_CACHE_INDEX = 0,
  UTC_MINUTES_CACHE_INDEX,
  UTC_SECONDS_CACHE_INDEX,
  UTC_CENTISECONDS_CACHE_INDEX,
  UTC_TIME_CACHE_INDICES
} NMEASentenceUTCCacheIndicies_t;

// The UTC field has internal structure
typedef enum NMEASentenceUTCFieldStates_tTag
{
  NMEA_FIELD_UTC_HOURS_STATE = 0,
  NMEA_FIELD_UTC_MINUTES_STATE,
  NMEA_FIELD_UTC_SECONDS_STATE,
  NMEA_FIELD_UTC_SEPERATOR_STATE,
  NMEA_FIELD_UTC_CENTISECONDS_STATE,
  NMEA_FIELD_UTC_EXIT_STATE,
  NMEA_FIELD_UTC_EMPTY_STATE,
  NMEA_FIELD_UTC_FAILED_STATE,
  NMEA_FIELD_UTC_STATES
} NMEASentenceUTCFieldStates_t;

// Date fields and sub-fields
#define NMEA_FIELD_DATE_JANUARY                         (1)
#define NMEA_FIELD_DATE_FEBRUARY                        (NMEA_FIELD_DATE_JANUARY   + 1)
#define NMEA_FIELD_DATE_MARCH                           (NMEA_FIELD_DATE_FEBRUARY  + 1)
#define NMEA_FIELD_DATE_APRIL                           (NMEA_FIELD_DATE_MARCH     + 1)
#define NMEA_FIELD_DATE_MAY                             (NMEA_FIELD_DATE_APRIL     + 1)
#define NMEA_FIELD_DATE_JUNE                            (NMEA_FIELD_DATE_MAY       + 1)
#define NMEA_FIELD_DATE_JULY                            (NMEA_FIELD_DATE_JUNE      + 1)
#define NMEA_FIELD_DATE_AUGUST                          (NMEA_FIELD_DATE_JULY      + 1)
#define NMEA_FIELD_DATE_SEPTEMBER                       (NMEA_FIELD_DATE_AUGUST    + 1)
#define NMEA_FIELD_DATE_OCTOBER                         (NMEA_FIELD_DATE_SEPTEMBER + 1)
#define NMEA_FIELD_DATE_NOVEMBER                        (NMEA_FIELD_DATE_OCTOBER   + 1)
#define NMEA_FIELD_DATE_DECEMBER                        (NMEA_FIELD_DATE_NOVEMBER  + 1)

#define NMEA_FIELD_DATE_DAYS_LENGTH                     (2)
#define NMEA_FIELD_DATE_MONTHS_LENGTH                   (2)
#define NMEA_FIELD_DATE_YEARS_LENGTH                    (2)
#define NMEA_FIELD_DATE_LEAP_YEARS_LENGTH               (4)                            // for full four-digit year fields

#define NMEA_FIELD_DATE_DAYS_MINIMUM                    (1)
#define NMEA_FIELD_DATE_DAYS_31_MAXIMUM                (31)                            // longest months
#define NMEA_FIELD_DATE_DAYS_MAXIMUM                   NMEA_FIELD_DATE_DAYS_31_MAXIMUM // coarse "days" check
#define NMEA_FIELD_DATE_DAYS_30_MAXIMUM                (30)                            // mostly shortest months
#define NMEA_FIELD_DATE_DAYS_LEAP_MAXIMUM              (29)                            // February leap-years
#define NMEA_FIELD_DATE_DAYS_28_MAXIMUM                (28)                            // february non-leap years

#define NMEA_FIELD_DATE_MONTHS_MINIMUM                  (1)
#define NMEA_FIELD_DATE_MONTHS_MAXIMUM                 (12)

#define NMEA_FIELD_DATE_YEARS_MINIMUM                   (0)                            // the NMEA date:year field is tow digits only
#define NMEA_FIELD_DATE_YEARS_MAXIMUM                  (99)

#define NMEA_FIELD_DATE_LEAP_YEARS_BASE               (1900)                            // this range is used to randomise the leap-
#define NMEA_FIELD_DATE_LEAP_YEARS_END                (2099)                            // years

#define NMEA_FIELD_DATE_LEAP_YEARS                       (4)                            // years divisible by 4 (no-remainder) MAY be leap-years
#define NMEA_FIELD_DATE_LEAP_YEARS_CENTURIES           (100)                            // years divisible by 100 (no-remainder) are NOT leap-years...
#define NMEA_FIELD_DATE_LEAP_YEARS_QUAD_CENTURIES      (400)                            // ...unless they are divisible by 400 (no-remainder)!

// The date field has internal structure
typedef enum NMEASentenceDateFieldStates_tTag
{
  NMEA_FIELD_DATE_DAYS_STATE  = 0,
  NMEA_FIELD_DATE_MONTHS_STATE,
  NMEA_FIELD_DATE_YEARS_STATE,
  NMEA_FIELD_DATE_EXIT_STATE,
  NMEA_FIELD_DATE_FAILED_STATE,
  NMEA_FIELD_DATE_STATES
} NMEASentenceDateFieldStates_t;

// Utc Time indices into the sentence capture state general number cache
typedef enum nmeaDateCacheIndicies_tTag
{
  UTC_DAYS_CACHE_INDEX = 0,
  UTC_MONTHS_CACHE_INDEX,
  UTC_YEARS_CACHE_INDEX,
  UTC_DATE_CACHE_INDICES
} nmeaDateCacheIndicies_t;

/******************************************************************************/
/* Satellite Id fields :                                                      */
/******************************************************************************/

#define NMEA_FIELD_SATELLITE_ID_LENGTH                   (2)

typedef enum nmeaSatelliteIdFieldStates_tTag
  {
  NMEA_FIELD_SATELLITE_ID_STATE = 0,
  NMEA_FIELD_SATELLITE_ID_EXIT_STATE,
  NMEA_FIELD_SATELLITE_ID_FAILED_STATE,
  NMEA_FIELD_SATELLITE_ID_STATES
  } nmeaSatelliteIdFieldStates_t;

// Satellite ID indices into the sentence capture state general number cache
typedef enum nmeaSatelliteIdCacheIndicies_tTag
  {
  SATELLITE_ID_IDENTIFIER_CACHE_INDEX,
  SATELLITE_ID_CACHE_INDICES
  } nmeaSatelliteIdCacheIndicies_t;

/******************************************************************************/
/* RMC constants and definitions :                                            */
/******************************************************************************/

// Speed over ground constants
#define NMEA_SOG_INTEGER_PART_MINIMUM_LENGTH      (1)
#define NMEA_SOG_INTEGER_PART_MAXIMUM_LENGTH      (3) // seems to always be three characters
                                                  
#define NMEA_SOG_FRACTIONAL_PART_MINIMUM_LENGTH   (1) // seems to always be one character
#define NMEA_SOG_FRACTIONAL_PART_MAXIMUM_LENGTH   (5)
                                                  
#define NMEA_SOG_INTEGER_MINIMUM_LIMIT            (0)
#define NMEA_SOG_INTEGER_MAXIMUM_LIMIT          (749) // fast civil airliner

#define NMEA_SOG_FRACTIONAL_MINIMUM_LIMIT         (0)
#define NMEA_SOG_FRACTIONAL_MAXIMUM_LIMIT         (9) // in practise this field seems to be one character only

#define NMEA_SOG_FIELD_LENGTH                     (5) // <integer : 3> + <'.' : 1> + <fractional : 1>

#define NMEA_SPEED_OVER_GROUND_MINIMUM            ((NMEA_GPS_FLOAT)0.0)
#define NMEA_SPEED_OVER_GROUND_MAXIMUM            ((NMEA_GPS_FLOAT)749.9)

// Course over ground constants
#define NMEA_COG_INTEGER_PART_MINIMUM_LENGTH      (1)
#define NMEA_COG_INTEGER_PART_MAXIMUM_LENGTH      (3) // seems to always be three characters
                                                  
#define NMEA_COG_FRACTIONAL_PART_MINIMUM_LENGTH   (1) // seems to always be one character
#define NMEA_COG_FRACTIONAL_PART_MAXIMUM_LENGTH   (5)

#define NMEA_COG_INTEGER_MINIMUM_LIMIT            (0)
#define NMEA_COG_INTEGER_MAXIMUM_LIMIT          (359) // degrees

#define NMEA_COG_FRACTIONAL_MINIMUM_LIMIT         (0)
#define NMEA_COG_FRACTIONAL_MAXIMUM_LIMIT         (9)

#define NMEA_COG_FIELD_LENGTH                     (5) // <integer : 3> + <'.' : 1> + <fractional : 1>

#define NMEA_COURSE_OVER_GROUND_MINIMUM           ((NMEA_GPS_FLOAT)0.0)
#define NMEA_COURSE_OVER_GROUND_MAXIMUM           ((NMEA_GPS_FLOAT)359.9)

// Magnetic variation constants
#define NMEA_MAV_INTEGER_PART_MINIMUM_LENGTH      (1)
#define NMEA_MAV_INTEGER_PART_MAXIMUM_LENGTH      (3) // seems to always be three characters

#define NMEA_MAV_FRACTIONAL_PART_MINIMUM_LENGTH   (1) // seems to always be one character
#define NMEA_MAV_FRACTIONAL_PART_MAXIMUM_LENGTH   (5)

#define NMEA_MAV_INTEGER_MINIMUM_LIMIT            (0)
#define NMEA_MAV_INTEGER_MAXIMUM_LIMIT          (179) // degrees

#define NMEA_MAV_FRACTIONAL_MINIMUM_LIMIT         (0)
#define NMEA_MAV_FRACTIONAL_MAXIMUM_LIMIT         (9)

#define NMEA_MAV_FIELD_LENGTH                     (5) // <integer : 3> + <'.' : 1> + <fractional : 1>

#define NMEA_MAGNETIC_VARIATION_MINIMUM           ((NMEA_GPS_FLOAT)0.0)
#define NMEA_MAGNETIC_VARIATION_MAXIMUM           ((NMEA_GPS_FLOAT)179.9)

/******************************************************************************/
/* GSA constants and definitions :                                            */
/******************************************************************************/

//#define NMEA_ACTIVE_SATELLITES_MINIMUM            (0) - kept in "NMEAGps.h"
//#define NMEA_ACTIVE_SATELLITES_MAXIMUM           (12)

#define NMEA_ACITVE_SATELLITES_FIELD_WIDTH        (2)
#define NMEA_ACITVE_SATELLITES_ASCII_FIELD_WIDTH  (NMEA_ACITVE_SATELLITES_FIELD_WIDTH + 1)

#define NMEA_DOP_INTEGER_PART_MINIMUM_LENGTH      (1)
#define NMEA_DOP_INTEGER_PART_MAXIMUM_LENGTH      (2)

#define NMEA_DOP_FRACTIONAL_PART_MINIMUM_LENGTH   (1)
#define NMEA_DOP_FRACTIONAL_PART_MAXIMUM_LENGTH   (2)

#define NMEA_DOP_INTEGER_MINIMUM_LIMIT            (0)
#define NMEA_DOP_INTEGER_MAXIMUM_LIMIT           (99)

#define NMEA_DOP_FRACTIONAL_MINIMUM_LIMIT         (0)
#define NMEA_DOP_FRACTIONAL_MAXIMUM_LIMIT         (9)

#define NMEA_DOP_MINIMUM                          ((NMEA_GPS_FLOAT)0.5)
#define NMEA_DOP_MAXIMUM                          ((NMEA_GPS_FLOAT)99.9)

/******************************************************************************/
/* GSV constants and definitions :                                            */
/******************************************************************************/

#define NMEA_GSV_NUMBER_OF_SENTENCES_CACHE_INDEX    (0) // Offsets for message-tracking data
#define NMEA_GSV_CURRENT_NUMBER_CACHE_INDEX         (NMEA_GSV_NUMBER_OF_SENTENCES_CACHE_INDEX + 1)
#define NMEA_GSV_SATELLITES_IN_VIEW_CACHE_INDEX     (NMEA_GSV_CURRENT_NUMBER_CACHE_INDEX      + 1)
#define NMEA_GSV_SATELLITES_STATISTICS_CACHE_INDEX  (NMEA_GSV_SATELLITES_IN_VIEW_CACHE_INDEX  + 1)
                               
// This counter indicates the current block and current variable to parse 
//  - block = (counter / 4) : variable = (counter % 4)
#define NMEA_GSV_SATELLITE_BLOCK_COUNTER_START      ((NMEA_GPS_INT16)-1)

#define NMEA_GSV_NUMBER_OF_SATELLITES_FIELD_WIDTH   (2)
                                                    
#define NMEA_GSV_SATELLITES_IN_VIEW_INTEGER_LENGTH  (2)
#define NMEA_GSV_SATELLITES_IN_VIEW_MINIMUM_LENGTH  (1)
#define NMEA_GSV_SATELLITES_IN_VIEW_MAXIMUM_LENGTH  (2)

#define NMEA_GSV_SATELLITES_IN_VIEW_MINIMUM         (NMEA_GSV_MINIMUM_NUMBER_OF_SATELLITES)
#define NMEA_GSV_SATELLITES_IN_VIEW_MAXIMUM         (NMEA_GSV_MAXIMUM_NUMBER_OF_SATELLITES)
                                                    
#define NMEA_GSV_SATELLITE_ID_MINIMUM_LENGTH        (2)
#define NMEA_GSV_SATELLITE_ID_MAXIMUM_LENGTH        (2)

#define NMEA_GSV_SATELLITE_ID_MINIMUM              NMEA_ACITVE_SATELLITES_MINIMUM_ID
#define NMEA_GSV_SATELLITE_ID_MAXIMUM              NMEA_ACITVE_SATELLITES_MAXIMUM_ID

#define NMEA_GSV_SATELLITE_ELEVATION_MINIMUM_LENGTH (1)
#define NMEA_GSV_SATELLITE_ELEVATION_MAXIMUM_LENGTH (2)

#define NMEA_GSV_ELEVATION_MINIMUM                (-90)
#define NMEA_GSV_ELEVATION_MAXIMUM                 (90)
#define NMEA_GSV_ELEVATION_FIELD_WIDTH              (2)
                
#define NMEA_GSV_SATELLITE_AZIMUTH_MINIMUM_LENGTH   (1)
#define NMEA_GSV_SATELLITE_AZIMUTH_MAXIMUM_LENGTH   (3)

#define NMEA_GSV_AZIMUTH_MINIMUM                    (0)
#define NMEA_GSV_AZIMUTH_MAXIMUM                  (359)
#define NMEA_GSV_AZIMUTH_FIELD_WIDTH                (3)
                     
#define NMEA_GSV_SATELLITE_C_N0_MINIMUM_LENGTH      (1)
#define NMEA_GSV_SATELLITE_C_N0_MAXIMUM_LENGTH      (2)

#define NMEA_GSV_C_N0_MINIMUM                       (0)
#define NMEA_GSV_C_N0_MAXIMUM                      (99)
#define NMEA_GSV_C_N0_FIELD_WIDTH                   (2)
                                                 
typedef enum nmeaGSVSatelliteStatistics_tTag
  {
  NMEA_GSV_STATISTIC_SATELLITE_ID = 0,
  NMEA_GSV_STATISTIC_SATELLITE_ELEVATION,
  NMEA_GSV_STATISTIC_SATELLITE_AZIMUTH,
  NMEA_GSV_STATISTIC_SATELLITE_C_N0,
  NMEA_GSV_STATISTIC_SATELLITE_FIELDS
  } nmeaGSVSatelliteStatistics_t;

/******************************************************************************/
/* GGA constants and definitions :                                            */
/******************************************************************************/

#define NMEA_GGA_NUMBER_OF_SATELLITES_MINIMUM             NMEA_ACTIVE_SATELLITES_MINIMUM
#define NMEA_GGA_NUMBER_OF_SATELLITES_MAXIMUM             NMEA_ACTIVE_SATELLITES_MAXIMUM
                                                          
#define NMEA_GGA_NUMBER_OF_SATELLITES_FIELD_WIDTH         (2)
#define NMEA_GGA_NUMBER_OF_SATELLITES_MINIMUM_FIELD_WIDTH (1)
#define NMEA_GGA_NUMBER_OF_SATELLITES_MAXIMUM_FIELD_WIDTH NMEA_GGA_NUMBER_OF_SATELLITES_FIELD_WIDTH

#define NMEA_GGA_ALTITUDE_INTEGER_PART_MINIMUM_LENGTH     (1)
#define NMEA_GGA_ALTITUDE_INTEGER_PART_MAXIMUM_LENGTH     (7)
#define NMEA_GGA_ALTITUDE_INTEGER_MAXIMUM_LIMIT           (9999999) // maximum elevation
#define NMEA_GGA_ALTITUDE_INTEGER_MINIMUM_LIMIT           (-NMEA_GGA_ALTITUDE_INTEGER_MAXIMUM_LIMIT)  // reference centre of the earth in metres

#define NMEA_GGA_ALTITUDE_FRACTIONAL_PART_MINIMUM_LENGTH  (1)
#define NMEA_GGA_ALTITUDE_FRACTIONAL_PART_MAXIMUM_LENGTH  (1)
#define NMEA_GGA_ALTITUDE_FRACTIONAL_MINIMUM_LIMIT        (0)
#define NMEA_GGA_ALTITUDE_FRACTIONAL_MAXIMUM_LIMIT        (9)
                                                          
#define NMEA_GGA_ALTITUDE_MAXIMUM                         ((NMEA_GPS_FLOAT)9999999.9)
#define NMEA_GGA_ALTITUDE_MINIMUM                         (-NMEA_GGA_ALTITUDE_MAXIMUM)

#define NMEA_GGA_GEOID_INTEGER_PART_MINIMUM_LENGTH        (1)
#define NMEA_GGA_GEOID_INTEGER_PART_MAXIMUM_LENGTH        (7)
#define NMEA_GGA_GEOID_INTEGER_MAXIMUM_LIMIT              (9999999) // maximum elevation
#define NMEA_GGA_GEOID_INTEGER_MINIMUM_LIMIT              (-NMEA_GGA_GEOID_INTEGER_MAXIMUM_LIMIT)  // reference centre of the earth in metres
                                                          
#define NMEA_GGA_GEOID_FRACTIONAL_PART_MINIMUM_LENGTH     (1)
#define NMEA_GGA_GEOID_FRACTIONAL_PART_MAXIMUM_LENGTH     (1)
#define NMEA_GGA_GEOID_FRACTIONAL_MINIMUM_LIMIT           (0)
#define NMEA_GGA_GEOID_FRACTIONAL_MAXIMUM_LIMIT           (9)

#define NMEA_GGA_GEOID_MAXIMUM                            ((NMEA_GPS_FLOAT)9999999.9)
#define NMEA_GGA_GEOID_MINIMUM                            (-NMEA_GGA_GEOID_MAXIMUM)

#define NMEA_GGA_DGPS_AGE_INTEGER_PART_MINIMUM_LENGTH     (1)
#define NMEA_GGA_DGPS_AGE_INTEGER_PART_MAXIMUM_LENGTH     (2)
#define NMEA_GGA_DGPS_AGE_INTEGER_MINIMUM_LIMIT           (0)
#define NMEA_GGA_DGPS_AGE_INTEGER_MAXIMUM_LIMIT          (99)

#define NMEA_GGA_DGPS_AGE_MINIMUM                         NMEA_GGA_DGPS_AGE_INTEGER_MINIMUM_LIMIT
#define NMEA_GGA_DGPS_AGE_MAXIMUM                         NMEA_GGA_DGPS_AGE_INTEGER_MAXIMUM_LIMIT

#define NMEA_GGA_DGPS_BASE_INTEGER_PART_MINIMUM_LENGTH    (1)
#define NMEA_GGA_DGPS_BASE_INTEGER_PART_MAXIMUM_LENGTH    (4)
#define NMEA_GGA_DGPS_BASE_INTEGER_MINIMUM_LIMIT          (0)
#define NMEA_GGA_DGPS_BASE_INTEGER_MAXIMUM_LIMIT       (9999)

#define NMEA_GGA_DGPS_BASE_MINIMUM                        NMEA_GGA_DGPS_BASE_INTEGER_MINIMUM_LIMIT
#define NMEA_GGA_DGPS_BASE_MAXIMUM                        NMEA_GGA_DGPS_BASE_INTEGER_MAXIMUM_LIMIT

typedef enum nmeaSentenceFixQualityField_tTag
{
  NMEA_FIELD_FIX_QUALITY_INVALID        = '0',
  NMEA_FIELD_FIX_QUALITY_GPS            = '1',
  NMEA_FIELD_FIX_QUALITY_DGPS           = '2',
  NMEA_FIELD_FIX_QUALITY_DEAD_RECKONING = '6',
  NMEA_FIELD_FIX_QUALITIES              =    4
} nmeaSentenceFixQualityField_t;

typedef enum nmeaSentenceLengthField_tTag
  {
  NMEA_FIELD_METRES     = 'M',
  NMEA_FIELD_LENGTHS    =    1
  } nmeaSentenceLengthField_tTag;

#define NMEA_SENTENCE_FIXED_FIELD_METRES_STRING        ("M")
#define NMEA_SENTENCE_FIXED_FIELD_METRES_LENGTH          (1)

/******************************************************************************/

// A subset of the complete NMEA 0183 talkers
typedef enum NMEASentenceTalkers_tTag
{
  NMEA_BD = 0, // BeiDou
  NMEA_CC,     // computer sentences
  NMEA_GA,     // Galileo
  NMEA_GL,     // GLONASS
  NMEA_GN,     // mixed GSPS and GLONASS
  NMEA_GP,     // GPS sentences
  NMEA_UP,     // microcontroller sentences
  NMEA_TALKERS
} NMEASentenceTalkers_t;

// Aggregation of the talker codes paired as ordinals and strings
typedef struct NMEATalkers_tTag
{
  NMEASentenceTalkers_t NMEATalkerCode;
  char                  NMEATalkerPrefix[NMEA_SENTENCE_TALKER_MAXIMUM_LENGTH];
} NMEATalkers_t;

// ublox NEO 6M default GPS sentences @ 9600 baud
typedef enum NMEASentenceTypes_tTag
{
  NMEA_NIL = -1,
  NMEA_GSV =  0,
  NMEA_RMC,
  NMEA_GSA,
  NMEA_GGA,
  NMEA_GLL,
#if _NMEA_SENTENCE_VTG_
  NMEA_VTG,
#endif
#if _NMEA_SENTENCE_TXT_
  NMEA_TXT,
#endif
  NMEA_SENTENCES
} NMEASentenceTypes_t;

// Sentence attributes - these are defined as exclusive binary numbers for easy 
// multiplicity (if required)
typedef enum nmeaSentenceFieldAttributes_tTag
{
  NMEA_FIELD_ATTRIBUTE_FIXED_LENGTH    = 1,
  NMEA_FIELD_ATTRIBUTE_VARIABLE_LENGTH = (NMEA_FIELD_ATTRIBUTE_FIXED_LENGTH << 1),
  NMEA_FIELD_ATTRIBUTES                = 2
} nmeaSentenceFieldAttributes_t;


typedef enum nmeaSentenceTupleLength_tTag
{
  NMEA_NIL_SENTENCE_TUPLE_LENGTH = 0,
  NMEA_GSV_SENTENCE_TUPLE_LENGTH = 4,
  NMEA_SENTENCE_TUPLE_LENGTHS    = 2
} nmeaSentenceTupleLength_tT;


// List of the sentences' number of fields, between the prefix <$><TT><SSS> and the checksum + terminators 
// <*><CC><CR><LF> and NOT including the number of ',' characters
typedef enum nmeaSentenceLengths_tTag
{
  NMEA_NIL_SENTENCE_LENGTH  =  0,
  NMEA_GSV_SENTENCE_LENGTH  = 19, // Fixed number of fields per sentence of (possibly) MULITPLE sentences
  NMEA_RMC_SENTENCE_LENGTH  = 12,
  NMEA_GSA_SENTENCE_LENGTH  = 17,
  NMEA_GGA_SENTENCE_LENGTH  = 14,
  NMEA_GLL_SENTENCE_LENGTH  =  7,
  NMEA_VTG_SENTENCE_LENGTH  =  9,
  NMEA_TXT_SENTENCE_LENGTH  =  0, // not currently decoded
  NMEA_TXT_SENTENCE_LENGTHS =  8
} nmeaSentenceLengths_t;

// List of states that link successive fields in a single sentence e.g. trigger in-field index 
// initialisation and incrementing through successive common field calls
typedef enum nmeaSentenceCommonFieldStates_tTag
  {
  NMEA_SENTENCE_COMMON_FIELD_STATE_INITIALISATION = 0,
  NMEA_SENTENCE_COMMON_FIELD_STATE_INDEXING,
  NMEA_SENTENCE_COMMON_FIELD_STATE_TERMINATION,
  NMEA_SENTENCE_COMMON_FIELD_STATES
  } nmeaSentenceCommonFieldStates_t;

// The parsing states of the outer parser
typedef enum NMEASentenceCaptureStates_tTag
{
  NMEA_SENTENCE_CAPTURE_STATE_INITIALISATION = 0,
  NMEA_SENTENCE_CAPTURE_STATE_SEARCHING,
  NMEA_SENTENCE_CAPTURE_STATE_MATCH_TALKER,
  NMEA_SENTENCE_CAPTURE_STATE_MATCH_SENTENCE,
  NMEA_SENTENCE_CAPTURE_STATE_PARSE_COMMA,
  NMEA_SENTENCE_CAPTURE_STATE_PARSE_SENTENCE,
  NMEA_SENTENCE_CAPTURE_STATE_COMPARE_CHECKSUMS,
  NMEA_SENTENCE_CAPTURE_STATE_COMPLETE,
  NMEA_SENTENCE_CAPTURE_STATE_PASSED,
  NMEA_SENTENCE_CAPTURE_STATE_FAILED,
  NMEA_SENTENCE_CAPTURE_STATES
} NMEASentenceCaptureStates_t;

// Any valid NMEA sentence is a combination (in no particular oder) of one or 
// more of the states corresponding to the field type being parsed
typedef enum NMEASentenceStates_tTag
{
  NMEA_SENTENCE_STATE_PARSE_FIELD,
  NMEA_SENTENCE_STATE_PARSE_COMMA,
  NMEA_SENTENCE_STATE_PARSE_SEPERATOR,
  NMEA_SENTENCE_STATE_IN_FIELD,
  NMEA_SENTENCE_STATE_EXIT_FIELD,
//  NMEA_SENTENCE_STATE_LATITUDE,
//  NMEA_SENTENCE_STATE_LONGITUDE,
//  NMEA_SENTENCE_STATE_NORTHINGS,
//  NMEA_SENTENCE_STATE_EASTINGS,
//  NMEA_SENTENCE_STATE_UTC,
//  NMEA_SENTENCE_STATE_DATA_STATUS,
//  NMEA_SENTENCE_STATE_FAA_MODE,
  NMEA_SENTENCE_STATE_COMPLETE,
  NMEA_SENTENCE_STATE_FAILED,
  NMEA_SENTENCE_STATE_INITIALISATION = 0x400, // move it well out of the way!
  NMEA_SENTENCE_STATES = 8
} NMEASentenceStates_t;

// This enumeration is a placeholder for special-to-type enumerations used 
// when parsing fields with internal structure. It MUST be at least as big 
// as the largest special-to-type enumeration!
typedef enum NMEASentenceGenericStates_tTag
{
  NMEA_SENTENCE_STATE_GENERIC_0 = 0,
  NMEA_SENTENCE_STATE_GENERIC_1,
  NMEA_SENTENCE_STATE_GENERIC_2,
  NMEA_SENTENCE_STATE_GENERIC_3,
  NMEA_SENTENCE_STATE_GENERIC_4,
  NMEA_SENTENCE_STATE_GENERIC_5,
  NMEA_SENTENCE_STATE_GENERIC_6,
  NMEA_SENTENCE_STATE_GENERIC_7,
  NMEA_SENTENCE_STATE_GENERIC_8,
  NMEA_SENTENCE_STATE_GENERIC_INITIALISATION = NMEA_SENTENCE_STATE_INITIALISATION,
  NMEA_SENTENCE_STATE_GENERICS = 9
} NMEASentenceGenericStates_t;

// Aggregation of the complete sentence state during parsing
typedef struct NMEASentenceCaptureState_tTag
{
  NMEASentenceCaptureStates_t      nmeaSentenceOuterState;       // the outer parser deals with '$' character(s), talker codes,
                                                                 // sentence codes and the received checksum
  NMEASentenceStates_t             nmeaSentenceInnerState;       // the inner parsers decode particular sentence types
  NMEASentenceGenericStates_t      nmeaSentenceGenericState;     // used to handle parsing of fields with internal structure
  nmeaSentenceCommonFieldStates_t  nmeaSentenceCommonFieldState; // linking states between fields in a single sentence
  NMEA_GPS_UINT8                   nmeaSentenceNewCharacters;    // new characters passed into the capture/decoder
  NMEA_GPS_UINT8                   nmeaSentenceCharactersUsed;   // index into the new characters
  NMEA_GPS_UINT8                   nmeaSentenceMatchLength;      // length of a pattern match
  NMEA_GPS_UINT8                   nmeaSentenceMatchIndex;       // index into a pattern match
  NMEA_GPS_UINT8                   nmeaSentenceFields;           // the number of fields/',' (comma) characters in the sentence 
                                                                 // being decoded
  NMEA_GPS_UINT8                   nmeaSentenceCharacterIndex;   // running total of valid characters in the current string
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
  NMEA_GPS_UCHAR                   nmeaSentence[NMEA_SENTENCE_MAXIMUM_LENGTH_STRING]; // for debug purposes rebuild the parsed string on-the-fly
#endif
  NMEA_GPS_UCHAR                   nmeaSentenceScratch[NMEA_SENTENCE_SCRATCH_MAXIMUM_LENGTH_STRING]; // text character cache
  nmeaNumericalVariant_t           nmeaSentenceNumberScratch[NMEA_SENTENCE_NUMBER_SCRATCH_LENGTH];   // a small cache of numbers-in-building
  nmeaVariableParser_t             nmeaSentenceVariableState;    // sub-parser for finding variable-type values
  bool                             nmeaSentenceEscapeFlag;
  NMEASentenceTypes_t              nmeaSentenceType;             // sentence type to-be/currently-being parse(d)
  NMEA_GPS_BOOLEAN                 multipleSentenceFlag;         // if a sentence is actually a group of messages (e.g. "GSV") this flag 
                                                                 // signals that state and stops the cache "nmeaSentenceNumberScratch" being 
                                                                 // reset between messages
  NMEA_GPS_INT16                   nmeaSentenceStateCounter;     // the state counter is used to correctly increment repeating groups of 
                                                                 // states : one or more consecutive ',' characters represent missing states!
                                                                 // To keep a repeating group of states correctly indexed the states need to 
                                                                 // be tracked external to the sentence parser
  NMEA_GPS_UINT8                   nmeaSentenceComputedChecksum; // NMEA 0183 checksum is computed as a binary XOR of all 
                                                                 // the characters between '$' and '*'. The upper and lower
                                                                 // nybbles of the result are expanded into the ASCII
                                                                 // codes for '0' .. 'F' to transmit. In receive the two 
                                                                 // ASCII codes are restored to their nybbles. The ordering 
                                                                 // in the sentence is (after the '*') <HIGH-NYBBLE><LOW-NYBBLE>
} NMEASentenceCaptureState_t;

// Aggregation of the sentence codes paired as ordinals and strings
typedef struct NMEATypes_tTag
{
  NMEASentenceTypes_t             NMEASentenceCode;
  NMEA_GPS_UCHAR                  NMEASentenceType[NMEA_SENTENCE_TYPE_MAXIMUM_LENGTH];
  // Corresponding to the field list (again NOT ','s)
  NMEA_GPS_UINT8                  nmeaSentenceFieldLength;    // the FIXED number of fields after the prefix <$><TT><SSS>
  nmeaSentenceFieldAttributes_t   nmeaSentenceFieldAttribute; // flag if any VARIABLE fields occur after the fixed fields
                                                              // List of the field/state decoder functions and attributes NOT including the seperating ','s
                                                              // '*domainHandling' is a catch-all to allow field-specific parameters or handlers to be
                                                              // passed
  NMEASentenceStates_t            (**nmeaSentenceFieidList)(const NMEA_GPS_UCHAR             *charactersToParse, // each field parser is called in a uniform
                                                                  NMEASentenceCaptureState_t *captureState,      // manner
                                                                  void                       *domainHandling);
  NMEA_GPS_UINT8                  nmeaSentenceGroupLength;    // if the sentence is of variable length this field holds the 
                                                              // number fields in each variable group (n-tuple)
  NMEASentenceStates_t            (**nmeaSentenceGroupList)(const NMEA_GPS_UCHAR             *charactersToParse, // each variable field parser is called in a 
                                                                  NMEASentenceCaptureState_t *captureState,      // uniform manner
                                                                  void                       *domainHandling);
  void                            (*NMEASentenceDomainHandler)(      NMEAGpsUpdate_t *gpsUpdate,
                                                               const NMEAGpsField_t   gpsField);
} NMEATypes_t;

/******************************************************************************/

extern const NMEA_GPS_UCHAR               nmeaSentenceTalkerMaximumLength;
extern const NMEA_GPS_UCHAR               nmeaSentenceTypesMaximumLength;
                                          
extern const NMEATalkers_t                nmeaTalkers[NMEA_TALKERS];
extern const NMEATypes_t                  nmeaTypes[NMEA_SENTENCES];
                                          
extern       NMEASentenceCaptureState_t   activeSentenceState;

extern const NMEA_GPS_UCHAR               nmeaNorthings[NMEA_FIELD_NORTHINGS];
extern const NMEA_GPS_UCHAR               nmeaEastings[NMEA_FIELD_EASTINGS];
extern const NMEA_GPS_UCHAR               nmeaDataValid[NMEA_FIELD_DATA_VALIDS];
extern const NMEA_GPS_UCHAR               nmeaFAAMode[NMEA_FIELD_FAA_MODES];
extern const NMEA_GPS_UCHAR               nmeaSMode[NMEA_FIELD_S_MODES];
extern const NMEA_GPS_UCHAR               nmeaFixMode[NMEA_FIELD_FIX_STATUSES];
extern const NMEA_GPS_UCHAR               nmeaFixQuality[NMEA_FIELD_FIX_QUALITIES];
extern const NMEA_GPS_UCHAR               nmeaMetres[NMEA_FIELD_LENGTHS];

/******************************************************************************/
/* Declaration of the sentence parser function chains                         */
/******************************************************************************/

extern NMEASentenceStates_t (*nmeaSentenceParserGLL[NMEA_GLL_SENTENCE_LENGTH])(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                                     NMEASentenceCaptureState_t *captureState,
                                                                                     void                       *domainHandling);
extern NMEASentenceStates_t (*nmeaSentenceParserGSV[NMEA_GSV_SENTENCE_LENGTH])(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                                     NMEASentenceCaptureState_t *captureState,
                                                                                     void                       *domainHandling);
extern NMEASentenceStates_t (*nmeaSentenceTupleParserGSV[NMEA_GSV_SENTENCE_TUPLE_LENGTH])(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                                                NMEASentenceCaptureState_t *captureState,
                                                                                                void                       *domainHandling);
extern NMEASentenceStates_t (*nmeaSentenceParserRMC[NMEA_RMC_SENTENCE_LENGTH])(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                                     NMEASentenceCaptureState_t *captureState,
                                                                                     void                       *domainHandling);
extern NMEASentenceStates_t (*nmeaSentenceParserGSA[NMEA_GSA_SENTENCE_LENGTH])(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                                     NMEASentenceCaptureState_t *captureState,
                                                                                     void                       *domainHandling);
extern NMEASentenceStates_t (*nmeaSentenceParserGGA[NMEA_GGA_SENTENCE_LENGTH])(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                                     NMEASentenceCaptureState_t *captureState,
                                                                                     void                       *domainHandling);

/******************************************************************************/

extern NMEASentenceCaptureStates_t      nmeaCaptureSentence(const NMEA_GPS_UCHAR             *newSentenceCharacters,
                                                            const NMEA_GPS_UINT8              numberOfCharacters,
                                                                  NMEASentenceCaptureState_t *sentenceState);
extern bool                             nmeaMatchTalker(const NMEA_GPS_UCHAR *matchCharacters,
                                                        const NMEA_GPS_UINT8  matchLength);
extern bool                             nmeaMatchSentence(const NMEA_GPS_UCHAR      *matchCharacters,
                                                          const NMEA_GPS_UINT8       matchLength,
                                                                NMEASentenceTypes_t *matchSentenceType);

extern bool                             nmeaComputeChecksum(const NMEA_GPS_UCHAR *matchedCharacters,
                                                            const NMEA_GPS_UINT8  matchedCharacterLength,
                                                                  NMEA_GPS_UINT8 *runningChecksum);
#ifdef _NMEA_INCLUDE_CHECKSUM_DEBUG_
extern NMEA_GPS_VOID                    initialiseChecksumTestBuffer(NMEA_GPS_UINT8 initialChecksum);
extern NMEA_GPS_VOID                    loadChecksumTestBuffer(NMEA_GPS_UINT8 latestChecksum);
extern NMEA_GPS_VOID                    printChecksumTestBuffer(NMEA_GPS_VOID);
#endif

extern NMEASentenceStates_t             nmeaParseSentence(const NMEA_GPS_UCHAR             *newSentenceCharacters,
                                                                NMEASentenceCaptureState_t *sentenceState);

extern NMEASentenceStates_t             nmeaLatitudeLongitudeParser(const nmeaSentenceLatLongSelector_t  latLongSelector,
                                                                    const NMEA_GPS_UCHAR                *charactersToParse,
                                                                          NMEASentenceCaptureState_t    *captureState,
                                                                          void                          *domainHandling);
extern NMEASentenceStates_t             nmeaNorthingsParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                  NMEASentenceCaptureState_t *captureState,
                                                                  void                       *domainHandling);
extern NMEASentenceStates_t             nmeaNorthingsParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                  NMEASentenceCaptureState_t *captureState,
                                                                  void                       *domainHandling);
extern NMEASentenceStates_t             nmeaEastingsParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                 NMEASentenceCaptureState_t *captureState,
                                                                 void                       *domainHandling);
extern NMEASentenceStates_t             nmeaLatitudeParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                 NMEASentenceCaptureState_t *captureState,
                                                                 void                       *domainHandling);
extern NMEASentenceStates_t             nmeaLongitudeParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                  NMEASentenceCaptureState_t *captureState,
                                                                  void                       *domainHandling);
extern NMEASentenceStates_t             nmeaFlagParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                             NMEASentenceCaptureState_t *captureState,
                                                             void                       *domainHandling,
                                                       const NMEA_GPS_UCHAR             *flagOptions,
                                                       const NMEA_GPS_UINT8              numberOfFlagOptions,
                                                       const NMEAGpsGroups_t             nmeaGpsGroup,
                                                       const NMEAGpsField_t              nmeaGpsField);
extern NMEASentenceStates_t             nmeaUTCParser(const NMEA_GPS_UCHAR            *charactersToParse,
                                                            NMEASentenceCaptureState_t *captureState,
                                                            void                       *domainHandling);
extern NMEASentenceStates_t             nmeaDataStatusParser(const NMEA_GPS_UCHAR       *charactersToParse,
                                                             NMEASentenceCaptureState_t *captureState,
                                                             void                       *domainHandling);
extern NMEASentenceStates_t             nmeaFAAModeParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                NMEASentenceCaptureState_t *captureState,
                                                                void                       *domainHandling);
extern NMEASentenceStates_t             nmeaOperatingModeParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                      NMEASentenceCaptureState_t *captureState,
                                                                      void                       *domainHandling);
extern NMEASentenceStates_t             nmeaFixModeParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                NMEASentenceCaptureState_t *captureState,
                                                                void                       *domainHandling);
extern NMEASentenceStates_t             nmeaFixQualityParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                   NMEASentenceCaptureState_t *captureState,
                                                                   void                       *domainHandling);
extern NMEASentenceStates_t             nmeaGGASatellitesInViewParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                            NMEASentenceCaptureState_t *captureState,
                                                                            void                       *domainHandling);
extern NMEASentenceStates_t             nmeaSatelliteIDParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                    NMEASentenceCaptureState_t *captureState,
                                                                    void                       *domainHandling);
#if (0)
extern NMEASentenceStates_t             nmeaGSVSatelliteIdParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                       NMEASentenceCaptureState_t *captureState,
                                                                       void                       *domainHandling);
extern NMEASentenceStates_t             nmeaGSVElevationParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                     NMEASentenceCaptureState_t *captureState,
                                                                     void                       *domainHandling);
extern NMEASentenceStates_t             nmeaGSVAzimuthParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                   NMEASentenceCaptureState_t *captureState,
                                                                   void                       *domainHandling);
extern NMEASentenceStates_t             nmeaGSVSNRdBParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                 NMEASentenceCaptureState_t *captureState,
                                                                 void                       *domainHandling);
#endif
extern NMEASentenceStates_t             nmeaGSVSatelliteFieldParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                          NMEASentenceCaptureState_t *captureState,
                                                                          void                       *domainHandling);
extern NMEASentenceStates_t             nmeaGSVGroupSizeParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                     NMEASentenceCaptureState_t *captureState,
                                                                     void                       *domainHandling);
extern NMEASentenceStates_t             nmeaGSVSentenceNumberParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                          NMEASentenceCaptureState_t *captureState,
                                                                          void                       *domainHandling);
extern NMEASentenceStates_t             nmeaGSVSatellitesInViewParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                            NMEASentenceCaptureState_t *captureState,
                                                                            void                       *domainHandling);
extern NMEASentenceStates_t             nmeaGSVSatelliteStatisticsParser(const NMEA_GPS_INT16                 satelliteStatisticsSelector,
                                                                        // const nmeaGSVSatelliteStatistics_t   satelliteStatisticsSelector,
                                                                         const NMEA_GPS_UCHAR                *charactersToParse,
                                                                               NMEASentenceCaptureState_t    *captureState,
                                                                               void                          *domainHandling);
extern NMEASentenceStates_t             nmeaSpeedOverGroundParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                        NMEASentenceCaptureState_t *captureState,
                                                                        void                       *domainHandling);
extern NMEASentenceStates_t             nmeaCourseOverGroundParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                         NMEASentenceCaptureState_t *captureState,
                                                                         void                       *domainHandling);
extern NMEASentenceStates_t             nmeaAltitudeParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                 NMEASentenceCaptureState_t *captureState,
                                                                 void                       *domainHandling);
extern NMEASentenceStates_t             nmeaGeoidSeperationParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                        NMEASentenceCaptureState_t *captureState,
                                                                        void                       *domainHandling);
extern NMEASentenceStates_t            nmeaDgpsCorrectionAgeParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                         NMEASentenceCaptureState_t *captureState,
                                                                         void                       *domainHandling);
extern NMEASentenceStates_t            nmeaDgpsBaseStationIdParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                         NMEASentenceCaptureState_t *captureState,
                                                                         void                       *domainHandling);
extern NMEASentenceStates_t             nmeaMetresParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                               NMEASentenceCaptureState_t *captureState,
                                                               void                       *domainHandling);
extern NMEASentenceStates_t             nmeaDateParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                             NMEASentenceCaptureState_t *captureState,
                                                             void                       *domainHandling);
extern NMEASentenceStates_t            nmeaMagneticVariationParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                         NMEASentenceCaptureState_t *captureState,
                                                                         void                       *domainHandling);
extern NMEASentenceStates_t            nmeaPositionpDopParser(const NMEA_GPS_UCHAR *charactersToParse,
                                                                    NMEASentenceCaptureState_t *captureState,
                                                                    void *domainHandling);
extern NMEASentenceStates_t            nmeaPositionhDopParser(const NMEA_GPS_UCHAR *charactersToParse,
                                                                    NMEASentenceCaptureState_t *captureState,
                                                                    void *domainHandling);
extern NMEASentenceStates_t            nmeaPositionvDopParser(const NMEA_GPS_UCHAR *charactersToParse,
                                                                    NMEASentenceCaptureState_t *captureState,
                                                                    void *domainHandling);
extern NMEASentenceGenericStates_t     nmeaVariableNumericFieldParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                            NMEASentenceCaptureState_t *captureState);

extern NMEA_GPS_VOID                   nmeaPrintCaptureState(const NMEASentenceCaptureStates_t captureState);
                                       
extern NMEA_GPS_BOOLEAN                nmeaPrintCapturedSentence(NMEA_GPS_UCHAR *activeSentence);


/******************************************************************************/

#endif

/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/
