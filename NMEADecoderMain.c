/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/
/*                                                                            */
/* NMEADecoderMain.c                                                          */
/* 23.12.19                                                                   */
/*                                                                            */
/* Ref : NMEA 0183 Specification v2.3                                         */
/*       ublox NEO-6 - Data Sheet                                             */
/*       SiRF NMEA Reference Manual Revision 2.1, Decemeber 2007              */
/*                                                                            */
/******************************************************************************/

#include <Windows.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include "NMEADecoder.h"
#include "NMEAEncoder.h"
#include "NMEATest.h"

/******************************************************************************/

#define NMEA_TEST_INITIAL                     (1)

#define NMEA_SEPERATING_LINE_CHARACTER      ('-')
#define NMEA_SEPERATING_LINE_MAXIMUM_LENGTH (128)

/******************************************************************************/
/* Decoder Variables :                                                        */
/******************************************************************************/

NMEA_GPS_UCHAR              nmeaSentence[NMEA_SENTENCE_MAXIMUM_LENGTH_STRING],
                            newSentence[NMEA_SENTENCE_MAXIMUM_LENGTH_STRING],

                            newEncoderSentence[NMEA_SENTENCE_MAXIMUM_LENGTH_STRING];

NMEA_GPS_UINT8              runningChecksum         = 0,
                            expectedChecksum        = 0,
                            bufferIndex             = 0,
                            sentenceIndex           = 0,
                            newEncoderSentenceIndex = 0;

NMEA_GPS_UINT32             testIndex               = 0;

NMEA_GPS_BOOLEAN            checksumSuccess         = false,
                            testSuccess             = true;

NMEA_GPS_INT                testExitCode            = 0;

NMEASentenceCaptureStates_t sentenceState           = NMEA_SENTENCE_CAPTURE_STATE_INITIALISATION;

/******************************************************************************/
/* Encoder Variables :                                                        */
/******************************************************************************/

tinymt32_t mtRandomState;

/******************************************************************************/
/* Test-specific functions :                                                  */
/******************************************************************************/

       int                         main(int argc, char *argv[]);
static NMEASentenceCaptureStates_t initialiseTests(NMEA_GPS_CHAR              *nmeaSentence,
                                                   NMEASentenceCaptureState_t *activeSentenceState);
static NMEA_GPS_BOOLEAN            nmeaLoadTestSentence(NMEA_GPS_CHAR *nmeaTestSentence,
                                                        NMEA_GPS_CHAR *nmeaTestBuffer,
                                                        NMEA_GPS_UINT8 *nmeaSentenceLength);
static NMEA_GPS_VOID               nmeaPrintSeperatingLine(NMEA_GPS_UINT8   lineLength,
                                                           NMEA_GPS_UCHAR   lineCharacter,
                                                           NMEA_GPS_BOOLEAN lineStartBreak,
                                                           NMEA_GPS_BOOLEAN lineEndBreak);
static NMEA_GPS_BOOLEAN            nmeaPrintTitleBlock(NMEA_GPS_UCHAR   *titleString,
                                                       NMEA_GPS_UINT16   titleOrdinal,
                                                       NMEA_GPS_UINT16   titleSubOrdinal,
                                                       NMEA_GPS_BOOLEAN  titleExitLane);

/******************************************************************************/

int main(int argc, char *argv[])
  {
  /******************************************************************************/

  NMEASentenceCaptureStates_t testCaptureState = NMEA_SENTENCE_CAPTURE_STATE_FAILED;

  NMEA_GPS_UINT32             testIndex        = NMEA_TEST_INITIAL,
                              testSubIndex     = NMEA_TEST_INITIAL,
                              singleIndex      = 0;

  /******************************************************************************/

  system("cls");

  printf("\n  NMEA Sentence Parser Test Harness: ");
  printf("\n  ---------------------------------\n");

/******************************************************************************/
/* Make the current fix "dirty" :                                             */
/******************************************************************************/

  gpsValidateFixGroup(&gpsLatestFix,
                       NMEA_GPS_ALL_GROUPS,
                       NMEA_GPS_FIELD_STATUS_INVALID);

  /******************************************************************************/
  /* Test 0_1 : test the numeric variable parser :                              */
  /******************************************************************************/

#if (0)
  nmeaPrintTitleBlock("test the numeric variable parser",
                      0,
                      0,
                      true);
#if (1)
  nmeaTestRandomVariableGenerator();
#endif

#if (0)
  nmeaTestSpeedOverGroundField();
#endif
#endif

#ifdef _NMEA_ACTIVE_SATELLITE_TEST_
  {
  tinymt32_t      randomSatellite;
  NMEA_GPS_UINT32 satelliteSeed    = MT_INITIAL_SEED;

  tinymt32_init(&randomSatellite, satelliteSeed);

  while (true)
    {
    newEncoderSentenceIndex = 0;

    initialiseSentence(&newEncoderSentence[0],
                            NMEA_SENTENCE_MAXIMUM_LENGTH_STRING,
                            NMEA_SENTENCE_END_OF_STRING);

    nmeaEncoderAddActiveSatellites(&newEncoderSentence[0],
                                   &newEncoderSentenceIndex,
                                   &randomSatellite,
                                   NMEA_ACTIVE_SATELLITES_MINIMUM,
                                   NMEA_ACTIVE_SATELLITES_MAXIMUM,
                                   NMEA_ACITVE_SATELLITES_MINIMUM_ID,
                                   NMEA_ACITVE_SATELLITES_MAXIMUM_ID,
                                   NMEA_ACITVE_SATELLITES_FIELD_WIDTH,
                                   NMEA_SENTENCE_FIELD_SEPERATOR);

    nmeaPrintCapturedSentence(&newEncoderSentence[0]);
    
    if (_kbhit())
      {
      testExitCode = _getch();
    
      if (testExitCode == NMEA_TEST_EXIT_CODE)
        {
        testExitCode = NMEA_SENTENCE_END_OF_STRING;
        break;
        }
      }
    }
  }
#endif

#define _NMEA_GENERATE_SCRAMBLED_NUMBERS_TEST_N_

#ifdef _NMEA_GENERATE_SCRAMBLED_NUMBERS_TEST_
#define NMEA_GENERATE_SCRAMBLED_NUMBERS_RANGE (127)

  {
  nmeaNumericalVariant_t numberBuffer[NMEA_GENERATE_SCRAMBLED_NUMBERS_RANGE];
  NMEA_GPS_UINT32 randomSeed = MT_INITIAL_SEED;

  nmeaGenerateScrambledSequentialNumbers(&numberBuffer[0],
                                          NMEA_GENERATE_SCRAMBLED_NUMBERS_RANGE,
                                          randomSeed);
  }

#endif

#define _NMEA_TEST_NUMBER_TO_ASCII_CONVERTOR_N_

#ifdef _NMEA_TEST_NUMBER_TO_ASCII_CONVERTOR_

  {
  NMEA_GPS_UCHAR  newSentence[NMEA_DECIMAL_EQUIVALENT_DIGITS + 1];
  NMEA_GPS_UINT8  newSentenceIndex = 0,
                  asciiIndex       = 0,
                  fieldWidth       = 2; // NMEA_DECIMAL_EQUIVALENT_DIGITS;
  NMEA_GPS_UINT32 newNumber        = 0; // 0x7fffffff;

  while (true)
    {
    // Clear the receiving sentence buffer
    for (asciiIndex = 0; asciiIndex <= NMEA_DECIMAL_EQUIVALENT_DIGITS; asciiIndex++)
      {
      newSentence[asciiIndex] = NMEA_SENTENCE_END_OF_STRING;
      }

    if (nmeaLoadIntegerAsDecimalAscii(&newSentence[0],
                                      &newSentenceIndex,
                                       newNumber,
                                       fieldWidth) == true)
      {
      if (atoi(newSentence) == newNumber)
        {
        if ((newNumber & 15) == 0)
          {
          printf("\n OK  : New number = %010d ~= Ascii version = %s", newNumber, newSentence);
          }
        }
      else
        {
        printf("\n NOK : New number = %010d ~= Ascii version = %s", newNumber, newSentence);
        break;
        }
      }

    newNumber = newNumber + 1;
    // newNumber = newNumber - 1;

    if (_kbhit())
      {
      testExitCode = _getch();
    
      if (testExitCode == NMEA_TEST_EXIT_CODE)
        {
        break;
        }
      }
    }
  }

#endif

#ifdef _NMEA_TEST_GGA_ENCODER_
  {
  NMEA_GPS_UCHAR                  newSentence[NMEA_SENTENCE_MAXIMUM_LENGTH_STRING];
  NMEA_GPS_UINT8                  newSentenceIndex = 0;
                                  
  tinymt32_t                      randomState;
  nmeaEncoderSentenceSeedScheme_t randomSeedScheme  = NMEA_ENCODER_SEED_SCHEME_TIME;

  nmeaEncoderGenerateSentenceGGA(&newSentence[0],
                                 &newSentenceIndex,
                                 &randomState,
                                  randomSeedScheme);

  newSentence[newSentenceIndex] = NMEA_SENTENCE_END_OF_STRING;

  printf("\n New GGA Sentence = %s\n", newSentence);
  }
#endif

  /******************************************************************************/
  /* Test 1 : detect escaped prefix and genuine prefix :                        */
  /******************************************************************************/

  nmeaPrintTitleBlock("detect escaped prefix and genuine prefix",
                      testIndex++,
                      0,
                      true);

  testCaptureState = initialiseTests(&nmeaSentence[0],
                                     &activeSentenceState);

  printf("\n Capture State = ");
  nmeaPrintCaptureState(testCaptureState);
  printf("\n");

  /******************************************************************************/
  /* Test 2 : detect legal talker code for GPS :                                */
  /******************************************************************************/

  nmeaPrintTitleBlock("detect legal talker code for GPS",
                       testIndex++,
                       0,
                       true);

  nmeaSentence[0] = '$';
  nmeaSentence[1] = 'G';
  nmeaSentence[2] = 'P';
  nmeaSentence[3] = NMEA_SENTENCE_END_OF_STRING;

  testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                          3,
                                         &activeSentenceState);

  printf("\n Capture State = ");
  nmeaPrintCaptureState(testCaptureState);
  printf("\n");

  /******************************************************************************/
  /* Test 3 : detect start character in talker code :                           */
  /******************************************************************************/

    nmeaPrintTitleBlock("detect start character in talker code",
                        testIndex++,
                        0,
                        true);

    testCaptureState = initialiseTests(&nmeaSentence[0],
                                       &activeSentenceState);

    nmeaSentence[0] = '$';
    nmeaSentence[1] = 'G';
    nmeaSentence[2] = '$';
    nmeaSentence[3] = NMEA_SENTENCE_END_OF_STRING;

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                            3,
                                           &activeSentenceState);

    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    printf("\n");

    /******************************************************************************/
    /* Test 4 : detect error character in talker code :                           */
    /******************************************************************************/

    nmeaPrintTitleBlock("detect error character in talker code",
                        testIndex++,
                        0,
                        true);

    testCaptureState = initialiseTests(&nmeaSentence[0],
                                       &activeSentenceState);

    nmeaSentence[0] = '$';
    nmeaSentence[1] = 'G';
    nmeaSentence[2] = '!';
    nmeaSentence[3] = NMEA_SENTENCE_END_OF_STRING;

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                            3,
                                           &activeSentenceState);

    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    printf("\n");

    /******************************************************************************/
    /* Test 5 : detect legal message code "GLL" :                                 */
    /******************************************************************************/

    nmeaPrintTitleBlock("detect legal message code ""GLL""",
                        testIndex++,
                        0,
                        true);

    testCaptureState = initialiseTests(&nmeaSentence[0],
                                       &activeSentenceState);
    bufferIndex = 0;

    nmeaSentence[bufferIndex++] = '$';
    nmeaSentence[bufferIndex++] = 'G';
    nmeaSentence[bufferIndex++] = 'P';
    nmeaSentence[bufferIndex++] = 'G';
    nmeaSentence[bufferIndex++] = 'L';
    nmeaSentence[bufferIndex++] = 'L';
    nmeaSentence[bufferIndex++] = NMEA_SENTENCE_FIELD_SEPERATOR;
    nmeaSentence[bufferIndex]   = NMEA_SENTENCE_END_OF_STRING;

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                            bufferIndex,
                                           &activeSentenceState);

    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    printf("\n");

    /******************************************************************************/
    /* Test 6 : detect error character in message code "GLL" :                    */
    /******************************************************************************/

    nmeaPrintTitleBlock("detect error character in message code ""GLL""",
                        testIndex++,
                        0,
                        true);

    testCaptureState = initialiseTests(&nmeaSentence[0],
                                       &activeSentenceState);

    bufferIndex = 0;

    nmeaSentence[bufferIndex++] = '$';
    nmeaSentence[bufferIndex++] = 'G';
    nmeaSentence[bufferIndex++] = 'P';
    nmeaSentence[bufferIndex++] = 'G';
    nmeaSentence[bufferIndex++] = '!';
    nmeaSentence[bufferIndex++] = 'L';
    nmeaSentence[bufferIndex]   = NMEA_SENTENCE_END_OF_STRING;

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                            bufferIndex,
                                           &activeSentenceState);

    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    printf("\n");

    /******************************************************************************/
    /* Test 7 : detect legal sentence "RMC" after aborted match on "GSV"          */
    /******************************************************************************/

    nmeaPrintTitleBlock("detect legal sentence ""RMC"" after aborted match on ""GSV""",
                        testIndex++,
                        0,
                        true);

    testCaptureState = initialiseTests(&nmeaSentence[0],
                                       &activeSentenceState);

    bufferIndex = 0;

    nmeaSentence[bufferIndex++] = '$';
    nmeaSentence[bufferIndex++] = 'G';
    nmeaSentence[bufferIndex++] = 'P';
    nmeaSentence[bufferIndex++] = 'G';
    nmeaSentence[bufferIndex++] = 'S';
    nmeaSentence[bufferIndex++] = '!';
    nmeaSentence[bufferIndex++] = '$';
    nmeaSentence[bufferIndex++] = 'G';
    nmeaSentence[bufferIndex++] = 'P';
    nmeaSentence[bufferIndex++] = 'R';
    nmeaSentence[bufferIndex++] = 'M';
    nmeaSentence[bufferIndex++] = 'C';
    nmeaSentence[bufferIndex++] = NMEA_SENTENCE_FIELD_SEPERATOR;
    nmeaSentence[bufferIndex]   = NMEA_SENTENCE_END_OF_STRING;

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                            bufferIndex,
                                           &activeSentenceState);

    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    printf("\n");

  /******************************************************************************/
  /* Test 8 : feed the state machine one character at a time                    */
  /******************************************************************************/

    nmeaPrintTitleBlock("feed the state machine one character at a time",
                        testIndex++,
                        0,
                        true);

    testCaptureState =  initialiseTests(&nmeaSentence[0],
                                        &activeSentenceState);

    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    printf("\n");

    bufferIndex = 0;

    nmeaSentence[bufferIndex++] = '$';
    nmeaSentence[bufferIndex]   = NMEA_SENTENCE_END_OF_STRING;

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                            bufferIndex,
                                           &activeSentenceState);

    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    printf("\n");

    bufferIndex = 0;

    nmeaSentence[bufferIndex++] = 'G';
    nmeaSentence[bufferIndex]   = NMEA_SENTENCE_END_OF_STRING;

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                            bufferIndex,
                                           &activeSentenceState);

    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    printf("\n");

    bufferIndex = 0;

    nmeaSentence[bufferIndex++] = 'P';
    nmeaSentence[bufferIndex]   = NMEA_SENTENCE_END_OF_STRING;

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                            bufferIndex,
                                           &activeSentenceState);

    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    printf("\n");

    bufferIndex = 0;

    nmeaSentence[bufferIndex++] = 'G';
    nmeaSentence[bufferIndex]   = NMEA_SENTENCE_END_OF_STRING;

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                            bufferIndex,
                                           &activeSentenceState);

    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    printf("\n");

    bufferIndex = 0;

    nmeaSentence[bufferIndex++] = 'L';
    nmeaSentence[bufferIndex]   = NMEA_SENTENCE_END_OF_STRING;

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                            bufferIndex,
                                           &activeSentenceState);

    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    printf("\n");

    bufferIndex = 0;

    nmeaSentence[bufferIndex++] = 'L';
    nmeaSentence[bufferIndex]   = NMEA_SENTENCE_END_OF_STRING;

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                            bufferIndex,
                                           &activeSentenceState);

    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    printf("\n");

    bufferIndex = 0;

    nmeaSentence[bufferIndex++] = NMEA_SENTENCE_FIELD_SEPERATOR;
    nmeaSentence[bufferIndex]   = NMEA_SENTENCE_END_OF_STRING;

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                            bufferIndex,
                                           &activeSentenceState);

    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    printf("\n");

  /******************************************************************************/
  /* Test 9 : get to the start of ""GLL"" first field                           */
  /******************************************************************************/

    nmeaPrintTitleBlock("get to the start of ""GLL"" first field",
                        testIndex++,
                        0,
                        true);

    testCaptureState = initialiseTests(&nmeaSentence[0],
                                       &activeSentenceState);

    bufferIndex = 0;

    nmeaSentence[bufferIndex++] = '$';
    nmeaSentence[bufferIndex++] = 'G';
    nmeaSentence[bufferIndex++] = 'P';
    nmeaSentence[bufferIndex++] = 'G';
    nmeaSentence[bufferIndex++] = 'L';
    nmeaSentence[bufferIndex++] = 'L';
    nmeaSentence[bufferIndex++] = NMEA_SENTENCE_FIELD_SEPERATOR;
    nmeaSentence[bufferIndex++] = '1';
    nmeaSentence[bufferIndex]   = NMEA_SENTENCE_END_OF_STRING;

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                            bufferIndex,
                                            &activeSentenceState);

    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    printf("\n");

    // Test the sentence field incrementing
    bufferIndex = 0;

    nmeaSentence[bufferIndex++] = NMEA_SENTENCE_FIELD_SEPERATOR;
    nmeaSentence[bufferIndex++] = '2';
    nmeaSentence[bufferIndex]   = NMEA_SENTENCE_END_OF_STRING;

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                            bufferIndex,
                                           &activeSentenceState);

    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    printf("\n");

    /******************************************************************************/
    /* Test 10 : decode a ""GLL"" first (latitude) field empty :                  */
    /******************************************************************************/

    nmeaPrintTitleBlock("decode a ""GLL"" first (latitude) field empty",
                        testIndex++,
                        0,
                        true);

    testCaptureState = initialiseTests(&nmeaSentence[0],
                                       &activeSentenceState);

    bufferIndex = 0;

    nmeaSentence[bufferIndex++] = '$';
    nmeaSentence[bufferIndex++] = 'G';
    nmeaSentence[bufferIndex++] = 'P';
    nmeaSentence[bufferIndex++] = 'G';
    nmeaSentence[bufferIndex++] = 'L';
    nmeaSentence[bufferIndex++] = 'L';
    nmeaSentence[bufferIndex++] = NMEA_SENTENCE_FIELD_SEPERATOR;
    nmeaSentence[bufferIndex++] = NMEA_SENTENCE_FIELD_SEPERATOR; // latitude
    nmeaSentence[bufferIndex++] = NMEA_FIELD_NORTHINGS_NORTH;
    nmeaSentence[bufferIndex++] = NMEA_SENTENCE_FIELD_SEPERATOR;
    nmeaSentence[bufferIndex++] = NMEA_SENTENCE_FIELD_SEPERATOR; // longitude
    nmeaSentence[bufferIndex++] = NMEA_SENTENCE_FIELD_SEPERATOR; // longitudeEastings
    nmeaSentence[bufferIndex++] = NMEA_SENTENCE_FIELD_SEPERATOR; // UTC
    nmeaSentence[bufferIndex++] = NMEA_SENTENCE_FIELD_SEPERATOR; // status
    nmeaSentence[bufferIndex++] = NMEA_SENTENCE_FIELD_SEPERATOR; // FAA mode
    nmeaSentence[bufferIndex]   = NMEA_SENTENCE_END_OF_STRING;

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                            bufferIndex,
                                           &activeSentenceState);

    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    nmeaPrintCapturedSentence(&activeSentenceState.nmeaSentence[0]);
    printf("\n");


    /******************************************************************************/
    /* Test 11 : decode a short ""GLL"" first (latitude) field :                  */
    /******************************************************************************/

    nmeaPrintTitleBlock("decode a short ""GLL"" first (latitude) field",
                        testIndex++,
                        0,
                        true);

    testCaptureState = initialiseTests(&nmeaSentence[0],
                                       &activeSentenceState);

    bufferIndex = 0;

    nmeaSentence[bufferIndex++] = '$';
    nmeaSentence[bufferIndex++] = 'G';
    nmeaSentence[bufferIndex++] = 'P';
    nmeaSentence[bufferIndex++] = 'G';
    nmeaSentence[bufferIndex++] = 'L';
    nmeaSentence[bufferIndex++] = 'L';
    nmeaSentence[bufferIndex++] = NMEA_SENTENCE_FIELD_SEPERATOR;
    nmeaSentence[bufferIndex++] = '1';
    nmeaSentence[bufferIndex++] = '2';
    nmeaSentence[bufferIndex++] = '3';
    nmeaSentence[bufferIndex++] = '4';
    nmeaSentence[bufferIndex++] = NMEA_FIELD_LATITUDE_SEPERATOR;
    nmeaSentence[bufferIndex++] = '5';
    nmeaSentence[bufferIndex++] = '6';
    nmeaSentence[bufferIndex++] = NMEA_SENTENCE_FIELD_SEPERATOR;
    nmeaSentence[bufferIndex]   = NMEA_SENTENCE_END_OF_STRING;

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                            bufferIndex,
                                           &activeSentenceState);

    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    nmeaPrintCapturedSentence(&activeSentenceState.nmeaSentence[0]);
    printf("\n");

    /******************************************************************************/
    /* Test 12  : decode a long ""GLL"" first "latitude" field :                  */
    /*      12a : add "latitudeNorthings"                                                 */
    /******************************************************************************/

    nmeaPrintTitleBlock("decode a long ""GLL"" first (latitude) field",
                        testIndex++,
                        0,
                        true);

    testCaptureState = initialiseTests(&nmeaSentence[0],
                                       &activeSentenceState);

    bufferIndex = 0;

    if (nmeaLoadTestSentence("$GPGLL,8327.06397,S",
                             &nmeaSentence[0],
                             &bufferIndex) == false)
      {
      printf("\n ERROR : bad input sentence!\n");
      exit(0);
      }

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                            bufferIndex,
                                           &activeSentenceState);

    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    nmeaPrintCapturedSentence(&activeSentenceState.nmeaSentence[0]);
    printf("\n");

  /******************************************************************************/
  /* Test 13 : decode a long "GLL" second "longitude" field :                   */
  /******************************************************************************/

    nmeaPrintTitleBlock("decode a long ""GLL"" second ""(longitude)"" field",
                        testIndex++,
                        0,
                        true);

    bufferIndex = 0;

    if (nmeaLoadTestSentence(",17959.99,E,",
                             &nmeaSentence[0],
                             &bufferIndex) == false)
      {
      printf("\n ERROR : bad input sentence!\n");
      exit(0);
      }

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                            bufferIndex,
                                            &activeSentenceState);

    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    nmeaPrintCapturedSentence(&activeSentenceState.nmeaSentence[0]);
    printf("\n");

  /******************************************************************************/
  /* Test 14 : decode a long "GLL" "UTC" field :                                */
  /******************************************************************************/

    nmeaPrintTitleBlock("decode a long ""GLL"" ""UTC"" field",
                        testIndex++,
                        0,
                        true);

    bufferIndex = 0;

    if (nmeaLoadTestSentence("17371254",
                              &nmeaSentence[0],
                              &bufferIndex) == false)
    {
      printf("\n ERROR : bad input sentence!\n");
      exit(0);
    }

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                            bufferIndex,
                                           &activeSentenceState);

    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    nmeaPrintCapturedSentence(&activeSentenceState.nmeaSentence[0]);
    printf("\n");

  /******************************************************************************/
  /* Test 15 : abort the first sentence, parse a second sentence :              */
  /******************************************************************************/
   
    nmeaPrintTitleBlock("abort the first sentence, parse a second sentence",
                        testIndex++,
                        0,
                        true);

    // Precompute the checksum for the complete sentence
    runningChecksum = 0;

    strcpy_s(newSentence, NMEA_SENTENCE_MAXIMUM_LENGTH_STRING, "GPGLL,4509.24507,N,06347.48211,W,050941.97,V,D");

#ifdef _NMEA_INCLUDE_CHECKSUM_DEBUG_
    initialiseChecksumTestBuffer(runningChecksum);
#endif

    checksumSuccess = nmeaComputeChecksum(&newSentence[0],
                                           (const NMEA_GPS_UINT8)strlen(newSentence),
                                          &runningChecksum);

#ifdef _NMEA_INCLUDE_CHECKSUM_DEBUG_
    printChecksumTestBuffer();

    runningChecksum = 0;

    initialiseChecksumTestBuffer(runningChecksum);
#endif

    bufferIndex = 0;

    if (nmeaLoadTestSentence("$�%^^^&gg33$$$$$GPGLL,4509.24507,N,06347.48211,W,05094197,",
                             &nmeaSentence[0],
                             &bufferIndex) == false)
       {
      printf("\n ERROR : bad input sentence!\n");
      exit(0);
      }

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                            bufferIndex,
                                           &activeSentenceState);

#ifdef _NMEA_INCLUDE_CHECKSUM_DEBUG_
    printChecksumTestBuffer();
#endif
    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    nmeaPrintCapturedSentence(&activeSentenceState.nmeaSentence[0]);
    printf("\n");

  /******************************************************************************/
  /* Test 16 : add "data valid status " and "FAA mode" :                        */
  /******************************************************************************/

    nmeaPrintTitleBlock("add ""data valid status"" and ""FAA mode""",
                        testIndex++,
                        0,
                        true);

    bufferIndex = 0;

    if (nmeaLoadTestSentence("V,D",
                             &nmeaSentence[0],
                             &bufferIndex) == false)
                           {
                           printf("\n ERROR : bad input sentence!\n");
                           exit(0);
                           }

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                           bufferIndex,
                                           &activeSentenceState);

#ifdef _NMEA_INCLUDE_CHECKSUM_DEBUG_
    printChecksumTestBuffer();
#endif
    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    nmeaPrintCapturedSentence(&activeSentenceState.nmeaSentence[0]);
    printf("\n");

    /******************************************************************************/
    /* Test 17 : test for the '*' (asterisk) character preceding the checksum :   */
    /******************************************************************************/

    nmeaPrintTitleBlock("Test %04d : test for the '*' (asterisk) character preceding the checksum",
                        testIndex++,
                        0,
                        true);

    bufferIndex = 0;

    if (nmeaLoadTestSentence("*",
                             &nmeaSentence[0],
                             &bufferIndex) == false)
      {
        printf("\n ERROR : bad input sentence!\n");
        exit(0);
      }

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                           bufferIndex,
                                           &activeSentenceState);

#ifdef _NMEA_INCLUDE_CHECKSUM_DEBUG_
    printChecksumTestBuffer();
#endif
    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    nmeaPrintCapturedSentence(&activeSentenceState.nmeaSentence[0]);
    printf("\n");

    /******************************************************************************/
    /* Test 18 : test for the expected checksum :                                 */
    /******************************************************************************/

    nmeaPrintTitleBlock("test for the expected checksum",
                        testIndex++,
                        0,
                        true);

    bufferIndex = 0;

    if (nmeaLoadTestSentence("6",
                             &nmeaSentence[0],
                             &bufferIndex) == false)
      {
      printf("\n ERROR : bad input sentence!\n");
      exit(0);
      }

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                            bufferIndex,
                                           &activeSentenceState);

    bufferIndex = 0;

    if (nmeaLoadTestSentence("A",
                             &nmeaSentence[0],
                             &bufferIndex) == false)
    {
      printf("\n ERROR : bad input sentence!\n");
      exit(0);
    }

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                            bufferIndex,
                                           &activeSentenceState);

#ifdef _NMEA_INCLUDE_CHECKSUM_DEBUG_
    printChecksumTestBuffer();
#endif
    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    nmeaPrintCapturedSentence(&activeSentenceState.nmeaSentence[0]);
    printf("\n");

  /******************************************************************************/
  /* Test 19 : feed an entire sentence one character at a time :                */
  /******************************************************************************/

    nmeaPrintTitleBlock("feed an entire sentence one character at a time",
                         testIndex++,
                         0,
                         true);

    bufferIndex = 0;

    strcpy_s(newSentence, NMEA_SENTENCE_MAXIMUM_LENGTH, "$�%^^^&gg33$$$$$GPGLL,7223.55321,S,17959.12345,E,164927.03,A,A*7E");

    printf("\n Expected character searches to confirm sentence = %02d\n", strlen(newSentence));

    if (nmeaLoadTestSentence(newSentence,
                             &nmeaSentence[0],
                             &bufferIndex) == false)
       {
                             printf("\n ERROR : bad input sentence!\n");
                             exit(0);
       }

    // Precompute the checksum for the complete sentence
    runningChecksum = 0;

    printf("\n *** PRE-COMPUTING CHECKSUM *** \n");

    strcpy_s(newSentence, NMEA_SENTENCE_MAXIMUM_LENGTH, "GPGLL,7223.55321,S,17959.12345,E,164927.03,A,A");

    checksumSuccess = nmeaComputeChecksum(newSentence,
                                          (const NMEA_GPS_UINT8)strlen(nmeaSentence),
                                          &runningChecksum);

    printf("\n *** PRE-COMPUTED CHECKSUM = %02X *** \n", runningChecksum);


#ifdef _NMEA_INCLUDE_CHECKSUM_DEBUG_
    initialiseChecksumTestBuffer(runningChecksum);
#endif

    for (sentenceIndex = 0; sentenceIndex < strlen(nmeaSentence); sentenceIndex++)
      {
      printf("\n Parser Entry %03d", (sentenceIndex + 1));

      testCaptureState = nmeaCaptureSentence(&nmeaSentence[sentenceIndex],
                                             1,
                                             &activeSentenceState);

#ifdef _NMEA_INCLUDE_CHECKSUM_DEBUG_
      printChecksumTestBuffer();
#endif
      printf("\n Capture State = ");
      nmeaPrintCaptureState(testCaptureState);
      nmeaPrintCapturedSentence(&activeSentenceState.nmeaSentence[0]);
      printf("\n");
      }

  /******************************************************************************/
  /* Test 20 : general checksum verification against a known sample :           */
  /*           "$GPGLL,3723.2475,N,12158.3416,W,161229.487,A,A,*41"             */
  /******************************************************************************/

    nmeaPrintTitleBlock("general checksum verification against a known sample",
                        testIndex++,
                        0,
                        true);

    runningChecksum  =    0;
    expectedChecksum = 0x41;

    strcpy_s(newSentence, NMEA_SENTENCE_MAXIMUM_LENGTH_STRING, "GPGLL,3723.2475,N,12158.3416,W,161229.487,A,A");
    // strcpy_s(newSentence, NMEA_SENTENCE_MAXIMUM_LENGTH_STRING, "GNGNS,122310.0,3722.425671,N,12258.856215,W,AA,15,0.9,1005.543,6.5,,");

    checksumSuccess = nmeaComputeChecksum(&newSentence[0],
                                           (const NMEA_GPS_UINT8)strlen(newSentence),
                                          &runningChecksum);

    if (runningChecksum == expectedChecksum)
      {
      printf(" Checksum test passed!\n");
      }
    else
      {
      printf(" Checksum test failed : %02x != %02x\n", runningChecksum, expectedChecksum);
      }

/******************************************************************************/
/*  Test 21 : beginning randomised-field generating encoder construction :    */
/******************************************************************************/

    nmeaPrintTitleBlock("beginning the randomised-field generating encoder construction : ",
                        testIndex++,
                        0,
                        true);

    nmeaEncoderGenerateSentenceGLL(&newEncoderSentence[0],
                                   &newEncoderSentenceIndex,
                                   &mtRandomState,
                                    NMEA_ENCODER_SEED_SCHEME_TIME);

    printf("\n New Encoder Sentence = %s\n", newEncoderSentence); 

/******************************************************************************/
/* Test 22 : try to decode a random 'GLL' sentence :                          */
/******************************************************************************/

    nmeaPrintTitleBlock("try to decode a random 'GLL' sentence : ",
                        testIndex++,
                        0,
                        true);

    bufferIndex = 0;

    if (nmeaLoadTestSentence( newEncoderSentence,
                             &nmeaSentence[0],
                             &bufferIndex) == false)
                             {
                             printf("\n ERROR : bad input sentence!\n");
                             exit(0);
                             }

    testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                            bufferIndex,
                                           &activeSentenceState);

#ifdef _NMEA_INCLUDE_CHECKSUM_DEBUG_
    printChecksumTestBuffer();
#endif
    printf("\n Capture State = ");
    nmeaPrintCaptureState(testCaptureState);
    nmeaPrintCapturedSentence(&activeSentenceState.nmeaSentence[0]);
    printf("\n");

    /******************************************************************************/
    /* Test 23 : and now with feeling! :                                          */
    /******************************************************************************/

    nmeaPrintTitleBlock("and now with feeling! : ",
                         testIndex++,
                         0,
                         true);

    testSuccess  = true;
    testSubIndex = 0;

    while (testSuccess == true)
      {
      printf("\n [%08d] Testing Random GLL Sentence : ", testSubIndex);
      printf("\n --------------------------------------\n");

      bufferIndex = 0;

      nmeaEncoderGenerateSentenceGLL(&newEncoderSentence[0],
                                     &newEncoderSentenceIndex,
                                     &mtRandomState,
                                      NMEA_ENCODER_SEED_SCHEME_TIME);
   
      printf("\n Input Sentence : ");
      nmeaPrintCapturedSentence(&newEncoderSentence[0]);

      if (nmeaLoadTestSentence( newEncoderSentence,
                               &nmeaSentence[0],
                               &bufferIndex) == false)
        {
        printf("\n ERROR : bad input sentence!\n");
        exit(0);
        }
   
      testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                              bufferIndex,
                                             &activeSentenceState);

#ifdef _NMEA_INCLUDE_CHECKSUM_DEBUG_
      printChecksumTestBuffer();
#endif
      printf("\n Capture State = ");
      nmeaPrintCaptureState(testCaptureState);
      nmeaPrintCapturedSentence(&activeSentenceState.nmeaSentence[0]);
      printf("\n");

      if (testCaptureState == NMEA_SENTENCE_CAPTURE_STATE_PASSED)
        {
        testSubIndex = testSubIndex + 1;

        Sleep(NMEA_TEST_SENTENCE_SLEEP_TIME_MILLISECONDS);
        }
      else
        {
        printf("\n Test %08d has failed!\n", testSubIndex);
        testSuccess = false;
        }

      if (_kbhit())
        {
        testExitCode = _getch();

        if (testExitCode == NMEA_TEST_EXIT_CODE)
          {
          break;
          }
        else
          {
          if (testExitCode == NMEA_TEST_WAIT_CODE)
            {
            while (!_kbhit())
              ;

            // Cancel the wait and delete the random character used 
            testExitCode = _getch();
            testExitCode = 0;
            }
          }
        }
      }

/******************************************************************************/
/* Test 24 : try to decode a random 'RMC' sentence :                          */
/******************************************************************************/

    nmeaPrintTitleBlock("try to decode a random 'RMC' sentence : ",
                         testIndex++,
                         0,
                         true);

    testSuccess  = true;
    testSubIndex = 0;

    while (testSuccess == true)
      {
      printf("\n [%08d] Testing Random RMC Sentence : ", testSubIndex);
      printf("\n --------------------------------------\n");

      bufferIndex = 0;

      nmeaEncoderGenerateSentenceRMC(&newEncoderSentence[0],
                                     &newEncoderSentenceIndex,
                                     &mtRandomState,
                                      NMEA_ENCODER_SEED_SCHEME_TIME);

      printf("\n Input Sentence : ");
      nmeaPrintCapturedSentence(&newEncoderSentence[0]);

      if (nmeaLoadTestSentence( newEncoderSentence,
                               &nmeaSentence[0],
                               &bufferIndex) == false)
        {
        printf("\n ERROR : bad input sentence!\n");
        exit(0);
        }

      testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                              bufferIndex,
                                              &activeSentenceState);

#ifdef _NMEA_INCLUDE_CHECKSUM_DEBUG_
      printChecksumTestBuffer();
#endif
      printf("\n Capture State = ");
      nmeaPrintCaptureState(testCaptureState);
      nmeaPrintCapturedSentence(&activeSentenceState.nmeaSentence[0]);
      printf("\n");

      if (testCaptureState == NMEA_SENTENCE_CAPTURE_STATE_PASSED)
        {
        testSubIndex = testSubIndex + 1;

        Sleep(NMEA_TEST_SENTENCE_SLEEP_TIME_MILLISECONDS);
        }
      else
        {
        printf("\n Test %08d has failed!\n", testSubIndex);
        testSuccess = false;
        }

      if (_kbhit())
        {
        testExitCode = _getch();

        if (testExitCode == NMEA_TEST_EXIT_CODE)
          {
          break;
          }
        else
          {
          if (testExitCode == NMEA_TEST_WAIT_CODE)
            {
            while (!_kbhit())
              ;

            // Cancel the wait and delete the random character used 
            testExitCode = _getch();
            testExitCode = 0;
            }
          }
        }
      }

/******************************************************************************/
/* Test 25 : try to decode a random "GSA" message                             */
/******************************************************************************/

    nmeaPrintTitleBlock("try to decode a random 'GSA' sentence : ",
                        testIndex++,
                        0,
                        true);

/******************************************************************************/
/* One character at each "capture" call :                                     */
/******************************************************************************/

    initialiseSentence(&newEncoderSentence[0],
                            NMEA_SENTENCE_MAXIMUM_LENGTH_STRING,
                            NMEA_SENTENCE_END_OF_STRING);

    nmeaEncoderGenerateSentenceGSA(&newEncoderSentence[0],
                                   &newEncoderSentenceIndex,
                                   &mtRandomState,
                                    NMEA_ENCODER_SEED_SCHEME_TIME);

    testCaptureState = NMEA_SENTENCE_CAPTURE_STATE_INITIALISATION;

    printf("\n Input Sentence : ");
    nmeaPrintCapturedSentence(&newEncoderSentence[0]);

    bufferIndex = 0;

    if (nmeaLoadTestSentence( newEncoderSentence,
                             &nmeaSentence[0],
                             &bufferIndex) == false)
      {
      printf("\n ERROR : bad input sentence!\n");
      exit(0);
      }

    for (sentenceIndex = 0; sentenceIndex < strlen(nmeaSentence); sentenceIndex++)
      {
      printf("\n Parser Entry %03d", (sentenceIndex + 1));

      testCaptureState = nmeaCaptureSentence(&nmeaSentence[sentenceIndex],
                                              1,
                                             &activeSentenceState);

      printf("\n Capture State = ");
      nmeaPrintCaptureState(testCaptureState);
      nmeaPrintCapturedSentence(&activeSentenceState.nmeaSentence[0]);
      printf("\n");
      }



/******************************************************************************/
/******************************************************************************/

    initialiseSentence(&newEncoderSentence[0],
                            NMEA_SENTENCE_MAXIMUM_LENGTH_STRING,
                            NMEA_SENTENCE_END_OF_STRING);

    testSuccess  = true;
    testSubIndex = 0;

    while (testSuccess == true)
      {
      printf("\n [%08d] Testing Random GSA Sentence : ", testSubIndex);
      printf("\n --------------------------------------\n");

      bufferIndex = 0;

      nmeaEncoderGenerateSentenceGSA(&newEncoderSentence[0],
                                     &newEncoderSentenceIndex,
                                     &mtRandomState,
                                     NMEA_ENCODER_SEED_SCHEME_TIME);

      printf("\n Input Sentence : ");
      nmeaPrintCapturedSentence(&newEncoderSentence[0]);

      if (nmeaLoadTestSentence( newEncoderSentence,
                               &nmeaSentence[0],
                               &bufferIndex) == false)
       {
       printf("\n ERROR : bad input sentence!\n");
       exit(0);
       }

      testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                              bufferIndex,
                                             &activeSentenceState);

#ifdef _NMEA_INCLUDE_CHECKSUM_DEBUG_
      printChecksumTestBuffer();
#endif
      printf("\n Capture State = ");
      nmeaPrintCaptureState(testCaptureState);
      nmeaPrintCapturedSentence(&activeSentenceState.nmeaSentence[0]);
      printf("\n");

      if (testCaptureState == NMEA_SENTENCE_CAPTURE_STATE_PASSED)
        {
        testSubIndex = testSubIndex + 1;

        Sleep(NMEA_TEST_SENTENCE_SLEEP_TIME_MILLISECONDS);
        }
      else
        {
        printf("\n Test %08d has failed!\n", testSubIndex);
        testSuccess = false;
        }

      if (_kbhit())
        {
        testExitCode = _getch();

        if (testExitCode == NMEA_TEST_EXIT_CODE)
          {
          break;
          }
        else
          {
          if (testExitCode == NMEA_TEST_WAIT_CODE)
            {
            while (!_kbhit())
              ;

            // Cancel the wait and delete the random character used 
            testExitCode = _getch();
            testExitCode = 0;
            }
          }
        }
      }

/******************************************************************************/
/* Test 26 : try to decode a random "GSV" message :                           */
/*            - "GSV" messages consist of one or more part-messages where     */
/*              each non-final part-message contains either four satellite    */
/*              data blocks and the final message contains one to four data   */
/*              blocks                                                        */
/******************************************************************************/

    NMEA_GPS_UINT8   numberOfGSVSentences  = 0,
                     characterIndex        = 0;
    NMEA_GPS_UCHAR **newGSVSentences       = NULL,
                    *newGSVSentenceIndices = NULL;

    nmeaPrintTitleBlock("try to decode a random 'GSV' sentence : ",
                        testIndex++,
                        0,
                        true);

    testSuccess  = true;
    testSubIndex = 0;

    while (testSuccess == true)
      {
      printf("\n [%08d] Testing Random GSV Sentence Group : ", testSubIndex);
      printf("\n --------------------------------------------\n");

      nmeaEncoderGenerateSentenceGSV(&newGSVSentences,
                                     &newGSVSentenceIndices,
                                     &numberOfGSVSentences,
                                     &mtRandomState,
                                      NMEA_ENCODER_SEED_SCHEME_TIME);
      
      for (singleIndex = /* (numberOfGSVSentences - 1) */ 0; singleIndex < numberOfGSVSentences; singleIndex++)
        {
        printf("\n GSV Sentence [%02d] of [%02d] : %s ", (singleIndex + 1), numberOfGSVSentences, *(newGSVSentences + singleIndex));
      
        // Feed the new sentence in one character at a time
        for (characterIndex = 0; characterIndex < *(newGSVSentenceIndices + singleIndex); characterIndex++)
          {
          testCaptureState = nmeaCaptureSentence((*(newGSVSentences + singleIndex) + characterIndex),
                                                 1,
                                                 &activeSentenceState);
          }
        }
      
      printf("\n Capture State = ");
      nmeaPrintCaptureState(testCaptureState);
      nmeaPrintCapturedSentence(&activeSentenceState.nmeaSentence[0]);
      printf("\n");
     
      if (testCaptureState == NMEA_SENTENCE_CAPTURE_STATE_PASSED)
        {
        testSubIndex = testSubIndex + 1;
      
        Sleep(NMEA_TEST_SENTENCE_SLEEP_TIME_MILLISECONDS);
        }
     else
       {
       printf("\n Test %08d has FAILED!\n", testSubIndex++);
       testSuccess = false;
       }

      // Free the sentences and indices allocated in "nmeaEncoderGenerateSentenceGSV()"
      for (singleIndex = 0; singleIndex < numberOfGSVSentences; singleIndex++)
        {
        free(*(newGSVSentences + singleIndex));
        }
      
      free(newGSVSentenceIndices);
      free(newGSVSentences);

      if (_kbhit())
        {
        testExitCode = _getch();

        if (testExitCode == NMEA_TEST_EXIT_CODE)
          {
          break;
          }
        else
          {
          if (testExitCode == NMEA_TEST_WAIT_CODE)
            {
            while (!_kbhit())
              ;

            // Cancel the wait and delete the random character used 
            testExitCode = _getch();
            testExitCode = 0;
            }
          }
        }
      }

/******************************************************************************/
/* Test 27 : try to decode a random "GGA" message :                           */
/******************************************************************************/

    initialiseSentence(&newEncoderSentence[0],
                            NMEA_SENTENCE_MAXIMUM_LENGTH_STRING,
                            NMEA_SENTENCE_END_OF_STRING);

    testSuccess  = true;
    testSubIndex = 0;

    while (testSuccess == true)
      {
      printf("\n [%08d] Testing Random GGA Sentence : ", testSubIndex);
      printf("\n --------------------------------------\n");

      bufferIndex = 0;

      nmeaEncoderGenerateSentenceGGA(&newEncoderSentence[0],
                                     &newEncoderSentenceIndex,
                                     &mtRandomState,
                                     NMEA_ENCODER_SEED_SCHEME_TIME);

      printf("\n Input Sentence : ");
      nmeaPrintCapturedSentence(&newEncoderSentence[0]);

      if (nmeaLoadTestSentence( newEncoderSentence,
                               &nmeaSentence[0],
                               &bufferIndex) == false)
       {
       printf("\n ERROR : bad input sentence!\n");
       exit(0);
       }

      testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                              bufferIndex,
                                             &activeSentenceState);

#ifdef _NMEA_INCLUDE_CHECKSUM_DEBUG_
      printChecksumTestBuffer();
#endif
      printf("\n Capture State = ");
      nmeaPrintCaptureState(testCaptureState);
      nmeaPrintCapturedSentence(&activeSentenceState.nmeaSentence[0]);
      printf("\n");

      if (testCaptureState == NMEA_SENTENCE_CAPTURE_STATE_PASSED)
        {
        testSubIndex = testSubIndex + 1;

        Sleep(NMEA_TEST_SENTENCE_SLEEP_TIME_MILLISECONDS);
        }
      else
        {
        printf("\n Test %08d has failed!\n", testSubIndex);
        testSuccess = false;
        }

      if (_kbhit())
        {
        testExitCode = _getch();

        if (testExitCode == NMEA_TEST_EXIT_CODE)
          {
          break;
          }
        else
          {
          if (testExitCode == NMEA_TEST_WAIT_CODE)
            {
            while (!_kbhit())
              ;

            // Cancel the wait and delete the random character used 
            testExitCode = _getch();
            testExitCode = 0;
            }
          }
        }
      }

 /******************************************************************************/

    return(0);

/******************************************************************************/
} /* end of main                                                              */

/******************************************************************************/
/* initialiseTests() :                                                        */
/*  --> nmeaSentence        : dummy incoming sentence                         */
/* <--> activeSentenceState : current state of a sentence in capture/decode   */
/*                                                                            */
/* - gets a test past '$'                                                     */
/*                                                                            */
/******************************************************************************/

static NMEASentenceCaptureStates_t initialiseTests(NMEA_GPS_CHAR              *nmeaSentence,
                                                   NMEASentenceCaptureState_t *activeSentenceState)
{
/******************************************************************************/

  NMEASentenceCaptureStates_t testCaptureState = NMEA_SENTENCE_CAPTURE_STATE_FAILED;

/******************************************************************************/

  nmeaSentence[0] = NMEA_SENTENCE_ESCAPE_CHARACTER;
  nmeaSentence[1] = NMEA_SENTENCE_PREFIX;
  nmeaSentence[2] = NMEA_SENTENCE_FIELD_SEPERATOR;
  nmeaSentence[3] = NMEA_SENTENCE_FIELD_SEPERATOR;
  nmeaSentence[4] = NMEA_SENTENCE_PREFIX;
  nmeaSentence[5] = NMEA_SENTENCE_END_OF_STRING;

  // Initialise the active sentence state
  testCaptureState = nmeaCaptureSentence( NMEA_SENTENCE_END_OF_STRING_STRING, // best not to use NULL!
                                          0,
                                          activeSentenceState);
                                       
  testCaptureState = nmeaCaptureSentence(&nmeaSentence[0],
                                          5,
                                          activeSentenceState);

/******************************************************************************/

  return(testCaptureState);

/******************************************************************************/
} /* end of initialiseTests                                                   */

/******************************************************************************/
/* nmeaLoadTestSentence() :                                                   */
/*  --> nmeaTestSentence   : a test sentence in the form of a string          */
/*  --> nmeaTestBuffer     : a buffer to laod the string into                 */
/*  --> nmeaSentenceLength : the length of the sentence read                  */
/*  <-- result           : error-detection                                    */
/*                                                                            */
/* - load a test sentence into the "input" buffer ready for parsing. The only */
/*   sanity check is for NULL pointers                                        */
/*                                                                            */
/******************************************************************************/

static NMEA_GPS_BOOLEAN nmeaLoadTestSentence(NMEA_GPS_CHAR  *nmeaTestSentence,
                                             NMEA_GPS_CHAR  *nmeaTestBuffer,
                                             NMEA_GPS_UINT8 *nmeaSentenceLength)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN    result        = false;
  NMEA_GPS_UINT8      sentenceIndex = 0;

/******************************************************************************/

  if ((nmeaTestSentence != NULL) && (strlen(nmeaTestSentence) != 0) && (nmeaTestBuffer != NULL))
    {
    while ((sentenceIndex < NMEA_SENTENCE_MAXIMUM_LENGTH) && ((*(nmeaTestSentence + sentenceIndex)) != NMEA_SENTENCE_END_OF_STRING))
      {
      *(nmeaTestBuffer + sentenceIndex) = *(nmeaTestSentence + sentenceIndex);

      sentenceIndex                     = sentenceIndex + 1;
      }

    *nmeaSentenceLength                 = sentenceIndex;

    *(nmeaTestBuffer + sentenceIndex)   = NMEA_SENTENCE_END_OF_STRING;

    result = true;
    }

/******************************************************************************/

  return(result);

/******************************************************************************/
} /* end of nmeaLoadTestSentence                                              */

/******************************************************************************/
/* nmeaPrintTitleBlock() :                                                    */
/*   --> titleString     : null-terminated string chracters to print          */
/*   --> titleOrdinal    : title block ordinal to print                       */
/*   --> titleSubOrdinal : title block sub-ordinal to print                   */
/*   --> titleExitLane   : on error either bail out here or return an error   */
/*                         to the caller                                      */
/*                                                                            */
/* - pretty-print a title block                                               */
/*                                                                            */
/******************************************************************************/

static NMEA_GPS_BOOLEAN nmeaPrintTitleBlock(NMEA_GPS_UCHAR   *titleString,
                                            NMEA_GPS_UINT16   titleOrdinal,
                                            NMEA_GPS_UINT16   titleSubOrdinal,
                                            NMEA_GPS_BOOLEAN  titleExitLane)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN titleResult = false;

/******************************************************************************/

  if (titleString != NULL)
    {
    nmeaPrintSeperatingLine(NMEA_SEPERATING_LINE_MAXIMUM_LENGTH,
                            NMEA_SEPERATING_LINE_CHARACTER,
                            true,
                            true);

    printf("  - Test %04d_%04d : ", titleOrdinal, titleSubOrdinal);
    printf("%s : ", titleString);

    nmeaPrintSeperatingLine(NMEA_SEPERATING_LINE_MAXIMUM_LENGTH,
                            NMEA_SEPERATING_LINE_CHARACTER,
                            true,
                            true);
    }
  else
    {
    if (titleExitLane == true)
      {
      printf("\n ERROR : the title string is missing - bailing out...\n");
      exit(0);
      }
    }

/******************************************************************************/

  return(titleResult);

/******************************************************************************/
} /* end of nmeaPrintTitleBlock                                               */

/******************************************************************************/
/* nmeaPrintSeperatingLine() :                                                */
/*  --> lineLength      : number of characters in line :                      */
/*                        0 { ... } NMEA_SEPERATING_LINE_MAXIMUM_LENGTH       */
/*  --> lineCharacter   : character to fill the line with                     */
/*  --> lineStartBreak  : print an opening '\n' [ false | true ]              */
/*  --> lineEndBreak    : print a closing  '\n' [ false | true ]              */
/*                                                                            */
/*  - print a neat paragraph-seperating line                                  */
/*                                                                            */
/******************************************************************************/

static NMEA_GPS_VOID nmeaPrintSeperatingLine(NMEA_GPS_UINT8   lineLength,
                                             NMEA_GPS_UCHAR   lineCharacter,
                                             NMEA_GPS_BOOLEAN lineStartBreak,
                                             NMEA_GPS_BOOLEAN lineEndBreak)
{
/******************************************************************************/

  if (lineLength > NMEA_SEPERATING_LINE_MAXIMUM_LENGTH)
    {
    lineLength = NMEA_SEPERATING_LINE_MAXIMUM_LENGTH;
    }

  if (lineStartBreak == true)
    {
    printf("\n");
    }

  while (lineLength > 0)
    {
    printf("%c", lineCharacter);

    lineLength = lineLength - 1;
    }

  if (lineEndBreak == true)
    {
    printf("\n");
    }

/******************************************************************************/
} /* end of nmeaPrintSeperatingLine                                           */

/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/
