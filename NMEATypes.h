/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/
/*                                                                            */
/* NMEATypes.h                                                                */
/* 02.04.20                                                                   */
/*                                                                            */
/* Ref : NMEA 0183 Specification v2.3                                         */
/*       ublox NEO-6 - Data Sheet                                             */
/*                                                                            */
/******************************************************************************/

#ifndef _NMEA_TYPES_H_
#define _NMEA_TYPES_H_

/******************************************************************************/

#include <stdbool.h>
#include <stdint.h>

/******************************************************************************/

#ifdef _NMEA_PARSER_WIN_INLINE_
#define _NMEA_INLINE_INCLUDE_  //inline
#define _NMEA_INLINE_FUNCTION_ //inline
#else
#define _NMEA_INLINE_INCLUDE_  //__inline__
#define _NMEA_INLINE_FUNCTION_ //inline
#endif

#define NMEA_GPS_VOID             void
#define NMEA_GPS_BOOLEAN          bool
#define NMEA_GPS_INT8             int8_t
#define NMEA_GPS_UINT8            uint8_t
#define NMEA_GPS_CHAR             char
#define NMEA_GPS_UCHAR            unsigned char
#define NMEA_GPS_INT16            int16_t
#define NMEA_GPS_UINT16           uint16_t
#define NMEA_GPS_SHORT            short
#define NMEA_GPS_USHORT           unsigned short
#define NMEA_GPS_INT32            int32_t
#define NMEA_GPS_UINT32           uint32_t
#define NMEA_GPS_INT              int
#define NMEA_GPS_UINT             unsigned int 
#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
#define NMEA_GPS_INT64            int64_t
#define NMEA_GPS_UINT64           uint64_t
#endif
#define NMEA_GPS_LONG                      long
#define NMEA_GPS_ULONG            unsigned long
#define NMEA_GPS_FLOAT            float
#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
#define NMEA_GPS_DOUBLE           double
#define NMEA_GPS_LONG_DOUBLE long double
#endif

// Macro expansion substitutions for numerical types
#define NMEA_GPS_LONG_SUBSTITUTION         .nmea_long_t
#define NMEA_GPS_ULONG_SUBSTITUTION        .nmea_ulong_t
#ifndef _NMEA_NUMERICAL_RANGE_ARM32_        
#define NMEA_GPS_INT64_SUBSTITUTION        .nmea_int64_t
#define NMEA_GPS_UINT64_SUBSTITUTION       .nmea_uint64_t
#endif                                     
#define NMEA_GPS_FLOAT_SUBSTITUTION        .nmea_float_t
#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
#define NMEA_GPS_DOUBLE_SUBSTITUTION       .nmea_double_t
#define NMEA_GPS_LONG_DOUBLE_SUBSTITUTION  .nmea_long_double_t
#endif

/******************************************************************************/

typedef enum nmeaNumericalFormats_tTag
  {
  NMEA_NUMERICAL_FORMAT_VOID = 0,
  NMEA_NUMERICAL_FORMAT_BOOLEAN,
  NMEA_NUMERICAL_FORMAT_INT8,
  NMEA_NUMERICAL_FORMAT_UINT8,
  NMEA_NUMERICAL_FORMAT_CHAR,
  NMEA_NUMERICAL_FORMAT_UCHAR,
  NMEA_NUMERICAL_FORMAT_INT16,
  NMEA_NUMERICAL_FORMAT_UINT16,
  NMEA_NUMERICAL_FORMAT_SHORT,
  NMEA_NUMERICAL_FORMAT_USHORT,
  NMEA_NUMERICAL_FORMAT_INT32,
  NMEA_NUMERICAL_FORMAT_UINT32,
  NMEA_NUMERICAL_FORMAT_INT,
  NMEA_NUMERICAL_FORMAT_UINT,
#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
  NMEA_NUMERICAL_FORMAT_INT64,
  NMEA_NUMERICAL_FORMAT_UINT64,
#endif
  NMEA_NUMERICAL_FORMAT_LONG,
  NMEA_NUMERICAL_FORMAT_ULONG,
  NMEA_NUMERICAL_FORMAT_FLOAT,
#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
  NMEA_NUMERICAL_FORMAT_DOUBLE,
  NMEA_NUMERICAL_FORMAT_LONG_DOUBLE,
#endif
  NMEA_NUMERICAL_FORMATS
  } nmeaNumericalFormats_t;

typedef enum nmeaNumericalLimitTests_tTag
  {
  NMEA_NUMERICAL_LIMIT_TEST_NOT_EQUAL = 0,
  NMEA_NUMERICAL_LIMIT_TEST_EQUAL,
  NMEA_NUMERICAL_LIMIT_TEST_GREATER,
  NMEA_NUMERICAL_LIMIT_TEST_GREATER_OR_EQUAL,
  NMEA_NUMERICAL_LIMIT_TEST_LESSER,
  NMEA_NUMERICAL_LIMIT_TEST_LESSER_OR_EQUAL,
  NMEA_NUMERICAL_LIMIT_TEST_APPROX_EQUAL,
  NMEA_NUMERICAL_LIMIT_TESTS
  } nmeaNumericalLimitTests_t;

// Numerical overlay for variant numerical handling
typedef union nmeaNumericalVariant_tTag
  {
  int8_t       nmea_int8_t;
  uint8_t       nmea_uint8_t;
  char  nmea_char_t;
  unsigned char  nmea_uchar_t;
  int16_t       nmea_int16_t;
  uint16_t       nmea_uint16_t;
  int16_t       nmea_short_t;
  uint16_t       nmea_ushort_t;
  uint32_t       nmea_uint32_t;
  int32_t       nmea_int32_t;
  int  nmea_int_t;
  unsigned int  nmea_uint_t;
#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
  uint64_t       nmea_uint64_t;
  int64_t       nmea_int64_t;
#endif
  long nmea_long_t;
  unsigned long nmea_ulong_t;
  float         nmea_float_t;
#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
  double        nmea_double_t;
  long double   nmea_long_double_t;
#endif
  } nmeaNumericalVariant_t;

/******************************************************************************/

#endif

/******************************************************************************/
/* NMEATypes.h                                                                */
/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/