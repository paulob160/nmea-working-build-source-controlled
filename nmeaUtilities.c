/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/
/*                                                                            */
/* NMEAUtilities.c                                                            */
/* 06.01.20                                                                   */
/*                                                                            */
/* Ref : NMEA 0183 Specification v2.3                                         */
/*       ublox NEO-6 - Data Sheet                                             */
/*                                                                            */
/* - GPS data conversion routines                                             */
/*                                                                            */
/******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include "NMEADecoder.h"
#include "NMEAUtilities.h"

/******************************************************************************/

// Decimal digit powers-of-ten in descending order
const NMEA_GPS_UINT8 degreePowers[NMEA_GPS_DEGREE_POWERS] = 
{
  NMEA_GPS_DIGIT_x_100,
  NMEA_GPS_DIGIT_x_10,
  NMEA_GPS_DIGIT_x_1
};

const NMEA_GPS_FLOAT minutesIntegerPowers[NMEA_GPS_INTEGER_MINUTES_POWERS] = {
                                                                             NMEA_GPS_DIGIT_x_10,
                                                                             NMEA_GPS_DIGIT_x_1
                                                                             };

const NMEA_GPS_FLOAT minutesFractionalPowers[NMEA_GPS_FRACTIONAL_MINUTES_POWERS] = 
{
  NMEA_GPS_DIGIT_x_0_1,
  NMEA_GPS_DIGIT_x_0_01,
  NMEA_GPS_DIGIT_x_0_001,
  NMEA_GPS_DIGIT_x_0_0001,
  NMEA_GPS_DIGIT_x_0_00001
};

const NMEA_GPS_FLOAT nmeaFloatIntegers[NMEA_FLOAT_INTEGER_TABLE_SIZE] = 
{
  NMEA_FLOAT_DIGIT_x_1,
  NMEA_FLOAT_DIGIT_x_10,   
  NMEA_FLOAT_DIGIT_x_100,  
  NMEA_FLOAT_DIGIT_x_1000, 
  NMEA_FLOAT_DIGIT_x_10000,
  NMEA_FLOAT_DIGIT_x_100000,
  NMEA_FLOAT_DIGIT_x_1000000,
};

// Table of single-precision floating-point fractional constants
const NMEA_GPS_FLOAT nmeaFloatFractionals[NMEA_FLOAT_FRACTIONAL_TABLE_SIZE] = 
{
  NMEA_GPS_DIGIT_x_0_1,
  NMEA_GPS_DIGIT_x_0_01,
  NMEA_GPS_DIGIT_x_0_001,
  NMEA_GPS_DIGIT_x_0_0001,
  NMEA_GPS_DIGIT_x_0_00001,
  NMEA_GPS_DIGIT_x_0_000001
};

// Table of decimal-equivalent values at each bit position of a "uint32_t"
#define NMEA_DECIMAL_EQUIVALENT_DIGITS      (10)
#define NMEA_DECIMAL_EQUIVALENT_BIT_WEIGHTS (32)

const NMEA_GPS_UCHAR nmeaBitWeightToDecimal[NMEA_DECIMAL_EQUIVALENT_BIT_WEIGHTS][NMEA_DECIMAL_EQUIVALENT_DIGITS] =
  {
    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 2 },
    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 4 },
    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 8 },
    { 0, 0, 0, 0, 0, 0, 0, 0, 1, 6 },
    { 0, 0, 0, 0, 0, 0, 0, 0, 3, 2 },
    { 0, 0, 0, 0, 0, 0, 0, 0, 6, 4 },
    { 0, 0, 0, 0, 0, 0, 0, 1, 2, 8 },
    { 0, 0, 0, 0, 0, 0, 0, 2, 5, 6 },
    { 0, 0, 0, 0, 0, 0, 0, 5, 1, 2 },
    { 0, 0, 0, 0, 0, 0, 1, 0, 2, 4 },
    { 0, 0, 0, 0, 0, 0, 2, 0, 4, 8 },
    { 0, 0, 0, 0, 0, 0, 4, 0, 9, 6 },
    { 0, 0, 0, 0, 0, 0, 8, 1, 9, 2 },
    { 0, 0, 0, 0, 0, 1, 6, 3, 8, 4 },
    { 0, 0, 0, 0, 0, 3, 2, 7, 6, 8 },
    { 0, 0, 0, 0, 0, 6, 5, 5, 3, 6 },
    { 0, 0, 0, 0, 1, 3, 1, 0, 7, 2 },
    { 0, 0, 0, 0, 2, 6, 2, 1, 4, 4 },
    { 0, 0, 0, 0, 5, 2, 4, 2, 8, 8 },
    { 0, 0, 0, 1, 0, 4, 8, 5, 7, 6 },
    { 0, 0, 0, 2, 0, 9, 7, 1, 5, 2 },
    { 0, 0, 0, 4, 1, 9, 4, 3, 0, 4 },
    { 0, 0, 0, 8, 3, 8, 8, 6, 0, 8 },
    { 0, 0, 1, 6, 7, 7, 7, 2, 1, 6 },
    { 0, 0, 3, 3, 5, 5, 4, 4, 3, 2 },
    { 0, 0, 6, 7, 1, 0, 8, 8, 6, 4 },
    { 0, 1, 3, 4, 2, 1, 7, 7, 2, 8 },
    { 0, 2, 6, 8, 4, 3, 5, 4 ,5 ,6 },
    { 0, 5, 3, 6, 8, 7, 0, 9, 1, 2 },
    { 1, 0, 7, 3, 7, 4, 1, 8, 2, 4 },
    { 2, 1, 4, 7, 4, 8, 3, 6, 4, 8 }
  };

/******************************************************************************/

_NMEA_INLINE_FUNCTION_ static bool nmeaTestNumericalLimit(const void                      *numberToTest,
                                                          const void                      *limit,
                                                          const void                      *epsilon,
                                                                bool                      *testResult,
                                                          const nmeaNumericalLimitTests_t  limitTest,
                                                          const nmeaNumericalFormats_t     numericalFormat);

/******************************************************************************/

#ifdef _NMEA_ENABLE_STRING_TO_FLOAT_
bool nmeaConvertStringToFloat(const NMEA_GPS_CHAR           *numericalCharacters,
                              const NMEA_GPS_UINT8           numericalCharactersLength,
                              const NMEA_GPS_FLOAT           numericalMinimumLimit,
                              const NMEA_GPS_FLOAT           numericalMaximumLimit,
                              const nmeaNumericalSignFlag_t  numericalSignFlag,
                              NMEA_GPS_FLOAT                *numericalResult)

/******************************************************************************/

  bool result = false;

/******************************************************************************/
/******************************************************************************/

  return(result);

/******************************************************************************/
} /* end of nmeaConvertStringToFloat                                          */
#endif

/******************************************************************************/
/* initialiseSentence()  :                                                    */
/*  --> sentence            : sentence buffer tp initialise                   */
/*  --> sentenceLength      : length of the sentence buffer                   */
/*  --> initialisationCharacter : character to write to each sentence buffer  */
/*                                cell                                        */
/*                                                                            */
/* - initialise a test sentence buffer with a common initialisation           */
/*   character in all the buffer cells                                        */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN initialiseSentence(      NMEA_GPS_UCHAR  *sentence,
                                    const NMEA_GPS_UINT16  sentenceLength,
                                    const NMEA_GPS_UCHAR   initialisationCharacter)
  {
/******************************************************************************/

  NMEA_GPS_BOOLEAN initialisationResult = false;

  NMEA_GPS_UINT16  bufferIndex = sentenceLength;

/******************************************************************************/

  if ((sentence != NULL) && (sentenceLength > 0))
    {
    do
      {
      bufferIndex = bufferIndex - 1;

      *(sentence + bufferIndex) = initialisationCharacter;
      } while (bufferIndex > 0);
    }

/******************************************************************************/

  return(initialisationResult);

/******************************************************************************/
  } /* end of initialiseSentence                                              */

/******************************************************************************/
/* nmeaPrintCaptureState() :                                                  */
/*  --> captureState : the outer parser state                                 */
/*                                                                            */
/* - print the current outer parser state during testing                      */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_VOID nmeaPrintCaptureState(const NMEASentenceCaptureStates_t captureState)
  {
/******************************************************************************/

  switch (captureState)
    {
    case NMEA_SENTENCE_CAPTURE_STATE_INITIALISATION    : printf("NMEA_SENTENCE_CAPTURE_STATE_INITIALISATION");
                                                         break;                                           
    case NMEA_SENTENCE_CAPTURE_STATE_SEARCHING         : printf("NMEA_SENTENCE_CAPTURE_STATE_SEARCHING");
                                                         break;
    case NMEA_SENTENCE_CAPTURE_STATE_MATCH_TALKER      : printf("NMEA_SENTENCE_CAPTURE_STATE_MATCH_TALKER");
                                                         break;                                           
    case NMEA_SENTENCE_CAPTURE_STATE_MATCH_SENTENCE    : printf("NMEA_SENTENCE_CAPTURE_STATE_MATCH_SENTENCE");
                                                         break;                                           
    case NMEA_SENTENCE_CAPTURE_STATE_PARSE_COMMA       : printf("NMEA_SENTENCE_CAPTURE_STATE_PARSE_COMMA");
                                                         break;                                           
    case NMEA_SENTENCE_CAPTURE_STATE_PARSE_SENTENCE    : printf("NMEA_SENTENCE_CAPTURE_STATE_PARSE_SENTENCE");
                                                         break;                                           
    case NMEA_SENTENCE_CAPTURE_STATE_COMPLETE          : printf("NMEA_SENTENCE_CAPTURE_STATE_COMPLETE");
                                                         break;
    case NMEA_SENTENCE_CAPTURE_STATE_COMPARE_CHECKSUMS : printf("NMEA_SENTENCE_CAPTURE_STATE_COMPARE_CHECKSUMS");
                                                         break;
    case NMEA_SENTENCE_CAPTURE_STATE_PASSED            : printf("NMEA_SENTENCE_CAPTURE_STATE_PASSED");
                                                         break;
    case NMEA_SENTENCE_CAPTURE_STATE_FAILED            : printf("NMEA_SENTENCE_CAPTURE_STATE_FAILED");
                                                         break;
    default                                            : printf("NMEA_SENTENCE_CAPTURE_STATE_UNKNOWN");
                                                         break;
    }

/******************************************************************************/
  } /* end of nmeaPrintCaptureState                                           */

/******************************************************************************/
/* nmeaPrintCapturedSentence() :                                              */
/*  --> activeSentence : current state of a sentence in capture/decode        */
/*  <-- result         : error-detection                                      */
/*                                                                            */
/* - print the NMEA 0183 sentence being parsed (if it exists)                 */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaPrintCapturedSentence(NMEA_GPS_UCHAR *activeSentence)
  {
/******************************************************************************/

  NMEA_GPS_BOOLEAN result = false;

/******************************************************************************/

  if (activeSentence != NULL)
    {
    printf("\n NMEA 0183 Sentence = ");

    if ((strlen((const NMEA_GPS_CHAR *)activeSentence) >= NMEA_SENTENCE_PREAMBLE_LENGTH) && (strlen((const NMEA_GPS_CHAR *)activeSentence) <= NMEA_SENTENCE_MAXIMUM_LENGTH))
      {
      printf("%s", activeSentence);
      }
    else
      {
      printf(" Bad or missing sentence!");
      }

    result = true;
    }

/******************************************************************************/

  return(result);

/******************************************************************************/
  } /* end of nmeaPrintCapturedSentence                                       */

/******************************************************************************/
/* nmeaVariableNumericFieldParser() :                                         */
/*                                                                            */
/*   --> charactersToParse : remaining characters in the parse string         */
/*  <--> captureState      : current state of parsing                         */
/*                                                                            */
/* - parse a decimal number whose length is a variable number of digits,      */
/*   terminating at an expected character. Screening for minimum and maximum  */
/*   (possibly signed) limits must be done by the caller. Return the number   */
/*   if successful. The terminating character is NOT consumed, the caller has */
/*   to consume it or pass it to its caller                                   */
/* - if the number is possibly signed :                                       */
/*     - the sign is optional                                                 */
/*     - if the sign is present it is added to the parsed sentence and used   */
/*       in the checksum                                                      */
/*                                                                            */
/******************************************************************************/

NMEASentenceGenericStates_t nmeaVariableNumericFieldParser(const NMEA_GPS_UCHAR             *charactersToParse,
                                                                 NMEASentenceCaptureState_t *captureState)
{
/******************************************************************************/

  NMEA_GPS_UCHAR nextCharacter   = 0;

/******************************************************************************/

  if ((charactersToParse != NULL) && (captureState != NULL))
    {
    if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_INITIALISATION)
      {
      captureState->nmeaSentenceVariableState.signState           = NMEA_VARIABLE_SIGN_STATE_NONE; // The REPORTING <-- sign state
      captureState->nmeaSentenceVariableState.integerPartIndex    = 0;
      captureState->nmeaSentenceVariableState.fractionalPartIndex = 0;


      if (captureState->nmeaSentenceVariableState.signFlag == NMEA_SENTENCE_NUMERICAL_SIGN_SIGNED) // The CONTROLLING --> sign state
        {
        captureState->nmeaSentenceGenericState = NMEA_VARIABLE_STATE_SIGN;
        }
      else
        {
        captureState->nmeaSentenceGenericState = NMEA_VARIABLE_STATE_INTEGER;
        }
      }

    while (captureState->nmeaSentenceCharactersUsed < captureState->nmeaSentenceNewCharacters)
      {
      switch (captureState->nmeaSentenceGenericState)
        {
        case NMEA_VARIABLE_STATE_SIGN :    // The sign is OPTIONAL even for signed numbers (if they are positive)
                                           nextCharacter = *(charactersToParse + captureState->nmeaSentenceCharactersUsed);
                                         
                                           if ((nextCharacter == NMEA_SENTENCE_SIGN_PLUS) || (nextCharacter == NMEA_SENTENCE_SIGN_MINUS))
                                             {
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_            
                                             captureState->nmeaSentence[captureState->nmeaSentenceCharacterIndex]    = nextCharacter;
#endif                                          
                                             // Load the character into the scratch buffer for processing on field parsing completion
                                             captureState->nmeaSentenceScratch[captureState->nmeaSentenceMatchIndex] = nextCharacter;
                                           
                                             captureState->nmeaSentenceMatchIndex     = captureState->nmeaSentenceMatchIndex     + 1;
                                             captureState->nmeaSentenceCharactersUsed = captureState->nmeaSentenceCharactersUsed + 1;
                                             captureState->nmeaSentenceCharacterIndex = captureState->nmeaSentenceCharacterIndex + 1;

                                             switch(nextCharacter)
                                               {
                                               case NMEA_SENTENCE_SIGN_PLUS  : captureState->nmeaSentenceVariableState.signState = NMEA_VARIABLE_SIGN_STATE_PLUS;
                                                                               break;

                                               case NMEA_SENTENCE_SIGN_MINUS : captureState->nmeaSentenceVariableState.signState = NMEA_VARIABLE_SIGN_STATE_MINUS;
                                                                               break;
                                               }
                                             }
                                           
                                           captureState->nmeaSentenceGenericState = NMEA_VARIABLE_STATE_INTEGER;
                                           
                                           break; 

        case NMEA_VARIABLE_STATE_INTEGER : nextCharacter = *(charactersToParse + captureState->nmeaSentenceCharactersUsed);

                                           if (!isdigit(nextCharacter))
                                             { // Possible terminator found
                                             if (captureState->nmeaSentenceVariableState.integerPartIndex >= captureState->nmeaSentenceVariableState.integerPartMinimumLength)
                                               {
                                               if (captureState->nmeaSentenceVariableState.variableType == NMEA_FIELD_DECIMAL_INTEGER)
                                                 { // For an integer, character-gathering has ended
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
                                                 captureState->nmeaSentence[captureState->nmeaSentenceCharacterIndex]    = NMEA_SENTENCE_END_OF_STRING;
#endif
                                                 // Convert the ASCII field to an integer
                                                 captureState->nmeaSentenceScratch[captureState->nmeaSentenceMatchIndex] = NMEA_SENTENCE_END_OF_STRING;

                                                 // A valid integer has been parsed (ditto for the float parse) : 
                                                 //   (i) by definition the ASCII characters are legal or the parser would have 
                                                 //       already rejected them
                                                 //  (ii) by definition the number will have the expected number of characters 
                                                 //       (excess characters are detected while the integer is building)
                                                 // (iii) given (i) and (ii) "atoi()" must return a valid number,
                                                 //       including '0'
                                                 captureState->nmeaSentenceVariableState.variableResult.nmea_int32_t = atoi((const NMEA_GPS_CHAR *)&captureState->nmeaSentenceScratch[0]);

                                                 captureState->nmeaSentenceGenericState = NMEA_VARIABLE_STATE_EXIT;
                                                 }
                                               else
                                                 { // For a float, the fractional part is pending
                                                 captureState->nmeaSentenceGenericState = NMEA_VARIABLE_STATE_DECIMAL_POINT;
                                                 }
                                               }
                                             else
                                               {
                                               captureState->nmeaSentenceGenericState = NMEA_VARIABLE_STATE_FAILED;
                                               }
                                             }
                                           else
                                             { // Building the integer field
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
                                             captureState->nmeaSentence[captureState->nmeaSentenceCharacterIndex]    = nextCharacter;
#endif
                                             captureState->nmeaSentenceScratch[captureState->nmeaSentenceMatchIndex] = nextCharacter;

                                             captureState->nmeaSentenceMatchIndex                     = captureState->nmeaSentenceMatchIndex                     + 1;
                                             captureState->nmeaSentenceVariableState.integerPartIndex = captureState->nmeaSentenceVariableState.integerPartIndex + 1;

                                             if (captureState->nmeaSentenceVariableState.integerPartIndex > captureState->nmeaSentenceVariableState.integerPartMaximumLength)
                                               {                                                          
                                               captureState->nmeaSentenceGenericState = NMEA_VARIABLE_STATE_FAILED;
                                               }
                                             else
                                               {
                                               captureState->nmeaSentenceCharactersUsed = captureState->nmeaSentenceCharactersUsed + 1;
                                               captureState->nmeaSentenceCharacterIndex = captureState->nmeaSentenceCharacterIndex + 1;
                                               }
                                             }

                                           break;

        case NMEA_VARIABLE_STATE_DECIMAL_POINT : // This state is very short at one-characters' length
                                                 if (*(charactersToParse + captureState->nmeaSentenceCharactersUsed) == NMEA_SENTENCE_DECIMAL_POINT)
                                                   {
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
                                                   captureState->nmeaSentence[captureState->nmeaSentenceCharacterIndex]    = NMEA_SENTENCE_DECIMAL_POINT;
#endif                                          
                                                   captureState->nmeaSentenceScratch[captureState->nmeaSentenceMatchIndex] = NMEA_SENTENCE_DECIMAL_POINT;
                                                
                                                   captureState->nmeaSentenceMatchIndex     = captureState->nmeaSentenceMatchIndex     + 1;
                                                   captureState->nmeaSentenceCharactersUsed = captureState->nmeaSentenceCharactersUsed + 1;
                                                   captureState->nmeaSentenceCharacterIndex = captureState->nmeaSentenceCharacterIndex + 1;

                                                   captureState->nmeaSentenceGenericState   = NMEA_VARIABLE_STATE_FRACTIONAL;
                                                   }                                        
                                                 else                                       
                                                   {                                        
                                                   captureState->nmeaSentenceGenericState   = NMEA_VARIABLE_STATE_FAILED;
                                                   }

                                                 break;

        case NMEA_VARIABLE_STATE_FRACTIONAL    : // Parse the fractional part of the variable
                                                 nextCharacter = *(charactersToParse + captureState->nmeaSentenceCharactersUsed);
                                                 
                                                 if (!isdigit(nextCharacter))
                                                   { // Possible terminator found, finish parsing the fractional part
                                                   if (captureState->nmeaSentenceVariableState.fractionalPartIndex >= captureState->nmeaSentenceVariableState.fractionalPartMinimumLength)
                                                     {
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
                                                     captureState->nmeaSentence[captureState->nmeaSentenceCharacterIndex]    = NMEA_SENTENCE_END_OF_STRING;
#endif
                                                     captureState->nmeaSentenceScratch[captureState->nmeaSentenceMatchIndex] = NMEA_SENTENCE_END_OF_STRING;

                                                     // Convert the ASCII field to a float; round at '(fractionalPartIndex + 1)'...
                                                     captureState->nmeaSentenceVariableState.variableResult.nmea_float_t     = strtof((const NMEA_GPS_CHAR *)&captureState->nmeaSentenceScratch[0], NULL);
#if(0)
                                                     // *** TEST ***
                                                     {
                                                     NMEA_GPS_FLOAT testFloat = 0.0;

                                                     nmeaConvertFloatingPoint(&captureState->nmeaSentenceScratch[0],
                                                                               captureState->nmeaSentenceMatchIndex,
                                                                               captureState->nmeaSentenceVariableState.fractionalPartIndex,
                                                                              &testFloat);

                                                     printf("\n Float variable = (%f == %f)\n", captureState->nmeaSentenceVariableState.variableResult.nmea_float_t, testFloat);
                                                     }
                                                     // *** TEST ***
#endif
                                                     captureState->nmeaSentenceGenericState = NMEA_VARIABLE_STATE_EXIT;
                                                     }
                                                   else
                                                     {
                                                     captureState->nmeaSentenceGenericState = NMEA_VARIABLE_STATE_FAILED;
                                                     }
                                                   }
                                                 else
                                                   {
#ifdef _NMEA_PARSER_SENTENCE_REPEAT_
                                                   captureState->nmeaSentence[captureState->nmeaSentenceCharacterIndex]    = nextCharacter;
#endif
                                                   // Load the character into the scratch buffer for processing on field parsing completion
                                                   captureState->nmeaSentenceScratch[captureState->nmeaSentenceMatchIndex] = nextCharacter;

                                                   captureState->nmeaSentenceMatchIndex                        = captureState->nmeaSentenceMatchIndex                        + 1;
                                                   captureState->nmeaSentenceVariableState.fractionalPartIndex = captureState->nmeaSentenceVariableState.fractionalPartIndex + 1;

                                                   if (captureState->nmeaSentenceVariableState.fractionalPartIndex > captureState->nmeaSentenceVariableState.fractionalPartMaximumLength)
                                                     {
                                                     captureState->nmeaSentenceGenericState = NMEA_VARIABLE_STATE_FAILED;
                                                     }
                                                   else
                                                     {
                                                     captureState->nmeaSentenceCharactersUsed = captureState->nmeaSentenceCharactersUsed + 1;
                                                     captureState->nmeaSentenceCharacterIndex = captureState->nmeaSentenceCharacterIndex + 1;
                                                     }
                                                   }

                                                 break;

        default                                :
                                                 break;
        }

      if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_EXIT)
        {
        // Add to the running checksum over the whole field
        nmeaComputeChecksum(&captureState->nmeaSentenceScratch[0],
                             captureState->nmeaSentenceMatchIndex,
                            &captureState->nmeaSentenceComputedChecksum);

        break;
        }
      else
        {
        if (captureState->nmeaSentenceGenericState == (NMEASentenceGenericStates_t)NMEA_VARIABLE_STATE_FAILED)
          {
          break;
          }
        }
      }
    }

/******************************************************************************/

  if (captureState != NULL)
    {
    return(captureState->nmeaSentenceGenericState);
    }
  else
    {
    return(NMEA_VARIABLE_STATE_FAILED);
    }

/******************************************************************************/
} /* end of nmeaVariableNumericFieldParser                                    */

/******************************************************************************/
/* nmeaConvertDegrees() :                                                     */
/*  --> degreesString : string of purely decimal ASCII characters plus '.'    */
/*                      indicating the start of a fractional field            */
/*  --> degreesStringLength    : length of the string passed                  */
/*  --> degreesTableIndexStart : offset into the "degrees" power table for    */
/*                               varying integer field sizes                  */
/*  --> degreesLength : length of the integer "degrees" field                 */
/*  --> minutesLength : length of the integer part of the "minutes" field.    */
/*                      The length of the fractional part can be deduced      */
/*  --> degreesMinimumLimit : the minimum limit to test a character against   */
/*  --> degreesMaximumLimit : the maximum limit to test a character against   */
/*  <-- degrees       : returns the degrees field                             */
/*  <-- minutes       : reutrns the minutes field                             */
/*                                                                            */
/* - special-to-type convertor from degrees + minutes strings to numerical    */
/*   values. Specifically designed to handle "latitude" and "longitude" NMEA  */
/*   sentence representations. The string must be correctly formed and may    */
/*   only contain ASCII characters [ '0' .. '9' ] and '.'                     */
/*   This is DESTRUCTIVE to the parameters "degrees* and "minutes" !!!        */
/*                                                                            */
/******************************************************************************/

bool nmeaConvertDegrees(const NMEA_GPS_CHAR  *degreesString,
                        const NMEA_GPS_UINT8  degreesStringLength,
                        const NMEA_GPS_UINT8  degreesTableIndexStart,
                        const NMEA_GPS_UINT8  degreesLength,
                        const NMEA_GPS_UINT8  minutesLength,
                        const NMEA_GPS_FLOAT  degreesMinimumLimit,
                        const NMEA_GPS_FLOAT  degreesMaximumLimit,
                              NMEA_GPS_UINT8 *degrees,
                              NMEA_GPS_FLOAT *minutes)
{
/******************************************************************************/

  bool           degreesResult               = false;
  NMEA_GPS_UINT8 degreesDecimalPointPosition = 0;
  NMEA_GPS_UINT8 digitIndex                  = 0;
  NMEA_GPS_FLOAT fractionalDegrees           = 0.0;

/******************************************************************************/

  degreesDecimalPointPosition = degreesLength + minutesLength;

  if ((degreesString != NULL) && 
      (degrees       != NULL) && 
      (minutes       != NULL) && 
      (*(degreesString + degreesDecimalPointPosition) == NMEA_SENTENCE_DECIMAL_POINT) &&
      (degreesStringLength > degreesDecimalPointPosition))
    {
    *degrees = 0;
    *minutes = 0.0;

    // Convert the ASCII "degrees" to numerical format
    for (digitIndex = 0; digitIndex < degreesLength; digitIndex++)
      {
      *degrees = *degrees + ((*(degreesString + digitIndex)) - NMEA_GPS_ASCII_DIGIT_MINIMUM) * degreePowers[digitIndex + degreesTableIndexStart];
      }

    // Convert the ASCII "integer minutes" to numerical format
    for (digitIndex = degreesLength; digitIndex < degreesDecimalPointPosition; digitIndex++)
      {
      *minutes = *minutes + ((*(degreesString + digitIndex)) - NMEA_GPS_ASCII_DIGIT_MINIMUM) * minutesIntegerPowers[digitIndex - degreesLength];
      }

    // Skip the decimal point (it is kept in the string for checksumming)
    degreesDecimalPointPosition = degreesDecimalPointPosition + NMEA_SENTENCE_DECIMAL_POINT_STRING_LENGTH;

    // Convert the ASCII fractional field
    for (digitIndex = degreesDecimalPointPosition; digitIndex < degreesStringLength; digitIndex++)
      {
      *minutes = *minutes + ((*(degreesString + digitIndex)) - NMEA_GPS_ASCII_DIGIT_MINIMUM) * minutesFractionalPowers[digitIndex - degreesDecimalPointPosition];
      }

    // Test the minutes are in range
    if ((*minutes >= NMEA_FIELD_LATITUDE_MINIMUM_MINUTES) && (*minutes < NMEA_FIELD_LATITUDE_MAXIMUM_MINUTES))
      {
      // Test the full degrees + minutes are in range - convert the minutes to a true fraction of degrees
      fractionalDegrees = ((NMEA_GPS_FLOAT)*degrees) + ((*minutes) / NMEA_FIELD_LATITUDE_MAXIMUM_MINUTES);

      if ((fractionalDegrees >= degreesMinimumLimit) && (fractionalDegrees <= degreesMaximumLimit))
        {
        degreesResult = true;
        }
      }
    }

/******************************************************************************/

  return(degreesResult);

/******************************************************************************/
} /* end of nmeaConvertDegrees                                                */

/******************************************************************************/
/* nmeaConvertFloatingPoint() :                                               */
/*  --> asciiFloatingPoint       : floating-point number in ASCII-string      */
/*                                 representation                             */
/*  --> asciiFloatingPointLength : the TOTAL length of the floating-point     */
/*                                 string :                                   */
/*               [ <sign> ] + { integer-part } + <decimal-point> +            */
/*                            { fractional-part }                             */
/*  --> asciiFractionalLength    : length of the fractional part of the       */
/*                                 floating-point number                      */
/* <--> floatingPoint            : generated single-precision floating-point  */
/*                                 number                                     */
/*                                                                            */
/* - return the floating-point value represented by an ASCII string. It is    */
/*   required the string is in the correct format                             */
/*                                                                            */
/******************************************************************************/

_NMEA_INLINE_FUNCTION_ NMEA_GPS_BOOLEAN nmeaConvertFloatingPoint(const NMEA_GPS_UCHAR *asciiFloatingPoint,
                                                                 const NMEA_GPS_UINT8  asciiFloatingPointLength,
                                                                 const NMEA_GPS_UINT8  asciiFractionalLength,
                                                                       NMEA_GPS_FLOAT *floatingPoint)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN floatingResult  = false;

  NMEA_GPS_FLOAT   ascii2Float     = 0.0;

  NMEA_GPS_INT8    fieldIndex      = 0,
                   fractionalIndex = 0;
  
  NMEA_GPS_UINT8   asciiIndex      = 0,
                   fieldLength     = 0;

/******************************************************************************/

  if ((asciiFloatingPoint        != NULL) && (floatingPoint            != NULL) && 
      ((asciiFloatingPointLength >  0)    && (asciiFloatingPointLength <= NMEA_FLOAT_MAXIMUM_ASCII_LENGTH)) &&
      (asciiFractionalLength     != 0))
    {
    // Compute the integer field length
    fieldLength = asciiFloatingPointLength - asciiFractionalLength - NMEA_FLOAT_DECIMAL_POINT_LENGTH;

    *floatingPoint = 0.0;

    // Match the field index to the floating-point integer table
    for (fieldIndex = (fieldLength - 1); fieldIndex >= 0; fieldIndex--)
      {
      // Convert the ASCII decimal characters to (floating) decimal digits
       ascii2Float = (NMEA_GPS_FLOAT)((*(asciiFloatingPoint + asciiIndex)) - NMEA_GPS_ASCII_DIGIT_ZERO);

      // Multiply the decimal digit by the current power-of-ten
      *floatingPoint = *floatingPoint + (ascii2Float * nmeaFloatIntegers[fieldIndex]);

       asciiIndex    = asciiIndex     + 1;
      }

    // Advance past the decimal-point
    asciiIndex = asciiIndex               + 1;
    fieldIndex = asciiFloatingPointLength - asciiFractionalLength;

    // Now pick up the floating-point fractional table digits
    for ( ; fieldIndex < asciiFloatingPointLength; fieldIndex++)
      {
      // Convert the ASCII decimal characters to (floating) decimal digits
      ascii2Float = (NMEA_GPS_FLOAT)((*(asciiFloatingPoint + asciiIndex)) - NMEA_GPS_ASCII_DIGIT_ZERO);

      // Multiply the decimal digit by the current power-of-ten
      *floatingPoint = *floatingPoint + (ascii2Float * nmeaFloatFractionals[fractionalIndex]);

       fractionalIndex = fractionalIndex + 1;
       asciiIndex      = asciiIndex      + 1;
      }

    floatingResult = true;
    }

/******************************************************************************/

  return(floatingResult);

/******************************************************************************/
} /* end of nmeaConvertFloatingPoint                                          */

/******************************************************************************/
/* nmeaConvertAsciiHexToHex() :                                               */
/*   --> hexNumberIn       : character to convert to hex                      */
/*   <-- hexCharacterOut   : converted character                              */
/*   <-- conversionSuccess : error reporting                                  */
/*                                                                            */
/* - convert an ASCII hex character to its numeric equivalent                 */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaConvertAsciiHexToHex(NMEA_GPS_UINT8  hexCharacterIn,
                                          NMEA_GPS_UINT8 *hexNumberOut)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN conversionSuccess = false;

/******************************************************************************/

  if (hexNumberOut != NULL)
    {
    if (isxdigit(hexCharacterIn))
      {
      if (hexCharacterIn <= NMEA_ASCII_9)
        {
        *hexNumberOut = hexCharacterIn - NMEA_ASCII_0;

         conversionSuccess = true;
        }
      else
        {
        if ((hexCharacterIn >= NMEA_ASCII_UPPERCASE_A) && (hexCharacterIn <= NMEA_ASCII_UPPERCASE_F))
          {
          *hexNumberOut = hexCharacterIn - NMEA_ASCII_UPPERCASE_A + NMEA_ASCII_TO_HEX_OFFSET;

           conversionSuccess = true;
          }
        else
          {
          if ((hexCharacterIn >= NMEA_ASCII_LOWERCASE_a) && (hexCharacterIn <= NMEA_ASCII_LOWERCASE_f))
            {
            *hexNumberOut = hexCharacterIn - NMEA_ASCII_LOWERCASE_a + NMEA_ASCII_TO_HEX_OFFSET;

            conversionSuccess = true;
            }
          }
        }
      }
    }

/******************************************************************************/

  return(conversionSuccess);

/******************************************************************************/
} /* end of nmeaConvertAsciiHexToHex                                          */

/******************************************************************************/
/* nmeaConvertHexToAsciiHex() :                                               */
/*   --> hexNumberIn       : character to convert to ASCII hex                */
/*   <-- hexCharacterOut   : converted character                              */
/*   --> hexCase           : select ASCII lower-case or upper-case            */
/*   <-- conversionSuccess : error reporting                                  */
/*                                                                            */
/* - convert a 4-bit hex number to its upper- or lower-case ASCII equivalent  */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaConvertHexToAsciiHex(const NMEA_GPS_UINT8                hexNumberIn,
                                                NMEA_GPS_UINT8               *hexCharacterOut,
                                          const nmeaGenerateAsciiDigitCase_t  hexCase)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN conversionSuccess = false;
  NMEA_GPS_UINT8   asciiHexOffset    = 0;

/******************************************************************************/

  if ((hexCharacterOut != NULL)     && 
      (hexNumberIn     <= 0x0F)     &&
      ((hexCase == NMEA_ASCII_LOWERCASE_a) || (hexCase == NMEA_GENERATE_ASCII_CASE_UPPER)))
    {
    if (hexCase == NMEA_GENERATE_ASCII_CASE_LOWER)
      {
      asciiHexOffset = NMEA_ASCII_LOWERCASE_a;
      }
    else
      {
      if (hexCase == NMEA_GENERATE_ASCII_CASE_UPPER)
        {
        asciiHexOffset = NMEA_ASCII_UPPERCASE_A;
        }
      }

    if (/* (hexNumberIn >= 0) && */ (hexNumberIn <= 9))
      {
      *hexCharacterOut = hexNumberIn + NMEA_ASCII_0;
      }
    else
      {
      *hexCharacterOut = hexNumberIn - NMEA_ASCII_TO_HEX_OFFSET + asciiHexOffset;
      }

    conversionSuccess = true;
    }

/******************************************************************************/

  return(conversionSuccess);

/******************************************************************************/
} /* end of nmeaConvertHexToAsciiHex                                          */

/******************************************************************************/
/* nmeaValidateUtcTime() :                                                    */
/*   --> utcHours        :                                                    */
/*   --> utcMinutes      :                                                    */
/*   --> utcSeconds      :                                                    */
/*   --> utcCentiseconds :                                                    */
/*   <<-- result         : error detection                                    */
/*                                                                            */
/* - test the assembled UTC time against the limits                           */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaValidateUtcTime(const NMEA_GPS_UINT8 utcHours,
                                     const NMEA_GPS_UINT8 utcMinutes,
                                     const NMEA_GPS_UINT8 utcSeconds,
                                     const NMEA_GPS_UINT8 utcCentiseconds)
{
/******************************************************************************/

  bool result = false;

/******************************************************************************/

  if (/* (utcHours >= NMEA_FIELD_UTC_HOURS_MINIMUM)                     && */ (utcHours <= NMEA_FIELD_UTC_HOURS_MAXIMUM))
    {
    if (/* (utcMinutes >= NMEA_FIELD_UTC_MINUTES_MINIMUM)               && */ (utcMinutes <= NMEA_FIELD_UTC_MINUTES_MAXIMUM))
      {
      if (/* (utcSeconds >= NMEA_FIELD_UTC_SECONDS_MINIMUM)             && */ (utcSeconds <= NMEA_FIELD_UTC_SECONDS_MAXIMUM))
        {
        if (/* (utcCentiseconds >= NMEA_FIELD_UTC_CENTISECONDS_MINIMUM) && */ (utcCentiseconds <= NMEA_FIELD_UTC_CENTISECONDS_MAXIMUM))
          {
          result = true;
          }
        }
      }
    }

/******************************************************************************/

  return(result);

/******************************************************************************/
} /* end of nmeaValidateUtcTime                                               */

/******************************************************************************/
/* nmeaValidateDate() :                                                       */
/*   --> dateDays        :                                                    */
/*   --> dateMonths      :                                                    */
/*   --> dateYears       :                                                    */
/*   <<-- result         : error detection                                    */
/*                                                                            */
/* - test the assembled date against the limits                               */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaValidateDate(const NMEA_GPS_UINT8 dateDays,
                                  const NMEA_GPS_UINT8 dateMonths,
                                  const NMEA_GPS_UINT8 dateYears)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN result    = false;

  NMEA_GPS_UINT8 maximumDays = 0;

/******************************************************************************/

  // First barrier is a coarse check on maximum ranges
  if ((dateDays      >= NMEA_FIELD_DATE_DAYS_MINIMUM)   && (dateDays   <= NMEA_FIELD_DATE_DAYS_MAXIMUM))
    {
    if ((dateMonths  >= NMEA_FIELD_DATE_MONTHS_MINIMUM) && (dateMonths <= NMEA_FIELD_DATE_MONTHS_MAXIMUM))
      {
      if (/* (dateYears >= NMEA_FIELD_DATE_YEARS_MINIMUM) && */ (dateYears  <= NMEA_FIELD_DATE_YEARS_MAXIMUM))
        {
        // Test for maximum days - months and years have already passed. Also months 
        // January, March, May, July, August,October and December have already passed
        switch(dateMonths)
          {
          case NMEA_FIELD_DATE_FEBRUARY  : maximumDays = NMEA_FIELD_DATE_DAYS_28_MAXIMUM; // set the default
                                         
                                           // The two-digit date field limits the checking for leap-years; can't test for (years % 400)
                                           if ((dateYears + 1) != NMEA_FIELD_DATE_LEAP_YEARS_CENTURIES)
                                             { // IF ((years % 100) != 0) could be a leap-year
                                             if ((dateYears % NMEA_FIELD_DATE_LEAP_YEARS) == 0)
                                               {
                                               maximumDays = NMEA_FIELD_DATE_DAYS_LEAP_MAXIMUM;
                                               }
                                             }
                                           break;

          case NMEA_FIELD_DATE_APRIL     :
          case NMEA_FIELD_DATE_JUNE      :
          case NMEA_FIELD_DATE_SEPTEMBER :
          case NMEA_FIELD_DATE_NOVEMBER  : maximumDays = NMEA_FIELD_DATE_DAYS_30_MAXIMUM;
                                           break;

          default                        : // Force a failure at the "days" test
                                           maximumDays = NMEA_FIELD_DATE_DAYS_MAXIMUM + 1;
                                           break;
          }

        // Minimum days have aready passed
        if (dateDays <= maximumDays)
          {
          result = true;
          }
        }
      }
    }

 /******************************************************************************/

  return(result);

/******************************************************************************/
  } /* end of nmeaValidateDate                                                */

/******************************************************************************/
/* nmeaTestNumericalLimit() :                                                 */
/*  --> numberToTest    : agnostic pointer to numerical type to range-test    */
/*  --> limit           : agnostic pointer to numerical type as test-limit    */
/*  --> epsilon         : agnostic pointer to approximation band :-           */
/*                          -<epsilon> + <numberToTest> + <epsilon>           */
/*  --> limitTest       : type of limit to test against                       */
/*  <-- testResult      : test pass or fail. All tests are :-                 */
/*                         IS <numberToTest> RELATION-TO <limit>              */
/*  --> numericalFormat : the numerical format for the number and limit       */
/*  <-- limitTestResult : error conditional                                   */
/*                                                                            */
/*  - test a number against a limit. To ease the pain the following rules     */
/*    apply :-                                                                */
/*                                                                            */
/*      (i) all signed integer types are converted to signed 32-bit numbers   */
/*          (or 64-bit signed numbers if enabled)                             */
/*     (ii) all unsigned integer types are converted to unsigned 32-bit       */
/*          numbers (or 64-bit unsignednumbers if enabled)                    */
/*    (iii) only 32-bit floating-point numbers are enabled by default. If     */
/*          otherwise enabled 64-bit and 80-bit floating-point numbers are    */
/*          recognised (range-test not yet implemented for 64- and 80-bit)    */
/*                                                                            */
/*  - this scheme is adopted to (i) simplify the range-testing (ii) favour a  */
/*    32-bit ARM architecture                                                 */
/*                                                                            */
/******************************************************************************/

_NMEA_INLINE_FUNCTION_ static bool nmeaTestNumericalLimit(const void                      *numberToTest,
                                                          const void                      *limit,
                                                          const void                      *epsilon,
                                                                bool                      *testResult,
                                                          const nmeaNumericalLimitTests_t  limitTest,
                                                          const nmeaNumericalFormats_t     numericalFormat)
{
/******************************************************************************/

  bool                   limitTestResult = true;
  nmeaNumericalVariant_t variantNumber,
                         variantLimit,
                         variantEpsilon;;
  nmeaNumericalFormats_t appliedVariant; // which of the numerical formats to 
                                         // use for the test
/******************************************************************************/

  if ((numberToTest != NULL) && (limit != NULL))
    {
    switch(numericalFormat)
      {
      /******************************************************************************/
      /* Signed-integer conversions :                                               */
      /******************************************************************************/

      case NMEA_NUMERICAL_FORMAT_UINT8       : variantNumber.nmea_ulong_t  = (NMEA_GPS_ULONG)(*((NMEA_GPS_UINT8 *)numberToTest));
                                               variantLimit.nmea_ulong_t   = (NMEA_GPS_ULONG)(*((NMEA_GPS_UINT8 *)limit));
                                               variantEpsilon.nmea_ulong_t = (NMEA_GPS_ULONG)(*((NMEA_GPS_ULONG *)epsilon));
                                               appliedVariant              =  NMEA_NUMERICAL_FORMAT_ULONG;
	                                             break;



      case NMEA_NUMERICAL_FORMAT_UCHAR       : variantNumber.nmea_ulong_t  = (NMEA_GPS_ULONG)(*((NMEA_GPS_UCHAR *)numberToTest));
                                               variantLimit.nmea_ulong_t   = (NMEA_GPS_ULONG)(*((NMEA_GPS_UCHAR *)limit));
                                               variantEpsilon.nmea_ulong_t = (NMEA_GPS_ULONG)(*((NMEA_GPS_ULONG *)epsilon));
                                               appliedVariant              =  NMEA_NUMERICAL_FORMAT_ULONG;
	                                             break;

      case NMEA_NUMERICAL_FORMAT_UINT16      : variantNumber.nmea_ulong_t  = (NMEA_GPS_ULONG)(*((NMEA_GPS_UINT16 *)numberToTest));
                                               variantLimit.nmea_ulong_t   = (NMEA_GPS_ULONG)(*((NMEA_GPS_UINT16 *)limit));
                                               variantEpsilon.nmea_ulong_t = (NMEA_GPS_ULONG)(*((NMEA_GPS_ULONG *)epsilon));
                                               appliedVariant              =  NMEA_NUMERICAL_FORMAT_ULONG;
	                                             break;



      case NMEA_NUMERICAL_FORMAT_USHORT      : variantNumber.nmea_ulong_t  = (NMEA_GPS_ULONG)(*((NMEA_GPS_USHORT *)numberToTest));
                                               variantLimit.nmea_ulong_t   = (NMEA_GPS_ULONG)(*((NMEA_GPS_USHORT *)limit));
                                               variantEpsilon.nmea_ulong_t = (NMEA_GPS_ULONG)(*((NMEA_GPS_ULONG *)epsilon));
                                               appliedVariant              =  NMEA_NUMERICAL_FORMAT_ULONG;
	                                             break;


      case NMEA_NUMERICAL_FORMAT_UINT32      : variantNumber.nmea_ulong_t  = (NMEA_GPS_ULONG)(*((NMEA_GPS_UINT32 *)numberToTest));
                                               variantLimit.nmea_ulong_t   = (NMEA_GPS_ULONG)(*((NMEA_GPS_UINT32 *)limit));
                                               variantEpsilon.nmea_ulong_t = (NMEA_GPS_ULONG)(*((NMEA_GPS_ULONG *)epsilon));
                                               appliedVariant              =  NMEA_NUMERICAL_FORMAT_ULONG;
	                                             break;

      case NMEA_NUMERICAL_FORMAT_UINT        : variantNumber.nmea_ulong_t  = (NMEA_GPS_ULONG)(*((NMEA_GPS_UINT *)numberToTest));
                                               variantLimit.nmea_ulong_t   = (NMEA_GPS_ULONG)(*((NMEA_GPS_UINT *)limit));
                                               variantEpsilon.nmea_ulong_t = (NMEA_GPS_ULONG)(*((NMEA_GPS_ULONG *)epsilon));
                                               appliedVariant              =  NMEA_NUMERICAL_FORMAT_ULONG;
	                                             break;

      case NMEA_NUMERICAL_FORMAT_ULONG       : variantNumber.nmea_ulong_t  = *((NMEA_GPS_ULONG *)numberToTest);
                                               variantLimit.nmea_ulong_t   = *((NMEA_GPS_ULONG *)limit);
                                               variantEpsilon.nmea_ulong_t = *((NMEA_GPS_ULONG *)epsilon);
                                               appliedVariant              =  NMEA_NUMERICAL_FORMAT_ULONG;
                                               break;

#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
      case NMEA_NUMERICAL_FORMAT_UINT64      : variantNumber.nmea_uint64_t  = *((NMEA_GPS_UINT64 *)numberToTest);
                                               variantLimit.nmea_uint64_t   = *((NMEA_GPS_UINT64 *)limit);
                                               variantEpsilon.nmea_uint64_t = *((NMEA_GPS_UINT64 *)epsilon);
                                               appliedVariant               =  NMEA_NUMERICAL_FORMAT_UINT64;
	                                             break;
#endif

/******************************************************************************/
/* Unsigned-integer conversions :                                             */
/******************************************************************************/

      case NMEA_NUMERICAL_FORMAT_INT8        : variantNumber.nmea_long_t  = (NMEA_GPS_LONG)(*((NMEA_GPS_INT8 *)numberToTest));
                                               variantLimit.nmea_long_t   = (NMEA_GPS_LONG)(*((NMEA_GPS_INT8 *)limit));
                                               variantEpsilon.nmea_long_t = (NMEA_GPS_LONG)(*((NMEA_GPS_LONG *)epsilon));
                                               appliedVariant             =  NMEA_NUMERICAL_FORMAT_LONG;
                                               break;

      case NMEA_NUMERICAL_FORMAT_CHAR        : variantNumber.nmea_long_t  = (NMEA_GPS_LONG)(*((NMEA_GPS_CHAR *)numberToTest));
                                               variantLimit.nmea_long_t   = (NMEA_GPS_LONG)(*((NMEA_GPS_CHAR *)limit));
                                               variantEpsilon.nmea_long_t = (NMEA_GPS_LONG)(*((NMEA_GPS_LONG *)epsilon));
                                               appliedVariant             =  NMEA_NUMERICAL_FORMAT_LONG;
                                               break;

      case NMEA_NUMERICAL_FORMAT_INT16       : variantNumber.nmea_long_t  = (NMEA_GPS_LONG)(*((NMEA_GPS_INT16 *)numberToTest));
                                               variantLimit.nmea_long_t   = (NMEA_GPS_LONG)(*((NMEA_GPS_INT16 *)limit));
                                               variantEpsilon.nmea_long_t = (NMEA_GPS_LONG)(*((NMEA_GPS_LONG *)epsilon));
                                               appliedVariant             =  NMEA_NUMERICAL_FORMAT_LONG;
                                               break;

      case NMEA_NUMERICAL_FORMAT_SHORT       : variantNumber.nmea_long_t  = (NMEA_GPS_LONG)(*((NMEA_GPS_SHORT *)numberToTest));
                                               variantLimit.nmea_long_t   = (NMEA_GPS_LONG)(*((NMEA_GPS_SHORT *)limit));
                                               variantEpsilon.nmea_long_t = (NMEA_GPS_LONG)(*((NMEA_GPS_LONG *)epsilon));
                                               appliedVariant             =  NMEA_NUMERICAL_FORMAT_LONG;
                                               break;

      case NMEA_NUMERICAL_FORMAT_INT32       : variantNumber.nmea_long_t  = (NMEA_GPS_LONG)(*((NMEA_GPS_INT32 *)numberToTest));
                                               variantLimit.nmea_long_t   = (NMEA_GPS_LONG)(*((NMEA_GPS_INT32 *)limit));
                                               variantEpsilon.nmea_long_t = (NMEA_GPS_LONG)(*((NMEA_GPS_LONG *)epsilon));
                                               appliedVariant             =  NMEA_NUMERICAL_FORMAT_LONG;
                                               break;

      case NMEA_NUMERICAL_FORMAT_INT         : variantNumber.nmea_long_t  = (NMEA_GPS_LONG)(*((NMEA_GPS_INT  *)numberToTest));
                                               variantLimit.nmea_long_t   = (NMEA_GPS_LONG)(*((NMEA_GPS_INT  *)limit));
                                               variantEpsilon.nmea_long_t = (NMEA_GPS_LONG)(*((NMEA_GPS_LONG *)epsilon));
                                               appliedVariant             =  NMEA_NUMERICAL_FORMAT_LONG;
                                               break;

      case NMEA_NUMERICAL_FORMAT_LONG        : variantNumber.nmea_long_t  = *((NMEA_GPS_LONG *)numberToTest);
                                               variantLimit.nmea_long_t   = *((NMEA_GPS_LONG *)limit);
                                               variantEpsilon.nmea_long_t = *((NMEA_GPS_LONG *)epsilon);
                                               appliedVariant             =  NMEA_NUMERICAL_FORMAT_LONG;
	                                             break;

#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
      case NMEA_NUMERICAL_FORMAT_INT64       : variantNumber.nmea_int64_t  = *((NMEA_GPS_INT64 *)numberToTest);
                                               variantLimit.nmea_int64_t   = *((NMEA_GPS_INT64 *)limit);
                                               variantEpsilon.nmea_int64_t = *((NMEA_GPS_INT64 *)epsilon);
                                               appliedVariant               =  NMEA_NUMERICAL_FORMAT_UINT64;
                                               break;
#endif

/******************************************************************************/
/* Floating-point conversions :                                               */
/******************************************************************************/

      case NMEA_NUMERICAL_FORMAT_FLOAT       : variantNumber.nmea_float_t  = *((NMEA_GPS_FLOAT *)numberToTest);
                                               variantLimit.nmea_float_t   = *((NMEA_GPS_FLOAT *)limit);
                                               variantEpsilon.nmea_float_t = *((NMEA_GPS_FLOAT *)epsilon);
                                               appliedVariant              =  NMEA_NUMERICAL_FORMAT_FLOAT;
	                                             break;

#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
      case NMEA_NUMERICAL_FORMAT_DOUBLE      : variantNumber.nmea_double_t  = *((NMEA_GPS_DOUBLE *)numberToTest);
                                               variantLimit.nmea_double_t   = *((NMEA_GPS_DOUBLE *)limit);
                                               variantEpsilon.nmea_double_t = *((NMEA_GPS_DOUBLE *)epsilon);
                                               appliedVariant               =   NMEA_NUMERICAL_FORMAT_DOUBLE;
	                                             break;

      case NMEA_NUMERICAL_FORMAT_LONG_DOUBLE : variantNumber.nmea_long_double_t  = *((NMEA_GPS_LONG_DOUBLE *)numberToTest);
                                               variantLimit.nmea_long_double_t   = *((NMEA_GPS_LONG_DOUBLE *)limit);
                                               variantEpsilon.nmea_long_double_t = *((NMEA_GPS_LONG_DOUBLE *)epsilon);
                                               appliedVariant                    =  NMEA_NUMERICAL_FORMAT_LONG_DOUBLE;
	                                             break;
#endif
      default                                : limitTestResult = false;
	                                             break;
      }

/******************************************************************************/
/* Do the range test if the numerical type was recognised                     */
/******************************************************************************/

    if (limitTestResult == true)
      {
      *testResult = false;

       switch(limitTest)
         {
         case NMEA_NUMERICAL_LIMIT_TEST_NOT_EQUAL        : switch(appliedVariant)
                                                            {
                                                            case NMEA_NUMERICAL_FORMAT_ULONG : if (variantNumber.nmea_ulong_t != variantLimit.nmea_ulong_t)
                                                                                                 {
                                                                                                 *testResult = true;
                                                                                                 }
                                                                                               break;
                                                            case NMEA_NUMERICAL_FORMAT_LONG  : if (variantNumber.nmea_long_t != variantLimit.nmea_long_t)
                                                                                                 {
                                                                                                 *testResult = true;
                                                                                                 }
                                                                                               break;
                                                            case NMEA_NUMERICAL_FORMAT_FLOAT : if (variantNumber.nmea_float_t != variantLimit.nmea_float_t)
                                                                                                 {
                                                                                                 *testResult = true;
                                                                                                 }
                                                                                               break;

                                                            case   NMEA_NUMERICAL_FORMAT_VOID        :
                                                            case   NMEA_NUMERICAL_FORMAT_BOOLEAN     :
                                                            case   NMEA_NUMERICAL_FORMAT_INT8        :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT8       :
                                                            case   NMEA_NUMERICAL_FORMAT_CHAR        :
                                                            case   NMEA_NUMERICAL_FORMAT_UCHAR       :
                                                            case   NMEA_NUMERICAL_FORMAT_INT16       :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT16      :
                                                            case   NMEA_NUMERICAL_FORMAT_SHORT       :
                                                            case   NMEA_NUMERICAL_FORMAT_USHORT      :
                                                            case   NMEA_NUMERICAL_FORMAT_INT32       :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT32      :
                                                            case   NMEA_NUMERICAL_FORMAT_INT         :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT        :
#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
                                                            case   NMEA_NUMERICAL_FORMAT_INT64       :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT64      :
#endif
#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
                                                            case   NMEA_NUMERICAL_FORMAT_DOUBLE      :
                                                            case   NMEA_NUMERICAL_FORMAT_LONG_DOUBLE :
#endif
                                                            case   NMEA_NUMERICAL_FORMATS            :
                                                                                               break;
                                                            }

                                                           break;

         case NMEA_NUMERICAL_LIMIT_TEST_EQUAL            : switch(appliedVariant)
                                                             {
                                                             case NMEA_NUMERICAL_FORMAT_ULONG : if (variantNumber.nmea_ulong_t == variantLimit.nmea_ulong_t)
                                                               {
                                                               *testResult = true;
                                                               }
                                                                                              break;
                                                             case NMEA_NUMERICAL_FORMAT_LONG  : if (variantNumber.nmea_long_t == variantLimit.nmea_long_t)
                                                               {
                                                               *testResult = true;
                                                               }
                                                                                              break;
                                                             case NMEA_NUMERICAL_FORMAT_FLOAT : if (variantNumber.nmea_float_t == variantLimit.nmea_float_t)
                                                               {
                                                               *testResult = true;
                                                               }
                                                                                              break;
                                                            case   NMEA_NUMERICAL_FORMAT_VOID        :
                                                            case   NMEA_NUMERICAL_FORMAT_BOOLEAN     :
                                                            case   NMEA_NUMERICAL_FORMAT_INT8        :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT8       :
                                                            case   NMEA_NUMERICAL_FORMAT_CHAR        :
                                                            case   NMEA_NUMERICAL_FORMAT_UCHAR       :
                                                            case   NMEA_NUMERICAL_FORMAT_INT16       :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT16      :
                                                            case   NMEA_NUMERICAL_FORMAT_SHORT       :
                                                            case   NMEA_NUMERICAL_FORMAT_USHORT      :
                                                            case   NMEA_NUMERICAL_FORMAT_INT32       :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT32      :
                                                            case   NMEA_NUMERICAL_FORMAT_INT         :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT        :
#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
                                                            case   NMEA_NUMERICAL_FORMAT_INT64       :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT64      :
#endif
#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
                                                            case   NMEA_NUMERICAL_FORMAT_DOUBLE      :
                                                            case   NMEA_NUMERICAL_FORMAT_LONG_DOUBLE :
#endif
                                                            case   NMEA_NUMERICAL_FORMATS            :
                                                                                               break;
                                                             }
                                                           break;

         case NMEA_NUMERICAL_LIMIT_TEST_GREATER          : switch(appliedVariant)
                                                             {
                                                             case NMEA_NUMERICAL_FORMAT_ULONG : if (variantNumber.nmea_ulong_t > variantLimit.nmea_ulong_t)
                                                               {
                                                               *testResult = true;
                                                               }
                                                                                              break;
                                                             case NMEA_NUMERICAL_FORMAT_LONG  : if (variantNumber.nmea_long_t > variantLimit.nmea_long_t)
                                                               {
                                                               *testResult = true;
                                                               }
                                                                                              break;
                                                             case NMEA_NUMERICAL_FORMAT_FLOAT : if (variantNumber.nmea_float_t > variantLimit.nmea_float_t)
                                                               {
                                                               *testResult = true;
                                                               }
                                                                                              break;
                                                            case   NMEA_NUMERICAL_FORMAT_VOID        :
                                                            case   NMEA_NUMERICAL_FORMAT_BOOLEAN     :
                                                            case   NMEA_NUMERICAL_FORMAT_INT8        :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT8       :
                                                            case   NMEA_NUMERICAL_FORMAT_CHAR        :
                                                            case   NMEA_NUMERICAL_FORMAT_UCHAR       :
                                                            case   NMEA_NUMERICAL_FORMAT_INT16       :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT16      :
                                                            case   NMEA_NUMERICAL_FORMAT_SHORT       :
                                                            case   NMEA_NUMERICAL_FORMAT_USHORT      :
                                                            case   NMEA_NUMERICAL_FORMAT_INT32       :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT32      :
                                                            case   NMEA_NUMERICAL_FORMAT_INT         :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT        :
#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
                                                            case   NMEA_NUMERICAL_FORMAT_INT64       :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT64      :
#endif
#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
                                                            case   NMEA_NUMERICAL_FORMAT_DOUBLE      :
                                                            case   NMEA_NUMERICAL_FORMAT_LONG_DOUBLE :
#endif
                                                            case   NMEA_NUMERICAL_FORMATS            :
                                                                                               break;
                                                             }
                                                           break;

         case NMEA_NUMERICAL_LIMIT_TEST_GREATER_OR_EQUAL : switch(appliedVariant)
                                                             {
                                                             case NMEA_NUMERICAL_FORMAT_ULONG : if (variantNumber.nmea_ulong_t >= variantLimit.nmea_ulong_t)
                                                               {
                                                               *testResult = true;
                                                               }
                                                                                              break;
                                                             case NMEA_NUMERICAL_FORMAT_LONG  : if (variantNumber.nmea_long_t >= variantLimit.nmea_long_t)
                                                               {
                                                               *testResult = true;
                                                               }
                                                                                              break;
                                                             case NMEA_NUMERICAL_FORMAT_FLOAT : if (variantNumber.nmea_float_t >= variantLimit.nmea_float_t)
                                                               {
                                                               *testResult = true;
                                                               }
                                                                                              break;
                                                            case   NMEA_NUMERICAL_FORMAT_VOID        :
                                                            case   NMEA_NUMERICAL_FORMAT_BOOLEAN     :
                                                            case   NMEA_NUMERICAL_FORMAT_INT8        :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT8       :
                                                            case   NMEA_NUMERICAL_FORMAT_CHAR        :
                                                            case   NMEA_NUMERICAL_FORMAT_UCHAR       :
                                                            case   NMEA_NUMERICAL_FORMAT_INT16       :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT16      :
                                                            case   NMEA_NUMERICAL_FORMAT_SHORT       :
                                                            case   NMEA_NUMERICAL_FORMAT_USHORT      :
                                                            case   NMEA_NUMERICAL_FORMAT_INT32       :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT32      :
                                                            case   NMEA_NUMERICAL_FORMAT_INT         :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT        :
#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
                                                            case   NMEA_NUMERICAL_FORMAT_INT64       :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT64      :
#endif
#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
                                                            case   NMEA_NUMERICAL_FORMAT_DOUBLE      :
                                                            case   NMEA_NUMERICAL_FORMAT_LONG_DOUBLE :
#endif
                                                            case   NMEA_NUMERICAL_FORMATS            :
                                                                                               break;
                                                             }
                                                           break;

         case NMEA_NUMERICAL_LIMIT_TEST_LESSER           : switch(appliedVariant)
                                                             {
                                                             case NMEA_NUMERICAL_FORMAT_ULONG : if (variantNumber.nmea_ulong_t < variantLimit.nmea_ulong_t)
                                                             {
                                                               *testResult = true;
                                                             }
                                                                                              break;
                                                             case NMEA_NUMERICAL_FORMAT_LONG  : if (variantNumber.nmea_long_t < variantLimit.nmea_long_t)
                                                             {
                                                               *testResult = true;
                                                             }
                                                                                              break;
                                                             case NMEA_NUMERICAL_FORMAT_FLOAT : if (variantNumber.nmea_float_t < variantLimit.nmea_float_t)
                                                             {
                                                               *testResult = true;
                                                             }
                                                                                              break;
                                                            case   NMEA_NUMERICAL_FORMAT_VOID        :
                                                            case   NMEA_NUMERICAL_FORMAT_BOOLEAN     :
                                                            case   NMEA_NUMERICAL_FORMAT_INT8        :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT8       :
                                                            case   NMEA_NUMERICAL_FORMAT_CHAR        :
                                                            case   NMEA_NUMERICAL_FORMAT_UCHAR       :
                                                            case   NMEA_NUMERICAL_FORMAT_INT16       :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT16      :
                                                            case   NMEA_NUMERICAL_FORMAT_SHORT       :
                                                            case   NMEA_NUMERICAL_FORMAT_USHORT      :
                                                            case   NMEA_NUMERICAL_FORMAT_INT32       :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT32      :
                                                            case   NMEA_NUMERICAL_FORMAT_INT         :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT        :
#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
                                                            case   NMEA_NUMERICAL_FORMAT_INT64       :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT64      :
#endif
#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
                                                            case   NMEA_NUMERICAL_FORMAT_DOUBLE      :
                                                            case   NMEA_NUMERICAL_FORMAT_LONG_DOUBLE :
#endif
                                                            case   NMEA_NUMERICAL_FORMATS            :
                                                                                               break;
                                                             }
                                                           break;

         case NMEA_NUMERICAL_LIMIT_TEST_LESSER_OR_EQUAL  : switch(appliedVariant)
                                                             {
                                                             case NMEA_NUMERICAL_FORMAT_ULONG : if (variantNumber.nmea_ulong_t <= variantLimit.nmea_ulong_t)
                                                               {
                                                               *testResult = true;
                                                               }
                                                                                              break;
                                                             case NMEA_NUMERICAL_FORMAT_LONG  : if (variantNumber.nmea_long_t <= variantLimit.nmea_long_t)
                                                               {
                                                               *testResult = true;
                                                               }
                                                                                              break;
                                                             case NMEA_NUMERICAL_FORMAT_FLOAT : if (variantNumber.nmea_float_t <= variantLimit.nmea_float_t)
                                                              {
                                                               *testResult = true;
                                                               }
                                                                                              break;
                                                            case   NMEA_NUMERICAL_FORMAT_VOID        :
                                                            case   NMEA_NUMERICAL_FORMAT_BOOLEAN     :
                                                            case   NMEA_NUMERICAL_FORMAT_INT8        :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT8       :
                                                            case   NMEA_NUMERICAL_FORMAT_CHAR        :
                                                            case   NMEA_NUMERICAL_FORMAT_UCHAR       :
                                                            case   NMEA_NUMERICAL_FORMAT_INT16       :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT16      :
                                                            case   NMEA_NUMERICAL_FORMAT_SHORT       :
                                                            case   NMEA_NUMERICAL_FORMAT_USHORT      :
                                                            case   NMEA_NUMERICAL_FORMAT_INT32       :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT32      :
                                                            case   NMEA_NUMERICAL_FORMAT_INT         :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT        :
#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
                                                            case   NMEA_NUMERICAL_FORMAT_INT64       :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT64      :
#endif
#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
                                                            case   NMEA_NUMERICAL_FORMAT_DOUBLE      :
                                                            case   NMEA_NUMERICAL_FORMAT_LONG_DOUBLE :
#endif
                                                            case   NMEA_NUMERICAL_FORMATS            :
                                                                                               break;
                                                             }
                                                           break;

         case NMEA_NUMERICAL_LIMIT_TEST_APPROX_EQUAL     : switch(appliedVariant)
                                                             {
                                                             case NMEA_NUMERICAL_FORMAT_ULONG : if ((variantNumber.nmea_ulong_t < (variantLimit.nmea_ulong_t + variantEpsilon.nmea_ulong_t)) &&
                                                                                                    (variantNumber.nmea_ulong_t > (variantLimit.nmea_ulong_t - variantEpsilon.nmea_ulong_t)))
                                                               {
                                                               *testResult = true;
                                                               }
                                                                                              break;
                                                             case NMEA_NUMERICAL_FORMAT_LONG  : if ((variantNumber.nmea_long_t < (variantLimit.nmea_long_t + variantEpsilon.nmea_long_t)) &&
                                                                                                    (variantNumber.nmea_long_t > (variantLimit.nmea_long_t - variantEpsilon.nmea_long_t)))
                                                               {
                                                               *testResult = true;
                                                               }
                                                                                              break;
                                                             case NMEA_NUMERICAL_FORMAT_FLOAT : if ((variantNumber.nmea_float_t < (variantLimit.nmea_float_t + variantEpsilon.nmea_float_t)) &&
                                                                                                    (variantNumber.nmea_float_t > (variantLimit.nmea_float_t - variantEpsilon.nmea_float_t)))
                                                               {
                                                               *testResult = true;
                                                               }
                                                                                              break;
                                                            case   NMEA_NUMERICAL_FORMAT_VOID        :
                                                            case   NMEA_NUMERICAL_FORMAT_BOOLEAN     :
                                                            case   NMEA_NUMERICAL_FORMAT_INT8        :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT8       :
                                                            case   NMEA_NUMERICAL_FORMAT_CHAR        :
                                                            case   NMEA_NUMERICAL_FORMAT_UCHAR       :
                                                            case   NMEA_NUMERICAL_FORMAT_INT16       :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT16      :
                                                            case   NMEA_NUMERICAL_FORMAT_SHORT       :
                                                            case   NMEA_NUMERICAL_FORMAT_USHORT      :
                                                            case   NMEA_NUMERICAL_FORMAT_INT32       :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT32      :
                                                            case   NMEA_NUMERICAL_FORMAT_INT         :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT        :
#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
                                                            case   NMEA_NUMERICAL_FORMAT_INT64       :
                                                            case   NMEA_NUMERICAL_FORMAT_UINT64      :
#endif
#ifndef _NMEA_NUMERICAL_RANGE_ARM32_
                                                            case   NMEA_NUMERICAL_FORMAT_DOUBLE      :
                                                            case   NMEA_NUMERICAL_FORMAT_LONG_DOUBLE :
#endif
                                                            case   NMEA_NUMERICAL_FORMATS            :
                                                                                               break;
                                                             }
                                                            break;

         default                                         : limitTestResult = false;
         }
      }
    }

/******************************************************************************/

  return(limitTestResult);

/******************************************************************************/
} /* end of nmeaTestNumericalLimit                                            */

/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/
