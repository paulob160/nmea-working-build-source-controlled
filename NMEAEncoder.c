/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/
/*                                                                            */
/* NMEAEncoder.c                                                              */
/* 13.01.20                                                                   */
/*                                                                            */
/* - provides test facilities for the decoder                                 */
/*                                                                            */
/* NOTE : when sentences are built it is the responsibility of the upper-     */
/*        level segment builders to increment the sentence length index       */
/*                                                                            */
/* Ref :         NMEA 0183 Specification v2.3                                 */
/*               ublox NEO-6 - Data Sheet                                     */
/* Trimble Inc. "https://www.trimble.com/OEM_ReceiverHelp/V4.44/en/NMEA-"     */
/*               0183messages_MessageOverview.html", Accessed 11.20 23.02.20  */
/*                                                                            */
/******************************************************************************/

#include <windows.h>
#include <timeapi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#ifdef _NMEA_ACTIVE_SATELLITE_TEST_
#include <conio.h>
#endif
#include "NMEADecoder.h"
#include "NMEAEncoder.h"
#include "NMEAUtilities.h"
#include "NMEAGps.h"
#include "tinymt32.h"

/******************************************************************************/
/* nmeaEncoderGenerateSentenceGLL() :                                         */
/* <--> newSentence              : new sentence character array               */
/* <--> newSentenceIndex         : new sentence character array index         */
/* <--> randomState              : random state for this sentence             */
/*  --> randomSeedScheme         : select a fixed or time-dependent seed      */
/*                                                                            */
/* - encode and generate an NMEA "GLL" sentence                               */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaEncoderGenerateSentenceGLL(      NMEA_GPS_UCHAR                  *newSentence,
                                                      NMEA_GPS_UINT8                  *newSentenceIndex,
                                                      tinymt32_t                      *randomState,
                                                const nmeaEncoderSentenceSeedScheme_t  randomSeedScheme)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN sentenceResult  = false;
  NMEA_GPS_UINT32  randomSeed      = 0;
  NMEA_GPS_UINT8   runningChecksum = 0;

/******************************************************************************/

  // Select the seeding scheme for the random-number generator
  if (randomSeedScheme == NMEA_ENCODER_SEED_SCHEME_TIME)
    {
    if (nmeaEncoderRandomSeed(&randomSeed) != true)
      {
      randomSeed = MT_INITIAL_SEED;
      }
    }
  else
    {
    randomSeed = MT_INITIAL_SEED;
    }
 
  // Initialise and begin with an NMEA prefix character
  nmeaEncoderStartSentence(newSentence,
                           newSentenceIndex,
                           NMEA_SENTENCE_MAXIMUM_LENGTH,
                           randomState,
                           randomSeed);

  // Add a random sender code from the possible selection and the sentence prefix 
  // followed by a ',' (comma) character
  nmeaEncoderAddGNSSAndSentencePrefix(newSentence,
                                      newSentenceIndex,
                                      randomState,
                                      NMEA_SENTENCE_TYPE_GLL_STRING);

  // Add latitude
  nmeaEncoderAddLatLongField(newSentence,
                             newSentenceIndex,
                             NMEA_FIELD_LAT_LONG_SELECT_LATITUDE,
                             randomState,
                             NMEA_ENCODER_ZEROES_PAD);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add the latitude hemisphere
  nmeaEncoderAddCompassPoints(newSentence,
                             newSentenceIndex,
                             randomState,
                             NMEA_ENCODER_COMPASS_POINTS_NS);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add longitude
  nmeaEncoderAddLatLongField(newSentence,
                             newSentenceIndex,
                             NMEA_FIELD_LAT_LONG_SELECT_LONGITUDE,
                             randomState,
                             NMEA_ENCODER_ZEROES_PAD);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add the longitude direction
  nmeaEncoderAddCompassPoints(newSentence,
                              newSentenceIndex,
                              randomState,
                              NMEA_ENCODER_COMPASS_POINTS_EW);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add UTC
  nmeaEncoderGenerateUTCTime(newSentence,
                             newSentenceIndex,
                             randomState);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add a valid indicator
  nmeaEncoderGenerateRandomSingleField( newSentence,
                                        newSentenceIndex,
                                        randomState,
                                       &nmeaDataValid[0],
                                        NMEA_FIELD_DATA_VALIDS);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add an FAA mode
  nmeaEncoderGenerateRandomSingleField( newSentence,
                                        newSentenceIndex,
                                        randomState,
                                       &nmeaFAAMode[0],
                                        NMEA_FIELD_FAA_MODES);

  nmeaComputeChecksum(&newSentence[NMEA_SENTENCE_PREFIX_LENGTH], // do not include the '$' (dollar) prefix character
                      ((*newSentenceIndex) - NMEA_SENTENCE_PREFIX_LENGTH),
                      &runningChecksum);

  // Add the closing '*' (asterisk) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_CHECKSUM_PREFIX);

  // Finally add the checksum and the terminator
  nmeaEncoderAddChecksum(newSentence,
                         newSentenceIndex,
                         runningChecksum);

  nmeaEncoderAddTerminator(newSentence,
                           newSentenceIndex);

  sentenceResult = true;

/******************************************************************************/

  return(sentenceResult);

/******************************************************************************/
} /* end of nmeaEncoderGenerateSentenceGLL                                    */

/******************************************************************************/
/* nmeaEncoderGenerateSentenceRMC() :                                         */
/* <--> newSentence              : new sentence character array               */
/* <--> newSentenceIndex         : new sentence character array index         */
/* <--> randomState              : random state for this sentence             */
/*  --> randomSeedScheme         : select a fixed or time-dependent seed      */
/*                                                                            */
/* - encode and generate an NMEA "RMC" sentence                               */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaEncoderGenerateSentenceRMC(      NMEA_GPS_UCHAR                  *newSentence,
                                                      NMEA_GPS_UINT8                  *newSentenceIndex,
                                                      tinymt32_t                      *randomState,
                                                const nmeaEncoderSentenceSeedScheme_t  randomSeedScheme)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN sentenceResult  = false;
  NMEA_GPS_UINT32  randomSeed      = 0;
  NMEA_GPS_UINT8   runningChecksum = 0;

/******************************************************************************/

  // Select the seeding scheme for the random-number generator
  if (randomSeedScheme == NMEA_ENCODER_SEED_SCHEME_TIME)
    {
    if (nmeaEncoderRandomSeed(&randomSeed) != true)
      {
      randomSeed = MT_INITIAL_SEED;
      }
    }
  else
    {
    randomSeed = MT_INITIAL_SEED;
    }

  // Initialise and begin with an NMEA prefix character
  nmeaEncoderStartSentence(newSentence,
                           newSentenceIndex,
                           NMEA_SENTENCE_MAXIMUM_LENGTH,
                           randomState,
                           randomSeed);

  // Add a random sender code from the possible selection and the sentence prefix 
  // followed by a ',' (comma) character
  nmeaEncoderAddGNSSAndSentencePrefix(newSentence,
                                      newSentenceIndex,
                                      randomState,
                                      NMEA_SENTENCE_TYPE_RMC_STRING);

  // Add UTC
  nmeaEncoderGenerateUTCTime(newSentence,
                             newSentenceIndex,
                             randomState);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add a valid indicator
  nmeaEncoderGenerateRandomSingleField( newSentence,
                                        newSentenceIndex,
                                        randomState,
                                       &nmeaDataValid[0],
                                        NMEA_FIELD_DATA_VALIDS);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add latitude
  nmeaEncoderAddLatLongField(newSentence,
                             newSentenceIndex,
                             NMEA_FIELD_LAT_LONG_SELECT_LATITUDE,
                             randomState,
                             NMEA_ENCODER_ZEROES_PAD);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add the latitude hemisphere
  nmeaEncoderAddCompassPoints(newSentence,
                              newSentenceIndex,
                              randomState,
                              NMEA_ENCODER_COMPASS_POINTS_NS);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add longitude
  nmeaEncoderAddLatLongField(newSentence,
                             newSentenceIndex,
                             NMEA_FIELD_LAT_LONG_SELECT_LONGITUDE,
                             randomState,
                             NMEA_ENCODER_ZEROES_PAD);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add the longitude direction
  nmeaEncoderAddCompassPoints(newSentence,
                              newSentenceIndex,
                              randomState,
                              NMEA_ENCODER_COMPASS_POINTS_EW);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add "speed over ground"
  nmeaEncoderAddXOverGround(newSentence,
                            newSentenceIndex,
                            NMEA_ENCODER_OVER_GROUND_SPEED,
                            NMEA_ENCODER_ZEROES_IGNORE);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add "course over ground"
  nmeaEncoderAddXOverGround(newSentence,
                            newSentenceIndex,
                            NMEA_ENCODER_OVER_GROUND_COURSE,
                            NMEA_ENCODER_ZEROES_IGNORE);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add UTC date
  nmeaEncoderAddDate(newSentence,
                     newSentenceIndex,
                     randomState);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add "magnetic variation"
  nmeaEncoderAddXOverGround(newSentence,
                            newSentenceIndex,
                            NMEA_ENCODER_OVER_GROUND_MAGNETIC_VARIATION,
                            NMEA_ENCODER_ZEROES_IGNORE);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

#if (1)
  // Add the "magnetic variation" direction
  nmeaEncoderAddCompassPoints(newSentence,
                              newSentenceIndex,
                              randomState,
                              NMEA_ENCODER_COMPASS_POINTS_EW);
#endif

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add an FAA mode
  nmeaEncoderGenerateRandomSingleField( newSentence,
                                        newSentenceIndex,
                                        randomState,
                                       &nmeaFAAMode[0],
                                        NMEA_FIELD_FAA_MODES);

  nmeaComputeChecksum(&newSentence[NMEA_SENTENCE_PREFIX_LENGTH], // do not include the '$' (dollar) prefix character
                      ((*newSentenceIndex) - NMEA_SENTENCE_PREFIX_LENGTH),
                      &runningChecksum);

  // Add the closing '*' (asterisk) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_CHECKSUM_PREFIX);

  // Finally add the checksum and the terminator
  nmeaEncoderAddChecksum(newSentence,
                         newSentenceIndex,
                         runningChecksum);

  nmeaEncoderAddTerminator(newSentence,
                           newSentenceIndex);

  sentenceResult = true;

/******************************************************************************/

  return(sentenceResult);

/******************************************************************************/
} /* end of nmeaEncoderGenerateSentenceRMC                                    */

/******************************************************************************/
/* nmeaEncoderGenerateSentenceGSA() :                                         */
/* <--> newSentence              : new sentence character array               */
/* <--> newSentenceIndex         : new sentence character array index         */
/* <--> randomState              : random state for this sentence             */
/*  --> randomSeedScheme         : select a fixed or time-dependent seed      */
/*                                                                            */
/* - encode and generate an NMEA "GSA" sentence                               */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaEncoderGenerateSentenceGSA(      NMEA_GPS_UCHAR                  *newSentence,
                                                      NMEA_GPS_UINT8                  *newSentenceIndex,
                                                      tinymt32_t                      *randomState,
                                                const nmeaEncoderSentenceSeedScheme_t  randomSeedScheme)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN sentenceResult  = false;
  NMEA_GPS_UINT32  randomSeed      = 0;
  NMEA_GPS_UINT8   runningChecksum = 0;

/******************************************************************************/

  // Select the seeding scheme for the random-number generator
  if (randomSeedScheme == NMEA_ENCODER_SEED_SCHEME_TIME)
    {
    if (nmeaEncoderRandomSeed(&randomSeed) != true)
      {
      randomSeed = MT_INITIAL_SEED;
      }
    }
  else
    {
    randomSeed = MT_INITIAL_SEED;
    }

  // Initialise and begin with an NMEA prefix character
  nmeaEncoderStartSentence(newSentence,
                           newSentenceIndex,
                           NMEA_SENTENCE_MAXIMUM_LENGTH,
                           randomState,
                           randomSeed);

  // Add a random sender code from the possible selection and the sentence prefix 
  // followed by a ',' (comma) character
  nmeaEncoderAddGNSSAndSentencePrefix(newSentence,
                                      newSentenceIndex,
                                      randomState,
                                      NMEA_SENTENCE_TYPE_GSA_STRING);

  // Add the 'S' mode
  nmeaEncoderGenerateRandomSingleField( newSentence,
                                        newSentenceIndex,
                                        randomState,
                                       &nmeaSMode[0],
                                        NMEA_FIELD_S_MODES);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add the fix status
  nmeaEncoderGenerateRandomSingleField( newSentence,
                                        newSentenceIndex,
                                        randomState,
                                       &nmeaFixMode[0],
                                        NMEA_FIELD_FIX_STATUSES);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add 1 { <satellite> } 12 identifiers - because each satellite ID is a field 
  // this function also adds the trailing field seperator per field
  nmeaEncoderAddActiveSatellites(newSentence,
                                 newSentenceIndex,
                                 randomState,
                                 NMEA_ACTIVE_SATELLITES_MINIMUM,
                                 NMEA_ACTIVE_SATELLITES_MAXIMUM,
                                 NMEA_ACITVE_SATELLITES_MINIMUM_ID,
                                 NMEA_ACITVE_SATELLITES_MAXIMUM_ID,
                                 NMEA_ACITVE_SATELLITES_FIELD_WIDTH,
                                 NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add Position DOP
  nmeaEncoderAddDilutionOfPrecision(newSentence,
                                    newSentenceIndex,
                                    randomState);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add Horizontal DOP
  nmeaEncoderAddDilutionOfPrecision(newSentence,
                                    newSentenceIndex,
                                    randomState);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add Vertical DOP
  nmeaEncoderAddDilutionOfPrecision(newSentence,
                                    newSentenceIndex,
                                    randomState);

  nmeaComputeChecksum(&newSentence[NMEA_SENTENCE_PREFIX_LENGTH], // do not include the '$' (dollar) prefix character
                       ((*newSentenceIndex) - NMEA_SENTENCE_PREFIX_LENGTH),
                      &runningChecksum);

  // Add the closing '*' (asterisk) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_CHECKSUM_PREFIX);

  // Finally add the checksum and the terminator
  nmeaEncoderAddChecksum(newSentence,
                         newSentenceIndex,
                         runningChecksum);

  nmeaEncoderAddTerminator(newSentence,
                           newSentenceIndex);

  sentenceResult = true;

/******************************************************************************/

  return(sentenceResult);

/******************************************************************************/
} /* end of nmeaEncoderGenerateSentenceGSA                                    */

/******************************************************************************/
/* nmeaEncoderGenerateSentenceGGA() :                                         */
/* <--> newSentence              : new sentence character array               */
/* <--> newSentenceIndex         : new sentence character array index         */
/* <--> randomState              : random state for this sentence             */
/*  --> randomSeedScheme         : select a fixed or time-dependent seed      */
/*                                                                            */
/* - encode and generate an NMEA "GGA" sentence                               */
/*                                                                            */
/* Ref : https://docs.novatel.com/OEM7/Content/Logs/GPGGA.htm*//*accessed :   */
/*       11:16 24.04.20                                                       */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaEncoderGenerateSentenceGGA(      NMEA_GPS_UCHAR                  *newSentence,
                                                      NMEA_GPS_UINT8                  *newSentenceIndex,
                                                      tinymt32_t                      *randomState,
                                                const nmeaEncoderSentenceSeedScheme_t  randomSeedScheme)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN       sentenceResult  = false;
  NMEA_GPS_UINT32        randomSeed      = 0;
  NMEA_GPS_UINT8         runningChecksum = 0,
                         fieldLength     = 0;
           
  NMEA_GPS_FLOAT         signDecision    = 0.0;

  NMEA_GPS_UCHAR         generatedField[NMEA_ENCODE_MAXIMUM_GENERATED_FIELD_LENGTH];

  nmeaNumericalVariant_t fieldValue;

/******************************************************************************/

  // Select the seeding scheme for the random-number generator
  if (randomSeedScheme == NMEA_ENCODER_SEED_SCHEME_TIME)
    {
    if (nmeaEncoderRandomSeed(&randomSeed) != true)
      {
      randomSeed = MT_INITIAL_SEED;
      }
    }
  else
    {
    randomSeed = MT_INITIAL_SEED;
    }

  // Initialise and begin with an NMEA prefix character
  nmeaEncoderStartSentence(newSentence,
                           newSentenceIndex,
                           NMEA_SENTENCE_MAXIMUM_LENGTH,
                           randomState,
                           randomSeed);

  // Add a random sender code from the possible selection and the sentence prefix 
  // followed by a ',' (comma) character
  nmeaEncoderAddGNSSAndSentencePrefix(newSentence,
                                      newSentenceIndex,
                                      randomState,
                                      NMEA_SENTENCE_TYPE_GGA_STRING);

  // Add UTC
  nmeaEncoderGenerateUTCTime(newSentence,
                             newSentenceIndex,
                             randomState);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add latitude
  nmeaEncoderAddLatLongField(newSentence,
                             newSentenceIndex,
                             NMEA_FIELD_LAT_LONG_SELECT_LATITUDE,
                             randomState,
                             NMEA_ENCODER_ZEROES_PAD);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add the latitude hemisphere
  nmeaEncoderAddCompassPoints(newSentence,
                              newSentenceIndex,
                              randomState,
                              NMEA_ENCODER_COMPASS_POINTS_NS);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add longitude
  nmeaEncoderAddLatLongField(newSentence,
                             newSentenceIndex,
                             NMEA_FIELD_LAT_LONG_SELECT_LONGITUDE,
                             randomState,
                             NMEA_ENCODER_ZEROES_PAD);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add the longitude direction
  nmeaEncoderAddCompassPoints(newSentence,
                              newSentenceIndex,
                              randomState,
                              NMEA_ENCODER_COMPASS_POINTS_EW);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add the fix quality
  nmeaEncoderGenerateRandomSingleField( newSentence,
                                        newSentenceIndex,
                                        randomState,
                                       &nmeaFixQuality[0],
                                        NMEA_FIELD_FIX_QUALITIES);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  /******************************************************************************/
  /* Add the number of satellites used for the fix                              */
  /******************************************************************************/

  nmeaGenerateRandomVariable(&generatedField[0],
                             &fieldValue,
                             &fieldLength,
                              NMEA_FIELD_DECIMAL_INTEGER,
                              NMEA_SENTENCE_NUMERICAL_SIGN_UNSIGNED,
                              NMEA_GGA_NUMBER_OF_SATELLITES_FIELD_WIDTH,
                              NMEA_GGA_NUMBER_OF_SATELLITES_MINIMUM,
                              NMEA_GGA_NUMBER_OF_SATELLITES_MAXIMUM,
                              0,
                              0,
                              0);
  
  Sleep(1); // force a random-number update

  nmeaLoadIntegerAsDecimalAscii(newSentence,
                                newSentenceIndex,
                                fieldValue.nmea_int32_t,
                                NMEA_GGA_NUMBER_OF_SATELLITES_FIELD_WIDTH,
                                NMEA_NUMBER_SIGN_NONE);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  /******************************************************************************/

  // Add Horizontal DOP
  nmeaEncoderAddDilutionOfPrecision(newSentence,
                                    newSentenceIndex,
                                    randomState);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  /******************************************************************************/
  /* Add the fix altitude                                                       */
  /******************************************************************************/
 
  // Make an arbitrary sign decision
  signDecision = tinymt32_generate_float(randomState);

  if (signDecision >= NMEA_BINARY_DECISION_THRESHOLD_FLOAT)
    {
    // Prefix the number as "signed" with a '-' character
    *(newSentence + *newSentenceIndex)    = NMEA_GPS_ASCII_DIGIT_MINUS;
    *newSentenceIndex = *newSentenceIndex + 1;
    }

  // Generate the value as unsigned and write it into the test string
  nmeaGenerateRandomVariable((newSentence + *newSentenceIndex),
                             &fieldValue,
                             &fieldLength,
                              NMEA_FIELD_DECIMAL_FLOATING,
                              NMEA_SENTENCE_NUMERICAL_SIGN_UNSIGNED,
                              NMEA_GGA_ALTITUDE_INTEGER_PART_MAXIMUM_LENGTH,
                              0, // NMEA_GGA_ALTITUDE_INTEGER_MINIMUM_LIMIT,
                              NMEA_GGA_ALTITUDE_INTEGER_MAXIMUM_LIMIT,
                              NMEA_GGA_ALTITUDE_FRACTIONAL_PART_MAXIMUM_LENGTH,
                              NMEA_GGA_ALTITUDE_FRACTIONAL_MINIMUM_LIMIT,
                              NMEA_GGA_ALTITUDE_FRACTIONAL_MAXIMUM_LIMIT);

  // The index needs to be written explicitly; it is not updated in "nmeaGenerateRandomVariable()"
  *newSentenceIndex = *newSentenceIndex + fieldLength;

  Sleep(1); // force a random-number update

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add the altitude units "metres" fixed field
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_FIELD_METRES);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  /******************************************************************************/
  /* Add the height above/below the WGS84 geoid                                 */
  /******************************************************************************/
 
  // Make an arbitrary sign decision
  signDecision = tinymt32_generate_float(randomState);

  if (signDecision >= NMEA_BINARY_DECISION_THRESHOLD_FLOAT)
    {
    // Prefix the number as "signed" with a '-' character
    *(newSentence + *newSentenceIndex)    = NMEA_GPS_ASCII_DIGIT_MINUS;
    *newSentenceIndex = *newSentenceIndex + 1;
    }

  // Generate the value as unsigned and write it into the test string
  nmeaGenerateRandomVariable((newSentence + *newSentenceIndex),
                             &fieldValue,
                             &fieldLength,
                              NMEA_FIELD_DECIMAL_FLOATING,
                              NMEA_SENTENCE_NUMERICAL_SIGN_UNSIGNED,
                              NMEA_GGA_GEOID_INTEGER_PART_MAXIMUM_LENGTH,
                              0,
                              NMEA_GGA_GEOID_INTEGER_MAXIMUM_LIMIT,
                              NMEA_GGA_GEOID_FRACTIONAL_PART_MAXIMUM_LENGTH,
                              NMEA_GGA_GEOID_FRACTIONAL_MINIMUM_LIMIT,
                              NMEA_GGA_GEOID_FRACTIONAL_MAXIMUM_LIMIT);

  // The index needs to be written explicitly; it is not updated in "nmeaGenerateRandomVariable()"
  *newSentenceIndex = *newSentenceIndex + fieldLength;

  Sleep(1); // force a random-number update

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  // Add the geoid units "metres" fixed field
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_FIELD_METRES);

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  /******************************************************************************/
  /* Add the age of differential correction data (when DGPS is in use)          */
  /******************************************************************************/

  nmeaGenerateRandomVariable((newSentence + *newSentenceIndex),
                             &fieldValue,
                             &fieldLength,
                              NMEA_FIELD_DECIMAL_INTEGER,
                              NMEA_SENTENCE_NUMERICAL_SIGN_UNSIGNED,
                              NMEA_GGA_DGPS_AGE_INTEGER_PART_MAXIMUM_LENGTH,
                              NMEA_GGA_DGPS_AGE_INTEGER_MINIMUM_LIMIT,
                              NMEA_GGA_DGPS_AGE_INTEGER_MAXIMUM_LIMIT,
                              0,
                              0,
                              0);

  // The index needs to be written explicitly; it is not updated in "nmeaGenerateRandomVariable()"
  *newSentenceIndex = *newSentenceIndex + fieldLength;

  Sleep(1); // force a random-number update

  // Add a ',' (comma) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_SEPERATOR);

  /******************************************************************************/
  /* Add the differential base station ID (when DGPS is in use)                 */
  /******************************************************************************/

  nmeaGenerateRandomVariable((newSentence + *newSentenceIndex),
                             &fieldValue,
                             &fieldLength,
                              NMEA_FIELD_DECIMAL_INTEGER,
                              NMEA_SENTENCE_NUMERICAL_SIGN_UNSIGNED,
                              NMEA_GGA_DGPS_BASE_INTEGER_PART_MAXIMUM_LENGTH,
                              NMEA_GGA_DGPS_BASE_INTEGER_MINIMUM_LIMIT,
                              NMEA_GGA_DGPS_BASE_INTEGER_MAXIMUM_LIMIT,
                              0,
                              0,
                              0);

  // The index needs to be written explicitly; it is not updated in "nmeaGenerateRandomVariable()"
  *newSentenceIndex = *newSentenceIndex + fieldLength;

  /******************************************************************************/

  nmeaComputeChecksum(&newSentence[NMEA_SENTENCE_PREFIX_LENGTH], // do not include the '$' (dollar) prefix character
                       ((*newSentenceIndex) - NMEA_SENTENCE_PREFIX_LENGTH),
                      &runningChecksum);

  // Add the closing '*' (asterisk) character
  nmeaEncoderAddCharacter(newSentence,
                          newSentenceIndex,
                          NMEA_SENTENCE_FIELD_CHECKSUM_PREFIX);

  // Finally add the checksum and the terminator
  nmeaEncoderAddChecksum(newSentence,
                         newSentenceIndex,
                         runningChecksum);

  nmeaEncoderAddTerminator(newSentence,
                           newSentenceIndex);

  sentenceResult = true;

/******************************************************************************/

  return(sentenceResult);

/******************************************************************************/
  } /*end of nmeaEncoderGenerateSentenceGGA                                   */

/******************************************************************************/
/* nmeaEncoderGenerateSentenceGSV() :                                         */
/* <--> newGSVSentences          : array of new sentence character arrays     */
/* <--> newGSVSentenceIndices    : new sentence character array indices       */
/* <--> numberOfGSVSentences     : number of GSV sentences generated          */
/* <--> randomState              : random state for this sentence             */
/*  --> randomSeedScheme         : select a fixed or time-dependent seed      */
/*                                                                            */
/* - encode and generate one or more GSV sentences. Allocates memory to store */
/*   the sentences and their respective lengths and passes the addresses to   */
/*   the calling function. It is the responsibility of the calling function   */
/*   to deallocate the memory                                                 */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaEncoderGenerateSentenceGSV(      NMEA_GPS_UCHAR                  ***newGSVSentences,
                                                      NMEA_GPS_UINT8                   **newGSVSentenceIndices,
                                                      NMEA_GPS_UINT8                    *numberOfGSVSentences,
                                                      tinymt32_t                        *randomState,
                                                const nmeaEncoderSentenceSeedScheme_t    randomSeedScheme)
  {
/******************************************************************************/

  NMEA_GPS_BOOLEAN       sentenceResult      = false;
  NMEA_GPS_UINT32        randomSeed          = 0;
  NMEA_GPS_UINT8         runningChecksum     = 0,
                         numberOfMessages    = 0,
                         remainingSatellites = 0,
                         numberOfSatellites  = 0,
                         messageIndex        = 0,
                         satelliteIndex      = 0,
                         satelliteNumber     = 0,
                         satellitesInMessage = 0,
                         fieldLength         = 0;

  NMEA_GPS_INT32         minimumLimit        = 0,
                         maximumLimit        = 0;

  NMEA_GPS_UCHAR         generatedField[NMEA_ENCODE_MAXIMUM_GENERATED_FIELD_LENGTH];

  // 1 - 64 scrambled satellite ids
  nmeaNumericalVariant_t randomSatelliteIds[NMEA_GSV_MAXIMUM_NUMBER_OF_SATELLITES],
                         fieldValue;

/******************************************************************************/

  // Select the seeding scheme for the random-number generator
  if (randomSeedScheme == NMEA_ENCODER_SEED_SCHEME_TIME)
    {
    if (nmeaEncoderRandomSeed(&randomSeed) != true)
      {
      randomSeed = MT_INITIAL_SEED;
      }
    }
  else
    {
    randomSeed = MT_INITIAL_SEED;
    }

  // Generate the scrambled satellite ids
  nmeaGenerateScrambledSequentialNumbers(&randomSatelliteIds[0],
                                          NMEA_GSV_MAXIMUM_NUMBER_OF_SATELLITES,
                                          randomSeed);

  // The generator/scrambler produces numbers in the range 0 <= <number> < NMEA_GSV_MAXIMUM_NUMBER_OF_SATELLITES;
  // the GPS signal uses 1 <= <number> <= NMEA_GSV_MAXIMUM_NUMBER_OF_SATELLITES
  for (satelliteIndex = 0; satelliteIndex < NMEA_GSV_MAXIMUM_NUMBER_OF_SATELLITES; satelliteIndex++)
    {
    randomSatelliteIds[satelliteIndex].nmea_uint8_t = randomSatelliteIds[satelliteIndex].nmea_uint8_t + 1;
    }

  // Generate the number of satellites for the set of messages
  while (numberOfSatellites < NMEA_GSV_MINIMUM_NUMBER_OF_SATELLITES)
    {
    numberOfSatellites = (NMEA_GPS_UINT8)(tinymt32_generate_float(randomState) * ((NMEA_GPS_FLOAT)NMEA_GSV_MAXIMUM_NUMBER_OF_SATELLITES));
    }

#ifdef _NMEA_FIXED_NUMBER_OF_SATELLITES_
  {
  NMEA_GPS_UINT8 fixedNumberOfSatellites[1] = { 0 };

  printf("\n Enter the number of satellites : ");
  scanf_s("%hhd", &fixedNumberOfSatellites[0]);

  numberOfSatellites = fixedNumberOfSatellites[0]; // force a final message size of 4
  }

#endif

   remainingSatellites  = numberOfSatellites;

   numberOfMessages     = ((numberOfSatellites - 1) / NMEA_GSV_MAXIMUM_SATELLITES_PER_MESSAGE) + 1;

  *numberOfGSVSentences = numberOfMessages;

  // Each message requires a character array for construction and storage
  *newGSVSentences       = (NMEA_GPS_UCHAR **)calloc(numberOfMessages, sizeof(NMEA_GPS_UCHAR *));
  *newGSVSentenceIndices = (NMEA_GPS_UINT8  *)calloc(numberOfMessages, sizeof(NMEA_GPS_UINT8));

  if ((*newGSVSentences != NULL) && (*newGSVSentenceIndices != NULL))
    {
    for (messageIndex = (NMEA_GSV_MINIMUM_NUMBER_OF_MESSAGES - 1); messageIndex < numberOfMessages; messageIndex++)
      {
      *((*newGSVSentences)       + messageIndex) = (NMEA_GPS_UCHAR *)calloc(1, NMEA_SENTENCE_MAXIMUM_LENGTH_STRING);

      *((*newGSVSentenceIndices) + messageIndex) = 0;
      }
    }

  if (((*newGSVSentences) != NULL) && ((*newGSVSentenceIndices) != NULL))
    {
    for (messageIndex = (NMEA_GSV_MINIMUM_NUMBER_OF_MESSAGES - 1); messageIndex < numberOfMessages; messageIndex++)
      {
      runningChecksum = 0;

      // Initialise and begin with an NMEA prefix character
      nmeaEncoderStartSentence(*((*newGSVSentences)       + messageIndex),
                                ((*newGSVSentenceIndices) + messageIndex),
                                NMEA_SENTENCE_MAXIMUM_LENGTH,
                                randomState,
                                randomSeed);
      
      // Add a random sender code from the possible selection and the sentence prefix 
      // followed by a ',' (comma) character
      nmeaEncoderAddGNSSAndSentencePrefix(*((*newGSVSentences)       + messageIndex),
                                           ((*newGSVSentenceIndices) + messageIndex),
                                           randomState,
                                           NMEA_SENTENCE_TYPE_GSV_STRING);
      
      // Add the total number of messages being output
      nmeaEncoderAddCharacter(*((*newGSVSentences)       + messageIndex),
                               ((*newGSVSentenceIndices) + messageIndex),
                               (numberOfMessages + NMEA_GPS_ASCII_DIGIT_ZERO));
      
      // Add a ',' (comma) character
      nmeaEncoderAddCharacter(*((*newGSVSentences)       + messageIndex),
                               ((*newGSVSentenceIndices) + messageIndex),
                               NMEA_SENTENCE_FIELD_SEPERATOR);
      
      // Add the number of this message; message numbers start from '1'
      nmeaEncoderAddCharacter(*((*newGSVSentences)       + messageIndex),
                               ((*newGSVSentenceIndices) + messageIndex),
                               ((messageIndex + NMEA_GSV_MINIMUM_NUMBER_OF_MESSAGES) + NMEA_GPS_ASCII_DIGIT_ZERO));
      
      // Add a ',' (comma) character
      nmeaEncoderAddCharacter(*((*newGSVSentences)       + messageIndex),
                               ((*newGSVSentenceIndices) + messageIndex),
                               NMEA_SENTENCE_FIELD_SEPERATOR);
      
      // Add the total number of satellites in view - just a two-digit field
      satelliteNumber = numberOfSatellites / NMEA_GPS_DIGIT_x_10; // the x10 digit
      
      // First digit
      nmeaEncoderAddCharacter(*((*newGSVSentences)       + messageIndex),
                               ((*newGSVSentenceIndices) + messageIndex),
                               (satelliteNumber + NMEA_GPS_ASCII_DIGIT_ZERO));
      
      satelliteNumber = numberOfSatellites - (satelliteNumber * NMEA_GPS_DIGIT_x_10 ); // the units digit
      
      // Second digit
      nmeaEncoderAddCharacter(*((*newGSVSentences)       + messageIndex),
                               ((*newGSVSentenceIndices) + messageIndex),
                               (satelliteNumber + NMEA_GPS_ASCII_DIGIT_ZERO));
      
      // Add a ',' (comma) character
      nmeaEncoderAddCharacter(*((*newGSVSentences)       + messageIndex),
                               ((*newGSVSentenceIndices) + messageIndex),
                               NMEA_SENTENCE_FIELD_SEPERATOR);
      
      /******************************************************************************/
      /* Add the satellite statistics for one message. This will be four for the    */
      /* fully filled messages and up to four for the final message                 *?
      /******************************************************************************/

      // Count down the satellite groups of four and detect the size of the final message
      if (remainingSatellites >= NMEA_GSV_MAXIMUM_SATELLITES_PER_MESSAGE)
        {
        satellitesInMessage = NMEA_GSV_MAXIMUM_SATELLITES_PER_MESSAGE;
        remainingSatellites = remainingSatellites - NMEA_GSV_MAXIMUM_SATELLITES_PER_MESSAGE;
        }
      else
        {
        // There are no empty packets...
        if (remainingSatellites == 0)
          {
          break;
          }
        else
          {
          satellitesInMessage = remainingSatellites;
          }
        }

      for (satelliteIndex = 0; satelliteIndex < satellitesInMessage; satelliteIndex++)
        {
        // Force an empty field if required
        if (tinymt32_generate_float(randomState) > NMEA_GSV_EMPTY_FIELD_THRESHOLD)
          {
          // Add a satellite id
          satelliteNumber = randomSatelliteIds[(messageIndex * NMEA_GSV_MAXIMUM_SATELLITES_PER_MESSAGE) + satelliteIndex].nmea_uint8_t / NMEA_GPS_DIGIT_x_10; // the x10 digit
          
          // First digit
          nmeaEncoderAddCharacter(*((*newGSVSentences)       + messageIndex),
                                   ((*newGSVSentenceIndices) + messageIndex),
                                   (satelliteNumber + NMEA_GPS_ASCII_DIGIT_ZERO));
          
          satelliteNumber = randomSatelliteIds[(messageIndex * NMEA_GSV_MAXIMUM_SATELLITES_PER_MESSAGE) + satelliteIndex].nmea_uint8_t - (satelliteNumber * NMEA_GPS_DIGIT_x_10 ); // the units digit
          
          // Second digit
          nmeaEncoderAddCharacter(*((*newGSVSentences)       + messageIndex),
                                   ((*newGSVSentenceIndices) + messageIndex),
                                   (satelliteNumber + NMEA_GPS_ASCII_DIGIT_ZERO));
          }

        // Add a ',' (comma) character
        nmeaEncoderAddCharacter(*((*newGSVSentences)       + messageIndex),
                                 ((*newGSVSentenceIndices) + messageIndex),
                                 NMEA_SENTENCE_FIELD_SEPERATOR);

        /******************************************************************************/
        /*                          Add a random elevation                            */
        /******************************************************************************/

        minimumLimit = NMEA_GSV_ELEVATION_MINIMUM;
        maximumLimit = NMEA_GSV_ELEVATION_MAXIMUM;
        
        // Force an empty field if required
        if (tinymt32_generate_float(randomState) > NMEA_GSV_EMPTY_FIELD_THRESHOLD)
          {
          nmeaGenerateRandomVariable(&generatedField[0],
                                     &fieldValue,
                                     &fieldLength,
                                      NMEA_FIELD_DECIMAL_INTEGER,
                                      NMEA_SENTENCE_NUMERICAL_SIGN_SIGNED,
                                      NMEA_GSV_ELEVATION_FIELD_WIDTH,
                                      minimumLimit,
                                      maximumLimit,
                                      0,
                                      0,
                                      0);
          
          nmeaLoadIntegerAsDecimalAscii(*((*newGSVSentences)       + messageIndex),
                                         ((*newGSVSentenceIndices) + messageIndex),
                                         fieldValue.nmea_int32_t,
                                         NMEA_GSV_ELEVATION_FIELD_WIDTH, // fieldLength,
                                         NMEA_NUMBER_SIGN_MINUS);
          }

        // Add a ',' (comma) character
        nmeaEncoderAddCharacter(*((*newGSVSentences)       + messageIndex),
                                 ((*newGSVSentenceIndices) + messageIndex),
                                 NMEA_SENTENCE_FIELD_SEPERATOR);

        /******************************************************************************/
        /*                          Add a random azimuth                              */
        /******************************************************************************/

        minimumLimit = NMEA_GSV_AZIMUTH_MINIMUM;
        maximumLimit = NMEA_GSV_AZIMUTH_MAXIMUM;
        
        // Force an empty field if required
        if (tinymt32_generate_float(randomState) > NMEA_GSV_EMPTY_FIELD_THRESHOLD)
          {
          nmeaGenerateRandomVariable(&generatedField[0],
                                     &fieldValue,
                                     &fieldLength,
                                      NMEA_FIELD_DECIMAL_INTEGER,
                                      NMEA_SENTENCE_NUMERICAL_SIGN_UNSIGNED,
                                      NMEA_GSV_AZIMUTH_FIELD_WIDTH,
                                      minimumLimit,
                                      maximumLimit,
                                      0,
                                      0,
                                      0);

          Sleep(1); // force a random-number update
          
          nmeaLoadIntegerAsDecimalAscii(*((*newGSVSentences)       + messageIndex),
                                         ((*newGSVSentenceIndices) + messageIndex),
                                         fieldValue.nmea_int32_t,
                                         NMEA_GSV_AZIMUTH_FIELD_WIDTH,
                                         NMEA_NUMBER_SIGN_NONE);
          }

        // Add a ',' (comma) character
        nmeaEncoderAddCharacter(*((*newGSVSentences)       + messageIndex),
                                 ((*newGSVSentenceIndices) + messageIndex),
                                 NMEA_SENTENCE_FIELD_SEPERATOR);


        /******************************************************************************/
        /*                          Add a random C / N0                               */
        /******************************************************************************/

        minimumLimit = NMEA_GSV_C_N0_MINIMUM;
        maximumLimit = NMEA_GSV_C_N0_MAXIMUM;
        
        // Force an empty field if required
        if (tinymt32_generate_float(randomState) > NMEA_GSV_EMPTY_FIELD_THRESHOLD)
          {
          nmeaGenerateRandomVariable(&generatedField[0],
                                     &fieldValue,
                                     &fieldLength,
                                      NMEA_FIELD_DECIMAL_INTEGER,
                                      NMEA_SENTENCE_NUMERICAL_SIGN_UNSIGNED,
                                      NMEA_GSV_C_N0_FIELD_WIDTH,
                                      minimumLimit,
                                      maximumLimit,
                                      0,
                                      0,
                                      0);
          
          Sleep(1); // force a random-number update
          
          nmeaLoadIntegerAsDecimalAscii(*((*newGSVSentences)       + messageIndex),
                                         ((*newGSVSentenceIndices) + messageIndex),
                                         fieldValue.nmea_int32_t,
                                         NMEA_GSV_C_N0_FIELD_WIDTH,
                                         NMEA_NUMBER_SIGN_NONE);
          }

        // The final 'C / N0' in a sentence is NOT followed by a ','
        if (satelliteIndex < (satellitesInMessage - 1))
          {
          // Add a ',' (comma) character
          nmeaEncoderAddCharacter(*((*newGSVSentences)       + messageIndex),
                                   ((*newGSVSentenceIndices) + messageIndex),
                                   NMEA_SENTENCE_FIELD_SEPERATOR);
          }

        /******************************************************************************/
        }

      nmeaComputeChecksum(*((*newGSVSentences)        + messageIndex)  + NMEA_SENTENCE_PREFIX_LENGTH, // do not include the '$' (dollar) prefix character
                           *((*newGSVSentenceIndices) + messageIndex)  - NMEA_SENTENCE_PREFIX_LENGTH,
                          &runningChecksum);

      // Add the closing '*' (asterisk) character
      nmeaEncoderAddCharacter(*((*newGSVSentences)       + messageIndex),
                               ((*newGSVSentenceIndices) + messageIndex),
                               NMEA_SENTENCE_FIELD_CHECKSUM_PREFIX);
      
      // Finally add the checksum and the terminator
      nmeaEncoderAddChecksum(*((*newGSVSentences)       + messageIndex),
                              ((*newGSVSentenceIndices) + messageIndex),
                              runningChecksum);
      
      nmeaEncoderAddTerminator(*((*newGSVSentences)       + messageIndex),
                                ((*newGSVSentenceIndices) + messageIndex));
      }

    sentenceResult = true;
    }

/******************************************************************************/

  return(sentenceResult);

/******************************************************************************/
  } /* end of nmeaEncoderGenerateSentenceGSV                                  */

/******************************************************************************/
/* nmeaEncoderStartSentence() :                                               */
/* <--> newSentence              : new sentence character array               */
/* <--> newSentenceIndex         : new sentence character array index         */
/*  --> newSentenceMaximumLength : new sentence maximum epxected length       */
/* <--> randomState              : random state for this sentence             */
/*  --> randomSeed               : seed the PRNG                              */
/*                                                                            */
/* - clears a new sentence to <end-of-string> characters and starts the new   */
/*   sentence with the prefix character '$'.                                  */
/*   Each sentence can potentially have its own PRNG which is initialised     */
/*   here                                                                     */
/*   Using arbitrary setence arrays, lengths and indicies allows multiple     */
/*   sentences to be built re-entrantly and simultaneously                    */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaEncoderStartSentence(NMEA_GPS_UCHAR  *newSentence,
                                          NMEA_GPS_UINT8  *newSentenceIndex,
                                          NMEA_GPS_UINT8   newSentenceMaximumLength,
                                          tinymt32_t      *randomState,
                                          NMEA_GPS_UINT32  randomSeed)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN sentenceResult = false;
  NMEA_GPS_UINT8   sentenceIndex  = 0;

/******************************************************************************/

  if ((newSentence != NULL) && (newSentenceIndex != NULL) && (randomState != NULL))
    {
    tinymt32_init(randomState, randomSeed);

    for (sentenceIndex = 0; sentenceIndex < newSentenceMaximumLength; sentenceIndex++)
      {
      *(newSentence + sentenceIndex)   = NMEA_SENTENCE_END_OF_STRING;
      }

    *newSentenceIndex                  = 0;
    *(newSentence + *newSentenceIndex) = NMEA_SENTENCE_PREFIX;
    *newSentenceIndex                  = *newSentenceIndex + 1;

     sentenceResult                    = true;
    }

/******************************************************************************/

  return(sentenceResult);

/******************************************************************************/
} /* end of nmeaEncoderStartSentence                                          */

/******************************************************************************/
/* nmeaEncoderAddGNSSAndSentencePrefix() :                                    */
/* <--> newSentence              : new sentence character array               */
/* <--> newSentenceIndex         : new sentence character array index         */
/*  --> randomState              : random state for this sentence             */
/*                                                                            */
/* - insert one of the GNSS identifiers and the GLL prefix to identify the    */
/*   sentence sender and the sentence type                                    */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaEncoderAddGNSSAndSentencePrefix(      NMEA_GPS_UCHAR *newSentence,
                                                           NMEA_GPS_UINT8 *newSentenceIndex,
                                                           tinymt32_t     *randomState,
                                                     const NMEA_GPS_UCHAR *sentenceType)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN sentenceResult = false;
  NMEA_GPS_UINT8   sentenceIndex  = 0;

  NMEA_GPS_UCHAR   sender[NMEA_SENTENCE_TALKER_STRING_LENGTH];

/******************************************************************************/

  if ((newSentence != NULL) && (newSentenceIndex != NULL) && 
      (randomState != NULL) && (sentenceType     != NULL))
    {
    if (nmeaEncoderGenerateRandomSender( randomState,
                                        &sender[0]) == true)
      {
      // Add the sender to the sentence
      strcpy_s((newSentence + *newSentenceIndex), NMEA_SENTENCE_TALKER_STRING_LENGTH, sender);

      *newSentenceIndex = *newSentenceIndex + NMEA_SENTENCE_TALKER_LENGTH;

      strcpy_s((newSentence + *newSentenceIndex), NMEA_SENTENCE_TYPE_STRING_LENGTH, sentenceType);

      *newSentenceIndex = *newSentenceIndex + NMEA_SENTENCE_TYPE_LENGTH;

      nmeaEncoderAddCharacter(newSentence,
                              newSentenceIndex,
                              NMEA_SENTENCE_FIELD_SEPERATOR);

      //*newSentenceIndex = *newSentenceIndex + NMEA_SENTENCE_FIELD_SEPERATOR_LENGTH;

       sentenceResult = true;
      }
    }

/******************************************************************************/

  return(sentenceResult);

/******************************************************************************/
} /* end of nmeaEncoderAddGNSSAndSentencePrefix                               */

/******************************************************************************/
/* nmeaGenerateRandomSender() :                                               */
/*  --> randomState  : random state for this sentence                         */
/*  <-- randomSender : the randomly-generated sender code                     */
/*                                                                            */
/* - randomly selects a sender code from one of the possibilities             */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaEncoderGenerateRandomSender(tinymt32_t     *randomState,
                                                 NMEA_GPS_UCHAR *sender)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN senderResult = false;

  NMEA_GPS_FLOAT   randomiser   = 0.0;
  NMEA_GPS_UINT8   senderIndex  = 0;

/******************************************************************************/

  if ((randomState != NULL) && (sender != NULL))
    {
    // Select the GNSS sender to add to the sentence - a random number in the 
    // range 0.0 <= random <= 1.0 is mulitplied by the number of possible
    // senders and truncated to an integer
    randomiser  = tinymt32_generate_float(randomState);

    senderIndex = (NMEA_GPS_UINT8)(randomiser * ((NMEA_GPS_FLOAT)NMEA_TALKERS));

    // Return the randomly-selected sender string
    strcpy_s(sender, NMEA_SENTENCE_TALKER_STRING_LENGTH, nmeaTalkers[senderIndex].NMEATalkerPrefix);

    senderResult = true;
    }

/******************************************************************************/

  return(senderResult);

/******************************************************************************/
} /* end of nmeaEncoderGenerateRandomSender                                   */

/******************************************************************************/
/* nmeaEncoderGenerateRandomSingleField() :                                   */
/* <--> newSentence              : new sentence character array               */
/* <--> newSentenceIndex         : new sentence character array index         */
/* <--> randomState              : random state for this sentence             */
/*  --> singleFieldOptions       : 1 { <single-field-character> } n           */
/*  --> numberOfOptions          : 1 .. n                                     */
/*                                                                            */
/* - randomly select a single-field character from the options available      */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaEncoderGenerateRandomSingleField(      NMEA_GPS_UCHAR *newSentence,
                                                            NMEA_GPS_UINT8 *newSentenceIndex,
                                                            tinymt32_t     *randomState,
                                                      const NMEA_GPS_UCHAR *singleFieldOptions,
                                                      const NMEA_GPS_UINT8  numberOfOptions)
{
/******************************************************************************/
  
  NMEA_GPS_BOOLEAN singleFieldResult = false;
  NMEA_GPS_FLOAT   randomiser        = 0.0;
  NMEA_GPS_UINT8   optionIndex       = 0;
  ;
/******************************************************************************/

  if ((newSentence        != NULL) && (newSentenceIndex != NULL) && (randomState     != NULL) && 
      (singleFieldOptions != NULL) && (numberOfOptions   > 0))
    {
    randomiser  = tinymt32_generate_float(randomState);

    optionIndex = (NMEA_GPS_UINT8)(randomiser * ((NMEA_GPS_FLOAT)numberOfOptions));

    *(newSentence + *newSentenceIndex) = *(singleFieldOptions + optionIndex);

    *newSentenceIndex                  = *newSentenceIndex + 1;

     singleFieldResult                 =  true;
    }

/******************************************************************************/

  return(singleFieldResult);

/******************************************************************************/
} /* end of nmeaEncoderGenerateRandomSingleField                              */

/******************************************************************************/
/* nmeaEncoderAddCharacter()                                                  */
/* <--> newSentence              : new sentence character array               */
/* <--> newSentenceIndex         : new sentence character array index         */
/*  --> newCharacter             : new character to add to the sentence       */
/*                                                                            */
/* - use this function to add field seperators to the sentence e.g. ','       */
/*   (comma) characters                                                       */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaEncoderAddCharacter(NMEA_GPS_UCHAR *newSentence,
                                         NMEA_GPS_UINT8 *newSentenceIndex,
                                         NMEA_GPS_UCHAR  newCharacter)
{
/******************************************************************************/
  
  NMEA_GPS_BOOLEAN characterResult = true;

/******************************************************************************/

  if ((newSentence != NULL) && (newSentenceIndex != NULL))
    {
    *(newSentence + *newSentenceIndex) =  newCharacter;

    *newSentenceIndex                  = *newSentenceIndex + 1;

     characterResult                   =  true;
    }

/******************************************************************************/

  return(characterResult);

/******************************************************************************/
} /* end of nmeaEncoderAddCharacter                                           */

/******************************************************************************/
/* nmeaLoadIntegerAsDecimalAscii() :                                          */
/*                                                                            */
/* - convert a (possibly) signed integer to it's decimal ASCII character      */
/*   representation and load it onto a sentence.                              */
/*   The entire number is converted i.e. all 32 bit-weights are examined and  */
/*   the decimal equivalent added to the digit accumulator. According to the  */
/*   required field-width of the result 1 { <digits> } 9 are emitted          */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaLoadIntegerAsDecimalAscii(NMEA_GPS_UCHAR  *newSentence,
                                               NMEA_GPS_UINT8  *newSentenceIndex,
                                               NMEA_GPS_INT32   newNumber,
                                               NMEA_GPS_UINT8   fieldWidth,
                                               nmeaNumberSign_t signState)
  {
/******************************************************************************/

  NMEA_GPS_BOOLEAN loadResult  = false;

  NMEA_GPS_UINT32  bitMask     = (1 << (NMEA_DECIMAL_EQUIVALENT_BIT_WEIGHTS - 1));
  NMEA_GPS_INT8    bitIndex    = 0,
                   digitIndex  = 0,
                   fieldOffset = 0;

  NMEA_GPS_UINT8   carry       = 0,
                   accumulator[NMEA_DECIMAL_EQUIVALENT_DIGITS] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  NMEA_GPS_INT32   signFlag = 0;

/******************************************************************************/

  if ((newSentence != NULL) && (newSentenceIndex != NULL) && (fieldWidth > 0) && (fieldWidth <= NMEA_DECIMAL_EQUIVALENT_MAXIMUM_FIELD_WIDTH))
    {
    // Check for the sign. Make negative numbers positive and save the negative state
    if (newNumber < 0)
      {
      newNumber = -newNumber;
      signFlag  = -1;
      }

    // Examine all the bit-weights of the "uint32_t"
    for (bitIndex = (NMEA_DECIMAL_EQUIVALENT_BIT_WEIGHTS - 1); bitIndex >= 0; bitIndex--)
      {
      // If a bit-weight is set, add it's decimal equivalent
      if ((bitMask & newNumber) == bitMask)
        {
        for (digitIndex = (NMEA_DECIMAL_EQUIVALENT_DIGITS - 1); digitIndex >= 0; digitIndex--)
          {
          accumulator[digitIndex] = accumulator[digitIndex] + nmeaBitWeightToDecimal[bitIndex][digitIndex] + carry;

          if (accumulator[digitIndex] >= NMEA_DECIMAL_EQUIVALENT_BASE)
            {
            accumulator[digitIndex] = accumulator[digitIndex] - NMEA_DECIMAL_EQUIVALENT_BASE;
            carry                   = 1;
            }
          else
            {
            carry = 0;
            }
          }
        }

      bitMask = bitMask >> 1;
      }

    // Convert the result to ASCII digits
    for (digitIndex = 0; digitIndex < NMEA_DECIMAL_EQUIVALENT_DIGITS; digitIndex++)
      {
      accumulator[digitIndex] = accumulator[digitIndex] + NMEA_GPS_ASCII_DIGIT_ZERO;
      }

    // Add the sign back in if required
    switch(signState)
      {
      case NMEA_NUMBER_SIGN_PLUS       : if (signFlag == 0)
                                           {
                                           *(newSentence + (*newSentenceIndex)) = NMEA_GPS_ASCII_DIGIT_PLUS;
                                           *newSentenceIndex                    = *newSentenceIndex + 1;
                                           }
                                         break;
      case NMEA_NUMBER_SIGN_MINUS      : if (signFlag == -1)
                                          {
                                          *(newSentence + (*newSentenceIndex)) = NMEA_GPS_ASCII_DIGIT_MINUS;
                                          *newSentenceIndex                    = *newSentenceIndex + 1;
                                          }
                                         break;

      case NMEA_NUMBER_SIGN_PLUS_MINUS : if (signFlag == 0)
                                           {
                                           *(newSentence + (*newSentenceIndex)) = NMEA_GPS_ASCII_DIGIT_PLUS;
                                           *newSentenceIndex                    = *newSentenceIndex + 1;
                                           }
                                         else
                                           {
                                           *(newSentence + (*newSentenceIndex)) = NMEA_GPS_ASCII_DIGIT_MINUS;
                                           *newSentenceIndex                    = *newSentenceIndex + 1;
                                           }
                                         break; 
      default                          :
      case NMEA_NUMBER_SIGN_NONE       : break;
      }

    // Finally copy the ASCII number to the output string
    fieldOffset = NMEA_DECIMAL_EQUIVALENT_DIGITS - fieldWidth;

    for (digitIndex = 0; digitIndex < fieldWidth; digitIndex++)
      {
      *(newSentence + (*newSentenceIndex)) = accumulator[fieldOffset + digitIndex];
      *newSentenceIndex                    = *newSentenceIndex + 1;
      }

    loadResult = true;
    }

/******************************************************************************/

  return(loadResult);

/******************************************************************************/
  } /* end of nmeaLoadIntegerAsDecimalAscii                                   */

/******************************************************************************/
/* nmeaEncoderAddCompassPoints() :                                            */
/* <--> newSentence              : new sentence character array               */
/* <--> newSentenceIndex         : new sentence character array index         */
/*  --> randomState              : random state for this sentence             */
/*  --> compassPoints            : [ [ 'E' | 'W' ] | [ 'N' | 'S' ] ]          */
/*  <-- latitudeNorthingsResult          : error handling                             */
/*                                                                            */
/* - randomly select the latitude hemisphere or longitude direction           */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaEncoderAddCompassPoints(      NMEA_GPS_UCHAR                   *newSentence,
                                                   NMEA_GPS_UINT8                   *newSentenceIndex,
                                                   tinymt32_t                       *randomState,
                                             const nmeaEncoderSelectCompassPoints_t  compassPoints)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN compassResult   = false;
  NMEA_GPS_FLOAT   randomiser      = 0.0;
  NMEA_GPS_UCHAR   hemisphere      = 0,
                   hemispherePlus  = 0,
                   hemisphereMinus = 0;

/******************************************************************************/

  if ((newSentence != NULL) && (newSentenceIndex != NULL) && (randomState != NULL))
    {
    if (compassPoints == NMEA_ENCODER_COMPASS_POINTS_NS)
      {
      hemispherePlus  = NMEA_FIELD_NORTHINGS_NORTH;
      hemisphereMinus = NMEA_FIELD_NORTHINGS_SOUTH;
      }
    else
      {
      hemispherePlus  = NMEA_FIELD_EASTINGS_EAST;
      hemisphereMinus = NMEA_FIELD_EASTINGS_WEST;
      }

    // Generate the hemisphere selector
    randomiser = tinymt32_generate_float(randomState);

    if (randomiser > NMEA_ENCODER_SIGN_DECISION_LIMIT)
      {
      hemisphere = hemispherePlus;
      }
    else
      {
      hemisphere = hemisphereMinus;
      }

    nmeaEncoderAddCharacter(newSentence,
                            newSentenceIndex,
                            hemisphere);

    compassResult = true;
    }

/******************************************************************************/

  return(compassResult);

/******************************************************************************/
} /* end of nmeaEncoderAddCompassPoints                                       */

/******************************************************************************/
/* nmeaEncoderAddLatLongField() :                                             */
/* <--> newSentence              : new sentence character array               */
/* <--> newSentenceIndex         : new sentence character array index         */
/*  --> latLongSelector          : select latitude or longitude constants     */
/*  <-- latLongResult            : error handling                             */
/*                                                                            */
/* - generate either a latitude northing or a longitude easting               */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaEncoderAddLatLongField(      NMEA_GPS_UCHAR                *newSentence,
                                                  NMEA_GPS_UINT8                *newSentenceIndex,
                                                  nmeaSentenceLatLongSelector_t  latLongSelector,
                                                  tinymt32_t                    *randomState,
                                            const nmeaEncoderZeroPad_t           zeroPad)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN latLongResult          = false;
                                                         
  NMEA_GPS_FLOAT   degreesMinimumLimit    = 0.0;
  NMEA_GPS_FLOAT   degreesMaximumLimit    = 0.0;
                                          
  NMEA_GPS_FLOAT   degreesField           = 0.0,
                   scaler                 = 0.0,
                   highestDegreesPower    = 0.0;

  NMEA_GPS_UINT8   degreesFieldFullLength = 0,
                   degreesFieldLength     = 0,
                   highestDegressIndex    = 0,
                   newDigit               = 0;

/******************************************************************************/

  if ((newSentence != NULL) && (newSentenceIndex != NULL))
    {
    // Select for the leading degree digits' length
    if (latLongSelector == NMEA_FIELD_LAT_LONG_SELECT_LATITUDE)
      {
      // Generate degrees-latitude integer power index i.e. ( 10 ^ <highestDegressIndex> )
      degreesField           = tinymt32_generate_float(randomState); // 0.0 <= <degreesField> < 1.0
                             
      highestDegressIndex    = (NMEA_GPS_UINT8)(((NMEA_GPS_FLOAT)NMEA_FIELD_LATITUDE_DEGREES_MINIMUM_LENGTH) + (degreesField * ((NMEA_GPS_FLOAT)NMEA_FIELD_LATITUDE_DEGREES_MAXIMUM_LENGTH)));
                             
      highestDegreesPower    = (NMEA_GPS_FLOAT)pow(NMEA_FLOAT_DIGIT_x_10, (highestDegressIndex - 1));
                             
      degreesMaximumLimit    = (highestDegressIndex == NMEA_FIELD_LATITUDE_DEGREES_MAXIMUM_LENGTH) ? 
                               ((NMEA_GPS_FLOAT)NMEA_FIELD_LATITUDE_MAXIMUM_LIMIT)                 : 
                               ((NMEA_GPS_FLOAT)pow(NMEA_FLOAT_DIGIT_x_10, highestDegressIndex) - 1);

      degreesFieldFullLength = NMEA_FIELD_LATITUDE_DEGREES_MAXIMUM_LENGTH;
      degreesFieldLength     = highestDegressIndex; // superfluous but easier to read!

      degreesMinimumLimit    = NMEA_FIELD_LATITUDE_MINIMUM_LIMIT;
      latLongResult          = true;
      }
    else
      {
      if (latLongSelector == NMEA_FIELD_LAT_LONG_SELECT_LONGITUDE)
        {
        // Generate degrees-longitude integer power index i.e. ( 10 ^ <highestDegressIndex> )
        degreesField           = tinymt32_generate_float(randomState); // 0.0 <= <degreesField> < 1.0
                               
        highestDegressIndex    = (NMEA_GPS_UINT8)(((NMEA_GPS_FLOAT)NMEA_FIELD_LONGITUDE_DEGREES_MINIMUM_LENGTH) + (degreesField * ((NMEA_GPS_FLOAT)NMEA_FIELD_LONGITUDE_DEGREES_MAXIMUM_LENGTH)));
                               
        highestDegreesPower    = (NMEA_GPS_FLOAT)pow(NMEA_FLOAT_DIGIT_x_10, (highestDegressIndex - 1));
                               
        degreesMaximumLimit    = (highestDegressIndex == NMEA_FIELD_LONGITUDE_DEGREES_MAXIMUM_LENGTH) ? 
                                 ((NMEA_GPS_FLOAT)NMEA_FIELD_LONGITUDE_MAXIMUM_LIMIT)                 : 
                                 ((NMEA_GPS_FLOAT)pow(NMEA_FLOAT_DIGIT_x_10, highestDegressIndex) - 1);

        degreesFieldFullLength = NMEA_FIELD_LONGITUDE_DEGREES_MAXIMUM_LENGTH;
        degreesFieldLength     = highestDegressIndex; // superfluous but easier to read!

        degreesMinimumLimit    = NMEA_FIELD_LONGITUDE_MINIMUM_LIMIT;
        latLongResult          = true;
        }
      }

    // Proceed if a valid lat/long selector has been found
    if (latLongResult == true)
      {
      // Ensure a fractional field of the maximum length is generated - even if 
      // not all of the digits are used
      scaler = ((NMEA_GPS_FLOAT)(1 << NMEA_FIELD_LATITUDE_FRACTION_POWER_SHIFT)); //((NMEA_GPS_FLOAT)(1 << NMEA_FIELD_LATITUDE_FRACTION_MAXIMUM_LENGTH));

      // Generate a random number as a set of '1 { <latitude> } 2' or '1 { <longitude> } 3' unsigned digits 
      latLongResult = nmeaGenerateRandomFields(&degreesField,
                                               &degreesMinimumLimit,
                                               &degreesMaximumLimit,
                                               &scaler,
                                                NMEA_ENCODER_NUMERICAL_LIMITS_RESPECT,
                                                randomState);

      if (zeroPad == NMEA_ENCODER_ZEROES_PAD) // if required, add leading zeroes to "degrees" that do not span the full field width 
        {
        if ((degreesFieldFullLength - degreesFieldLength) != 0)
          {
          for (newDigit = 0; newDigit < (degreesFieldFullLength - degreesFieldLength); newDigit++)
            {
            *(newSentence + *newSentenceIndex) = NMEA_GPS_ASCII_DIGIT_ZERO;

            *newSentenceIndex                  = *newSentenceIndex + 1;
            }
          }
        }

      // Extract the degrees-only field digits
      while (highestDegressIndex > 0)
        {
        // Get the decimal digit at this power of 10
        newDigit            = (NMEA_GPS_UINT8)(degreesField / highestDegreesPower);

        // Remove it from the total
        degreesField        = degreesField - ((NMEA_GPS_FLOAT)newDigit) * highestDegreesPower;

        // Add the digit to the sentence and advance the sentence index
        *(newSentence + *newSentenceIndex) = newDigit + NMEA_GPS_ASCII_DIGIT_ZERO;
        *newSentenceIndex                  = *newSentenceIndex + 1;

        // Get the next lowest power of 10
        highestDegreesPower = highestDegreesPower / NMEA_ENCODE_LATITUDE_DEGREES_x10;

        highestDegressIndex = highestDegressIndex - 1;
        }

      // Convert the fractional part into integer + fractional minutes
      degreesField = degreesField * NMEA_FIELD_LATITUDE_MAXIMUM_MINUTES;

      highestDegressIndex = NMEA_FIELD_LATITUDE_MINUTES_LENGTH;
      highestDegreesPower = NMEA_ENCODE_LATITUDE_DEGREES_x10;

      while (highestDegressIndex > 0)
        {
        // Get the decimal digit at this power of 10
        newDigit             = (NMEA_GPS_UINT8)(degreesField / highestDegreesPower);

        // Remove it from the total
        degreesField        = degreesField - ((NMEA_GPS_FLOAT)newDigit) * highestDegreesPower;

        // Add the digit to the sentence and advance the sentence index
        *(newSentence + *newSentenceIndex) = newDigit + NMEA_GPS_ASCII_DIGIT_ZERO;
        *newSentenceIndex                  = *newSentenceIndex + 1;

        // Get the next lowest power of 10
        highestDegreesPower = highestDegreesPower / NMEA_ENCODE_LATITUDE_DEGREES_x10;

        highestDegressIndex = highestDegressIndex - 1;
        }

      // Insert the integer <-> fractional seperator
      *(newSentence + *newSentenceIndex) = NMEA_FIELD_LATITUDE_SEPERATOR;
      *newSentenceIndex                  = *newSentenceIndex + 1;

      // The fractional field can be from one to five digits in length
      highestDegressIndex = (tinymt32_generate_uint32(randomState) % NMEA_FIELD_LATITUDE_FRACTION_MAXIMUM_LENGTH) + 1;

      while (highestDegressIndex > 0)
        {
        // Promote the fractional power to x1
        degreesField        = degreesField * NMEA_ENCODE_LATITUDE_DEGREES_x10;

        // Extract the digit
        newDigit            = (NMEA_GPS_UINT8)(degreesField);

        // Remove it from the total
        degreesField        = degreesField - ((NMEA_GPS_FLOAT)newDigit);

        // Add the digit to the sentence and advance the sentence index
        *(newSentence + *newSentenceIndex) = newDigit + NMEA_GPS_ASCII_DIGIT_ZERO;
        *newSentenceIndex                  = *newSentenceIndex + 1;

        highestDegressIndex = highestDegressIndex - 1;
        }
      }
    }

/******************************************************************************/

  return(latLongResult);

/******************************************************************************/
} /* end of nmeaEncoderAddLatLongField                                        */

/******************************************************************************/
/* nmeaEncoderGenerateUTCTime() :                                             */
/* <--> newSentence              : new sentence character array               */
/* <--> newSentenceIndex         : new sentence character array index         */
/*  --> randomState              : random state for this sentence             */
/*                                                                            */
/* - generate UTC time as hh:mm:ss:cc                                         */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaEncoderGenerateUTCTime(NMEA_GPS_UCHAR *newSentence,
                                            NMEA_GPS_UINT8 *newSentenceIndex,
                                            tinymt32_t     *randomState)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN      utcResult    = false;
  NMEA_GPS_INT32        minimumLimit = NMEA_FIELD_UTC_HOURS_MINIMUM;
  NMEA_GPS_INT32        maximumLimit = NMEA_FIELD_UTC_HOURS_MAXIMUM;
  nmeaEncoderSignFlag_t signFlag     = NMEA_ENCODER_SIGN_FLAG_UNSIGNED;
  NMEA_GPS_UCHAR        generatedField[NMEA_ENCODE_MAXIMUM_GENERATED_FIELD_LENGTH];
  NMEA_GPS_UINT8        fieldIndex   = 0;

/******************************************************************************/

  if ((newSentence != NULL) && (newSentenceIndex != NULL) && (randomState != NULL))
    {
    // Generate "hours"
    if (nmeaGenerateSignedIntegerRandomFields(&generatedField[0],
                                               NULL,
                                              &minimumLimit,
                                              &maximumLimit,
                                               signFlag,
                                               NMEA_FIELD_UTC_HOURS_LENGTH,
                                               randomState) == true)
      {
      for (fieldIndex = 0; fieldIndex < NMEA_FIELD_UTC_HOURS_LENGTH; fieldIndex++)
        {
        *(newSentence + *newSentenceIndex) = generatedField[fieldIndex];
    
        *newSentenceIndex                  = *newSentenceIndex + 1;
        }
    
      minimumLimit = NMEA_FIELD_UTC_MINUTES_MINIMUM;
      maximumLimit = NMEA_FIELD_UTC_MINUTES_MAXIMUM;
    
      if (nmeaGenerateSignedIntegerRandomFields(&generatedField[0],
                                                 NULL,
                                                &minimumLimit,
                                                &maximumLimit,
                                                 signFlag,
                                                 NMEA_FIELD_UTC_MINUTES_LENGTH,
                                                 randomState) == true)
        {
        for (fieldIndex = 0; fieldIndex < NMEA_FIELD_UTC_MINUTES_LENGTH; fieldIndex++)
          {
          *(newSentence + *newSentenceIndex) = generatedField[fieldIndex];
    
          *newSentenceIndex                  = *newSentenceIndex + 1;
          }
    
        minimumLimit = NMEA_FIELD_UTC_SECONDS_MINIMUM;
        maximumLimit = NMEA_FIELD_UTC_SECONDS_MAXIMUM;
    
        if (nmeaGenerateSignedIntegerRandomFields(&generatedField[0],
                                                   NULL,
                                                  &minimumLimit,
                                                  &maximumLimit,
                                                   signFlag,
                                                   NMEA_FIELD_UTC_SECONDS_LENGTH,
                                                   randomState) == true)
          {
          for (fieldIndex = 0; fieldIndex < NMEA_FIELD_UTC_SECONDS_LENGTH; fieldIndex++)
            {
            *(newSentence + *newSentenceIndex) = generatedField[fieldIndex];
    
            *newSentenceIndex                  = *newSentenceIndex + 1;
            }
    
          nmeaEncoderAddCharacter(newSentence,
                                  newSentenceIndex,
                                  NMEA_SENTENCE_DECIMAL_POINT);
    
          minimumLimit = NMEA_FIELD_UTC_CENTISECONDS_MINIMUM;
          maximumLimit = NMEA_FIELD_UTC_CENTISECONDS_MAXIMUM;
    
          if (nmeaGenerateSignedIntegerRandomFields(&generatedField[0],
                                                     NULL,
                                                    &minimumLimit,
                                                    &maximumLimit,
                                                     signFlag,
                                                     NMEA_FIELD_UTC_CENTISECONDS_LENGTH,
                                                     randomState) == true)
             {
             for (fieldIndex = 0; fieldIndex < NMEA_FIELD_UTC_CENTISECONDS_LENGTH; fieldIndex++)
               {
               *(newSentence + *newSentenceIndex) = generatedField[fieldIndex];
            
               *newSentenceIndex                  = *newSentenceIndex + 1;
               }
    
             utcResult = true;
             }
           }
         }
       }
   }

/******************************************************************************/

  return(utcResult);

/******************************************************************************/
} /* end of nmeaEncoderGenerateUTCTime                                        */

/******************************************************************************/
/* nmeaEncoderAddDilutionOfPrecision() :                                      */
/* <--> newSentence              : new sentence character array               */
/* <--> newSentenceIndex         : new sentence character array index         */
/*  --> randomState              : random state for this sentence             */
/*                                                                            */
/* - generate generic "dilution of precision". All DOPs are of a common       */
/*   format :                                                                 */
/*            0.5 { <DOP> } 99.9                                              */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaEncoderAddDilutionOfPrecision(NMEA_GPS_UCHAR *newSentence,
                                                   NMEA_GPS_UINT8 *newSentenceIndex,
                                                   tinymt32_t     *randomState)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN       dopResult      = false;
                         
  NMEA_GPS_UCHAR         generatedField[NMEA_ENCODE_MAXIMUM_GENERATED_FIELD_LENGTH];
                         
  NMEA_GPS_UINT8         variableLength = 0,
                         variableIndex  = 0;

  nmeaNumericalVariant_t variableValue;

/******************************************************************************/

  if ((newSentence != NULL) && (newSentenceIndex != NULL) && (randomState != NULL))
    {
    // Generate a random value for "[ <speed> | <course> ] over ground"
    if (nmeaGenerateRandomVariable(&generatedField[0],
                                   &variableValue,
                                   &variableLength,
                                    NMEA_FIELD_DECIMAL_FLOATING,
                                    NMEA_SENTENCE_NUMERICAL_SIGN_UNSIGNED,
                                    NMEA_DOP_INTEGER_PART_MAXIMUM_LENGTH,
                                    NMEA_DOP_INTEGER_MINIMUM_LIMIT,
                                    NMEA_DOP_INTEGER_MAXIMUM_LIMIT,
                                    NMEA_DOP_FRACTIONAL_PART_MAXIMUM_LENGTH,
                                    NMEA_DOP_FRACTIONAL_MINIMUM_LIMIT,
                                    NMEA_DOP_FRACTIONAL_MAXIMUM_LIMIT) == true)
       {
       if ((variableValue.nmea_float_t >= NMEA_DOP_MINIMUM) && (variableValue.nmea_float_t <= NMEA_DOP_MAXIMUM))
         {
         for (variableIndex = 0; variableIndex < variableLength; variableIndex++)
           {
           // Strip out any leading zero; there can only be at most one as the integer part 
           // is only two digits long and "0.x" is definately required!
           if (variableIndex == 0)
             {
             if (generatedField[variableIndex] != NMEA_GPS_ASCII_DIGIT_ZERO)
               {
               *(newSentence + *newSentenceIndex) = generatedField[variableIndex];

               *newSentenceIndex                  = *newSentenceIndex + 1;
               }
             }
           else
             {
             *(newSentence + *newSentenceIndex) = generatedField[variableIndex];

             *newSentenceIndex                  = *newSentenceIndex + 1;
             }
           }

         *(newSentence + *newSentenceIndex) = NMEA_SENTENCE_END_OF_STRING;

          dopResult = true;
         }
       }
    }

  // Force the random-number-generator to generate a new number next time round
  Sleep(NMEA_ENCODE_RANDOMISATION_SLEEP_DELAY);

/******************************************************************************/

  return(dopResult);

/******************************************************************************/
} /* end of nmeaEncoderAddDilutionOfPrecision                                 */

/******************************************************************************/
/* nmeaEncoderAddXOverGround() :                                              */
/* <--> newSentence              : new sentence character array               */
/* <--> newSentenceIndex         : new sentence character array index         */
/*  --> overGroundType           : [ "...COURSE* | "...SPEED" ]               */
/*  --> zeroPad                  : add leading zeroes (or not) if required    */
/*                                                                            */
/* - generate "SpeedOverGround" or "CourseOverGround" unsigned floating-point */
/*   variables                                                                */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaEncoderAddXOverGround(      NMEA_GPS_UCHAR           *newSentence,
                                                 NMEA_GPS_UINT8           *newSentenceIndex,
                                           const nmeaEncoderXOverGround_t  overGroundType,
                                           const nmeaEncoderZeroPad_t      zeroPad)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN       overGroundResult       = false;
                  
  NMEA_GPS_UINT8         integerFieldLength     = 0,
                         integerFieldFullLength = 0,
                         fractionalFieldLength  = 0;

  NMEA_GPS_INT32         integerMinimumLimit    = 0,
                         integerMaximumLimit    = 0,
                         fractionalMinimumLimit = 0,
                         fractionalMaximumLimit = 0;

  nmeaNumericalVariant_t variableValue;
  nmeaEncoderSignFlag_t  signFlag               = NMEA_ENCODER_SIGN_FLAG_UNSIGNED;
  tinymt32_t             variableRandomState;   
  NMEA_GPS_UINT32        variableRandomSeed     = 0;
  NMEA_GPS_FLOAT         integerFieldScaler     = 0.0,
                         fractionalFieldScaler  = 0.0,
                         minimumLimit           = 0.0,
                         maximumLimit           = 0.0;

  NMEA_GPS_UCHAR         generatedField[NMEA_ENCODE_MAXIMUM_GENERATED_FIELD_LENGTH];

  NMEA_GPS_UINT8         variableLength         = 0,
                         variableIndex          = 0;

/******************************************************************************/

  if ((newSentence != NULL) && (newSentenceIndex != NULL)  &&
      ((overGroundType == NMEA_ENCODER_OVER_GROUND_COURSE) || 
       (overGroundType == NMEA_ENCODER_OVER_GROUND_SPEED)  ||
       (overGroundType == NMEA_ENCODER_OVER_GROUND_MAGNETIC_VARIATION)))
    { // Use a local randon-number generator
    if (nmeaEncoderRandomSeed(&variableRandomSeed) != true)
      {
      variableRandomSeed = MT_INITIAL_SEED;
      }

    tinymt32_init(&variableRandomState, variableRandomSeed);

    // Scale the maximum integer and fractional field sizes - produce a degree of randomisation in the actual field sizes
    integerFieldScaler    = tinymt32_generate_float(&variableRandomState); // 0.0 <= <fieldScaler> < 1.0
    Sleep(NMEA_ENCODE_RANDOMISATION_SLEEP_DELAY);
    fractionalFieldScaler = tinymt32_generate_float(&variableRandomState); // 0.0 <= <fieldScaler> < 1.0

    if (overGroundType == NMEA_ENCODER_OVER_GROUND_COURSE)
      { // Set up for "speed over ground"
      integerFieldLength     = (NMEA_GPS_UINT8)(integerFieldScaler * ((NMEA_GPS_FLOAT)NMEA_COG_INTEGER_PART_MAXIMUM_LENGTH));
      integerFieldLength     = integerFieldLength + NMEA_COG_INTEGER_PART_MINIMUM_LENGTH;
      integerFieldFullLength = NMEA_COG_INTEGER_PART_MAXIMUM_LENGTH;

      fractionalFieldLength  = (NMEA_GPS_UINT8)(fractionalFieldScaler * ((NMEA_GPS_FLOAT)NMEA_COG_FRACTIONAL_PART_MAXIMUM_LENGTH));
      fractionalFieldLength  = fractionalFieldLength + NMEA_COG_FRACTIONAL_PART_MINIMUM_LENGTH;

      integerMinimumLimit    = NMEA_COG_INTEGER_MINIMUM_LIMIT;
      integerMaximumLimit    = NMEA_COG_INTEGER_MAXIMUM_LIMIT;
      fractionalMinimumLimit = NMEA_COG_FRACTIONAL_MINIMUM_LIMIT;
      fractionalMaximumLimit = NMEA_COG_FRACTIONAL_MAXIMUM_LIMIT;

      overGroundResult       = true;
      }                       
    else
      {
      if (overGroundType == NMEA_ENCODER_OVER_GROUND_SPEED)
        { // Set up for "speed over ground"
        integerFieldLength     = (NMEA_GPS_UINT8)(integerFieldScaler * ((NMEA_GPS_FLOAT)NMEA_SOG_INTEGER_PART_MAXIMUM_LENGTH));
        integerFieldLength     = integerFieldLength + NMEA_SOG_INTEGER_PART_MINIMUM_LENGTH;
        integerFieldFullLength = NMEA_SOG_INTEGER_PART_MAXIMUM_LENGTH;

        fractionalFieldLength  = (NMEA_GPS_UINT8)(fractionalFieldScaler * ((NMEA_GPS_FLOAT)NMEA_SOG_FRACTIONAL_PART_MAXIMUM_LENGTH));
        fractionalFieldLength  = fractionalFieldLength + NMEA_SOG_FRACTIONAL_PART_MINIMUM_LENGTH;

        integerMinimumLimit    = NMEA_SOG_INTEGER_MINIMUM_LIMIT;
        integerMaximumLimit    = NMEA_SOG_INTEGER_MAXIMUM_LIMIT;
        fractionalMinimumLimit = NMEA_SOG_FRACTIONAL_MINIMUM_LIMIT;
        fractionalMaximumLimit = NMEA_SOG_FRACTIONAL_MAXIMUM_LIMIT;

        overGroundResult       = true;
        }
      else
        { 
        if (overGroundType == NMEA_ENCODER_OVER_GROUND_MAGNETIC_VARIATION) 
          { // Set up for "magnetic variation"
          integerFieldLength     = (NMEA_GPS_UINT8)(integerFieldScaler * ((NMEA_GPS_FLOAT)NMEA_MAV_INTEGER_PART_MAXIMUM_LENGTH));
          integerFieldLength     = integerFieldLength + NMEA_MAV_INTEGER_PART_MINIMUM_LENGTH;
          integerFieldFullLength = NMEA_MAV_INTEGER_PART_MAXIMUM_LENGTH;

          fractionalFieldLength  = (NMEA_GPS_UINT8)(fractionalFieldScaler * ((NMEA_GPS_FLOAT)NMEA_MAV_FRACTIONAL_PART_MAXIMUM_LENGTH));
          fractionalFieldLength  = fractionalFieldLength + NMEA_MAV_FRACTIONAL_PART_MINIMUM_LENGTH;

          integerMinimumLimit    = NMEA_MAV_INTEGER_MINIMUM_LIMIT;
          integerMaximumLimit    = NMEA_MAV_INTEGER_MAXIMUM_LIMIT;
          fractionalMinimumLimit = NMEA_MAV_FRACTIONAL_MINIMUM_LIMIT;
          fractionalMaximumLimit = NMEA_MAV_FRACTIONAL_MAXIMUM_LIMIT;

          overGroundResult       = true;
          }
        }
      }

    if (overGroundResult == true)
      {
      overGroundResult           = false;

      variableValue.nmea_float_t = 0.0;
      
      // Scale the fractional parts from the integer representation to the fractional representation
      if (fractionalMinimumLimit != 0)
        {
        minimumLimit = ((NMEA_GPS_FLOAT)fractionalMinimumLimit);
      
        while (minimumLimit >= NMEA_FLOAT_DIGIT_x_1)
          {
          minimumLimit = minimumLimit / NMEA_FLOAT_DIGIT_x_10;
          }
        }
      
      minimumLimit = minimumLimit + ((NMEA_GPS_FLOAT)integerMinimumLimit);
      
      if (fractionalMaximumLimit != 0)
        {
        maximumLimit = ((NMEA_GPS_FLOAT)fractionalMaximumLimit);
      
        while (maximumLimit >= NMEA_FLOAT_DIGIT_x_1)
          {
          maximumLimit = maximumLimit / NMEA_FLOAT_DIGIT_x_10;
          }
        }
      
      maximumLimit = maximumLimit + ((NMEA_GPS_FLOAT)integerMaximumLimit);
      
      // Generate a random value for "[ <speed> | <course> ] over ground"
      if (nmeaGenerateRandomVariable(&generatedField[0],
                                     &variableValue,
                                     &variableLength,
                                      NMEA_FIELD_DECIMAL_FLOATING,
                                      signFlag,
                                      integerFieldLength,
                                      integerMinimumLimit,
                                      integerMaximumLimit,
                                      fractionalFieldLength,
                                      fractionalMinimumLimit,
                                      fractionalMaximumLimit) == true)
        {
        if ((variableValue.nmea_float_t >= minimumLimit) && (variableValue.nmea_float_t <= maximumLimit))
          {
          if (zeroPad == NMEA_ENCODER_ZEROES_PAD) // if required, add leading zeroes to numbers that do not span the full field width 
            {
            if ((integerFieldFullLength - integerFieldLength) != 0)
              {
              for (variableIndex = 0; variableIndex < (integerFieldFullLength - integerFieldLength); variableIndex++)
                {
                *(newSentence + *newSentenceIndex) = NMEA_GPS_ASCII_DIGIT_ZERO;
      
                *newSentenceIndex                  = *newSentenceIndex + 1;
                }
              }
            }
      
          for (variableIndex = 0; variableIndex < variableLength; variableIndex++)
            {
            *(newSentence + *newSentenceIndex) = generatedField[variableIndex];
          
            *newSentenceIndex                  = *newSentenceIndex + 1;
            }
      
          *(newSentence + *newSentenceIndex) = NMEA_SENTENCE_END_OF_STRING;
          }
      
        overGroundResult = true;
        }
      }
    }

/******************************************************************************/

  return(overGroundResult);

/******************************************************************************/
} /* end of nmeaEncoderAddXOverGround                                         */

/******************************************************************************/
/* nmeaEncoderAddDate() :                                                     */
/* <--> newSentence              : new sentence character array               */
/* <--> newSentenceIndex         : new sentence character array index         */
/*  --> randomState              : random state for this sentence             */
/*                                                                            */
/* - generate an UTC date with corrections for leap-years                     */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaEncoderAddDate(NMEA_GPS_UCHAR *newSentence,
                                    NMEA_GPS_UINT8 *newSentenceIndex,
                                    tinymt32_t     *randomState)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN      dateResult   = false,
                        leapYear     = false;

  nmeaEncoderSignFlag_t signFlag     = NMEA_ENCODER_SIGN_FLAG_UNSIGNED;

  NMEA_GPS_UCHAR        generatedField[NMEA_ENCODE_MAXIMUM_GENERATED_FIELD_LENGTH];

  NMEA_GPS_UINT32       minimumLimit = NMEA_FIELD_DATE_LEAP_YEARS_BASE;
  NMEA_GPS_UINT32       maximumLimit = NMEA_FIELD_DATE_LEAP_YEARS_END;

  NMEA_GPS_UINT32       day          = 0,
                        month        = 0,
                        year         = 0;

  NMEA_GPS_UINT8        fieldWidth   = 0;

/******************************************************************************/

  if ((newSentence != NULL) && (newSentenceIndex != NULL) && (randomState != NULL))
    {
    // Generate a 4-digit year for leap testing
    if (nmeaGenerateSignedIntegerRandomFields(&generatedField[0],
                                              &year,
                                              &minimumLimit,
                                              &maximumLimit,
                                               signFlag,
                                               NMEA_FIELD_DATE_LEAP_YEARS_LENGTH,
                                               randomState) == true)
      {
      if ((year % NMEA_FIELD_DATE_LEAP_YEARS_QUAD_CENTURIES) == 0)
        { // Years divisible by 400 (no-remainder) are leap-years
        leapYear = true;
        }
      else
        {
        if ((year % NMEA_FIELD_DATE_LEAP_YEARS_CENTURIES) != 0)
          { // Years divisible by 100 (no-remainder) are NOT leap-years...
          if ((year % NMEA_FIELD_DATE_LEAP_YEARS) == 0)
            { // ...but years divisible by 4 (no-remainder) ARE!
            leapYear = true;
            }
          }
        }

      // Generate a 2-digit month (keep adding to the generated string)
      minimumLimit = NMEA_FIELD_DATE_MONTHS_MINIMUM;
      maximumLimit = NMEA_FIELD_DATE_MONTHS_MAXIMUM;

      if (nmeaGenerateSignedIntegerRandomFields(&generatedField[NMEA_FIELD_DATE_LEAP_YEARS_LENGTH],
                                                &month,
                                                &minimumLimit,
                                                &maximumLimit,
                                                 signFlag,
                                                 NMEA_FIELD_DATE_MONTHS_LENGTH,
                                                 randomState) == true)
        {
        minimumLimit = NMEA_FIELD_DATE_MONTHS_MINIMUM;

        switch(month)
          {
          case NMEA_FIELD_DATE_FEBRUARY   : if (leapYear == true)
                                              {
                                              maximumLimit = NMEA_FIELD_DATE_DAYS_LEAP_MAXIMUM;
                                              }
                                            else
                                              {
                                              maximumLimit = NMEA_FIELD_DATE_DAYS_28_MAXIMUM;
                                              }

                                            break;
          case  NMEA_FIELD_DATE_APRIL     :
          case  NMEA_FIELD_DATE_JUNE      :
          case  NMEA_FIELD_DATE_SEPTEMBER :
          case  NMEA_FIELD_DATE_NOVEMBER  : maximumLimit = NMEA_FIELD_DATE_DAYS_30_MAXIMUM;
                                            break;
          case  NMEA_FIELD_DATE_JANUARY   :
          case  NMEA_FIELD_DATE_MARCH     :
          case  NMEA_FIELD_DATE_MAY       :
          case  NMEA_FIELD_DATE_JULY      :
          case  NMEA_FIELD_DATE_AUGUST    :
          case  NMEA_FIELD_DATE_OCTOBER   :
          case  NMEA_FIELD_DATE_DECEMBER  : maximumLimit = NMEA_FIELD_DATE_DAYS_31_MAXIMUM;
                                            break;

          default                         : maximumLimit = minimumLimit; // BAD!
                                            break;
          };

        if (maximumLimit != minimumLimit)
          { // Generate a 2-digit day (keep adding to the generated string)
          if (nmeaGenerateSignedIntegerRandomFields(&generatedField[NMEA_FIELD_DATE_LEAP_YEARS_LENGTH + NMEA_FIELD_DATE_MONTHS_LENGTH],
                                                    &day,
                                                    &minimumLimit,
                                                    &maximumLimit,
                                                     signFlag,
                                                     NMEA_FIELD_DATE_DAYS_LENGTH,
                                                     randomState) == true)
            {
            // Load the days onto the output string
            for (fieldWidth = 0; fieldWidth < NMEA_FIELD_DATE_DAYS_LENGTH; fieldWidth++)
              {
              *(newSentence + *newSentenceIndex) =  generatedField[NMEA_FIELD_DATE_LEAP_YEARS_LENGTH + NMEA_FIELD_DATE_MONTHS_LENGTH + fieldWidth];

              *newSentenceIndex                  = *newSentenceIndex + 1;
              }

            // Load the months onto the output string
            for (fieldWidth = 0; fieldWidth < NMEA_FIELD_DATE_MONTHS_LENGTH; fieldWidth++)
              {
              *(newSentence + *newSentenceIndex) =  generatedField[NMEA_FIELD_DATE_LEAP_YEARS_LENGTH + fieldWidth];

              *newSentenceIndex                  = *newSentenceIndex + 1;
              }

            // Load the intra-century ordinals onto the output string
            for (fieldWidth = 0; fieldWidth < NMEA_FIELD_DATE_YEARS_LENGTH; fieldWidth++)
              {
              *(newSentence + *newSentenceIndex) =  generatedField[NMEA_FIELD_DATE_YEARS_LENGTH + fieldWidth];
            
              *newSentenceIndex                  = *newSentenceIndex + 1;
              }

            dateResult = true;
            }
          }
        }
      }
    }

/******************************************************************************/

  return(dateResult);

/******************************************************************************/
} /* end of nmeaEncoderAddDate                                                */

/******************************************************************************/
/* nmeaEncoderAddActiveSatellites() :                                         */
/* <--> newSentence               : new sentence character array              */
/* <--> newSentenceIndex          : new sentence character array index        */
/*  --> runningChecksum           : computed checksum over the whole string   */
/*  --> activeSatellitesMinimum,  : minimum number of active satellites found */
/*  --> activeSatellitesMaximum,  : maximum number of active satellites found */
/*  --> activeSatellitesMinimumId : minimum ID of active satellites found     */
/*  --> activeSatellitesMaximumId : maximum ID of active satellites found     */
/*  --> fieldSeperator            : inter-field seperation character          */
/*                                                                            */
/* - inserts active satellite IDs into a "GSA" message each seperated by a    */
/*   field seperation character :                                             */
/*                                                                            */
/*     <active satellite ID> = 1 { <satellite ID> } 64                        */
/*     0 { <active satellite ID> } 12                                         */
/*                                                                            */
/*     e.g. 33,07,21,20,39,,,,,,,, - five active satellites in 12 fields each */
/*          field followed by a trailing seperator character                  */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaEncoderAddActiveSatellites(      NMEA_GPS_UCHAR *newSentence,
                                                      NMEA_GPS_UINT8 *newSentenceIndex,
                                                      tinymt32_t     *randomState,
                                                const NMEA_GPS_UINT8  activeSatellitesMinimum,
                                                const NMEA_GPS_UINT8  activeSatellitesMaximum,
                                                const NMEA_GPS_UINT8  activeSatellitesMinimumId,
                                                const NMEA_GPS_UINT8  activeSatellitesMaximumId,
                                                const NMEA_GPS_UINT8  idFieldWidth,
                                                const NMEA_GPS_UCHAR  fieldSeperator)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN activeSatelliteResult = false;

  NMEA_GPS_UCHAR   satelliteAsciiField[NMEA_ACITVE_SATELLITES_ASCII_FIELD_WIDTH];

  NMEA_GPS_UINT32  minimumSatelites      = activeSatellitesMinimum,
                   maximumSatelites      = activeSatellitesMaximum,
                   minimumSatelliteId    = activeSatellitesMinimumId,
                   maximumSatelliteId    = activeSatellitesMaximumId;

  NMEA_GPS_UINT8   activeSatellites      = 0,
                   satelliteID           = 0,
                   satelliteIndex        = 0,
                   satelliteIdIndex      = 0;

  NMEA_GPS_FLOAT   scaler                = 0.0;

#if _NMEA_ACTIVE_SATELLITE_TEST_
  NMEA_GPS_INT32  newSatellite           = 0;
#endif

/******************************************************************************/

  if ((newSentence != NULL) && (newSentenceIndex != NULL) && (randomState != NULL) && (idFieldWidth > 0))
    {
    // Constrain the range of satellites active to the message dimensions
    if (minimumSatelites > NMEA_ACTIVE_SATELLITES_MAXIMUM)
      {
      minimumSatelites = NMEA_ACTIVE_SATELLITES_MINIMUM;
      }

    if (maximumSatelites > NMEA_ACTIVE_SATELLITES_MAXIMUM)
      {
      maximumSatelites = NMEA_ACTIVE_SATELLITES_MAXIMUM;
      }

    if (minimumSatelites > maximumSatelites)
      {
      minimumSatelites = activeSatellitesMaximum;
      maximumSatelites = activeSatellitesMinimum;
      }

    // Constrain the satellite IDs to the specification
    if (minimumSatelliteId > NMEA_ACITVE_SATELLITES_MAXIMUM_ID)
      {
      minimumSatelliteId = NMEA_ACITVE_SATELLITES_MINIMUM_ID;
      }

    if (maximumSatelliteId > NMEA_ACITVE_SATELLITES_MAXIMUM_ID)
      {
      maximumSatelliteId = NMEA_ACITVE_SATELLITES_MAXIMUM_ID;
      }

    if (minimumSatelliteId > maximumSatelliteId)
      {
      minimumSatelliteId = activeSatellitesMaximumId;
      maximumSatelliteId = activeSatellitesMinimumId;
      }

    // Ready to roll...firstly choose how many ID slots will be filled
    scaler           = tinymt32_generate_float01(randomState);                                      //  0 >=      <scaler>      <=  1
    activeSatellites = (NMEA_GPS_UINT8)(scaler * ((NMEA_GPS_FLOAT)NMEA_ACTIVE_SATELLITES_MAXIMUM)); //  0 >= <activeSatellites> <= 12

    // Fill the active satellite slots and add the trailing seperator per slot
    for (satelliteIndex = 0; satelliteIndex < activeSatellites; satelliteIndex++)
      {
      if ((activeSatelliteResult = nmeaGenerateSignedIntegerRandomFields(&satelliteAsciiField[0],
#if _NMEA_ACTIVE_SATELLITE_TEST_
                                                                         &newSatellite,
#else
                                                                          NULL,
#endif
                                                                         &minimumSatelliteId,
                                                                         &maximumSatelliteId,
                                                                          NMEA_ENCODER_SIGN_FLAG_UNSIGNED,
                                                                          NMEA_ACITVE_SATELLITES_FIELD_WIDTH,
                                                                          randomState)) == true)
        {
#if _NMEA_ACTIVE_SATELLITE_TEST_
        if ((newSatellite == (NMEA_ACITVE_SATELLITES_MINIMUM_ID - 1)) || (newSatellite == NMEA_ACITVE_SATELLITES_MAXIMUM_ID))
          {
          printf("\n\n Satellite ID %02d found", newSatellite);

          while (!_kbhit())
            ;
          }
#endif
        for (satelliteIdIndex = 0; satelliteIdIndex < NMEA_ACITVE_SATELLITES_FIELD_WIDTH; satelliteIdIndex++)
          {
          *(newSentence + *newSentenceIndex) =  satelliteAsciiField[satelliteIdIndex];

          *newSentenceIndex                  = *newSentenceIndex + 1;
          }

        // Add a ',' (comma) character
        nmeaEncoderAddCharacter(newSentence,
                                newSentenceIndex,
                                NMEA_SENTENCE_FIELD_SEPERATOR);
        }
      else
        {
        break;
        }
      }

    // Add the remaining empty slot seperators, if any
    if ((activeSatelliteResult == true) || (activeSatellites == 0))
      {
      for ( ; satelliteIndex < NMEA_ACTIVE_SATELLITES_MAXIMUM; satelliteIndex++)
        {
        // Add a ',' (comma) character
        nmeaEncoderAddCharacter(newSentence,
                                newSentenceIndex,
                                NMEA_SENTENCE_FIELD_SEPERATOR);
        }
      }
    }

/******************************************************************************/

  return(activeSatelliteResult);

/******************************************************************************/
} /* end of nmeaEncoderAddActiveSatellites                                    */

/******************************************************************************/
/* nmeaEncoderAddChecksum() :                                                 */
/* <--> newSentence              : new sentence character array               */
/* <--> newSentenceIndex         : new sentence character array index         */
/*  --> runningChecksum          : computed checksum over the whole string    */
/*                                                                            */
/* - format and add the checksum to the generated sentnce string              */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaEncoderAddChecksum(NMEA_GPS_UCHAR *newSentence,
                                        NMEA_GPS_UINT8 *newSentenceIndex,
                                        NMEA_GPS_UINT8  runningChecksum)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN checksumResult = false;
  NMEA_GPS_UINT8   asciiDigit     = 0;

/******************************************************************************/

  if ((newSentence != NULL) && (*newSentenceIndex <= NMEA_SENTENCE_MAXIMUM_LENGTH) && 
                               (*newSentenceIndex >= NMEA_SENTENCE_PREAMBLE_LENGTH))
    {
    // Convert and add the upper nybble of the checksum
    nmeaConvertHexToAsciiHex( (runningChecksum >> NMEA_GPS_NYBBLE_SHIFT),
                             &asciiDigit,
                              NMEA_GENERATE_ASCII_CASE_UPPER);

    *(newSentence + *newSentenceIndex) = asciiDigit;

    *newSentenceIndex                  = *newSentenceIndex + 1;

    // Convert and add the lower nybble of the checksum
    nmeaConvertHexToAsciiHex( (runningChecksum & NMEA_GPS_NYBBLE_LOWER_MASK),
                             &asciiDigit,
                              NMEA_GENERATE_ASCII_CASE_UPPER);

    *(newSentence + *newSentenceIndex) = asciiDigit;

    *newSentenceIndex                  = *newSentenceIndex + 1;
    }

/******************************************************************************/

  return(checksumResult);

/******************************************************************************/
} /* end of nmeaEncoderAddChecksum                                            */

/******************************************************************************/
/* nmeaEncoderAddTerminator() :                                               */
/* <--> newSentence              : new sentence character array               */
/* <--> newSentenceIndex         : new sentence character array index         */
/*                                                                            */
/* - add the NMEA sentence terminator of <CR><LF>                             */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaEncoderAddTerminator(NMEA_GPS_UCHAR *newSentence,
                                          NMEA_GPS_UINT8 *newSentenceIndex)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN terminatorSuccess = false;

/******************************************************************************/

  if ((newSentence != NULL) && (newSentenceIndex != NULL))
    {
    *(newSentence + *newSentenceIndex) = NMEA_SENTENCE_TERMINATOR_CR;

    *newSentenceIndex                  = *newSentenceIndex + 1;

    *(newSentence + *newSentenceIndex) = NMEA_SENTENCE_TERMINATOR_LF;

    *newSentenceIndex                  = *newSentenceIndex + 1;

    terminatorSuccess = true;
    }

/******************************************************************************/

  return(terminatorSuccess);

/******************************************************************************/
} /* end of nmeaEncoderAddTerminator                                          */

/******************************************************************************/
/* nmeaEncoderRandomSeed() :                                                  */
/*   --> randomSeed : the generated random number seed                        */
/*   <-- seedResult : error handling                                          */
/*                                                                            */
/* - generate a 32-bit random-number seed based on the system time            */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaEncoderRandomSeed(NMEA_GPS_UINT32 *randomSeed)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN seedResult = false;
  MMTIME           systemTime;

/******************************************************************************/

  if (timeGetSystemTime(&systemTime,
                         sizeof(systemTime)) == TIMERR_NOERROR)
    {
    *randomSeed = systemTime.u.ms;

     seedResult = true;
    }

/******************************************************************************/

  return(seedResult);

/******************************************************************************/
} /* end of nmeaEncoderRandomSeed                                             */

/******************************************************************************/
/* nmeaGenerateSignedIntegerRandomFields() :                                  */
/*   --> generatedField : generated ASCII representation of the random signed */
/*                        integer                                             */
/*  <--> generatedValue : pointer to the generated integer OR NULL            */
/*   --> minimumLimit   : minimum signed numerical limit                      */
/*   --> maximumLimit   : maximum signed numerical limit                      */
/*   --> signFlag       : flag to include (or not) the sign field             */
/*   --> fieldWidth     : number of digit positions to generate               */
/*                        (automatically adds one for the sign)               */
/*   --> randomState    : random state for this sentence                      */
/*                                                                            */
/* - generate a (possibly) signed integer and convert to ASCII representation */
/*                                                                            */
/*   NOTES :  (i) if the ASCII representation of the limits do not fit in the */
/*                field width the function will truncate the limits           */
/*           (ii) if the field width is wider than the maximum number of      */
/*                ASCII digits the leading decimal places will be filled with */
/*                '0'                                                         */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaGenerateSignedIntegerRandomFields(      NMEA_GPS_UCHAR                *generatedField,
                                                             NMEA_GPS_INT32                *generatedValue,
                                                       const NMEA_GPS_INT32                *minimumLimit,
                                                       const NMEA_GPS_INT32                *maximumLimit,
                                                       const nmeaEncoderSignFlag_t          signFlag,
                                                             NMEA_GPS_UINT8                 fieldWidth,
                                                             tinymt32_t                    *randomState)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN generatorSuccess = false;
  NMEA_GPS_INT32   lowerLimit       = *minimumLimit,
                   upperLimit       = *maximumLimit,
                   testGeneration   = 0,
                   testSign         = 0,
                   range            = 0;
  NMEA_GPS_FLOAT   candidate        = 0.0;

/******************************************************************************/

  if ((generatedField != NULL) && (minimumLimit != NULL) && (maximumLimit != NULL) && (randomState != NULL) && (fieldWidth > 0))
    {
    // Make sure the upper limit is greater than the lower limit
    if (*minimumLimit > *maximumLimit)
      {
      lowerLimit = *maximumLimit;
      upperLimit = *minimumLimit;
      }

    // If the result is to be positive, check the lower limit is >= 0
    if (signFlag == NMEA_ENCODER_SIGN_FLAG_UNSIGNED)
      {
      if (lowerLimit < 0)
        {
        lowerLimit = 0;
        }
      }

    // Compute the maximum unsigned number that will fit in the field width; 
    // if either the upper or lower limit will exceed this then truncate the 
    // limits
    range = (NMEA_GPS_INT32)(pow(NMEA_GPS_DIGIT_x_10, fieldWidth) - 1); 

    if ((range - abs(upperLimit)) < 0)
      {
      if (upperLimit < 0)
        {
        upperLimit = -range;
        }
      else
        {
        upperLimit = range;
        }
      }

    if ((range - abs(lowerLimit)) < 0)
      {
      if (lowerLimit < 0)
        {
        lowerLimit = -range;
        }
      else
        {
        lowerLimit = range;
        }
      }

    if (upperLimit != lowerLimit)
      {
      range = upperLimit - lowerLimit + 1;

      // Generate a candidate number
      candidate      = tinymt32_generate_float(randomState); // 0.0 >= candidate < 1.0
      testGeneration = lowerLimit + (NMEA_GPS_INT32)(candidate * ((NMEA_GPS_FLOAT)range));
      
      testSign       = (NMEA_GPS_INT32)((tinymt32_generate_float(randomState) >= NMEA_ENCODER_SIGN_DECISION_LIMIT) ? NMEA_ENCODER_SIGN_POSITIVE : NMEA_ENCODER_SIGN_NEGATIVE);
      testGeneration = testGeneration * testSign;
      
      while ((testGeneration < lowerLimit) || (testGeneration > upperLimit))
        {
        candidate      = tinymt32_generate_float(randomState); // 0.0 >= candidate < 1.0
        testGeneration = lowerLimit + (NMEA_GPS_INT32)(candidate * ((NMEA_GPS_FLOAT)range));
      
        testSign       = (NMEA_GPS_INT32)((tinymt32_generate_float(randomState) >= NMEA_ENCODER_SIGN_DECISION_LIMIT) ? NMEA_ENCODER_SIGN_POSITIVE : NMEA_ENCODER_SIGN_NEGATIVE);
        testGeneration = testGeneration * testSign;
        }
      
      // Number found - return (possibly) the value and build the return 
      // string (reuse "range" as an index)
      if (generatedValue != NULL)
        {
        *generatedValue = testGeneration;
        }

      range = 0;
      
      if (signFlag == NMEA_ENCODER_SIGN_FLAG_SIGNED)
        {
        // For a signed number, the first digit position is the sign...
        if (testGeneration < 0)
          {
          *(generatedField + range) =   NMEA_SENTENCE_SIGN_MINUS;
           testGeneration            = -testGeneration;
          }
        else
          {
          *(generatedField + range) = NMEA_SENTENCE_SIGN_PLUS;
          }
      
        range = range + 1;
        }
      
      // Convert the integer to an ASCII string (reuse "upperLimit" as powers-of-ten)
      while (fieldWidth > 0)
        {
        upperLimit = (NMEA_GPS_INT32)pow(NMEA_GPS_DIGIT_x_10, (fieldWidth - 1));
      
        // Extract the digit field (reuse "lowerLimit" as division result)
        lowerLimit = testGeneration / upperLimit; // all-integer division!
      
        *(generatedField + range) = lowerLimit + NMEA_GPS_ASCII_DIGIT_MINIMUM;
      
        // IF this power is found subtract it from the number
        if (lowerLimit > 0)
          {
          testGeneration = testGeneration - (lowerLimit * upperLimit);
          }
      
        fieldWidth = fieldWidth - 1;
        range      = range      + 1;
        }
      
      generatorSuccess = true;
      }
    }

/******************************************************************************/

  return(generatorSuccess);

/******************************************************************************/
} /* end of nmeaGenerateSignedIntegerRandomFields                             */

/******************************************************************************/
/* nmeaGenerateRandomVariable() :                                             */
/* <--> variableString              : ASCII representation of the random      */
/*                                    variable with leading zeroes            */
/* <--> variableValue               : variable value of mutable type          */
/* <--> variableLength              : final length of the ASCII variable      */
/*  --> variableType                : [ <integer> | <float> ]                 */
/*  --> signFlag                    : [ <unsigned> | <signed> ] (1)           */
/*  --> integerFieldLength          : number of characters in the integer     */
/*                                    part                                    */
/*  --> integerFieldMinimumLimit    : lower range of the integer part value   */
/*  --> integerFieldMaximumLimit    : upper range of the integer part value   */
/*  --> fractionalFieldLength       : number of characters in the fractional  */
/*                                    part (1)                                */
/*  --> fractionalFieldMinimumLimit : lower range of the fractional part      */
/*                                    value (1)                               */
/*  --> fractionalFieldMaximumLimit : upper range of the fractional part      */
/*                                    value (1)                               */
/*                                                                            */
/* (1) : the fractional part steering constants are ignored if the variable   */
/*       is an integer                                                        */
/*                                                                            */
/* - generate a [ <signed> ][ <integer> | <float> ] random variable as an     */
/*   ASCII string. Also return the value as a 'C' variable                    */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaGenerateRandomVariable(      NMEA_GPS_UCHAR             *variableString,
                                                  nmeaNumericalVariant_t     *variableValue,
                                                  NMEA_GPS_UINT8             *variableLength,
                                            const nmeaSentenceVariableType_t  variableType,
                                            const nmeaNumericalSignFlag_t     signFlag,
                                            const NMEA_GPS_UINT8              integerFieldLength,
                                            const NMEA_GPS_INT32              integerFieldMinimumLimit,
                                            const NMEA_GPS_INT32              integerFieldMaximumLimit,
                                            const NMEA_GPS_UINT8              fractionalFieldLength,
                                            const NMEA_GPS_INT32              fractionalFieldMinimumLimit,
                                            const NMEA_GPS_INT32              fractionalFieldMaximumLimit)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN randomVariableResult = false;
  NMEA_GPS_UINT8   fieldIndex           = 0,
                   fieldFractionalIndex = 0;

  tinymt32_t       variableRandomState;
  NMEA_GPS_UINT32  variableRandomSeed   = 0;

  NMEA_GPS_UCHAR   generatedField[NMEA_ENCODE_MAXIMUM_GENERATED_FIELD_LENGTH];

/******************************************************************************/

  if ((variableString != NULL) && (variableValue != NULL) && (variableLength != NULL))
    {
    if ((variableType == NMEA_FIELD_DECIMAL_INTEGER) || (variableType == NMEA_FIELD_DECIMAL_FLOATING))
      {
      if ((signFlag == NMEA_SENTENCE_NUMERICAL_SIGN_UNSIGNED) || (signFlag == NMEA_SENTENCE_NUMERICAL_SIGN_SIGNED))
        {
        if (integerFieldLength > 0)
          {
          // Use a local random generator
          if (nmeaEncoderRandomSeed(&variableRandomSeed) != true)
            {
            variableRandomSeed = MT_INITIAL_SEED;
            }
          
          tinymt32_init(&variableRandomState, variableRandomSeed);

          // Generate the integer part of the variable
          if ( nmeaGenerateSignedIntegerRandomFields(&generatedField[0],
                                                      NULL,
                                                     &integerFieldMinimumLimit,
                                                     &integerFieldMaximumLimit,
                                                      signFlag,
                                                      integerFieldLength,
                                                     &variableRandomState) == true)
            {
            *variableLength = integerFieldLength;

            if (signFlag == NMEA_SENTENCE_NUMERICAL_SIGN_SIGNED)
              {
              *variableLength = *variableLength + 1;
              }

            for (fieldIndex = 0; fieldIndex < *variableLength; fieldIndex++)
              {
              *(variableString + fieldIndex) = *(generatedField + fieldIndex);
              }

            *(variableString + fieldIndex) = NMEA_SENTENCE_END_OF_STRING;

             variableValue->nmea_int32_t   = atoi(variableString);

             randomVariableResult          = true;

            // If the variable is a float, generate the fractional part
            if (variableType == NMEA_FIELD_DECIMAL_FLOATING)
              {
              // Now the whole floating-point number has to be correct
              randomVariableResult = false;

              // Load the decimal point into the output string
              *(variableString + fieldIndex) = NMEA_SENTENCE_DECIMAL_POINT;

               fieldIndex                    =  fieldIndex  + 1;
              *variableLength                = *variableLength + NMEA_FLOAT_DECIMAL_POINT_LENGTH + fractionalFieldLength;

              if ( nmeaGenerateSignedIntegerRandomFields(&generatedField[0],
                                                          NULL,
                                                         &fractionalFieldMinimumLimit,
                                                         &fractionalFieldMaximumLimit,
                                                          NMEA_SENTENCE_NUMERICAL_SIGN_UNSIGNED,
                                                          fractionalFieldLength,
                                                         &variableRandomState) == true)
                {
                for ( ; fieldIndex < *variableLength; fieldIndex++)
                  {
                  *(variableString + fieldIndex) = *(generatedField + fieldFractionalIndex);

                  fieldFractionalIndex = fieldFractionalIndex + 1;
                  }

                *(variableString + fieldIndex) = NMEA_SENTENCE_END_OF_STRING;

                 variableValue->nmea_float_t = (NMEA_GPS_FLOAT)atof(variableString);

                randomVariableResult           = true;
                }
              else
                {
                *variableString = NMEA_SENTENCE_END_OF_STRING;
                }
              }
            }
          else
            {
            *variableString = NMEA_SENTENCE_END_OF_STRING;
            }
          }
        }
      }
    }

/******************************************************************************/

  return(randomVariableResult);

/******************************************************************************/
  } /* end of nmeaGenerateRandomVariable                                      */

/******************************************************************************/
/*                                                                            */
/*            *** NOT A REAL-TIME FUNCTION - TEST ONLY ***                    */
/*                                                                            */
/* nmeaGenerateRandomFields() :                                               */
/*  <--> generatedField : the number generated if successful                  */
/*   --> minimumLimit   : lowest  allowed value for the generated number      */
/*   --> maximumLimit   : highest allowed value for the generated number      */
/*   --> scaler         : power-of-two divisor to generate fractional fields  */
/*                        MUST ALWAYS BE > 0.0!                               */
/*   --> limitRespect   : flag to signal "limits to be obeyed" or "limits to  */
/*                        be ignored"                                         */
/*   --> randomState    : state of the PRNG                                   */
/*                                                                            */
/* - common function to generate various field random values. All numbers are */
/*   generated as floats to be converted by the caller. The scaler moves the  */
/*   decimal-point to the left to generate integer + fractional fields        */
/*                                                                            */
/*            *** NOT A REAL-TIME FUNCTION - TEST ONLY ***                    */
/*                                                                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaGenerateRandomFields(      NMEA_GPS_FLOAT                      *generatedField,
                                          const NMEA_GPS_FLOAT                      *minimumLimit,
                                          const NMEA_GPS_FLOAT                      *maximumLimit,
                                          const NMEA_GPS_FLOAT                      *scaler,
                                          const nmeaEncoderRespectNumericalLimits_t  limitRespect,
                                                tinymt32_t                          *randomState)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN generatorSuccess =  false;
  NMEA_GPS_FLOAT   testGeneration   =  0.0,
                   testSign         =  0.0,
                   lowerLimit       = *minimumLimit,
                   upperLimit       = *maximumLimit,
                   candidate        =  0.0,
                   range            =  0.0,
                   limitTest        =  0.0;

/******************************************************************************/

  if ((generatedField != NULL) && (minimumLimit != NULL) && (maximumLimit != NULL) && (*scaler > 0.0))
    {
    *generatedField = 0.0;

    // If the limits are not seperated it is too fine to call...
    if (*minimumLimit != *maximumLimit)
      {
      // Make sure the limiting test will work
      if (*minimumLimit > *maximumLimit)
        {
        lowerLimit = *maximumLimit;
        upperLimit = *minimumLimit;
        }

      // Restrict the search range
      range = upperLimit - lowerLimit;

      // Generate a candidate number
      candidate      = tinymt32_generate_float01(randomState); // 0.0 >= candidate < 1.0
      testGeneration = lowerLimit + (candidate * range);

      testSign       = (tinymt32_generate_float(randomState) >= NMEA_ENCODER_SIGN_DECISION_LIMIT) ? NMEA_ENCODER_SIGN_POSITIVE : NMEA_ENCODER_SIGN_NEGATIVE;
      testGeneration = testGeneration * testSign;

      while ((testGeneration < lowerLimit) || (testGeneration > upperLimit))
        {
        candidate      = tinymt32_generate_float01(randomState); // 0.0 >= candidate <= 1.0
        testGeneration = lowerLimit + (candidate * range);

        testSign       = (tinymt32_generate_float(randomState) >= NMEA_ENCODER_SIGN_DECISION_LIMIT) ? NMEA_ENCODER_SIGN_POSITIVE : NMEA_ENCODER_SIGN_NEGATIVE;
        testGeneration = testGeneration * testSign;
        }

      *generatedField = testGeneration;

       generatorSuccess = true;
       }
    }

/******************************************************************************/

  return(generatorSuccess);

/******************************************************************************/
} /* end of nmeaGenerateRandomFields                                          */

/******************************************************************************/
/* nmeaGenerateScrambledSequentialNumbers() :                                 */
/*   <-- numberBuffer : array of 1 { <numbers> } numberRange                  */
/*   --> numberRange  : highest number to generate. The actual numbers        */
/*                      generated will be 0 { <value> } (numberRange - 1)     */
/*                      The buffer length must be >= numberRange              */
/*                                                                            */
/* - generate a scrambled set of otherwise sequential numbers e.g.            */
/*     0, 1, 2, 3 ~-> 1, 3, 0, 2                                              */
/*                                                                            */
/*   Although this could be used to generate random numbers of any type ON    */
/*   THE INTEGER montonicity (e.g. 0, 1, 2... or 0.0, 1.0, 2.0... etc.,.) for */
/*   now it just deals "uint8_t" values                                       */
/*                                                                            */
/******************************************************************************/

#define _NMEA_BUFFER_SORT_N_

#ifdef _NMEA_BUFFER_SORT_
static NMEA_GPS_INT quickSortCompare(const void *arg1, const void *arg2 );
#endif

NMEA_GPS_BOOLEAN nmeaGenerateScrambledSequentialNumbers(nmeaNumericalVariant_t *numberBuffer,
                                                        NMEA_GPS_UINT32         numberRange,
                                                        NMEA_GPS_UINT32         randomSeed)
  {
/******************************************************************************/

  NMEA_GPS_BOOLEAN  scrambledState  = false;

  NMEA_GPS_UINT8    occupancyCells  = 0,
                    newNumber       = 0;


  NMEA_GPS_UINT32   bufferIndex     = 0,
                    newNumberIndex  = 0,
                    newNumberOffset = 0,
                   *bufferOccupancy = NULL; // a bit-field is used to determine numbers used

  tinymt32_t        randomState;

/******************************************************************************/

  if ((numberBuffer != NULL) && (numberRange > 0))
    {
    nmeaEncoderRandomSeed(&randomSeed);
    tinymt32_init(&randomState, randomSeed);

    // How many 32-bit bit-fields are needed to track the numbers used ?
    occupancyCells = (numberRange / NMEA_GPS_BITFIELD_DEFAULT_BITS) + 1;

    if ((bufferOccupancy = calloc(occupancyCells, sizeof(NMEA_GPS_UINT32))) != NULL)
      {
      // Initialise the buffer to "unoccupied"
      for (bufferIndex = 0; bufferIndex < numberRange; bufferIndex++)
        {
        (numberBuffer + bufferIndex)->nmea_ulong_t = NMEA_SENTENCE_NUMBER_SCRATCH_EMPTY;
        }
      
      // Fill the buffer linearly with scrambled numbers
      for (bufferIndex = 0; bufferIndex < numberRange; bufferIndex++)
        {
        do
          {
          // Generate a unique random number from 0 <= <random-number> < numberRange
          newNumber = (NMEA_GPS_UINT8)(tinymt32_generate_float(&randomState) * ((NMEA_GPS_FLOAT)numberRange));
          
          // Find the offset into the used number tracker
          newNumberIndex  = newNumber / NMEA_GPS_BITFIELD_DEFAULT_BITS;
          newNumberOffset = 1 << (newNumber % NMEA_GPS_BITFIELD_DEFAULT_BITS);

#ifdef _NMEA_BUFFER_SORT_
          printf("\n ** bufferIndex = %0d : newNumber = %06d : newNumberIndex = %06d : newNumberOffset = %6d", bufferIndex, newNumber, newNumberIndex, newNumberOffset);
#endif
          }
        while (((*(bufferOccupancy + newNumberIndex)) & newNumberOffset) == newNumberOffset);

        // Unique new number found, write it to the buffer and mark it's use
         (numberBuffer    + bufferIndex)->nmea_uint8_t = newNumber;
        *(bufferOccupancy + newNumberIndex)            = *(bufferOccupancy + newNumberIndex) | newNumberOffset;
        }

#ifdef _NMEA_BUFFER_SORT_
  // To test all the numbers in the range are generated they are sorted
      qsort( (void *)numberBuffer, (size_t)numberRange, sizeof(uint32_t), quickSortCompare );
#endif
      free(bufferOccupancy);
      }
    }

/******************************************************************************/

  return(scrambledState);

/******************************************************************************/
  } /* end of nmeaGenerateScrambledSequentialNumbers                          */

/******************************************************************************/
/* quickSortCompare() :                                                       */
/******************************************************************************/

static NMEA_GPS_INT quickSortCompare(const void *arg1, const void *arg2 )
  {
/******************************************************************************/

  NMEA_GPS_INT sorted = 0;

/******************************************************************************/

  if (*((uint32_t *)arg1) != *((uint32_t *)arg2)) // return '0' if equal
    {
    if (*((uint32_t *)arg1) > *((uint32_t *)arg2)) // return '1' if 'arg1' > 'arg2'
      {
      sorted = 1;
      }
    else                                                      // return '-1' if 'arg1' < 'arg2'
      {
      sorted = -1;
      }
    }

/******************************************************************************/

  return(sorted);

/******************************************************************************/
  } /* end of quickSortCompare                                                */

/******************************************************************************/
/* NMEAEncoder.c                                                              */
/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/