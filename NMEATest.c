/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/
/*                                                                            */
/* NMEATest.c                                                                 */
/* 10.03.20                                                                   */
/*                                                                            */
/* - test harnesses                                                           */
/*                                                                            */
/******************************************************************************/

#ifdef _NMEA_VARIABLE_TEST_PRINT_
#include <Windows.h>
#include <conio.h>
#endif
#include <stdlib.h>
#include <math.h>
#include "NMEAUtilities.h"
#include "NMEAEncoder.h"
#include "NMEADecoder.h"
#include "NMEATest.h"

/******************************************************************************/

#define _NMEA_VARIABLE_TEST_PRINT_

#define NMEA_VARIABLE_TEST_TARGET   (1048576)
#define NMEA_VARIABLE_TEST_PROGRESS   (16383)

/******************************************************************************/
/* TEST : sentence state parser initialisation                                */
/* nmeaTestInitialiseSentenceState() :                                        */
/*   <--> sentenceState : parser control variables                            */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaTestInitialiseSentenceState(NMEASentenceCaptureState_t *sentenceState)
{
/******************************************************************************/

  NMEA_GPS_BOOLEAN testResult = false;

/******************************************************************************/

  if (sentenceState != NULL)
    {
    sentenceState->nmeaSentenceOuterState       = NMEA_SENTENCE_CAPTURE_STATE_SEARCHING;
    sentenceState->nmeaSentenceInnerState       = NMEA_SENTENCE_STATE_INITIALISATION;
    sentenceState->nmeaSentenceGenericState     = NMEA_SENTENCE_STATE_INITIALISATION;
    sentenceState->nmeaSentenceNewCharacters    = 0;
    sentenceState->nmeaSentenceCharactersUsed   = 0;
    sentenceState->nmeaSentenceMatchIndex       = 0;
    sentenceState->nmeaSentenceMatchLength      = 0;
    sentenceState->nmeaSentenceCharacterIndex   = 0;
    #ifdef _NMEA_PARSER_SENTENCE_REPEAT_
    sentenceState->nmeaSentence[0]              = NMEA_SENTENCE_END_OF_STRING;
    #endif
    sentenceState->nmeaSentenceScratch[0]       = NMEA_SENTENCE_END_OF_STRING;
    sentenceState->nmeaSentenceComputedChecksum = 0;
    sentenceState->nmeaSentenceEscapeFlag       = false;
    sentenceState->nmeaSentenceType             = NMEA_NIL;

    testResult                                  = true;
    }

/******************************************************************************/

  return(testResult);

/******************************************************************************/
} /* end of nmeaTestInitialiseSentenceState                                   */

/******************************************************************************/
/* TEST : function "nmeaVariableNumericFieldParser"                           */
/*        nmeaTestRandomVariableGenerator() :                                 */
/******************************************************************************/

#define NMEA_TEST_SIGNED_INTEGER_MINIMUM_LIMIT   (-1375)
#define NMEA_TEST_SIGNED_INTEGER_MAXIMUM_LIMIT      (23)
#define NMEA_TEST_SIGNED_INTEGER_FIELD_WIDTH         (5)

#define NMEA_TEST_UNSIGNED_INTEGER_MINIMUM_LIMIT  (-999)
#define NMEA_TEST_UNSIGNED_INTEGER_MAXIMUM_LIMIT   (999)
#define NMEA_TEST_UNSIGNED_INTEGER_FIELD_WIDTH       (3)

// There is no such thing as a signed fractional part
#define NMEA_TEST_FRACTIONAL_MINIMUM_LIMIT        (5555)
#define NMEA_TEST_FRACTIONAL_MAXIMUM_LIMIT        (7777)
#define NMEA_TEST_FRACTIONAL_FIELD_WIDTH             (4)
#define NMEA_TEST_FRACTIONAL_MAXIMUM_VALUE        ((NMEA_GPS_FLOAT)5555.7777)
#define NMEA_TEST_FRACTIONAL_MINIMUM_VALUE        ((NMEA_GPS_FLOAT)(-5555.7777))

NMEA_GPS_BOOLEAN nmeaTestRandomVariableGenerator(void)
{
/******************************************************************************/

  NMEASentenceCaptureState_t sentenceState;
  NMEA_GPS_BOOLEAN           testResult      = false;
  NMEA_GPS_INT               testExitCode    = 0;
  NMEA_GPS_UINT              testIndex       = 0,
                             testPasses      = 0,
                             testFails       = 0;
  NMEA_GPS_INT32             testValue       = 0;

  NMEA_GPS_FLOAT             testFloatValue  = 0.0,
                             signDecision    = 0.0;

  NMEA_GPS_UINT8             testFloatLength = 0,
                             sentenceIndex   = 0;

  NMEA_GPS_UINT32            randomSeed      = 0;
  tinymt32_t                 randomState;
  
/******************************************************************************/

  // Test for signed integers :
  if (nmeaTestInitialiseSentenceState(&sentenceState) != false)
    {
    printf("\n\n *** SIGNED INTEGERS *** \n\n");

    while (true)
      {
      testIndex = testIndex + 1;

      if (nmeaGenerateRandomVariable(&sentenceState.nmeaSentence[0],
                                     &sentenceState.nmeaSentenceVariableState.variableResult,
                                     &testFloatLength,
                                      NMEA_FIELD_DECIMAL_INTEGER,
                                      NMEA_SENTENCE_NUMERICAL_SIGN_SIGNED,
                                      NMEA_TEST_SIGNED_INTEGER_FIELD_WIDTH,
                                      NMEA_TEST_SIGNED_INTEGER_MINIMUM_LIMIT,
                                      NMEA_TEST_SIGNED_INTEGER_MAXIMUM_LIMIT, 
                                      0,
                                      0,
                                      0) == true)
        {
        testValue = sentenceState.nmeaSentenceVariableState.variableResult.nmea_int32_t;

        if ((testValue >= -NMEA_TEST_SIGNED_INTEGER_MINIMUM_LIMIT) || (testValue <= NMEA_TEST_SIGNED_INTEGER_MAXIMUM_LIMIT))
          {
          testPasses = testPasses + 1;

#ifdef _NMEA_VARIABLE_TEST_PRINT_
          printf("\n Signed integer variable parser test [%08d] : %08d passed : %08d failed : %s ", testIndex, testPasses, testFails, sentenceState.nmeaSentence);
#endif
          if (testValue >= 0)
            {
#ifdef _NMEA_VARIABLE_TEST_PRINT_
            printf("POSITIVE");
#endif
            }  
          else
            {
#ifdef _NMEA_VARIABLE_TEST_PRINT_
            printf("NEGATIVE");
#endif
            }
          }
        else
          {
          testFails = testFails + 1;

#ifdef _NMEA_VARIABLE_TEST_PRINT_
          printf("\n Signed integer variable parser range test [%08d] FAIL : %08d passed : %08d failed : %d", testIndex, testPasses, testFails, testValue);
#endif
          }

#ifdef _NMEA_VARIABLE_TEST_PRINT_
        Sleep(NMEA_TEST_VARIABLE_SLEEP_TIME_MILLISECONDS);
#endif
        }
      else
        {
        testFails = testFails + 1;

#ifdef _NMEA_VARIABLE_TEST_PRINT_
        printf("\n Signed integer variable parser test [%08d] FAIL : %08d passed : %08d failed", testIndex, testPasses, testFails);
#endif
        }

      if (_kbhit())
        {
        testExitCode = _getch();

        if (testExitCode == NMEA_TEST_EXIT_CODE)
          {
          break;
          }
        else
          {
          if (testExitCode == NMEA_TEST_WAIT_CODE)
            {
            while (!_kbhit())
              ;

            // Cancel the wait and delete the random character used 
            testExitCode = _getch();
            testExitCode = 0;
            }
          }
        }

#ifndef _NMEA_VARIABLE_TEST_PRINT_
    if ((testIndex & NMEA_VARIABLE_TEST_PROGRESS) == 0)
      {
      printf(".");
      }

    if (testIndex == NMEA_VARIABLE_TEST_TARGET)
      {
      printf("\n Signed integer variable parser test [%08d] : %08d passed : %08d failed : %s \n", testIndex, testPasses, testFails, sentenceState.nmeaSentence);
      break;
      }
#endif
      }
    }

  // Test for unsigned integers :
  testIndex  = 0;
  testPasses = 0;
  testFails  = 0;

  if (nmeaTestInitialiseSentenceState(&sentenceState) != false)
    {
    printf("\n\n *** UNSIGNED INTEGERS *** \n\n");

    while (true)
      {
      testIndex = testIndex + 1;

      if (nmeaGenerateRandomVariable(&sentenceState.nmeaSentence[0],
                                     &sentenceState.nmeaSentenceVariableState.variableResult,
                                     &testFloatLength,
                                     NMEA_FIELD_DECIMAL_INTEGER,
                                     NMEA_SENTENCE_NUMERICAL_SIGN_UNSIGNED,
                                     NMEA_TEST_UNSIGNED_INTEGER_FIELD_WIDTH,
                                     NMEA_TEST_UNSIGNED_INTEGER_MINIMUM_LIMIT,
                                     NMEA_TEST_UNSIGNED_INTEGER_MAXIMUM_LIMIT,
                                     0,
                                     0,
                                     0) == true)                                         
        {
        testValue = sentenceState.nmeaSentenceVariableState.variableResult.nmea_int32_t;

        if ((testValue >= NMEA_TEST_UNSIGNED_INTEGER_MINIMUM_LIMIT) || (testValue <= NMEA_TEST_UNSIGNED_INTEGER_MAXIMUM_LIMIT))
          {
          testPasses = testPasses + 1;

#ifdef _NMEA_VARIABLE_TEST_PRINT_
          printf("\n Unsigned integer variable parser test [%08d] PASS : %08d passed : %08d failed : %s ", testIndex, testPasses, testFails, sentenceState.nmeaSentence);
#endif

          if (testValue >= 0)
            {
#ifdef _NMEA_VARIABLE_TEST_PRINT_
            printf("POSITIVE");
#endif
            }  
          else
            {
#ifdef _NMEA_VARIABLE_TEST_PRINT_
            printf("NEGATIVE");
#endif
            }
          }
        else
          {
          testFails = testFails + 1;

#ifdef _NMEA_VARIABLE_TEST_PRINT_
          printf("\n Unsigned integer variable parser range test [%08d] FAIL : %08d passed : %08d failed : %d", testIndex, testPasses, testFails, testValue);
#endif
          }

#ifdef _NMEA_VARIABLE_TEST_PRINT_
        Sleep(NMEA_TEST_VARIABLE_SLEEP_TIME_MILLISECONDS);
#endif
        }
      else
        {
        testFails = testFails + 1;

#ifdef _NMEA_VARIABLE_TEST_PRINT_
        printf("\n Unsigned integer variable parser test [%08d] : %08d passed : %06d failed", testIndex, testPasses, testFails);
#endif
        }

      if (_kbhit())
        {
        testExitCode = _getch();

        if (testExitCode == NMEA_TEST_EXIT_CODE)
          {
          break;
          }
        else
          {
          if (testExitCode == NMEA_TEST_WAIT_CODE)
            {
            while (!_kbhit())
              ;

            // Cancel the wait and delete the random character used 
            testExitCode = _getch();
            testExitCode = 0;
            }
          }
        }

#ifndef _NMEA_VARIABLE_TEST_PRINT_
      if ((testIndex & NMEA_VARIABLE_TEST_PROGRESS) == 0)
        {
        printf(".");
        }

      if (testIndex == NMEA_VARIABLE_TEST_TARGET)
        {
        printf("\n Unsigned integer variable parser test [%08d] : %08d passed : %08d failed : %s \n", testIndex, testPasses, testFails, sentenceState.nmeaSentence);
        break;
        }
#endif
      }
    }

  // Test for signed floats
  testIndex  = 0;
  testPasses = 0;
  testFails  = 0;

  NMEA_GPS_FLOAT testScale            = 0.0;
  NMEA_GPS_UINT8 integerFieldWidth    = 0,
                 fractionalFieldWidth = 0;
  NMEA_GPS_INT32 integerLimit         = 0,
                 fractionalLimit      = 0;

  // Create a random variable for numeric field generation
  nmeaEncoderRandomSeed(&randomSeed);
  tinymt32_init(&randomState, randomSeed);

  if (nmeaTestInitialiseSentenceState(&sentenceState) != false)
    {
    printf("\n\n *** SIGNED FLOATS *** \n\n");
    while (true)
      {
      testIndex = testIndex + 1;

      // Generate the size of the integer part
      testScale = tinymt32_generate_float(&randomState);

      integerFieldWidth    = (NMEA_GPS_UINT8)(testScale * ((NMEA_GPS_FLOAT)NMEA_MAXIMUM_INTEGER_DECIMAL_PLACES));
      fractionalFieldWidth = (NMEA_GPS_UINT8)(testScale * ((NMEA_GPS_FLOAT)NMEA_MAXIMUM_FRACTIONAL_DECIMAL_PLACES));

      if (integerFieldWidth < NMEA_MINIMUM_INTEGER_DECIMAL_PLACES)
        {
        integerFieldWidth = NMEA_MINIMUM_INTEGER_DECIMAL_PLACES;
        }

      if (fractionalFieldWidth < NMEA_MINIMUM_FRACTIONAL_DECIMAL_PLACES)
        {
        fractionalFieldWidth = NMEA_MINIMUM_FRACTIONAL_DECIMAL_PLACES;
        }

      // Use the field size to set the maximum limit - this must fit in the number of 
      // decimal digits used for the field width
      integerLimit = (NMEA_GPS_INT32)pow(NMEA_FLOAT_DIGIT_x_10, integerFieldWidth);
      integerLimit = integerLimit - 1;

      fractionalLimit = (NMEA_GPS_INT32)pow(NMEA_FLOAT_DIGIT_x_10, fractionalFieldWidth);
      fractionalLimit = fractionalLimit - 1;

      // Generate the size of the fractional part
      testScale = tinymt32_generate_float(&randomState);

        // Make an arbitrary sign decision
      signDecision = tinymt32_generate_float(&randomState);
      
      if (signDecision >= NMEA_BINARY_DECISION_THRESHOLD_FLOAT)
       {
       // Prefix the number as "signed" with a '-' character
       sentenceState.nmeaSentence[0] = NMEA_GPS_ASCII_DIGIT_MINUS;
       sentenceIndex                 = 1;
       }
      else
        {
        sentenceIndex                  = 0;
        }

      // Generate the value as unsigned and write it into the test string
      if (nmeaGenerateRandomVariable(&sentenceState.nmeaSentence[sentenceIndex],
                                     &sentenceState.nmeaSentenceVariableState.variableResult,
                                     &testFloatLength,
                                      NMEA_FIELD_DECIMAL_FLOATING,
                                      NMEA_SENTENCE_NUMERICAL_SIGN_UNSIGNED,
                                      integerFieldWidth,    // NMEA_TEST_UNSIGNED_INTEGER_FIELD_WIDTH,
                                      0, // -integerLimit,         //NMEA_TEST_UNSIGNED_INTEGER_MINIMUM_LIMIT,
                                      integerLimit,         //NMEA_TEST_UNSIGNED_INTEGER_MAXIMUM_LIMIT,
                                      fractionalFieldWidth, // NMEA_TEST_FRACTIONAL_FIELD_WIDTH,
                                      0,
                                      fractionalLimit) == true)                                         
        {
        testFloatValue = sentenceState.nmeaSentenceVariableState.variableResult.nmea_float_t;

        // Check to see if the number is negative
        if (sentenceIndex == 1)
          {
          testFloatValue                                                      = -testFloatValue;
          sentenceState.nmeaSentenceVariableState.variableResult.nmea_float_t = testFloatValue;
          }

        if ((testFloatValue >= NMEA_FRACTIONAL_MINIMUM_VALUE) || (testFloatValue <= NMEA_FRACTIONAL_MAXIMUM_VALUE))
          {
          testPasses = testPasses + 1;

#ifdef _NMEA_VARIABLE_TEST_PRINT_
          printf("\n Signed float variable parser test [%08d] PASS : %08d passed : %08d failed : %s ", testIndex, testPasses, testFails, sentenceState.nmeaSentence);
#endif

          if (testFloatValue >= 0)
            {
#ifdef _NMEA_VARIABLE_TEST_PRINT_
            printf("POSITIVE");
#endif
            }  
          else
            {
#ifdef _NMEA_VARIABLE_TEST_PRINT_
            printf("NEGATIVE");
#endif
            }

#ifndef _NMEA_VARIABLE_PARSE_TEST_
          {
          NMEASentenceCaptureState_t  captureState;
          NMEASentenceGenericStates_t variableState = NMEA_VARIABLE_STATE_INITIALISATION;

          captureState.nmeaSentenceVariableState.variableType                = NMEA_FIELD_DECIMAL_FLOATING;

          captureState.nmeaSentenceVariableState.integerPartMinimumLength    = 1; // NMEA_TEST_UNSIGNED_INTEGER_FIELD_WIDTH;
          captureState.nmeaSentenceVariableState.integerPartMaximumLength    = integerFieldWidth; // NMEA_TEST_UNSIGNED_INTEGER_FIELD_WIDTH;
          
          captureState.nmeaSentenceVariableState.fractionalPartMinimumLength = 1;
          captureState.nmeaSentenceVariableState.fractionalPartMaximumLength = fractionalLimit;
          
          captureState.nmeaSentenceVariableState.signFlag                    = NMEA_ENCODER_SIGN_FLAG_SIGNED;
          
          captureState.nmeaSentenceVariableState.variableResult.nmea_float_t = 0;

          captureState.nmeaSentenceMatchIndex                                = 0;
          captureState.nmeaSentenceCharacterIndex                            = 0;
          captureState.nmeaSentenceCharactersUsed                            = 0;
          captureState.nmeaSentenceNewCharacters                             = (NMEA_GPS_UINT8)(strlen(&sentenceState.nmeaSentence[0]) + 1); // include the end-of-string
                                                                                                                                             // as a terminator is required
          captureState.nmeaSentence[0]                                       = NMEA_SENTENCE_END_OF_STRING;
          // Start the variable parser state machine
          captureState.nmeaSentenceGenericState = NMEA_VARIABLE_STATE_INITIALISATION;
          
          // Hand-off to the variable parser state machine
          variableState = nmeaVariableNumericFieldParser(&sentenceState.nmeaSentence[0],
                                                         &captureState);

           if (variableState == NMEA_VARIABLE_STATE_EXIT)
             {
             if (sentenceState.nmeaSentenceVariableState.variableResult.nmea_float_t == captureState.nmeaSentenceVariableState.variableResult.nmea_float_t)
               {
               if (!strcmp(&sentenceState.nmeaSentence[0], &captureState.nmeaSentence[0]))
                 {
                 printf("\n Numeric String Generated %s == Numeric String Parsed %s \n", sentenceState.nmeaSentence, captureState.nmeaSentence);
                 }
               else
                 {
                 printf("\n Numeric re-conversion failed! \n");
                 exit(0);
                 }
               }
             }
          }
#endif

          }
        else
          {
          testFails = testFails + 1;

#ifdef _NMEA_VARIABLE_TEST_PRINT_
          printf("\n Signed float variable parser range test [%08d] FAIL : %08d passed : %08d failed : %f", testIndex, testPasses, testFails, testFloatValue);
#endif
          }

#ifdef _NMEA_VARIABLE_TEST_PRINT_
        Sleep(NMEA_TEST_VARIABLE_SLEEP_TIME_MILLISECONDS);
#endif
        }
      else
        {
        testFails = testFails + 1;

#ifdef _NMEA_VARIABLE_TEST_PRINT_
        printf("\n Signed float variable parser test [%08d] : %06d passed : %08d failed", testIndex, testPasses, testFails);
#endif
        }

      if (_kbhit())
        {
        testExitCode = _getch();

        if (testExitCode == NMEA_TEST_EXIT_CODE)
          {
          break;
          }
        else
          {
          if (testExitCode == NMEA_TEST_WAIT_CODE)
            {
            while (!_kbhit())
              ;

            // Cancel the wait and delete the random character used 
            testExitCode = _getch();
            testExitCode = 0;
            }
          }
        }

#ifndef _NMEA_VARIABLE_TEST_PRINT_
      if ((testIndex & NMEA_VARIABLE_TEST_PROGRESS) == 0)
        {
        printf(".");
        }

      if (testIndex == NMEA_VARIABLE_TEST_TARGET)
        {
        printf("\n Signed float variable parser test [%08d] : %08d passed : %08d failed : %s \n", testIndex, testPasses, testFails, sentenceState.nmeaSentence);
        break;
        }
#endif
      }
    }

  // Test for unsigned floats
  testIndex  = 0;
  testPasses = 0;
  testFails  = 0;

  if (nmeaTestInitialiseSentenceState(&sentenceState) != false)
    {
    printf("\n\n *** UNSIGNED FLOATS *** \n\n");

    while (true)
      {
      testIndex = testIndex + 1;

      if (nmeaGenerateRandomVariable(&sentenceState.nmeaSentence[0],
                                     &sentenceState.nmeaSentenceVariableState.variableResult,
                                     &testFloatLength,
                                      NMEA_FIELD_DECIMAL_FLOATING,
                                      NMEA_SENTENCE_NUMERICAL_SIGN_UNSIGNED,
                                      NMEA_TEST_UNSIGNED_INTEGER_FIELD_WIDTH,
                                      NMEA_TEST_UNSIGNED_INTEGER_MINIMUM_LIMIT,
                                      NMEA_TEST_UNSIGNED_INTEGER_MAXIMUM_LIMIT,
                                      NMEA_TEST_FRACTIONAL_FIELD_WIDTH,
                                      NMEA_TEST_FRACTIONAL_MINIMUM_LIMIT,
                                      NMEA_TEST_FRACTIONAL_MAXIMUM_LIMIT) == true)                                         
        {
        testFloatValue = sentenceState.nmeaSentenceVariableState.variableResult.nmea_float_t;

        if ((testFloatValue >= NMEA_TEST_FRACTIONAL_MINIMUM_VALUE) || (testFloatValue <= NMEA_TEST_FRACTIONAL_MAXIMUM_VALUE))
          {
          testPasses = testPasses + 1;

#ifdef _NMEA_VARIABLE_TEST_PRINT_
          printf("\n Unsigned float variable parser test [%08d] PASS : %08d passed : %08d failed : %s ", testIndex, testPasses, testFails, sentenceState.nmeaSentence);
#endif

          if (testFloatValue >= 0)
            {
#ifdef _NMEA_VARIABLE_TEST_PRINT_
            printf("POSITIVE");
#endif
            }  
          else
            {
#ifdef _NMEA_VARIABLE_TEST_PRINT_
            printf("NEGATIVE");
#endif
            }
          }
        else
          {
          testFails = testFails + 1;

#ifdef _NMEA_VARIABLE_TEST_PRINT_
          printf("\n Unsigned float variable parser range test [%08d] FAIL : %08d passed : %08d failed : %f", testIndex, testPasses, testFails, testFloatValue);
#endif
          }

#ifdef _NMEA_VARIABLE_TEST_PRINT_
        Sleep(NMEA_TEST_VARIABLE_SLEEP_TIME_MILLISECONDS);
#endif
        }
      else
        {
        testFails = testFails + 1;

#ifdef _NMEA_VARIABLE_TEST_PRINT_
        printf("\n Unsigned float variable parser test [%08d] : %08d passed : %08d failed", testIndex, testPasses, testFails);
#endif
        }

      if (_kbhit())
        {
        testExitCode = _getch();

        if (testExitCode == NMEA_TEST_EXIT_CODE)
          {
          break;
          }
        }

#ifndef _NMEA_VARIABLE_TEST_PRINT_
      if ((testIndex & NMEA_VARIABLE_TEST_PROGRESS) == 0)
        {
        printf(".");
        }

      if (testIndex == NMEA_VARIABLE_TEST_TARGET)
        {
        printf("\n Unsigned float variable parser test [%08d] : %08d passed : %08d failed : %s \n", testIndex, testPasses, testFails, sentenceState.nmeaSentence);
        break;
        }  
#endif
      }
    }

#ifndef _NMEA_VARIABLE_TEST_PRINT_
  printf("\n\n Press %c to continue...", NMEA_TEST_EXIT_CODE);

  while (true)
    {
    if (_kbhit())
      {
      testExitCode = _getch();
    
      if (testExitCode == NMEA_TEST_EXIT_CODE)
        {
        break;
        }
      }
    }
#endif

/******************************************************************************/

  return(testResult);

/******************************************************************************/
} /* end of nmeaTestRandomVariableGenerator                                   */

/******************************************************************************/
/* TEST : function "nmeaTestSpeedOverGroundField()"                           */
/*        nmeaTestSpeedOverGroundField() :                                    */
/* - test the sentence field "SpeedOverGround" generator                      */
/******************************************************************************/

NMEA_GPS_BOOLEAN nmeaTestSpeedOverGroundField(void)
{
/******************************************************************************/

  NMEASentenceCaptureState_t sentenceState;
  NMEA_GPS_UINT8             sentenceIndex  = 0;

  NMEA_GPS_FLOAT             sogValue       = 0.0;

  NMEA_GPS_UINT32            testIndex      = 0,
                             testPasses     = 0,
                             testFails      = 0;
  NMEA_GPS_INT               testExitCode   = 0;

  NMEA_GPS_BOOLEAN           sogResult      = false;

/******************************************************************************/

  if (nmeaTestInitialiseSentenceState(&sentenceState) != false)
    {
    printf("\n\n *** SPEED-OVER-GROUND *** \n\n");

    while (true)
      {
      testIndex = testIndex + 1;

      sentenceIndex = 0;

      if (nmeaEncoderAddXOverGround(&sentenceState.nmeaSentence[0],
                                    &sentenceIndex,
                                     NMEA_ENCODER_OVER_GROUND_SPEED,
                                     NMEA_ENCODER_ZEROES_PAD) == true)
        {
        sogValue = (NMEA_GPS_FLOAT)atof((const char *)&sentenceState.nmeaSentence[0]);

        if ((sogValue >= NMEA_SPEED_OVER_GROUND_MINIMUM) && (NMEA_SPEED_OVER_GROUND_MAXIMUM))
          {
          testPasses = testPasses + 1;

#ifdef _NMEA_VARIABLE_TEST_PRINT_

          printf("\n Speed-over-ground generator test [%08d] PASS : %08d passed : %08d failed : %s ", testIndex, testPasses, testFails, sentenceState.nmeaSentence);
#endif

#ifdef _NMEA_VARIABLE_TEST_PRINT_
          Sleep(NMEA_TEST_VARIABLE_SLEEP_TIME_MILLISECONDS);
#endif
          }
        else
          {
          testFails = testFails + 1;

#ifdef _NMEA_VARIABLE_TEST_PRINT_
          printf("\n Speed-over-ground generator test [%08d] : %08d passed : %08d failed", testIndex, testPasses, testFails);
#endif
          }

#ifndef _NMEA_VARIABLE_TEST_PRINT_
        if ((testIndex & NMEA_VARIABLE_TEST_PROGRESS) == 0)
          {
          printf(".");
          }

        if (testIndex == NMEA_VARIABLE_TEST_TARGET)
          {
          printf("\n Speed-over-ground generator test [%08d] : %08d passed : %08d failed : %s \n", testIndex, testPasses, testFails, sentenceState.nmeaSentence);
          break;
          }  
#endif
        }

      if (_kbhit())
        {
        testExitCode = _getch();

        if (testExitCode == NMEA_TEST_EXIT_CODE)
          {
          break;
          }
        }
      }
    }

#ifndef _NMEA_VARIABLE_TEST_PRINT_
  printf("\n\n Press %c to continue...", NMEA_TEST_EXIT_CODE);

  while (true)
    {
    if (_kbhit())
      {
      testExitCode = _getch();

      if (testExitCode == NMEA_TEST_EXIT_CODE)
        {
        break;
       }
      }
    }
#endif

/******************************************************************************/

  return(sogResult);

/******************************************************************************/
} /* end of nmeaTestSpeedOverGroundField                                      */

/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/